<?php

namespace App\paddock\Twitter\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditTwitterAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'twitter_id' => 'required|integer',
            'name' => 'required|string',
            'screen_name' => 'required|string',
            'description' => 'nullable|string',
            'profile_image_url' => 'required|url',
            'owner' => 'boolean',
        ];
    }
}
