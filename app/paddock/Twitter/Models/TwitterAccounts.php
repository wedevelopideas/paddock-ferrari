<?php

namespace App\paddock\Twitter\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TwitterAccounts.
 *
 * @property int $id
 * @property mixed $twitter_id
 * @property string $name
 * @property string $screen_name
 * @property string|null $description
 * @property string $profile_image_url
 * @property bool $owner
 */
class TwitterAccounts extends Model
{
    /**
     * Main part of the twitter profile link.
     *
     * @var string
     */
    private const LINK = 'https://www.twitter.com/';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'twitter_id',
        'name',
        'screen_name',
        'description',
        'profile_image_url',
        'owner',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'twitter_id' => 'bigint',
        'owner' => 'boolean',
    ];

    /**
     * Checks if you're the owner of a Twitter account.
     *
     * @return bool|string
     */
    public function owner()
    {
        if ($this->owner !== false) {
            return '<i class="check circle icon"></i>';
        }

        return false;
    }

    /**
     * Link to the twitter profile.
     *
     * @return string
     */
    public function twitterLink()
    {
        return self::LINK.$this->screen_name;
    }
}
