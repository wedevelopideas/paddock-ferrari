<?php

namespace App\paddock\Twitter\Repositories;

use App\paddock\Twitter\Models\TwitterAccounts;

class TwitterAccountsRepository
{
    /**
     * @var TwitterAccounts
     */
    private $twitterAccounts;

    /**
     * TwitterAccountsRepository constructor.
     * @param TwitterAccounts $twitterAccounts
     */
    public function __construct(TwitterAccounts $twitterAccounts)
    {
        $this->twitterAccounts = $twitterAccounts;
    }

    /**
     * Counts all Twitter accounts.
     *
     * @return mixed
     */
    public function count()
    {
        return $this->twitterAccounts
            ->count();
    }

    /**
     * Get the account by its id.
     *
     * @param int $id
     * @return mixed
     */
    public function getAccountByID(int $id)
    {
        return $this->twitterAccounts
            ->where('id', $id)
            ->first();
    }

    /**
     * Get all accounts.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->twitterAccounts
            ->orderBy('screen_name', 'ASC')
            ->get();
    }

    /**
     * Store Twitter account.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $account = new TwitterAccounts();
        $account->twitter_id = $data['twitter_id'];
        $account->name = $data['name'];
        $account->screen_name = $data['screen_name'];
        $account->description = $data['description'];
        $account->profile_image_url = $data['profile_image_url'];
        $account->owner = $data['owner'];
        $account->save();
    }

    /**
     * Update Twitter account.
     *
     * @param int $id
     * @param array $data
     */
    public function update(int $id, array $data)
    {
        $account = TwitterAccounts::find($id);

        if ($account) {
            $account->twitter_id = $data['twitter_id'];
            $account->name = $data['name'];
            $account->screen_name = $data['screen_name'];
            $account->description = $data['description'];
            $account->profile_image_url = $data['profile_image_url'];
            $account->owner = $data['owner'];
            $account->save();
        }
    }
}
