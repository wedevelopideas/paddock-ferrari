<?php

namespace App\paddock\Drivers\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditDriversRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id' => 'required|string',
            'status' => 'required|integer',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'hashtag' => 'required|string',
            'dateofbirth' => 'required|date',
            'dateofdeath' => 'date|nullable',
            'image' => 'string|nullable',
        ];
    }
}
