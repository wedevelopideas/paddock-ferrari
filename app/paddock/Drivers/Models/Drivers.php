<?php

namespace App\paddock\Drivers\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Drivers.
 *
 * @property int $id
 * @property string $country_id
 * @property int $status
 * @property string $first_name
 * @property string $last_name
 * @property string $name
 * @property string $slug
 * @property string|null $hashtag
 * @property \Illuminate\Support\Carbon $dateofbirth
 * @property \Illuminate\Support\Carbon|null $dateofdeath
 * @property string|null $image
 */
class Drivers extends Model
{
    /**
     * Status of a works driver.
     *
     * @var int
     */
    public const WORKS = 1;

    /**
     * Status of a test driver.
     *
     * @var int
     */
    public const TEST = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country_id',
        'status',
        'first_name',
        'last_name',
        'name',
        'slug',
        'hashtag',
        'dateofbirth',
        'dateofdeath',
        'image',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'dateofbirth',
        'dateofdeath',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Set the driver's name.
     *
     * @param string $value
     */
    public function setNameAttribute(string $value)
    {
        $this->attributes['name'] = $value;
    }

    /**
     * Set the driver's slug.
     *
     * @param string $value
     */
    public function setSlugAttribute(string $value)
    {
        $this->attributes['slug'] = Str::slug($value);
    }
}
