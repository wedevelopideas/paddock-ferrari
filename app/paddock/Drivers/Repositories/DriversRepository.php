<?php

namespace App\paddock\Drivers\Repositories;

use Illuminate\Support\Facades\DB;
use App\paddock\Drivers\Models\Drivers;

class DriversRepository
{
    /**
     * @var Drivers
     */
    private $drivers;

    /**
     * DriversRepository constructor.
     * @param Drivers $drivers
     */
    public function __construct(Drivers $drivers)
    {
        $this->drivers = $drivers;
    }

    /**
     * Counts all drivers.
     *
     * @return mixed
     */
    public function count()
    {
        return $this->drivers
            ->count();
    }

    /**
     * Get all drivers.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->drivers
            ->orderBy('last_name', 'ASC')
            ->orderBy('first_name', 'ASC')
            ->get();
    }

    /**
     * Get driver by its id.
     *
     * @param int $id
     * @return mixed
     */
    public function getDriverByID(int $id)
    {
        return $this->drivers
            ->where('id', $id)
            ->first();
    }

    /**
     * Get all driver birthdays.
     *
     * @return mixed
     */
    public function getDriverBirthdays()
    {
        return $this->drivers
            ->orderBy(DB::RAW('month(dateofbirth)'), 'ASC')
            ->orderBy(DB::RAW('day(dateofbirth)'), 'ASC')
            ->get();
    }

    /**
     * Get driver by its id.
     *
     * @param string $slug
     * @return mixed
     */
    public function getDriverBySlug(string $slug)
    {
        return $this->drivers
            ->where('slug', $slug)
            ->first();
    }

    /**
     * Get all driver birthdays.
     *
     * @return mixed
     */
    public function getDriverDeaths()
    {
        return $this->drivers
            ->where('dateofdeath', '!=', null)
            ->orderBy(DB::RAW('month(dateofdeath)'), 'ASC')
            ->orderBy(DB::RAW('day(dateofdeath)'), 'ASC')
            ->get();
    }

    /**
     * Get all driver events.
     *
     * @return mixed
     */
    public function getDriverEvents()
    {
        $birthdays = $this->drivers
            ->select('dateofbirth as date', 'name', DB::RAW('\'birthday\' as type'))
            ->where(DB::RAW('DATE_FORMAT(dateofbirth, "%m-%d")'), '>=', DB::RAW('DATE_FORMAT(CURDATE(), "%m-%d")'));

        return $this->drivers
            ->select('dateofdeath as date', 'name', DB::RAW('\'death\' as type'))
            ->where(DB::RAW('DATE_FORMAT(dateofdeath, "%m-%d")'), '>=', DB::RAW('DATE_FORMAT(CURDATE(), "%m-%d")'))
            ->where('dateofdeath', '!=', null)
            ->union($birthdays)
            ->orderBy(DB::RAW('month(date)'), 'ASC')
            ->orderBy(DB::RAW('day(date)'), 'ASC')
            ->limit(5)
            ->get();
    }

    /**
     * Store drivers.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $driver = new Drivers();
        $driver->country_id = $data['country_id'];
        $driver->status = $data['status'];
        $driver->first_name = $data['first_name'];
        $driver->last_name = $data['last_name'];
        $driver->name = $data['first_name'].' '.$data['last_name'];
        $driver->slug = $data['first_name'].' '.$data['last_name'];
        $driver->hashtag = $data['hashtag'];
        $driver->dateofbirth = $data['dateofbirth'];
        $driver->dateofdeath = $data['dateofdeath'];
        $driver->image = $data['image'];
        $driver->save();
    }

    /**
     * Update drivers.
     *
     * @param int $id
     * @param array $data
     */
    public function update(int $id, array $data)
    {
        $driver = Drivers::find($id);

        if ($driver) {
            $driver->country_id = $data['country_id'];
            $driver->status = $data['status'];
            $driver->first_name = $data['first_name'];
            $driver->last_name = $data['last_name'];
            $driver->name = $data['first_name'].' '.$data['last_name'];
            $driver->slug = $data['first_name'].' '.$data['last_name'];
            $driver->hashtag = $data['hashtag'];
            $driver->dateofbirth = $data['dateofbirth'];
            $driver->dateofdeath = $data['dateofdeath'];
            $driver->image = $data['image'];
            $driver->save();
        }
    }
}
