<?php

namespace App\paddock\Languages\Repositories;

use App\paddock\Languages\Models\Languages;

class LanguagesRepository
{
    /**
     * @var Languages
     */
    private $languages;

    /**
     * LanguagesRepository constructor.
     * @param Languages $languages
     */
    public function __construct(Languages $languages)
    {
        $this->languages = $languages;
    }

    /**
     * Counts all languages.
     *
     * @return mixed
     */
    public function count()
    {
        return $this->languages
            ->count();
    }

    /**
     * Get all active languages.
     *
     * @return mixed
     */
    public function getActiveLanguages()
    {
        return $this->languages
            ->where('active', 1)
            ->get();
    }

    /**
     * Get all languages.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->languages
            ->orderBy('code', 'ASC')
            ->get();
    }

    /**
     * Get all new active languages.
     *
     * @return mixed
     */
    public function getNewsActiveLanguages()
    {
        return $this->languages
            ->where('news', 1)
            ->get();
    }

    /**
     * Get language by its code.
     *
     * @param string $code
     * @return mixed
     */
    public function getLanguageByCode(string $code)
    {
        return $this->languages
            ->where('code', $code)
            ->first();
    }

    /**
     * Store languages.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $lang = new Languages();
        $lang->code = $data['code'];
        $lang->name = $data['name'];
        $lang->motorsport_link = $data['motorsport_link'];
        $lang->news = $data['news'];
        $lang->active = $data['active'];
        $lang->save();
    }

    /**
     * Update languages.
     *
     * @param string $code
     * @param array $data
     */
    public function update(string $code, array $data)
    {
        $lang = Languages::find($code);

        if ($lang) {
            $lang->code = $data['code'];
            $lang->name = $data['name'];
            $lang->motorsport_link = $data['motorsport_link'];
            $lang->news = $data['news'];
            $lang->active = $data['active'];
            $lang->save();
        }
    }
}
