<?php

namespace App\paddock\Languages\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Languages.
 *
 * @property string $code
 * @property string $name
 * @property string $motorsport_link
 * @property bool $news
 * @property bool $active
 */
class Languages extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'motorsport_link',
        'news',
        'active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'code';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'news' => 'boolean',
        'active' => 'boolean',
    ];

    /**
     * Returns active status.
     *
     * @return string
     */
    public function getActiveStatus()
    {
        if ($this->active !== false) {
            return '<i class="check circle icon"></i>';
        }

        return '<i class="times circle icon"></i>';
    }

    /**
     * Returns flag in Semantic UI style.
     *
     * @return string
     */
    public function getFlag()
    {
        if ($this->code === 'en') {
            return 'england';
        }

        return $this->code;
    }

    /**
     * Returns news status.
     *
     * @return string
     */
    public function getNewsStatus()
    {
        if ($this->news !== false) {
            return '<i class="check circle icon"></i>';
        }

        return '<i class="times circle icon"></i>';
    }
}
