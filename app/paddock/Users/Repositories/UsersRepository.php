<?php

namespace App\paddock\Users\Repositories;

use App\paddock\Users\Models\Users;

class UsersRepository
{
    /**
     * @var Users
     */
    private $users;

    /**
     * UsersRepository constructor.
     * @param Users $users
     */
    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    /**
     * Count all users.
     *
     * @return mixed
     */
    public function count()
    {
        return $this->users
            ->count();
    }

    /**
     * Get all users.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->users
            ->orderBy('last_name', 'ASC')
            ->orderBy('first_name', 'ASC')
            ->get();
    }

    /**
     * Get the user by its id.
     *
     * @param int|null $id
     * @return mixed
     */
    public function getUserByID($id)
    {
        return $this->users
            ->where('id', $id)
            ->first();
    }

    /**
     * Get the user by its username.
     *
     * @param string $user_name
     * @return mixed
     */
    public function getUserByUserName(string $user_name)
    {
        return $this->users
            ->where('user_name', $user_name)
            ->first();
    }

    /**
     * Update user's language.
     *
     * @param int|null $user_id
     * @param array $data
     */
    public function updateLanguage($user_id, array $data)
    {
        $user = Users::find($user_id);

        if ($user) {
            $user->lang = $data['lang'];
            $user->save();
        }
    }

    /**
     * Update user's timezone.
     *
     * @param int|null $user_id
     * @param array $data
     */
    public function updateTimezone($user_id, array $data)
    {
        $user = Users::find($user_id);

        if ($user) {
            $user->timezone = $data['timezone'];
            $user->save();
        }
    }
}
