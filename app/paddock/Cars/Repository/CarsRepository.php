<?php

namespace App\paddock\Cars\Repository;

use App\paddock\Cars\Models\Cars;

class CarsRepository
{
    /**
     * @var Cars
     */
    private $cars;

    /**
     * CarsRepository constructor.
     * @param Cars $cars
     */
    public function __construct(Cars $cars)
    {
        $this->cars = $cars;
    }

    /**
     * Get all cars.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->cars
            ->orderBy('name', 'ASC')
            ->get();
    }

    /**
     * Get car by its id.
     *
     * @param int $id
     * @return mixed
     */
    public function getCarByID(int $id)
    {
        return $this->cars
            ->where('id', $id)
            ->first();
    }

    /**
     * Store cars.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $car = new Cars();
        $car->name = $data['name'];
        $car->slug = $data['name'];
        $car->save();
    }

    /**
     * Update cars.
     *
     * @param int $id
     * @param array $data
     */
    public function update(int $id, array $data)
    {
        $car = Cars::find($id);

        if ($car) {
            $car->name = $data['name'];
            $car->slug = $data['name'];
            $car->save();
        }
    }
}
