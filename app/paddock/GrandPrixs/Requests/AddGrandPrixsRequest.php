<?php

namespace App\paddock\GrandPrixs\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddGrandPrixsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id' => 'required|string',
            'name' => 'required|string|unique:grand_prixs',
            'full_name' => 'required|string',
            'hashtag' => 'required|string',
            'emoji' => 'required|string',
            'timezone' => 'required|timezone',
            'active' => 'boolean',
        ];
    }
}
