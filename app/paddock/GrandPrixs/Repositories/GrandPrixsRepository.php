<?php

namespace App\paddock\GrandPrixs\Repositories;

use App\paddock\GrandPrixs\Models\GrandPrixs;

class GrandPrixsRepository
{
    /**
     * @var GrandPrixs
     */
    private $grandPrixs;

    /**
     * GrandPrixsRepository constructor.
     * @param GrandPrixs $grandPrixs
     */
    public function __construct(GrandPrixs $grandPrixs)
    {
        $this->grandPrixs = $grandPrixs;
    }

    /**
     * Count all grand prix.
     *
     * @return mixed
     */
    public function count()
    {
        return $this->grandPrixs
            ->count();
    }

    /**
     * Get all grand prix.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->grandPrixs
            ->orderBy('name', 'ASC')
            ->get();
    }

    /**
     * Get grand prix by its id.
     *
     * @param int $id
     * @return mixed
     */
    public function getGrandPrixByID(int $id)
    {
        return $this->grandPrixs
            ->where('id', $id)
            ->first();
    }

    /**
     * Get grand prix by its slug.
     *
     * @param string $slug
     * @return mixed
     */
    public function getGrandPrixBySlug(string $slug)
    {
        return $this->grandPrixs
            ->where('slug', $slug)
            ->first();
    }

    /**
     * Get the timezone of the current Grand Prix.
     *
     * @param int $gp
     * @return mixed
     */
    public function getGrandPrixTimezone(int $gp)
    {
        return $this->grandPrixs
            ->select('timezone')
            ->where('id', $gp)
            ->first();
    }

    /**
     * Store grand prix.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $gp = new GrandPrixs();
        $gp->country_id = $data['country_id'];
        $gp->name = $data['name'];
        $gp->slug = $data['name'];
        $gp->full_name = $data['full_name'];
        $gp->hashtag = $data['hashtag'];
        $gp->emoji = $data['emoji'];
        $gp->timezone = $data['timezone'];
        $gp->active = $data['active'];
        $gp->save();
    }

    /**
     * Update grand prix.
     *
     * @param int $id
     * @param array $data
     */
    public function update(int $id, array $data)
    {
        $gp = GrandPrixs::find($id);

        if ($gp) {
            $gp->country_id = $data['country_id'];
            $gp->name = $data['name'];
            $gp->slug = $data['name'];
            $gp->full_name = $data['full_name'];
            $gp->hashtag = $data['hashtag'];
            $gp->emoji = $data['emoji'];
            $gp->timezone = $data['timezone'];
            $gp->active = $data['active'];
            $gp->save();
        }
    }
}
