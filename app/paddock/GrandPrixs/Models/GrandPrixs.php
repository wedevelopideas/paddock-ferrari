<?php

namespace App\paddock\GrandPrixs\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GrandPrixs.
 *
 * @property int $id
 * @property string $country_id
 * @property string $name
 * @property string $slug
 * @property string $full_name
 * @property string $hashtag
 * @property string $emoji
 * @property string $timezone
 * @property int $active
 */
class GrandPrixs extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country_id',
        'name',
        'slug',
        'full_name',
        'hashtag',
        'emoji',
        'timezone',
        'active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Set the grand prix slug.
     *
     * @param string $value
     */
    public function setSlugAttribute(string $value)
    {
        $this->attributes['slug'] = Str::slug($value);
    }
}
