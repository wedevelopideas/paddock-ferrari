<?php

namespace App\paddock\Hashtags\Repositories;

use App\paddock\Hashtags\Models\Hashtags;

class HashtagsRepository
{
    /**
     * @var Hashtags
     */
    private $hashtags;

    /**
     * HashtagsRepository constructor.
     * @param Hashtags $hashtags
     */
    public function __construct(Hashtags $hashtags)
    {
        $this->hashtags = $hashtags;
    }

    /**
     * Count all hashtags.
     *
     * @return mixed
     */
    public function count()
    {
        return $this->hashtags
            ->count();
    }

    /**
     * Get all hashtags.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->hashtags
            ->orderBy('name', 'ASC')
            ->get();
    }

    /**
     * Get hashtag by its id.
     *
     * @param int $id
     * @return mixed
     */
    public function getHashtagByID(int $id)
    {
        return $this->hashtags
            ->where('id', $id)
            ->first();
    }

    /**
     * Store hashtags.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $hashtag = new Hashtags();
        $hashtag->name = $data['name'];
        $hashtag->slug = $data['name'];
        $hashtag->save();
    }

    /**
     * Update hashtags.
     *
     * @param int $id
     * @param array $data
     */
    public function update(int $id, array $data)
    {
        $hashtag = Hashtags::find($id);

        if ($hashtag) {
            $hashtag->name = $data['name'];
            $hashtag->slug = $data['name'];
            $hashtag->save();
        }
    }
}
