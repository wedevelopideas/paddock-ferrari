<?php

namespace App\paddock\Seasons\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class AddSeasonsRacesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'season' => [
                'required',
                'integer',
                Rule::unique('seasons_races')->where(function ($query) {
                    $query->where('season', $this->season);
                    $query->where('gp_id', $this->gp_id);
                    $query->where('track_id', $this->track_id);
                    $query->where('raceday', $this->raceday);
                }),
            ],
            'gp_id' => [
                'required',
                'integer',
                Rule::unique('seasons_races')->where(function ($query) {
                    $query->where('season', $this->season);
                    $query->where('gp_id', $this->gp_id);
                    $query->where('track_id', $this->track_id);
                    $query->where('raceday', $this->raceday);
                }),
            ],
            'track_id' => [
                'required',
                'integer',
                Rule::unique('seasons_races')->where(function ($query) {
                    $query->where('season', $this->season);
                    $query->where('gp_id', $this->gp_id);
                    $query->where('track_id', $this->track_id);
                    $query->where('raceday', $this->raceday);
                }),
            ],
            'raceday' => [
                'required',
                'date',
                Rule::unique('seasons_races')->where(function ($query) {
                    $query->where('season', $this->season);
                    $query->where('gp_id', $this->gp_id);
                    $query->where('track_id', $this->track_id);
                    $query->where('raceday', $this->raceday);
                }),
             ],
        ];
    }
}
