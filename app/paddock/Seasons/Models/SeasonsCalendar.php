<?php

namespace App\paddock\Seasons\Models;

use Illuminate\Database\Eloquent\Model;
use App\paddock\GrandPrixs\Models\GrandPrixs;

/**
 * Class SeasonsCalendar.
 *
 * @property int $id
 * @property int $season
 * @property int $gp_id
 * @property int $session_id
 * @property \Illuminate\Support\Carbon $start
 * @property \Illuminate\Support\Carbon $end
 * @property-read \App\paddock\GrandPrixs\Models\GrandPrixs $grandPrix
 */
class SeasonsCalendar extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'seasons_calendar';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'season',
        'gp_id',
        'session_id',
        'start',
        'end',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'start',
        'end',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Grand Prix relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function grandPrix()
    {
        return $this->belongsTo(GrandPrixs::class, 'gp_id');
    }

    /**
     * Get the start or end time of the session.
     *
     * @param int $session
     * @return mixed
     */
    public function sessionTime(int $session)
    {
        return $this
            ->where('season', $this->season)
            ->where('gp_id', $this->gp_id)
            ->where('session_id', $session)
            ->first();
    }
}
