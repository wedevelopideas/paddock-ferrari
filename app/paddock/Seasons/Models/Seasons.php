<?php

namespace App\paddock\Seasons\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Seasons.
 *
 * @property int $id
 * @property int $season
 */
class Seasons extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'season',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
