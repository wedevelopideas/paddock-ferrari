<?php

namespace App\paddock\Seasons\Repositories;

use App\paddock\Seasons\Models\Seasons;

class SeasonsRepository
{
    /**
     * @var Seasons
     */
    private $seasons;

    /**
     * SeasonsRepository constructor.
     * @param Seasons $seasons
     */
    public function __construct(Seasons $seasons)
    {
        $this->seasons = $seasons;
    }

    /**
     * Count all seasons.
     *
     * @return mixed
     */
    public function count()
    {
        return $this->seasons
            ->count();
    }

    /**
     * Get all seasons.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->seasons
            ->orderBy('season', 'ASC')
            ->get();
    }

    /**
     * Get season by its id.
     *
     * @param int $id
     * @return mixed
     */
    public function getSeasonByID(int $id)
    {
        return $this->seasons
            ->where('id', $id)
            ->first();
    }

    /**
     * Get season by its year.
     *
     * @param int $season
     * @return mixed
     */
    public function getSeasonBySeason(int $season)
    {
        return $this->seasons
            ->where('season', $season)
            ->first();
    }

    /**
     * Store seasons.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $seasons = new Seasons();
        $seasons->season = $data['season'];
        $seasons->save();
    }

    /**
     * Update seasons.
     *
     * @param int $id
     * @param array $data
     */
    public function update(int $id, array $data)
    {
        $seasons = Seasons::find($id);

        if ($seasons) {
            $seasons->season = $data['season'];
            $seasons->save();
        }
    }
}
