<?php

namespace App\paddock\Seasons\Repositories;

use App\paddock\Seasons\Models\SeasonsDrivers;

class SeasonsDriversRepository
{
    /**
     * @var SeasonsDrivers
     */
    private $seasonsDrivers;

    /**
     * SeasonsDriversRepository constructor.
     * @param SeasonsDrivers $seasonsDrivers
     */
    public function __construct(SeasonsDrivers $seasonsDrivers)
    {
        $this->seasonsDrivers = $seasonsDrivers;
    }

    /**
     * Get driver by id.
     *
     * @param int $id
     * @return mixed
     */
    public function getDriverByID(int $id)
    {
        return $this->seasonsDrivers
            ->where('id', $id)
            ->first();
    }

    /**
     * Get drivers by the season.
     *
     * @param int $season
     * @return mixed
     */
    public function getDriversBySeason(int $season)
    {
        return $this->seasonsDrivers
            ->select('drivers.id', 'drivers.name', 'drivers.slug', 'drivers.image', 'drivers.hashtag')
            ->join('drivers', 'drivers.id', '=', 'seasons_drivers.driver_id')
            ->where('seasons_drivers.season', $season)
            ->get();
    }

    /**
     * Store season drivers.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $driver = new SeasonsDrivers();
        $driver->season = $data['season'];
        $driver->driver_id = $data['driver_id'];
        $driver->save();
    }

    /**
     * Update season drivers.
     *
     * @param int $id
     * @param array $data
     */
    public function update(int $id, array $data)
    {
        $driver = SeasonsDrivers::find($id);

        if ($driver) {
            $driver->season = $data['season'];
            $driver->driver_id = $data['driver_id'];
            $driver->save();
        }
    }
}
