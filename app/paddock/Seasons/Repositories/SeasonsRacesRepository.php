<?php

namespace App\paddock\Seasons\Repositories;

use Illuminate\Support\Facades\DB;
use App\paddock\Seasons\Models\SeasonsRaces;

class SeasonsRacesRepository
{
    /**
     * @var SeasonsRaces
     */
    private $seasonsRaces;

    /**
     * SeasonsRacesRepository constructor.
     * @param SeasonsRaces $seasonsRaces
     */
    public function __construct(SeasonsRaces $seasonsRaces)
    {
        $this->seasonsRaces = $seasonsRaces;
    }

    /**
     * Count all season races.
     *
     * @return mixed
     */
    public function count()
    {
        return $this->seasonsRaces
            ->count();
    }

    /**
     * Get current race for the dashboard.
     *
     * @param int $season
     * @param int $gp
     * @return mixed
     */
    public function getCurrentRace(int $season, int $gp)
    {
        return $this->seasonsRaces
            ->select('grand_prixs.full_name', 'grand_prixs.country_id as country', 'grand_prixs.name', 'grand_prixs.slug', 'grand_prixs.hashtag', 'seasons_races.raceday')
            ->join('grand_prixs', 'grand_prixs.id', '=', 'seasons_races.gp_id')
            ->where('seasons_races.season', $season)
            ->where('seasons_races.gp_id', $gp)
            ->first();
    }

    /**
     * Get race by its id.
     *
     * @param int $id
     * @return mixed
     */
    public function getRaceByID(int $id)
    {
        return $this->seasonsRaces
            ->where('id', $id)
            ->first();
    }

    /**
     * Get race by season and grand prix.
     *
     * @param int $season
     * @param string $slug
     * @return mixed
     */
    public function getRaceBySeasonAndGP(int $season, string $slug)
    {
        return $this->seasonsRaces
            ->join('grand_prixs', 'grand_prixs.id', '=', 'seasons_races.gp_id')
            ->where('seasons_races.season', $season)
            ->where('grand_prixs.slug', $slug)
            ->first();
    }

    /**
     * Get all race events.
     *
     * @return mixed
     */
    public function getRaceEvents()
    {
        return $this->seasonsRaces
            ->where(DB::RAW('DATE_FORMAT(raceday, "%m-%d")'), '>=', DB::RAW('DATE_FORMAT(CURDATE(), "%m-%d")'))
            ->orderBy(DB::RAW('month(raceday)'), 'ASC')
            ->orderBy(DB::RAW('day(raceday)'), 'ASC')
            ->orderBy('season', 'ASC')
            ->limit(5)
            ->get();
    }

    /**
     * Get all races of a season.
     *
     * @param int $season
     * @return mixed
     */
    public function getRacesBySeason(int $season)
    {
        return $this->seasonsRaces
            ->join('grand_prixs', 'grand_prixs.id', '=', 'seasons_races.gp_id')
            ->where('seasons_races.season', $season)
            ->orderBy('seasons_races.id', 'ASC')
            ->get();
    }

    /**
     * Store season races.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $race = new SeasonsRaces();
        $race->season = $data['season'];
        $race->gp_id = $data['gp_id'];
        $race->track_id = $data['track_id'];
        $race->raceday = $data['raceday'];
        $race->status = $data['status'];
        $race->save();
    }

    /**
     * Update season races.
     *
     * @param int $id
     * @param array $data
     */
    public function update(int $id, array $data)
    {
        $race = SeasonsRaces::find($id);

        if ($race) {
            $race->season = $data['season'];
            $race->gp_id = $data['gp_id'];
            $race->track_id = $data['track_id'];
            $race->raceday = $data['raceday'];
            $race->status = $data['status'];
            $race->save();
        }
    }
}
