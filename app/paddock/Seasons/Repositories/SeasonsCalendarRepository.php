<?php

namespace App\paddock\Seasons\Repositories;

use App\paddock\Seasons\Models\SeasonsCalendar;

class SeasonsCalendarRepository
{
    /**
     * @var SeasonsCalendar
     */
    private $seasonsCalendar;

    /**
     * SeasonsCalendarRepository constructor.
     * @param SeasonsCalendar $seasonsCalendar
     */
    public function __construct(SeasonsCalendar $seasonsCalendar)
    {
        $this->seasonsCalendar = $seasonsCalendar;
    }

    /**
     * Get all sessions of the current season.
     *
     * @param int $season
     * @return mixed
     */
    public function getCurrentSeasonCalendar(int $season)
    {
        return $this->seasonsCalendar
            ->where('season', $season)
            ->groupBy('gp_id')
            ->orderBy('id')
            ->get();
    }

    /**
     * Get all sessions for a Grand Prix.
     *
     * @param int $season
     * @param int $gp
     * @return mixed
     */
    public function getSessionsForGP(int $season, int $gp)
    {
        return $this->seasonsCalendar
            ->where('season', $season)
            ->where('gp_id', $gp)
            ->get();
    }
}
