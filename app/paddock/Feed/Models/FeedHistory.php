<?php

namespace App\paddock\Feed\Models;

use App\paddock\Feed\HistoryTweet;
use App\paddock\Users\Models\Users;
use Illuminate\Database\Eloquent\Model;

/**
 * Class FeedHistory.
 *
 * @property int $id
 * @property int $author_id
 * @property string $lang
 * @property \Illuminate\Support\Carbon $date
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $link
 * @property string $domain
 * @property-read \App\paddock\Users\Models\Users $author
 */
class FeedHistory extends Model
{
    use HistoryTweet;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'feed_history';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'author_id',
        'lang',
        'title',
        'description',
        'image',
        'link',
        'domain',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'date',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Users relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(Users::class, 'author_id');
    }

    /**
     * Get the description of the feed entry.
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|string|null
     */
    public function getDescription()
    {
        if ($this->description) {
            return $this->description;
        }

        return trans('feed.no_description');
    }

    /**
     * Returns flag in Semantic UI style.
     *
     * @return string
     */
    public function getFlag()
    {
        if ($this->lang === 'en') {
            return 'england';
        }

        return $this->lang;
    }
}
