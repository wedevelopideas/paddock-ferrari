<?php

namespace App\paddock\Feed\Models;

use App\paddock\Feed\FDATweet;
use App\paddock\Users\Models\Users;
use Illuminate\Database\Eloquent\Model;

/**
 * Class FeedFDA.
 *
 * @property int $id
 * @property int $author_id
 * @property string $lang
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $link
 * @property string $domain
 * @property string $hashtags
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\paddock\Users\Models\Users $author
 */
class FeedFDA extends Model
{
    use FDATweet;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'feed_fda';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'author_id',
        'lang',
        'title',
        'description',
        'image',
        'link',
        'domain',
        'hashtags',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Users relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(Users::class, 'author_id');
    }

    /**
     * Get the description of the feed entry.
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|string|null
     */
    public function getDescription()
    {
        if ($this->description) {
            return $this->description;
        }

        return trans('feed.no_description');
    }

    /**
     * Returns flag in Semantic UI style.
     *
     * @return string
     */
    public function getFlag()
    {
        if ($this->lang === 'en') {
            return 'england';
        }

        return $this->lang;
    }
}
