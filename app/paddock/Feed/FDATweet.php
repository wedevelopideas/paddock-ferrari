<?php

namespace App\paddock\Feed;

trait FDATweet
{
    /**
     * Creates twitter intent link.
     *
     * @return string
     */
    public function getTweetLink(): string
    {
        $intent = 'https://twitter.com/intent/tweet?';

        $content = 'text='.$this->getTitle();

        $content .= $this->getHashtag();

        $content .= $this->getLink();

        return $intent.$content;
    }

    /**
     * Get hashtag for tweet.
     *
     * @return string
     */
    private function getHashtag(): string
    {
        return urlencode(' #FDA');
    }

    /**
     * Get link for tweet.
     *
     * @return string
     */
    private function getLink(): string
    {
        return '&url='.$this->link;
    }

    /**
     * Get title for tweet.
     *
     * @return null|string
     */
    private function getTitle(): ?string
    {
        $search = [
            '/FDA\b/u',
            '/FIA Formula 2\b/u',
            '/Formula 2\b/u',
            '/Formula 3\b/u',
        ];

        $replace = [
            '#FDA',
            '#F2',
            '#F2',
            '#F3',
        ];

        $title = (string) $this->title;

        $output = preg_replace($search, $replace, $title);

        if ($output !== null) {
            return urlencode($output);
        }
    }
}
