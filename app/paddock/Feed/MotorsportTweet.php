<?php

namespace App\paddock\Feed;

trait MotorsportTweet
{
    /**
     * Creates twitter intent link.
     *
     * @return string
     */
    public function getTweetLink(): string
    {
        $intent = 'https://twitter.com/intent/tweet?';

        $content = 'text='.$this->getTitle();

        $content .= $this->getHashtag();

        $content .= $this->getLink();

        return $intent.$content;
    }

    /**
     * Get hashtag for tweet.
     *
     * @return string
     */
    private function getHashtag(): string
    {
        return urlencode(' #ForzaFerrari');
    }

    /**
     * Get link for tweet.
     *
     * @return string
     */
    private function getLink(): string
    {
        return '&url='.$this->link;
    }

    /**
     * Get title for tweet.
     *
     * @return null|string
     */
    private function getTitle(): ?string
    {
        $search = [
            '/Kimi Räikkönens\b/u',
            '/Kimi Raikkonen\b/u',
            '/Kimi\b/u',
            '/Räikkönen\b/u',
            '/Raikkonen\b/u',
            '/Raikkonens\b/u',
            '/Sebastian\b/u',
            '/Vettel\b/u',
            '/Vettels\b/u',
            '/Seb\b/u',
            '/Ferrari\b/u',
            '/Ferraris\b/u',
            '/Ferrari Driver Academy\b/u',
            '/Maurizio\b/u',
            '/Arrivabene\b/u',
            '/Free practice 1\b/u',
            '/Free practice 2\b/u',
            '/Free Practice 3\b/u',
            '/1.Freies Training\b/u',
            '/2.Freies Training\b/u',
            '/3.Freies Training\b/u',
            '/Libere 2\b/u',
            '/Libere 3\b/u',
            '/Fri träning 2\b/u',
            '/T3\b/i',
            '/Libres 2\b/u',
            '/Libres 3\b/u',
            '/Scuderia Ferrari\b/u',
        ];

        $replace = [
            '#Kimi7\'s',
            '#Kimi7',
            '#Kimi7',
            '#Kimi7',
            '#Kimi7',
            '#Kimi7\'s',
            '#Seb5',
            '#Seb5',
            '#Seb5\'s',
            '#Seb5',
            '#Ferrari',
            '#Ferrari\'s',
            '#FDA',
            '#Maurizio',
            '#Arrivabene',
            '#FP1',
            '#FP2',
            '#FP3',
            '#FP1',
            '#FP2',
            '#FP3',
            '#FP2',
            '#FP3',
            '#FP2',
            '#FP3',
            '#FP2',
            '#FP3',
            '@ScuderiaFerrari',
        ];

        $title = (string) $this->title;

        $output = preg_replace($search, $replace, $title);

        if ($output !== null) {
            return urlencode($output);
        }
    }
}
