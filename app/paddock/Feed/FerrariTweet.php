<?php

namespace App\paddock\Feed;

use App\paddock\GrandPrixs\Models\GrandPrixs;

trait FerrariTweet
{
    /**
     * Creates twitter intent link.
     *
     * @return string
     */
    public function getTweetLink(): string
    {
        $intent = 'https://twitter.com/intent/tweet?';

        $content = 'text='.$this->getGPHashtag();

        $content .= $this->getTitle();

        $content .= $this->getHashtag();

        $content .= $this->getLink();

        return $intent.$content;
    }

    /**
     * Get GP hashtag for tweet.
     *
     * @return string
     */
    private function getGPHashtag(): string
    {
        $hashtag = substr($this->hashtags, 1);

        if (! empty($hashtag)) {
            $grandprix = GrandPrixs::where('hashtag', 'LIKE', '%'.$hashtag.'%')->first();

            if ($grandprix !== null) {
                $output = '#'.$grandprix->hashtag.' '.$grandprix->emoji.' ';
            } else {
                $output = $this->hashtags.' ';
            }
        } else {
            $output = '';
        }

        return urlencode($output);
    }

    /**
     * Get hashtag for tweet.
     *
     * @return string
     */
    private function getHashtag(): string
    {
        return urlencode(' #ForzaFerrari #essereFerrari 🔴');
    }

    /**
     * Get link for tweet.
     *
     * @return string
     */
    private function getLink(): string
    {
        return '&url='.$this->link;
    }

    /**
     * Get title for tweet.
     *
     * @return null|string
     */
    private function getTitle(): ?string
    {
        $search = [
            '/Kimi\b/u',
            '/Seb\b/u',
            '/Sebastian\b/u',
            '/Vettel\b/u',
            '/Maurizio\b/u',
            '/Ferrari\b/u',
            '/Charles\b/u',
            '/Leclerc\b/u',
        ];

        $replace = [
            '#Kimi7',
            '#Seb5',
            '#Seb5',
            '#Seb5',
            '#Maurizio',
            '#Ferrari',
            '#Charles16',
            '#Charles16',
        ];

        $title = (string) $this->title;

        $output = preg_replace($search, $replace, $title);

        if ($output !== null) {
            return urlencode($output);
        }
    }
}
