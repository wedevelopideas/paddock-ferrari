<?php

namespace App\paddock\Feed\Repositories;

use Illuminate\Support\Facades\DB;
use App\paddock\Feed\Models\FeedFDA;
use App\paddock\Feed\Models\FeedFerrari;
use App\paddock\Feed\Models\FeedHistory;
use App\paddock\Feed\Models\FeedMotorsport;

class FeedRepository
{
    /**
     * @var FeedFDA
     */
    private $feedFDA;

    /**
     * @var FeedFerrari
     */
    private $feedFerrari;

    /**
     * @var FeedHistory
     */
    private $feedHistory;

    /**
     * @var FeedMotorsport
     */
    private $feedMotorsport;

    /**
     * FeedRepository constructor.
     * @param FeedFDA $feedFDA
     * @param FeedFerrari $feedFerrari
     * @param FeedHistory $feedHistory
     * @param FeedMotorsport $feedMotorsport
     */
    public function __construct(
        FeedFDA $feedFDA,
        FeedFerrari $feedFerrari,
        FeedHistory $feedHistory,
        FeedMotorsport $feedMotorsport
    ) {
        $this->feedFDA = $feedFDA;
        $this->feedFerrari = $feedFerrari;
        $this->feedHistory = $feedHistory;
        $this->feedMotorsport = $feedMotorsport;
    }

    /**
     * Get all articles by the user id.
     *
     * @param int $user_id
     * @param string $lang
     * @return mixed
     */
    public function getAllArticlesByUserID(int $user_id, string $lang)
    {
        $history = $this->feedHistory
            ->select('title', 'description', 'image', DB::raw('DATE_FORMAT(CONCAT(YEAR(CURDATE()),\'-\',MONTH(date),\'-\',DAYOFMONTH(date)), "%Y-%m-%d %H:%i:%s") as date'))
            ->where('author_id', $user_id)
            ->where('lang', $lang);

        $motorsport = $this->feedMotorsport
            ->select('title', 'description', 'image', 'created_at as date')
            ->where('author_id', $user_id)
            ->where('lang', $lang);

        $fda = $this->feedFDA
            ->select('title', 'description', 'image', 'created_at as date')
            ->where('author_id', $user_id)
            ->where('lang', $lang);

        return $this->feedFerrari
            ->select('title', 'description', 'image', 'created_at as date')
            ->where('author_id', $user_id)
            ->where('lang', $lang)
            ->union($history)
            ->union($motorsport)
            ->union($fda)
            ->orderBy('date', 'DESC')
            ->get();
    }

    /**
     * Shows the latest articles of the ferrari and motorsport feed.
     *
     * @param string $lang
     * @return mixed
     */
    public function getLatestArticles(string $lang)
    {
        $data = [];

        $fda = $this->feedFDA
            ->select(DB::raw('\'fda\' as type'), 'title', 'created_at as date')
            ->where('lang', $lang);

        $ferrari = $this->feedFerrari
            ->select(DB::raw('\'ferrari\' as type'), 'title', 'created_at as date')
            ->where('lang', $lang);

        $history = $this->feedHistory
            ->select(DB::raw('\'history\' as type'), 'title', DB::raw('DATE_FORMAT(CONCAT(YEAR(CURDATE()),\'-\',MONTH(date),\'-\',DAYOFMONTH(date)), "%Y-%m-%d %H:%i:%s") as date'))
            ->where('lang', $lang);

        $entries = $this->feedMotorsport
            ->select(DB::raw('\'motorsport\' as type'), 'title', 'created_at as date')
            ->where('lang', $lang)
            ->union($fda)
            ->union($ferrari)
            ->union($history)
            ->limit(5)
            ->orderBy('date', 'DESC')
            ->get();

        foreach ($entries as $e => $entry) {
            $data[$e]['type'] = $this->setLatestArticlesType($entry->type);

            $data[$e]['title'] = $entry->title;

            $data[$e]['date'] = $entry->date->timezone(auth()->user()->timezone)->diffForHumans();

            $data[$e]['link'] = $this->setLatestArticlesLink($entry->type);
        }

        return $data;
    }

    /**
     * Sets the link for the latest articles.
     *
     * @param string $type
     * @return string
     */
    private function setLatestArticlesLink(string $type)
    {
        if ($type === 'fda') {
            return route('feed.fda');
        }

        if ($type === 'ferrari') {
            return route('feed.ferrari');
        }

        if ($type === 'history') {
            return route('feed.history');
        }

        if ($type === 'motorsport') {
            return route('feed.motorsport');
        }

        return '';
    }

    /**
     * Sets the type for the latest articles.
     *
     * @param string $type
     * @return string
     */
    private function setLatestArticlesType(string $type)
    {
        if ($type === 'fda') {
            return strtoupper($type);
        }

        return ucfirst($type);
    }
}
