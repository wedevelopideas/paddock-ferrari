<?php

namespace App\paddock\Feed\Repositories;

use App\paddock\Feed\Models\FeedFerrari;

class FeedFerrariRepository
{
    /**
     * @var FeedFerrari
     */
    private $feedFerrari;

    /**
     * FeedFerrariRepository constructor.
     * @param FeedFerrari $feedFerrari
     */
    public function __construct(FeedFerrari $feedFerrari)
    {
        $this->feedFerrari = $feedFerrari;
    }

    /**
     * Get all ferrari feeds.
     *
     * @param string $lang
     * @return mixed
     */
    public function getAll(string $lang)
    {
        return $this->feedFerrari
            ->where('lang', $lang)
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    /**
     * Get feeds by date and session.
     *
     * @param string $date
     * @param int $session
     * @return mixed
     */
    public function getEntryByDateAndSession(string $date, int $session)
    {
        return $this->feedFerrari
            ->where('created_at', 'LIKE', '%'.$date.'%')
            ->where('session', $session)
            ->get();
    }

    /**
     * Get feed by date, session and lang.
     *
     * @param string $date
     * @param int $session
     * @param string $lang
     * @return mixed
     */
    public function getFeedByDateAndSessionAndLang(string $date, int $session, string $lang)
    {
        return $this->feedFerrari
            ->where('created_at', 'LIKE', '%'.$date.'%')
            ->where('session', $session)
            ->where('lang', $lang)
            ->firstOrFail();
    }

    /**
     * Get feed by hashtag and lang.
     *
     * @param string $hashtag
     * @param string $lang
     * @return mixed
     */
    public function getFeedByHashtagAndLang(string $hashtag, string $lang)
    {
        return $this->feedFerrari
            ->where('lang', $lang)
            ->where('hashtags', '#'.$hashtag)
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    /**
     * Get latest article.
     *
     * @param string $lang
     * @return mixed
     */
    public function getLatestArticle(string $lang)
    {
        return $this->feedFerrari
            ->where('lang', $lang)
            ->orderBy('id', 'DESC')
            ->first();
    }
}
