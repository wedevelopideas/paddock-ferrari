<?php

namespace App\paddock\Feed\Repositories;

use App\paddock\Feed\Models\FeedMotorsport;

class FeedMotorsportRepository
{
    /**
     * @var FeedMotorsport
     */
    private $feedMotorsport;

    /**
     * FeedMotorsportRepository constructor.
     * @param FeedMotorsport $feedMotorsport
     */
    public function __construct(FeedMotorsport $feedMotorsport)
    {
        $this->feedMotorsport = $feedMotorsport;
    }

    /**
     * Get all motorsport feeds.
     *
     * @param string $lang
     * @return mixed
     */
    public function getAll(string $lang)
    {
        return $this->feedMotorsport
            ->where('lang', $lang)
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    /**
     * Get feed by its id.
     *
     * @param int $id
     * @return mixed
     */
    public function getFeedByID(int $id)
    {
        return $this->feedMotorsport
            ->where('id', $id)
            ->firstOrFail();
    }

    /**
     * Get feeds by date.
     *
     * @param string $date
     * @return mixed
     */
    public function getFeedsByDate(string $date)
    {
        return $this->feedMotorsport
            ->where('created_at', 'LIKE', '%'.$date.'%')
            ->get();
    }

    /**
     * Get latest article.
     *
     * @param string $lang
     * @return mixed
     */
    public function getLatestArticle(string $lang)
    {
        return $this->feedMotorsport
            ->where('lang', $lang)
            ->orderBy('id', 'DESC')
            ->first();
    }
}
