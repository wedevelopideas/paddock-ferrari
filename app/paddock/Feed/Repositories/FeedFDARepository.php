<?php

namespace App\paddock\Feed\Repositories;

use App\paddock\Feed\Models\FeedFDA;

class FeedFDARepository
{
    /**
     * @var FeedFDA
     */
    private $feedFDA;

    /**
     * FeedFDARepository constructor.
     * @param FeedFDA $feedFDA
     */
    public function __construct(FeedFDA $feedFDA)
    {
        $this->feedFDA = $feedFDA;
    }

    /**
     * Get all fda feeds.
     *
     * @param string $lang
     * @return mixed
     */
    public function getAll(string $lang)
    {
        return $this->feedFDA
            ->where('lang', $lang)
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    /**
     * Get feeds by date.
     *
     * @param string $date
     * @return mixed
     */
    public function getEntryByDate(string $date)
    {
        return $this->feedFDA
            ->where('created_at', 'LIKE', '%'.$date.'%')
            ->get();
    }

    /**
     * Get feed by date and lang.
     *
     * @param string $date
     * @param string $lang
     * @return mixed
     */
    public function getFeedByDateAndLang(string $date, string $lang)
    {
        return $this->feedFDA
            ->where('created_at', 'LIKE', '%'.$date.'%')
            ->where('lang', $lang)
            ->firstOrFail();
    }

    /**
     * Get latest article.
     *
     * @param string $lang
     * @return mixed
     */
    public function getLatestArticle(string $lang)
    {
        return $this->feedFDA
            ->where('lang', $lang)
            ->orderBy('id', 'DESC')
            ->first();
    }
}
