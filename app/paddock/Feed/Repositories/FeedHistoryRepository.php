<?php

namespace App\paddock\Feed\Repositories;

use App\paddock\Feed\Models\FeedHistory;

class FeedHistoryRepository
{
    /**
     * @var FeedHistory
     */
    private $feedHistory;

    /**
     * FeedHistoryRepository constructor.
     * @param FeedHistory $feedHistory
     */
    public function __construct(FeedHistory $feedHistory)
    {
        $this->feedHistory = $feedHistory;
    }

    /**
     * Get all historical feeds.
     *
     * @param string $lang
     * @return mixed
     */
    public function getAll(string $lang)
    {
        return $this->feedHistory
            ->where('lang', $lang)
            ->orderBy('id', 'DESC')
            ->get();
    }

    /**
     * Get entry of the day.
     *
     * @param string $lang
     * @param string $date
     * @return mixed
     */
    public function getCurrentEntry(string $lang, string $date)
    {
        return $this->feedHistory
            ->where('lang', $lang)
            ->where('date', 'LIKE', '%'.$date.'%')
            ->first();
    }

    /**
     * Get feed by date and lang.
     *
     * @param string $date
     * @param string $lang
     * @return mixed
     */
    public function getFeedByDateAndLang(string $date, string $lang)
    {
        return $this->feedHistory
            ->where('lang', $lang)
            ->where('date', $date)
            ->firstOrFail();
    }

    /**
     * Get feeds by date.
     *
     * @param string $date
     * @return mixed
     */
    public function getFeedsByDate(string $date)
    {
        return $this->feedHistory
            ->where('date', $date)
            ->get();
    }

    /**
     * Get latest article.
     *
     * @param string $lang
     * @param string $date
     * @return mixed
     */
    public function getLatestArticle(string $lang, string $date)
    {
        return $this->feedHistory
            ->where('lang', $lang)
            ->where('date', 'LIKE', '%'.$date.'%')
            ->first();
    }
}
