<?php

namespace App\paddock;

use Carbon\Carbon;
use App\paddock\Settings\Repositories\SettingsRepository;
use App\paddock\GrandPrixs\Repositories\GrandPrixsRepository;

class Timezone
{
    /**
     * @var GrandPrixsRepository
     */
    private $grandPrixsRepository;

    /**
     * @var SettingsRepository
     */
    private $settingsRepository;

    /**
     * Timezone constructor.
     * @param GrandPrixsRepository $grandPrixsRepository
     * @param SettingsRepository $settingsRepository
     */
    public function __construct(
        GrandPrixsRepository $grandPrixsRepository,
        SettingsRepository $settingsRepository
    ) {
        $this->grandPrixsRepository = $grandPrixsRepository;
        $this->settingsRepository = $settingsRepository;
    }

    /**
     * Get the timezone of the current Grand Prix.
     *
     * @return string
     */
    public function awayzone()
    {
        $gp = $this->settingsRepository->getSettings('grandprix');

        $timezone = $this->grandPrixsRepository->getGrandPrixTimezone((int) $gp);

        if ($timezone) {
            return Carbon::now($timezone->timezone)->format('H:m');
        }
    }

    /**
     * Get the timezone of the user.
     *
     * @return string
     */
    public function homezone()
    {
        $timezone = auth()->user()->timezone;

        return Carbon::now($timezone)->format('H:m');
    }
}
