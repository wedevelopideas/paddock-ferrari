<?php

namespace App\paddock\SocialMedias\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddSocialMediasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:social_medias',
            'image' => 'string|nullable',
            'website' => 'url|nullable',
            'facebook' => 'url|nullable',
            'twitter' => 'url|nullable',
            'instagram' => 'url|nullable',
            'youtube' => 'url|nullable',
        ];
    }
}
