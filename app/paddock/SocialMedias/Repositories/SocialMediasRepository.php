<?php

namespace App\paddock\SocialMedias\Repositories;

use App\paddock\SocialMedias\Models\SocialMedias;

class SocialMediasRepository
{
    /**
     * @var SocialMedias
     */
    private $socialMedias;

    /**
     * SocialMediasRepository constructor.
     * @param SocialMedias $socialMedias
     */
    public function __construct(SocialMedias $socialMedias)
    {
        $this->socialMedias = $socialMedias;
    }

    /**
     * Get all social medias.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->socialMedias
            ->orderBy('name', 'ASC')
            ->get();
    }

    /**
     * Get social media by its id.
     *
     * @param int $id
     * @return mixed
     */
    public function getSocialMediaByID(int $id)
    {
        return $this->socialMedias
            ->where('id', $id)
            ->first();
    }

    /**
     * Store social media.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $social = new SocialMedias();
        $social->name = $data['name'];
        $social->image = $data['image'];
        $social->website = $data['website'];
        $social->facebook = $data['facebook'];
        $social->twitter = $data['twitter'];
        $social->instagram = $data['instagram'];
        $social->youtube = $data['youtube'];
        $social->save();
    }

    /**
     * Update social media.
     *
     * @param int $id
     * @param array $data
     */
    public function update(int $id, array $data)
    {
        $social = SocialMedias::find($id);

        if ($social) {
            $social->name = $data['name'];
            $social->image = $data['image'];
            $social->website = $data['website'];
            $social->facebook = $data['facebook'];
            $social->twitter = $data['twitter'];
            $social->instagram = $data['instagram'];
            $social->youtube = $data['youtube'];
            $social->save();
        }
    }
}
