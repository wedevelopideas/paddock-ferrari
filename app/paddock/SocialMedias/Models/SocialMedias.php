<?php

namespace App\paddock\SocialMedias\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SocialMedias.
 *
 * @property int $id
 * @property string $name
 * @property string|null $image
 * @property string|null $website
 * @property string|null $facebook
 * @property string|null $twitter
 * @property string|null $instagram
 * @property string|null $youtube
 */
class SocialMedias extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'image',
        'website',
        'facebook',
        'twitter',
        'instagram',
        'youtube',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
