<?php

namespace App\paddock\Tracks\Repositories;

use App\paddock\Tracks\Models\Tracks;

class TracksRepository
{
    /**
     * @var Tracks
     */
    private $tracks;

    /**
     * TracksRepository constructor.
     * @param Tracks $tracks
     */
    public function __construct(Tracks $tracks)
    {
        $this->tracks = $tracks;
    }

    /**
     * Count all tracks.
     *
     * @return mixed
     */
    public function count()
    {
        return $this->tracks
            ->count();
    }

    /**
     * Get all tracks.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->tracks
            ->orderBy('name', 'ASC')
            ->get();
    }

    /**
     * Get track by its id.
     *
     * @param int $id
     * @return mixed
     */
    public function getTrackByID(int $id)
    {
        return $this->tracks
            ->where('id', $id)
            ->first();
    }

    /**
     * Store tracks.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $track = new Tracks();
        $track->country_id = $data['country_id'];
        $track->name = $data['name'];
        $track->slug = $data['name'];
        $track->laps = $data['laps'];
        $track->length = $data['length'];
        $track->active = $data['active'];
        $track->save();
    }

    /**
     * Update tracks.
     *
     * @param int $id
     * @param array $data
     */
    public function update(int $id, array $data)
    {
        $track = Tracks::find($id);

        if ($track) {
            $track->country_id = $data['country_id'];
            $track->name = $data['name'];
            $track->slug = $data['name'];
            $track->laps = $data['laps'];
            $track->length = $data['length'];
            $track->active = $data['active'];
            $track->save();
        }
    }
}
