<?php

namespace App\paddock\Tracks\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Tracks.
 *
 * @property int $id
 * @property string $country_id
 * @property string $name
 * @property string $slug
 * @property int $laps
 * @property float $length
 * @property int $active
 */
class Tracks extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country_id',
        'name',
        'slug',
        'laps',
        'length',
        'active',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Set the track's slug.
     *
     * @param string $value
     */
    public function setSlugAttribute(string $value)
    {
        $this->attributes['slug'] = Str::slug($value);
    }
}
