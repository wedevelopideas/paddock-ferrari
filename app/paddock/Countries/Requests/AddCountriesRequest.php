<?php

namespace App\paddock\Countries\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddCountriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|string|unique:countries',
            'name' => 'required|string',
        ];
    }
}
