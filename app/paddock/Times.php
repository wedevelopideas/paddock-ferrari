<?php

namespace App\paddock;

trait Times
{
    /**
     * Returns laptimes.
     *
     * @return string
     */
    public function secintime()
    {
        if ($this->laptime !== 0) {
            $min = (int) ($this->laptime / 60000);
            $sek = ((int) ($this->laptime / 1000)) % 60;
            $ms = $this->laptime % 1000;

            return sprintf('%01d:%02d.%03d', $min, $sek, $ms);
        }

        return 'No time set';
    }

    /**
     * Returns and calculates gaps.
     *
     * @param int $laptime
     * @param int|null $fastest
     * @return string
     */
    public function gap(int $laptime, int $fastest = null)
    {
        if ($laptime !== 0 && $fastest !== null) {
            $gap = $laptime - $fastest;

            if ($gap > 0) {
                $sec = ($gap / 1000) % 60;
                $msec = $gap % 1000;

                return '+'.sprintf('%01d.%03d', $sec, $msec);
            }
        }
    }
}
