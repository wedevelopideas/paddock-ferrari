<?php

namespace App\paddock\Media\Youtube\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class YouTubeVideos.
 *
 * @property int $id
 * @property string $youtube_id
 * @property string $title
 * @property string $description
 * @property string $thumbnail
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string $teaser
 */
class YouTubeVideos extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'youtube_videos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'youtube_id',
        'title',
        'description',
        'thumbnail',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
    ];

    /**
     * Get the teaser image for the dashboard.
     *
     * @return string
     */
    public function getTeaserAttribute()
    {
        return 'https://i.ytimg.com/vi/'.$this->youtube_id.'/maxresdefault.jpg';
    }

    /**
     * Set the thumbnail for the video.
     *
     * @param string $value
     */
    public function setThumbnailAttribute(string $value)
    {
        $this->attributes['thumbnail'] = 'https://i.ytimg.com/vi/'.$value.'/mqdefault.jpg';
    }
}
