<?php

namespace App\paddock\Media\Youtube\Repositories;

use App\paddock\Media\Youtube\Models\YouTubeVideos;

class YouTubeVideosRepository
{
    /**
     * @var YouTubeVideos
     */
    private $youtubeVideos;

    /**
     * YouTubeVideosRepository constructor.
     * @param YouTubeVideos $youtubeVideos
     */
    public function __construct(YouTubeVideos $youtubeVideos)
    {
        $this->youtubeVideos = $youtubeVideos;
    }

    /**
     * Counts all youtube videos.
     *
     * @return mixed
     */
    public function count()
    {
        return $this->youtubeVideos
            ->count();
    }

    /**
     * Get all youtube videos.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->youtubeVideos
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    /**
     * Get the latest video by youtube.
     *
     * @return mixed
     */
    public function getLatestVideo()
    {
        return $this->youtubeVideos
            ->orderBy('created_at', 'DESC')
            ->first();
    }

    /**
     * Get the latest videos by youtube.
     *
     * @return mixed
     */
    public function getLatestVideos()
    {
        return $this->youtubeVideos
            ->orderBy('created_at', 'DESC')
            ->limit(6)
            ->get();
    }

    /**
     * Get youtube video by its id.
     *
     * @param int $id
     * @return mixed
     */
    public function getVideoByID(int $id)
    {
        return $this->youtubeVideos
            ->where('id', $id)
            ->first();
    }

    /**
     * Get youtube video by its youtube id.
     *
     * @param string $youtube_id
     * @return mixed
     */
    public function getVideoByYouTubeID(string $youtube_id)
    {
        return $this->youtubeVideos
            ->where('youtube_id', $youtube_id)
            ->first();
    }

    /**
     * Store youtube video.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $video = new YouTubeVideos();
        $video->youtube_id = $data['youtube_id'];
        $video->title = $data['title'];
        $video->description = $data['description'];
        $video->thumbnail = $data['youtube_id'];
        $video->save();
    }

    /**
     * Update youtube video.
     *
     * @param int $id
     * @param array $data
     */
    public function update(int $id, array $data)
    {
        $video = YouTubeVideos::find($id);

        if ($video) {
            $video->youtube_id = $data['youtube_id'];
            $video->title = $data['title'];
            $video->description = $data['description'];
            $video->thumbnail = $data['youtube_id'];
            $video->save();
        }
    }
}
