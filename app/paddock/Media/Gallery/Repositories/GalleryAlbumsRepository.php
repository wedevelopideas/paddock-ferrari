<?php

namespace App\paddock\Media\Gallery\Repositories;

use App\paddock\Media\Gallery\Models\GalleryAlbums;

class GalleryAlbumsRepository
{
    /**
     * @var GalleryAlbums
     */
    private $galleryAlbums;

    /**
     * GalleryAlbumsRepository constructor.
     * @param GalleryAlbums $galleryAlbums
     */
    public function __construct(GalleryAlbums $galleryAlbums)
    {
        $this->galleryAlbums = $galleryAlbums;
    }

    /**
     * Get gallery album by id.
     *
     * @param int $id
     * @return mixed
     */
    public function getAlbumByID(int $id)
    {
        return $this->galleryAlbums
            ->where('id', $id)
            ->first();
    }

    /**
     * Get gallery album by slug.
     *
     * @param string $slug
     * @return mixed
     */
    public function getAlbumBySlug(string $slug)
    {
        return $this->galleryAlbums
            ->where('slug', $slug)
            ->first();
    }

    /**
     * Get all gallery albums.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->galleryAlbums
            ->join('gallery_images', 'gallery_images.id', '=', 'gallery_albums.cover_id')
            ->orderBy('gallery_images.created_at', 'DESC')
            ->get();
    }

    /**
     * Get the latest gallery albums.
     *
     * @return mixed
     */
    public function getLatestGalleryAlbums()
    {
        return $this->galleryAlbums
            ->join('gallery_images', 'gallery_images.id', '=', 'gallery_albums.cover_id')
            ->orderBy('gallery_images.created_at', 'DESC')
            ->limit(6)
            ->get();
    }

    /**
     * Stores gallery album.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $album = new GalleryAlbums();
        $album->name = $data['name'];
        $album->slug = $data['name'];
        $album->cover_id = $data['cover_id'];
        $album->creator_id = $data['creator_id'];
        $album->save();
    }

    /**
     * Updates gallery album.
     *
     * @param int $id
     * @param array $data
     */
    public function update(int $id, array $data)
    {
        $album = GalleryAlbums::find($id);

        if ($album) {
            $album->name = $data['name'];
            $album->slug = $data['name'];
            $album->save();
        }
    }
}
