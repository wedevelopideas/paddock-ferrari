<?php

namespace App\paddock\Media\Gallery\Repositories;

use App\paddock\Media\Gallery\Models\GalleryImages;

class GalleryImagesRepository
{
    /**
     * @var GalleryImages
     */
    private $galleryImages;

    /**
     * GalleryImagesRepository constructor.
     * @param GalleryImages $galleryImages
     */
    public function __construct(GalleryImages $galleryImages)
    {
        $this->galleryImages = $galleryImages;
    }

    /**
     * Get all images by album id.
     *
     * @param int $album_id
     * @return mixed
     */
    public function getAllImagesByAlbumID(int $album_id)
    {
        return $this->galleryImages
            ->where('album_id', $album_id)
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    /**
     * Get image by album id and id.
     *
     * @param int $album_id
     * @param int $id
     * @return mixed
     */
    public function getImageByAlbumIDAndID(int $album_id, int $id)
    {
        return $this->galleryImages
            ->where('album_id', $album_id)
            ->where('id', $id)
            ->first();
    }

    /**
     * Stores gallery images.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $image = new GalleryImages();
        $image->album_id = $data['album_id'];
        $image->creator_id = $data['creator_id'];
        $image->image = $data['image'];
        $image->source = $data['source'];
        $image->save();
    }

    /**
     * Updates gallery images.
     *
     * @param int $id
     * @param array $data
     */
    public function update(int $id, array $data)
    {
        $image = GalleryImages::find($id);

        if ($image) {
            $image->album_id = $data['album_id'];
            $image->creator_id = $data['creator_id'];
            $image->image = $data['image'];
            $image->source = $data['source'];
            $image->save();
        }
    }
}
