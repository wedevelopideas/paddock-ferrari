<?php

namespace App\paddock\Media\Gallery\Repositories;

use App\paddock\Media\Gallery\Models\GalleryTwitter;

class GalleryTwitterRepository
{
    /**
     * @var GalleryTwitter
     */
    private $galleryTwitter;

    /**
     * GalleryTwitterRepository constructor.
     * @param GalleryTwitter $galleryTwitter
     */
    public function __construct(GalleryTwitter $galleryTwitter)
    {
        $this->galleryTwitter = $galleryTwitter;
    }

    /**
     * Get all twitter accounts.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->galleryTwitter
            ->select('twitter_accounts.name as name', 'banner.image as profile_banner', 'image.image as profile_image')
            ->leftJoin('twitter_accounts', 'twitter_accounts.twitter_id', '=', 'gallery_twitter.twitter_id')
            ->leftJoin('gallery_images as banner', 'banner.id', '=', 'gallery_twitter.profile_banner_id')
            ->leftJoin('gallery_images as image', 'image.id', '=', 'gallery_twitter.profile_image_id')
            ->orderBy('twitter_accounts.screen_name', 'ASC')
            ->get();
    }
}
