<?php

namespace App\paddock\Media\Gallery\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class GalleryTwitter.
 *
 * @property int $id
 * @property int $twitter_id
 * @property int|null $profile_banner_id
 * @property int|null $profile_image_id
 */
class GalleryTwitter extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'gallery_twitter';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'twitter_id',
        'profile_banner_id',
        'profile_image_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
