<?php

namespace App\paddock\Media\Gallery\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class GalleryImages.
 *
 * @property int $id
 * @property int $album_id
 * @property int $creator_id
 * @property string $image
 * @property string $source
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 */
class GalleryImages extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'album_id',
        'creator_id',
        'image',
        'source',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
