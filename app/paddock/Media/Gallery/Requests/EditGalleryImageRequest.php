<?php

namespace App\paddock\Media\Gallery\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditGalleryImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required|url',
            'source' => 'required|string',
        ];
    }
}
