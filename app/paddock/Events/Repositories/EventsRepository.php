<?php

namespace App\paddock\Events\Repositories;

use App\paddock\Events\Models\Events;

class EventsRepository
{
    /**
     * @var Events
     */
    private $events;

    /**
     * EventsRepository constructor.
     * @param Events $events
     */
    public function __construct(Events $events)
    {
        $this->events = $events;
    }

    /**
     * Get all events by the user language.
     *
     * @param string $lang
     * @return mixed
     */
    public function getAllEventsByLang(string $lang)
    {
        return $this->events
            ->where('lang', $lang)
            ->get();
    }

    /**
     * Get data of event by driver id and date.
     *
     * @param int $driver_id
     * @param string $event_date
     * @return mixed
     */
    public function getEventByDriverIdAndDate(int $driver_id, string $event_date)
    {
        return $this->events
            ->where('driver_id', $driver_id)
            ->where('event_date', $event_date)
            ->get();
    }

    /**
     * Get data of event by driver id and date.
     *
     * @param string $lang
     * @param int $driver_id
     * @param string $event_date
     * @return mixed
     */
    public function getEventByDriverIdDateAndLang(string $lang, int $driver_id, string $event_date)
    {
        return $this->events
            ->where('lang', $lang)
            ->where('driver_id', $driver_id)
            ->where('event_date', $event_date)
            ->first();
    }

    /**
     * Get event by its id.
     *
     * @param int $id
     * @return mixed
     */
    public function getEventByID(int $id)
    {
        return $this->events
            ->where('id', $id)
            ->first();
    }

    /**
     * Store events.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $event = new Events();
        $event->type = $data['type'];
        $event->lang = $data['lang'];
        $event->driver_id = $data['driver_id'];
        $event->event_date = $data['event_date'];
        $event->event_text = $data['event_text'];
        $event->event_twitter = $data['event_text'];
        $event->event_hashtags = $data['event_hashtags'];
        $event->event_link = $data['event_link'];
        $event->event_f1tv_link = $data['event_f1tv_link'];
        $event->save();
    }

    /**
     * Update events.
     *
     * @param int $id
     * @param array $data
     */
    public function update(int $id, array $data)
    {
        $event = Events::find($id);

        if ($event) {
            $event->type = $data['type'];
            $event->lang = $data['lang'];
            $event->driver_id = $data['driver_id'];
            $event->event_date = $data['event_date'];
            $event->event_text = $data['event_text'];
            $event->event_twitter = $data['event_text'];
            $event->event_hashtags = $data['event_hashtags'];
            $event->event_link = $data['event_link'];
            $event->event_f1tv_link = $data['event_f1tv_link'];
            $event->save();
        }
    }
}
