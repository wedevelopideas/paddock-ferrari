<?php

namespace App\paddock\Events\Models;

use App\paddock\Drivers\Models\Drivers;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Events.
 *
 * @property int $id
 * @property int $type
 * @property string $lang
 * @property int $driver_id
 * @property \Illuminate\Support\Carbon $event_date
 * @property string $event_text
 * @property string $event_twitter
 * @property string $event_hashtags
 * @property string|null $event_link
 * @property string|null $event_f1tv_link
 * @property-read \App\paddock\Drivers\Models\Drivers $driver
 */
class Events extends Model
{
    /**
     * Part of the twitter share link.
     *
     * @var string
     */
    private const LINK = 'https://twitter.com/intent/tweet?';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'lang',
        'driver_id',
        'event_date',
        'event_text',
        'event_twitter',
        'event_hashtags',
        'event_link',
        'event_f1tv_link',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'event_date',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Set the event text for Twitter.
     *
     * @param string $value
     */
    public function setEventTwitterAttribute(string $value)
    {
        $this->attributes['event_twitter'] = urlencode($value);
    }

    /**
     * Driver's relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function driver()
    {
        return $this->belongsTo(Drivers::class);
    }

    /**
     * Returns flag in Semantic UI style.
     *
     * @return string
     */
    public function getFlag()
    {
        if ($this->lang === 'en') {
            return 'england';
        }

        return $this->lang;
    }

    /**
     * Creates twitter link.
     *
     * @return string
     */
    public function getTwitterLink()
    {
        $intent = self::LINK;

        $intent .= 'text='.$this->event_twitter;

        if ($this->event_hashtags) {
            $intent .= ' '.urlencode($this->event_hashtags);
        }

        if (! $this->event_hashtags) {
            $intent .= ' '.urlencode('#ForzaFerrari');
        }

        if ($this->event_link) {
            $intent .= '&url='.$this->event_link;
        }

        return $intent;
    }
}
