<?php

namespace App\paddock\Events\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddEventsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|integer',
            'lang' => 'required|string', //Todo: Improvement
            'driver_id' => 'required|integer', //Todo: Improvement
            'event_date' => 'required|date',
            'event_text' => 'required|string',
            'event_hashtags' => 'required|string',
            'event_link' => 'nullable|url',
        ];
    }
}
