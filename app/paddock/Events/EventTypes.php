<?php

namespace App\paddock\Events;

class EventTypes
{
    /**
     * Sets the id for the birthday.
     *
     * @var int
     */
    public const BIRTHDAY = 1;

    /**
     * Sets the id for the day of death.
     *
     * @var int
     */
    public const DEATH = 2;
}
