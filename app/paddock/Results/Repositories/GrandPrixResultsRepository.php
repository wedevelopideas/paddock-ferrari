<?php

namespace App\paddock\Results\Repositories;

use App\paddock\Results\Models\Results;

class GrandPrixResultsRepository
{
    /**
     * @var Results
     */
    private $results;

    /**
     * GrandPrixResultsRepository constructor.
     * @param Results $results
     */
    public function __construct(Results $results)
    {
        $this->results = $results;
    }

    /**
     * Get all results of the Grand Prix.
     *
     * @param int $gp_id
     * @return array
     */
    public function getResults(int $gp_id)
    {
        return [
            'laps' => $this->getLaps($gp_id),
            'podiums' => $this->getPodiums($gp_id),
            'points' => $this->getPoints($gp_id),
            'poles' => $this->getPoles($gp_id),
            'wins' => $this->getWins($gp_id),
        ];
    }

    /**
     * Get the results for the race of the Grand Prix.
     *
     * @param int $gp_id
     * @return mixed
     */
    public function getResultsForRace(int $gp_id)
    {
        return $this->results
            ->where('gp_id', $gp_id)
            ->where('session_id', 1) //TODO:Needs to be improved
            ->where('team', 1)
            ->orderBy('dateofresult', 'DESC')
            ->orderBy('place', 'ASC')
            ->orderBy('laps', 'DESC')
            ->get();
    }

    /**
     * Get number of laps of the Grand Prix.
     *
     * @param int $gp_id
     * @return mixed
     */
    private function getLaps(int $gp_id)
    {
        return $this->results
            ->where('gp_id', $gp_id)
            ->where('session_id', 1)  //TODO:Needs to be improved
            ->where('team', 1)
            ->sum('laps');
    }

    /**
     * Get number of podiums of the Grand Prix.
     *
     * @param int $gp_id
     * @return mixed
     */
    private function getPodiums(int $gp_id)
    {
        return $this->results
            ->where('gp_id', $gp_id)
            ->where('session_id', 1)  //TODO:Needs to be improved
            ->where('team', 1)
            ->whereIn('place', [1, 2, 3])
            ->count('place');
    }

    /**
     * Get number of points of the Grand Prix.
     *
     * @param int $gp_id
     * @return mixed
     */
    private function getPoints(int $gp_id)
    {
        return $this->results
            ->where('gp_id', $gp_id)
            ->where('session_id', 1)
            ->where('team', 1)
            ->sum('points');
    }

    /**
     * Get number of pole positions of the Grand Prix.
     *
     * @param int $gp_id
     * @return mixed
     */
    private function getPoles(int $gp_id)
    {
        return $this->results
            ->where('gp_id', $gp_id)
            ->where('session_id', 2) //TODO:Needs to be improved
            ->where('team', 1)
            ->where('place', 1)
            ->count('place');
    }

    /**
     * Get number of wins of the Grand Prix.
     *
     * @param int $gp_id
     * @return mixed
     */
    public function getWins(int $gp_id)
    {
        return $this->results
            ->where('gp_id', $gp_id)
            ->where('session_id', 1) //TODO:Needs to be improved
            ->where('team', 1)
            ->where('place', 1)
            ->count('place');
    }
}
