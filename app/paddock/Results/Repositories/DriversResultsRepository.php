<?php

namespace App\paddock\Results\Repositories;

use App\paddock\Results\Models\Results;

class DriversResultsRepository
{
    /**
     * @var Results
     */
    private $results;

    /**
     * DriversResultsRepository constructor.
     * @param Results $results
     */
    public function __construct(Results $results)
    {
        $this->results = $results;
    }

    /**
     * Get all wins of the driver.
     *
     * @param int $id
     * @return mixed
     */
    public function getWinsByID(int $id)
    {
        return $this->results
            ->select('grand_prixs.full_name as grandprix', 'tracks.name as track', 'results.dateofresult')
            ->join('grand_prixs', 'grand_prixs.id', '=', 'results.gp_id')
            ->join('tracks', 'tracks.id', '=', 'results.track_id')
            ->where('results.session_id', 1) //TODO:Needs to be improved
            ->where('results.driver_id', $id)
            ->where('results.place', 1)
            ->get();
    }
}
