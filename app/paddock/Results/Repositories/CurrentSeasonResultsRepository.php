<?php

namespace App\paddock\Results\Repositories;

use App\paddock\Results\Models\Results;

class CurrentSeasonResultsRepository
{
    /**
     * @var Results
     */
    private $results;

    /**
     * CurrentSeasonResultsRepository constructor.
     * @param Results $results
     */
    public function __construct(Results $results)
    {
        $this->results = $results;
    }

    /**
     * Get all results of the current season.
     *
     * @param int $season
     * @return array
     */
    public function getResults(int $season)
    {
        return [
            'laps' => $this->getLaps($season),
            'podiums' => $this->getPodiums($season),
            'points' => $this->getPoints($season),
            'poles' => $this->getPoles($season),
            'season' => $season,
            'wins' => $this->getWins($season),
        ];
    }

    /**
     * Get number of laps of the current season.
     *
     * @param int $season
     * @return mixed
     */
    private function getLaps(int $season)
    {
        return $this->results
            ->where('season', $season)
            ->where('session_id', 1)  //TODO:Needs to be improved
            ->where('team', 1)
            ->sum('laps');
    }

    /**
     * Get number of podiums of the current season.
     *
     * @param int $season
     * @return mixed
     */
    private function getPodiums(int $season)
    {
        return $this->results
            ->where('season', $season)
            ->where('session_id', 1)  //TODO:Needs to be improved
            ->where('team', 1)
            ->whereIn('place', [1, 2, 3])
            ->count('place');
    }

    /**
     * Get number of points of the current season.
     *
     * @param int $season
     * @return mixed
     */
    private function getPoints(int $season)
    {
        return $this->results
            ->where('season', $season)
            ->where('session_id', 1)
            ->where('team', 1)
            ->sum('points');
    }

    /**
     * Get number of pole positions of the current season.
     *
     * @param int $season
     * @return mixed
     */
    private function getPoles(int $season)
    {
        return $this->results
            ->where('season', $season)
            ->where('session_id', 2) //TODO:Needs to be improved
            ->where('team', 1)
            ->where('place', 1)
            ->count('place');
    }

    /**
     * Get number of wins of the current season.
     *
     * @param int $season
     * @return mixed
     */
    private function getWins(int $season)
    {
        return $this->results
            ->where('season', $season)
            ->where('session_id', 1) //TODO:Needs to be improved
            ->where('team', 1)
            ->where('place', 1)
            ->count('place');
    }
}
