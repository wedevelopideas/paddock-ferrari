<?php

namespace App\paddock\Results\Repositories;

use App\paddock\Results\Models\Results;

class ChampionshipResultsRepository
{
    /**
     * @var Results
     */
    private $results;

    /**
     * ChampionshipResultsRepository constructor.
     * @param  Results  $results
     */
    public function __construct(Results $results)
    {
        $this->results = $results;
    }

    /**
     * Get current standing by the driver.
     *
     * @param  int  $season
     * @param  int  $driver_id
     * @return mixed
     */
    public function getStandingByDriver(int $season, int $driver_id)
    {
        return $this->results
            ->where('season', $season)
            ->where('session_id', 1)
            ->where('driver_id', $driver_id)
            ->sum('points');
    }

    /**
     * Get current standing by the team.
     *
     * @param  int  $season
     * @return mixed
     */
    public function getStandingByTeam(int $season)
    {
        return $this->results
            ->where('season', $season)
            ->where('session_id', 1)
            ->where('team', 1)
            ->sum('points');
    }
}
