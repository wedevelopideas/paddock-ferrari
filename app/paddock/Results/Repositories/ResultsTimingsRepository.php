<?php

namespace App\paddock\Results\Repositories;

use App\paddock\Results\Models\ResultsTimings;

class ResultsTimingsRepository
{
    /**
     * @var ResultsTimings
     */
    private $resultsTimings;

    /**
     * ResultsTimingsRepository constructor.
     * @param ResultsTimings $resultsTimings
     */
    public function __construct(ResultsTimings $resultsTimings)
    {
        $this->resultsTimings = $resultsTimings;
    }

    /**
     * Get times of the driver for the whole weekend.
     *
     * @param int $season
     * @param int $gp
     * @param int $driver
     * @return mixed
     */
    public function getTimesForDriver(int $season, int $gp, int $driver)
    {
        return $this->resultsTimings
            ->where('season', $season)
            ->where('gp_id', $gp)
            ->where('driver_id', $driver)
            ->where('laptime', '!=', 0)
            ->orderBy('laptime', 'ASC')
            ->get();
    }

    /**
     * Get laptime of the grand prix weekend by the driver.
     *
     * @param int $season
     * @param int $gp
     * @param int $session
     * @param int $driver
     * @return mixed
     */
    public function getTimeBySeasonGPSessionAndDriver(int $season, int $gp, int $session, int $driver)
    {
        return $this->resultsTimings
            ->where('season', $season)
            ->where('gp_id', $gp)
            ->where('session', $session)
            ->where('driver_id', $driver)
            ->first();
    }

    /**
     * Get laptime of the grand prix weekend by the driver.
     *
     * @param int $season
     * @param int $gp
     * @param int $driver
     * @return mixed
     */
    public function getTimesBySeasonGPAndDriver(int $season, int $gp, int $driver)
    {
        return $this->resultsTimings
            ->where('season', $season)
            ->where('gp_id', $gp)
            ->where('driver_id', $driver)
            ->where('laptime', '!=', '0')
            ->orderBy('laptime', 'ASC')
            ->get();
    }
}
