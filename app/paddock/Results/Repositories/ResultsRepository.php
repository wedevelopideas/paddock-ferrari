<?php

namespace App\paddock\Results\Repositories;

use App\paddock\Results\Models\Results;

class ResultsRepository
{
    /**
     * @var Results
     */
    private $results;

    /**
     * ResultsRepository constructor.
     * @param Results $results
     */
    public function __construct(Results $results)
    {
        $this->results = $results;
    }

    /**
     * Get all results by Grand Prix.
     *
     * @return mixed
     */
    public function getAllResultsByGrandPrix()
    {
        return $this->results
            ->join('grand_prixs', 'grand_prixs.id', '=', 'results.gp_id')
            ->groupBy('results.gp_id')
            ->orderBy('grand_prixs.full_name', 'ASC')
            ->get();
    }

    /**
     * Store results.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $result = new Results();
        $result->season = $data['season'];
        $result->gp_id = $data['gp_id'];
        $result->track_id = $data['track_id'];
        $result->session_id = $data['session_id'];
        $result->dateofresult = $data['dateofresult'];
        $result->car_id = $data['car_id'];
        $result->car_nr = $data['car_nr'];
        $result->driver_id = $data['driver_id'];
        $result->team = $data['team'];
        $result->place = $data['place'];
        $result->laps = $data['laps'];
        $result->points = $data['points'];
        $result->dnf = $data['dnf'];
        $result->dns = $data['dns'];
        $result->dsq = $data['dsq'];
        $result->save();
    }
}
