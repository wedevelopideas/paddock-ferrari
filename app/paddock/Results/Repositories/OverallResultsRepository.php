<?php

namespace App\paddock\Results\Repositories;

use App\paddock\Results\Models\Results;

class OverallResultsRepository
{
    /**
     * @var Results
     */
    private $results;

    /**
     * CurrentSeasonResultsRepository constructor.
     * @param Results $results
     */
    public function __construct(Results $results)
    {
        $this->results = $results;
    }

    /**
     * Get all results of the current season.
     *
     * @return array
     */
    public function getResults()
    {
        return [
            'laps' => $this->getLaps(),
            'podiums' => $this->getPodiums(),
            'points' => $this->getPoints(),
            'poles' => $this->getPoles(),
            'wins' => $this->getWins(),
        ];
    }

    /**
     * Get number of laps of the current season.
     *
     * @return mixed
     */
    private function getLaps()
    {
        return $this->results
            ->where('session_id', 1)  //TODO:Needs to be improved
            ->where('team', 1)
            ->sum('laps');
    }

    /**
     * Get number of podiums of the current season.
     *
     * @return mixed
     */
    private function getPodiums()
    {
        return $this->results
            ->where('session_id', 1)  //TODO:Needs to be improved
            ->where('team', 1)
            ->whereIn('place', [1, 2, 3])
            ->count('place');
    }

    /**
     * Get number of points of the current season.
     *
     * @return mixed
     */
    private function getPoints()
    {
        return $this->results
            ->where('session_id', 1)
            ->where('team', 1)
            ->sum('points');
    }

    /**
     * Get number of pole positions of the current season.
     *
     * @return mixed
     */
    private function getPoles()
    {
        return $this->results
            ->where('session_id', 2) //TODO:Needs to be improved
            ->where('team', 1)
            ->where('place', 1)
            ->count('place');
    }

    /**
     * Get number of wins of the current season.
     *
     * @return mixed
     */
    private function getWins()
    {
        return $this->results
            ->where('session_id', 1) //TODO:Needs to be improved
            ->where('team', 1)
            ->where('place', 1)
            ->count('place');
    }
}
