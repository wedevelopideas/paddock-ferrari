<?php

namespace App\paddock\Results;

class Gaps
{
    /**
     * @param int $time
     * @param int $timeLastYear
     * @return string
     */
    public static function gapintime(int $time, int $timeLastYear)
    {
        if ($time == 0 || $timeLastYear == 0) {
            $abstand = '';
        } else {
            $gap = $time - $timeLastYear;

            $gap = (string) $gap;

            preg_match('/([-+]{0,1})([0-9]{1,})/', $gap, $matches);

            $sek = ((int) ($matches[2] / 1000)) % 60;
            $ms = $matches[2] % 1000;

            if (empty($matches[1])) {
                $sign = '+';
            } else {
                $sign = $matches[1];
            }

            $abstand = $sign.sprintf('%01d.%03d', $sek, $ms);
        }

        return $abstand;
    }

    /**
     * @param int $time
     * @param int $timeLastYear
     * @return string
     */
    public static function team(int $time, int $timeLastYear)
    {
        if ($time == 0 || $timeLastYear == 0) {
            $abstand = '';
        } else {
            $gap = $time - $timeLastYear;

            $gap = (string) $gap;

            preg_match('/([-+]{0,1})([0-9]{1,})/', $gap, $matches);

            $sek = ((int) ($matches[2] / 1000)) % 60;
            $ms = $matches[2] % 1000;

            if (! empty($matches[1])) {
                $sign = '+';
            } else {
                $sign = $matches[1];
            }

            $abstand = $sign.sprintf('%01d.%03d', $sek, $ms);
        }

        return $abstand;
    }
}
