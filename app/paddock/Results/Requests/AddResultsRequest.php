<?php

namespace App\paddock\Results\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddResultsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'season' => 'required|integer',
            'gp_id' => 'required|integer',
            'track_id' => 'required|integer',
            'session_id' => 'required|integer',
            'dateofresult' => 'required|date',
            'car_id' => 'required|integer',
            'car_nr' => 'required|integer',
            'driver_id' => 'required|integer',
            'team' => 'required|boolean',
            'place' => 'required|integer',
            'laps' => 'required|integer',
            'points' => 'required|integer',
            'dnf' => 'required|boolean',
            'dns' => 'required|boolean',
            'dsq' => 'required|boolean',
        ];
    }
}
