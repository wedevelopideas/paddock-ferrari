<?php

namespace App\paddock\Results\Models;

use App\paddock\Times;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ResultsTimings.
 *
 * @property int $id
 * @property int $season
 * @property int $gp_id
 * @property int $session
 * @property int $driver_id
 * @property int $laptime
 */
class ResultsTimings extends Model
{
    use Times;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'season',
        'gp_id',
        'session',
        'driver_id',
        'laptime',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
