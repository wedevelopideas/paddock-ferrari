<?php

namespace App\paddock\Results\Models;

use App\paddock\Drivers\Models\Drivers;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Results.
 *
 * @property int $id
 * @property int $season
 * @property int $gp_id
 * @property int $track_id
 * @property int $session_id
 * @property string $dateofresult
 * @property int $car_id
 * @property int $car_nr
 * @property int $driver_id
 * @property int $team
 * @property int $place
 * @property int $laps
 * @property float $points
 * @property int $dnf
 * @property int $dns
 * @property int $dsq
 * @property-read \App\paddock\Drivers\Models\Drivers $driver
 */
class Results extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'season',
        'gp_id',
        'track_id',
        'session_id',
        'dateofresult',
        'car_id',
        'car_nr',
        'driver_id',
        'team',
        'place',
        'laps',
        'points',
        'dnf',
        'dns',
        'dsq',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Drivers relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function driver()
    {
        return $this->belongsTo(Drivers::class, 'driver_id');
    }

    /**
     * Shows the DNF status.
     *
     * @return string
     */
    public function dnfStatus()
    {
        if ($this->dnf) {
            return '<i class="red times circle icon">';
        }

        return '<i class="green check circle icon">';
    }

    /**
     * Shows the DNS status.
     *
     * @return string
     */
    public function dnsStatus()
    {
        if ($this->dns) {
            return '<i class="red times circle icon">';
        }

        return '<i class="green check circle icon">';
    }
}
