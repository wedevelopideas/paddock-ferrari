<?php

namespace App\paddock\Reports\Repositories;

use App\paddock\Reports\Models\ReportsTires2019;

class ReportsTires2019Repository
{
    /**
     * @var ReportsTires2019
     */
    private $reportsTires2019;

    /**
     * ReportsTiresRepository constructor.
     * @param ReportsTires2019 $reportsTires2019
     */
    public function __construct(ReportsTires2019 $reportsTires2019)
    {
        $this->reportsTires2019 = $reportsTires2019;
    }

    /**
     * Get race by season and grand prix.
     *
     * @param int $season
     * @param string $slug
     * @return mixed
     */
    public function getRaceBySeasonAndGP(int $season, string $slug)
    {
        return $this->reportsTires2019
            ->join('grand_prixs', 'grand_prixs.id', '=', 'reports_tires_2019.gp_id')
            ->where('reports_tires_2019.season', $season)
            ->where('grand_prixs.slug', $slug)
            ->first();
    }

    /**
     * Get 2019 tyre reports for the driver by season and grand prix.
     *
     * @param int $season
     * @param int $gp_id
     * @param string $slug
     * @return mixed
     */
    public function getTyreReportsForDriver(int $season, int $gp_id, string $slug)
    {
        return $this->reportsTires2019
            ->join('drivers', 'drivers.id', '=', 'reports_tires_2019.driver_id')
            ->where('reports_tires_2019.season', $season)
            ->where('reports_tires_2019.gp_id', $gp_id)
            ->where('drivers.slug', $slug)
            ->first();
    }

    /**
     * Get 2019 tyre reports for the drivers by season and grand prix.
     *
     * @param int $season
     * @param int $gp_id
     * @return mixed
     */
    public function getTyreReportsForDrivers(int $season, int $gp_id)
    {
        return $this->reportsTires2019
            ->join('drivers', 'drivers.id', '=', 'reports_tires_2019.driver_id')
            ->where('reports_tires_2019.season', $season)
            ->where('reports_tires_2019.gp_id', $gp_id)
            ->get();
    }

    /**
     * Store 2019 tyre reports.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $report = new ReportsTires2019();
        $report->season = $data['season'];
        $report->race = $data['race'];
        $report->gp_id = $data['gp_id'];
        $report->driver_id = $data['driver_id'];
        $report->c1 = $data['c1'];
        $report->c2 = $data['c2'];
        $report->c3 = $data['c3'];
        $report->c4 = $data['c4'];
        $report->c5 = $data['c5'];
        $report->save();
    }
}
