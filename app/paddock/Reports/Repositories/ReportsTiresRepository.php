<?php

namespace App\paddock\Reports\Repositories;

use App\paddock\Reports\Models\ReportsTires;

class ReportsTiresRepository
{
    /**
     * @var ReportsTires
     */
    private $reportsTires;

    /**
     * ReportsTiresRepository constructor.
     * @param ReportsTires $reportsTires
     */
    public function __construct(ReportsTires $reportsTires)
    {
        $this->reportsTires = $reportsTires;
    }

    /**
     * Get race by season and grand prix.
     *
     * @param int $season
     * @param string $slug
     * @return mixed
     */
    public function getRaceBySeasonAndGP(int $season, string $slug)
    {
        return $this->reportsTires
            ->join('grand_prixs', 'grand_prixs.id', '=', 'reports_tires.gp_id')
            ->where('reports_tires.season', $season)
            ->where('grand_prixs.slug', $slug)
            ->first();
    }

    /**
     * Get tyre reports for the drivers by season and grand prix.
     *
     * @param int $season
     * @param int $gp_id
     * @return mixed
     */
    public function getTyreReportsForDrivers(int $season, int $gp_id)
    {
        return $this->reportsTires
            ->join('drivers', 'drivers.id', '=', 'reports_tires.driver_id')
            ->where('reports_tires.season', $season)
            ->where('reports_tires.gp_id', $gp_id)
            ->get();
    }

    /**
     * Store tyre reports.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $report = new ReportsTires();
        $report->season = $data['season'];
        $report->race = $data['race'];
        $report->gp_id = $data['gp_id'];
        $report->driver_id = $data['driver_id'];
        $report->hypersoft = $data['hypersoft'];
        $report->ultrasoft = $data['ultrasoft'];
        $report->supersoft = $data['supersoft'];
        $report->soft = $data['soft'];
        $report->medium = $data['medium'];
        $report->hard = $data['hard'];
        $report->superhard = $data['superhard'];
        $report->save();
    }
}
