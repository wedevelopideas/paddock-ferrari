<?php

namespace App\paddock\Reports\Repositories;

use App\paddock\Reports\Models\ReportsNominatedTires;

class ReportsNominatedTiresRepository
{
    /**
     * @var ReportsNominatedTires
     */
    private $reportsNominatedTires;

    /**
     * ReportsNominatedTiresRepository constructor.
     * @param ReportsNominatedTires $reportsNominatedTires
     */
    public function __construct(ReportsNominatedTires $reportsNominatedTires)
    {
        $this->reportsNominatedTires = $reportsNominatedTires;
    }

    /**
     * Get nominated tires for season and grand prix.
     *
     * @param int $season
     * @param int $gp_id
     * @return mixed
     */
    public function getNominatedTires(int $season, int $gp_id)
    {
        return $this->reportsNominatedTires
            ->where('season', $season)
            ->where('gp_id', $gp_id)
            ->first();
    }
}
