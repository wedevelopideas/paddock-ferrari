<?php

namespace App\paddock\Reports\Repositories;

use App\paddock\Reports\Models\ReportsTechnicals;

class ReportsTechnicalsRepository
{
    /**
     * @var ReportsTechnicals
     */
    private $reportsTechnicals;

    /**
     * ReportsTechnicalsRepository constructor.
     * @param ReportsTechnicals $reportsTechnicals
     */
    public function __construct(ReportsTechnicals $reportsTechnicals)
    {
        $this->reportsTechnicals = $reportsTechnicals;
    }

    /**
     * Get technical reports for the driver by season and grand prix.
     *
     * @param int $season
     * @param int $gp_id
     * @param string $slug
     * @return mixed
     */
    public function getTechnicalReportsForDriver(int $season, int $gp_id, string $slug)
    {
        return $this->reportsTechnicals
            ->join('drivers', 'drivers.id', '=', 'reports_technicals.driver_id')
            ->where('reports_technicals.season', $season)
            ->where('reports_technicals.gp_id', $gp_id)
            ->where('drivers.slug', $slug)
            ->first();
    }

    /**
     * Get technical reports for the drivers by season and grand prix.
     *
     * @param int $season
     * @param int $gp_id
     * @return mixed
     */
    public function getTechnicalReportsForDrivers(int $season, int $gp_id)
    {
        return $this->reportsTechnicals
            ->join('drivers', 'drivers.id', '=', 'reports_technicals.driver_id')
            ->where('reports_technicals.season', $season)
            ->where('reports_technicals.gp_id', $gp_id)
            ->get();
    }

    /**
     * Store technical reports.
     *
     * @param array $data
     */
    public function store(array $data)
    {
        $report = new ReportsTechnicals();
        $report->season = $data['season'];
        $report->race = $data['race'];
        $report->gp_id = $data['gp_id'];
        $report->driver_id = $data['driver_id'];
        $report->ice = $data['ice'];
        $report->tc = $data['tc'];
        $report->mguh = $data['mguh'];
        $report->mguk = $data['mguk'];
        $report->es = $data['es'];
        $report->ce = $data['ce'];
        $report->save();
    }
}
