<?php

namespace App\paddock\Reports\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReportsNominatedTires.
 *
 * @property int $id
 * @property int $season
 * @property int $race
 * @property int $gp_id
 * @property string $compound1
 * @property string $compound2
 * @property string $compound3
 */
class ReportsNominatedTires extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'season',
        'race',
        'gp_id',
        'compound1',
        'compound2',
        'compound3',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
