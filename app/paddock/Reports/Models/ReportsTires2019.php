<?php

namespace App\paddock\Reports\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReportsTires2019.
 *
 * @property int $id
 * @property int $season
 * @property int $race
 * @property int $gp_id
 * @property int $driver_id
 * @property int $c1
 * @property int $c2
 * @property int $c3
 * @property int $c4
 * @property int $c5
 */
class ReportsTires2019 extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reports_tires_2019';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'season',
        'race',
        'gp_id',
        'driver_id',
        'c1',
        'c2',
        'c3',
        'c4',
        'c5',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Shows the emoji for the compounds.
     *
     * @param object $compound
     * @param string $tyre
     * @return string
     */
    public function selectedCompound($compound, string $tyre)
    {
        if ($compound->compound1 === $tyre) {
            return '⚪';
        } elseif ($compound->compound2 === $tyre) {
            return '💛';
        } elseif ($compound->compound3 === $tyre) {
            return '❤️';
        }
    }
}
