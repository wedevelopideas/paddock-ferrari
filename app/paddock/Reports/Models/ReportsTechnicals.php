<?php

namespace App\paddock\Reports\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReportsTechnicals.
 *
 * @property int $id
 * @property int $season
 * @property int $race
 * @property int $gp_id
 * @property int $driver_id
 * @property int $ice
 * @property int $tc
 * @property int $mguh
 * @property int $mguk
 * @property int $es
 * @property int $ce
 */
class ReportsTechnicals extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'season',
        'race',
        'gp_id',
        'driver_id',
        'ice',
        'tc',
        'mguh',
        'mguk',
        'es',
        'ce',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
