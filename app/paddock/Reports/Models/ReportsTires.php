<?php

namespace App\paddock\Reports\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReportsTires.
 *
 * @property int $id
 * @property int $season
 * @property int $race
 * @property int $gp_id
 * @property int $driver_id
 * @property int $hypersoft
 * @property int $ultrasoft
 * @property int $supersoft
 * @property int $soft
 * @property int $medium
 * @property int $hard
 * @property int $superhard
 */
class ReportsTires extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'season',
        'race',
        'gp_id',
        'driver_id',
        'hypersoft',
        'ultrasoft',
        'supersoft',
        'soft',
        'medium',
        'hard',
        'superhard',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
