<?php

namespace App\paddock\Settings\Repositories;

use App\paddock\Settings\Models\Settings;

class SettingsRepository
{
    /**
     * @var Settings
     */
    private $settings;

    /**
     * SettingsRepository constructor.
     * @param Settings $settings
     */
    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }

    /**
     * Get all settings.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->settings
            ->get();
    }

    /**
     * Get a settings value.
     *
     * @param string $key
     * @return string
     */
    public function getSettings(string $key)
    {
        $query = $this->settings
            ->where('key', $key)
            ->first();

        if ($query) {
            return $query->value;
        }

        return '';
    }

    /**
     * Set a value for settings.
     *
     * @param string $key
     * @param string $value
     */
    public function setSettings(string $key, string $value)
    {
        $setting = Settings::where('key', $key)->first();

        if ($setting) {
            $setting->value = $value;
            $setting->save();
        }
    }
}
