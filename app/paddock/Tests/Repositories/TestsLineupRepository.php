<?php

namespace App\paddock\Tests\Repository;

use App\paddock\Tests\Models\TestsLineup;

class TestsLineupRepository
{
    /**
     * @var TestsLineup
     */
    private $testsLineup;

    /**
     * TestsLineupRepository constructor.
     * @param TestsLineup $testsLineup
     */
    public function __construct(TestsLineup $testsLineup)
    {
        $this->testsLineup = $testsLineup;
    }
}
