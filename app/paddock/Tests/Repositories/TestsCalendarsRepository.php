<?php

namespace App\paddock\Tests\Repository;

use App\paddock\Tests\Models\TestsCalendars;

class TestsCalendarsRepository
{
    /**
     * @var TestsCalendars
     */
    private $testsCalendars;

    /**
     * TestsCalendarsRepository constructor.
     * @param TestsCalendars $testsCalendars
     */
    public function __construct(TestsCalendars $testsCalendars)
    {
        $this->testsCalendars = $testsCalendars;
    }

    /**
     * Get all tests.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->testsCalendars
            ->select('season', 'week')
            ->selectRaw('MIN(testday) as start')
            ->selectRaw('MAX(testday) as end')
            ->orderBy('testday', 'DESC')
            ->groupBy('season')
            ->groupBy('week')
            ->get();
    }

    /**
     * Get test week calendar by season and week.
     *
     * @param int $season
     * @param int $week
     * @return mixed
     */
    public function getTestWeekBySeasonAndWeek(int $season, int $week)
    {
        return $this->testsCalendars
            ->select('tests_calendars.testday', 'drivers.name')
            ->leftJoin('tests_lineup', function ($join) use ($week) {
                $join->on('tests_lineup.testday', '=', 'tests_calendars.testday');
                $join->where('tests_calendars.week', $week);
            })
            ->leftJoin('drivers', 'tests_lineup.driver_id', '=', 'drivers.id')
            ->where('tests_calendars.season', $season)
            ->where('tests_calendars.week', $week)
            ->get();
    }

    /**
     * Get track for test week by season and week.
     *
     * @param int $season
     * @param int $week
     * @return mixed
     */
    public function getTestWeeksTrack(int $season, int $week)
    {
        return $this->testsCalendars
            ->join('tracks', 'tracks.id', '=', 'tests_calendars.track_id')
            ->where('tests_calendars.season', $season)
            ->where('tests_calendars.week', $week)
            ->first();
    }
}
