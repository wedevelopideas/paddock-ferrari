<?php

namespace App\paddock\Tests\Repositories;

use App\paddock\Tests\Models\TestsResults;

class TestsResultsRepository
{
    /**
     * @var TestsResults
     */
    private $testsResults;

    /**
     * TestsResultsRepository constructor.
     * @param TestsResults $testsResults
     */
    public function __construct(TestsResults $testsResults)
    {
        $this->testsResults = $testsResults;
    }

    /**
     * Get fastest test result by season and week.
     *
     * @param int $season
     * @param int $week
     * @return mixed
     */
    public function getFastestTestResultBySeasonAndWeek(int $season, int $week)
    {
        return $this->testsResults
            ->where('season', $season)
            ->where('week', $week)
            ->where('laptime', '!=', 0)
            ->min('laptime');
    }

    /**
     * Get test results by season and week and sorted by fastest.
     *
     * @param int $season
     * @param int $week
     * @return mixed
     */
    public function getFastestTestResultsBySeasonAndWeek(int $season, int $week)
    {
        return $this->testsResults
            ->select('tests_results.testday', 'drivers.name as drivername', 'tests_results.laptime', 'tests_results.laps', 'tracks.length as tracklength', 'tracks.laps as racelaps')
            ->join('tracks', 'tracks.id', '=', 'tests_results.track_id')
            ->join('drivers', 'drivers.id', '=', 'tests_results.driver_id')
            ->where('tests_results.season', $season)
            ->where('tests_results.week', $week)
            ->where('tests_results.laptime', '!=', 0)
            ->orderBy('tests_results.laptime', 'ASC')
            ->get();
    }

    /**
     * Get test results by season and week.
     *
     * @param int $season
     * @param int $week
     * @return mixed
     */
    public function getTestResultsBySeasonAndWeek(int $season, int $week)
    {
        return $this->testsResults
            ->select('tests_results.testday', 'drivers.name as drivername', 'tests_results.laptime', 'tests_results.laps', 'tracks.length as tracklength', 'tracks.laps as racelaps')
            ->join('tracks', 'tracks.id', '=', 'tests_results.track_id')
            ->join('drivers', 'drivers.id', '=', 'tests_results.driver_id')
            ->where('tests_results.season', $season)
            ->where('tests_results.week', $week)
            ->get();
    }
}
