<?php

namespace App\paddock\Tests\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TestsCalendars.
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $testday
 * @property int $season
 * @property int $week
 * @property int $track_id
 */
class TestsCalendars extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'testday',
        'season',
        'week',
        'track_id',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'testday',
        'start',
        'end',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Returns the driver for the test plan.
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|mixed|null|string
     */
    public function driver()
    {
        if ($this->name !== null) {
            return $this->name;
        }

        return trans('common.unknown');
    }
}
