<?php

namespace App\paddock\Tests\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TestsLineup.
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $testday
 * @property int $season
 * @property int $week
 * @property int $driver_id
 * @property int $status
 */
class TestsLineup extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tests_lineup';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'testday',
        'season',
        'week',
        'driver_id',
        'status',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'testday',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
