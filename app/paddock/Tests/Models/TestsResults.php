<?php

namespace App\paddock\Tests\Models;

use App\paddock\Times;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TestsResults.
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $testday
 * @property int $season
 * @property int $week
 * @property int $track_id
 * @property int $driver_id
 * @property int $laptime
 * @property int $laps
 */
class TestsResults extends Model
{
    use Times;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'testday',
        'season',
        'week',
        'track_id',
        'driver_id',
        'laptime',
        'laps',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'testday',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
