<?php

namespace App\Http\Controllers\SQL;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Shows the sql dashboard.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('sql.index');
    }
}
