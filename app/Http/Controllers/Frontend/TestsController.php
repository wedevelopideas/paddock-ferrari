<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Tests\Repositories\TestsResultsRepository;
use App\paddock\Tests\Repository\TestsCalendarsRepository;

class TestsController extends Controller
{
    /**
     * @var TestsCalendarsRepository
     */
    private $testsCalendarsRepository;

    /**
     * @var TestsResultsRepository
     */
    private $testsResultsRepository;

    /**
     * TestsController constructor.
     * @param TestsCalendarsRepository $testsCalendarsRepository
     * @param TestsResultsRepository $testsResultsRepository
     */
    public function __construct(
        TestsCalendarsRepository $testsCalendarsRepository,
        TestsResultsRepository $testsResultsRepository
    ) {
        $this->testsCalendarsRepository = $testsCalendarsRepository;
        $this->testsResultsRepository = $testsResultsRepository;
    }

    /**
     * Shows all tests.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $tests = $this->testsCalendarsRepository->getAll();

        return view('tests.index')
            ->with('tests', $tests);
    }

    /**
     * Shows test results by season and test week.
     *
     * @param int $season
     * @param int $week
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function results(int $season, int $week)
    {
        $results = $this->testsResultsRepository->getTestResultsBySeasonAndWeek($season, $week);

        $fastest = $this->testsResultsRepository->getFastestTestResultBySeasonAndWeek($season, $week);
        $fastests = $this->testsResultsRepository->getFastestTestResultsBySeasonAndWeek($season, $week);

        return view('tests.results')
            ->with('fastest', $fastest)
            ->with('fastests', $fastests)
            ->with('results', $results);
    }
}
