<?php

namespace App\Http\Controllers\Frontend;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\paddock\Feed\Repositories\FeedHistoryRepository;

class FeedHistoryController extends Controller
{
    /**
     * @var FeedHistoryRepository
     */
    private $feedHistoryRepository;

    /**
     * FeedHistoryController constructor.
     * @param FeedHistoryRepository $feedHistoryRepository
     */
    public function __construct(FeedHistoryRepository $feedHistoryRepository)
    {
        $this->feedHistoryRepository = $feedHistoryRepository;
    }

    /**
     * Shows the whole feed associated with the History of Ferrari.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $currentDate = Carbon::now()->format('m-d');
        $lang = auth()->user()->lang;

        $current = $this->feedHistoryRepository->getCurrentEntry($lang, $currentDate);
        $feeds = $this->feedHistoryRepository->getAll($lang);

        return view('feed.history.index')
            ->with('current', $current)
            ->with('feeds', $feeds);
    }

    /**
     * Get feed entry by its date.
     *
     * @param string $date
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(string $date)
    {
        $lang = auth()->user()->lang;

        $feed = $this->feedHistoryRepository->getFeedByDateAndLang($date, $lang);
        $feeds = $this->feedHistoryRepository->getFeedsByDate($date);

        return view('feed.history.view')
            ->with('feed', $feed)
            ->with('feeds', $feeds);
    }
}
