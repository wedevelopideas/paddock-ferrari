<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Drivers\Repositories\DriversRepository;
use App\paddock\Results\Repositories\DriversResultsRepository;

class DriversStatsController extends Controller
{
    /**
     * @var DriversRepository
     */
    private $driversRepository;

    /**
     * @var DriversResultsRepository
     */
    private $driversResultsRepository;

    /**
     * DriversStatsController constructor.
     * @param DriversRepository $driversRepository
     * @param DriversResultsRepository $driversResultsRepository
     */
    public function __construct(
        DriversRepository $driversRepository,
        DriversResultsRepository $driversResultsRepository
    ) {
        $this->driversRepository = $driversRepository;
        $this->driversResultsRepository = $driversResultsRepository;
    }

    /**
     * Shows all wins of the driver.
     *
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function wins(string $slug)
    {
        $driver = $this->driversRepository->getDriverBySlug($slug);

        $wins = $this->driversResultsRepository->getWinsByID($driver->id);

        return view('drivers.stats.wins')
            ->with('driver', $driver)
            ->with('wins', $wins);
    }
}
