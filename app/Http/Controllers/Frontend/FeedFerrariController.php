<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Feed\Repositories\FeedFerrariRepository;

class FeedFerrariController extends Controller
{
    /**
     * @var FeedFerrariRepository
     */
    private $feedFerrariRepository;

    /**
     * FeedFerrariController constructor.
     * @param FeedFerrariRepository $feedFerrariRepository
     */
    public function __construct(FeedFerrariRepository $feedFerrariRepository)
    {
        $this->feedFerrariRepository = $feedFerrariRepository;
    }

    /**
     * Shows the whole feed associated to Ferrari.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $lang = auth()->user()->lang;

        $feeds = $this->feedFerrariRepository->getAll($lang);

        return view('feed.ferrari.index')
            ->with('feeds', $feeds);
    }

    /**
     * Get feed entry by its date and session.
     *
     * @param string $date
     * @param int $session
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(string $date, int $session)
    {
        $lang = auth()->user()->lang;

        $feed = $this->feedFerrariRepository->getFeedByDateAndSessionAndLang($date, $session, $lang);

        $feeds = $this->feedFerrariRepository->getEntryByDateAndSession($date, $session);

        return view('feed.ferrari.view')
            ->with('feed', $feed)
            ->with('feeds', $feeds);
    }
}
