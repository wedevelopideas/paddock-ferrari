<?php

namespace App\Http\Controllers\Frontend;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\paddock\Feed\Requests\FeedMetaRequest;
use App\paddock\Feed\Repositories\FeedFDARepository;
use App\paddock\Feed\Repositories\FeedFerrariRepository;
use App\paddock\Feed\Repositories\FeedHistoryRepository;
use App\paddock\Feed\Repositories\FeedMotorsportRepository;

class FeedController extends Controller
{
    /**
     * @var FeedFDARepository
     */
    private $feedFDARepository;

    /**
     * @var FeedFerrariRepository
     */
    private $feedFerrariRepository;

    /**
     * @var FeedHistoryRepository
     */
    private $feedHistoryRepository;

    /**
     * @var FeedMotorsportRepository
     */
    private $feedMotorsportRepository;

    /**
     * FeedController constructor.
     * @param FeedFDARepository $feedFDARepository
     * @param FeedFerrariRepository $feedFerrariRepository
     * @param FeedHistoryRepository $feedHistoryRepository
     * @param FeedMotorsportRepository $feedMotorsportRepository
     */
    public function __construct(
        FeedFDARepository $feedFDARepository,
        FeedFerrariRepository $feedFerrariRepository,
        FeedHistoryRepository $feedHistoryRepository,
        FeedMotorsportRepository $feedMotorsportRepository
    ) {
        $this->feedFDARepository = $feedFDARepository;
        $this->feedFerrariRepository = $feedFerrariRepository;
        $this->feedHistoryRepository = $feedHistoryRepository;
        $this->feedMotorsportRepository = $feedMotorsportRepository;
    }

    /**
     * Shows all feeds.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $lang = auth()->user()->lang;
        $date = Carbon::now()->format('m-d');

        $fda = $this->feedFDARepository->getLatestArticle($lang);
        $ferrari = $this->feedFerrariRepository->getLatestArticle($lang);
        $history = $this->feedHistoryRepository->getLatestArticle($lang, $date);
        $motorsport = $this->feedMotorsportRepository->getLatestArticle($lang);

        return view('feed.index')
            ->with('fda', $fda)
            ->with('ferrari', $ferrari)
            ->with('history', $history)
            ->with('motorsport', $motorsport);
    }

    /**
     * Shows the form to read out social media tags.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function meta()
    {
        return view('feed.meta');
    }

    /**
     * Handles social media tag results.
     *
     * @param FeedMetaRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function metaResults(FeedMetaRequest $request)
    {
        stream_context_set_default([
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
            ],
        ]);

        $data = get_meta_tags($request->input('link'));

        $results = [
            'title' => html_entity_decode($data['twitter:title'], ENT_QUOTES, 'UTF-8'),
            'description' => $data['twitter:description'] ? html_entity_decode($data['twitter:description'], ENT_QUOTES, 'UTF-8') : trans('feed.no_description'),
            'image' => $data['twitter:image'],
            'link' => $request->input('link'),
        ];

        return redirect()
            ->route('feed.meta')
            ->with('results', $results);
    }
}
