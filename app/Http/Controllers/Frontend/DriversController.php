<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Drivers\Repositories\DriversRepository;

class DriversController extends Controller
{
    /**
     * @var DriversRepository
     */
    private $driversRepository;

    /**
     * DriversController constructor.
     * @param DriversRepository $driversRepository
     */
    public function __construct(DriversRepository $driversRepository)
    {
        $this->driversRepository = $driversRepository;
    }

    /**
     * Shows the list of all drivers.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $drivers = $this->driversRepository->getAll();

        return view('drivers.index')
            ->with('drivers', $drivers);
    }

    /**
     * Get all driver dates.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dates()
    {
        $birthdays = $this->driversRepository->getDriverBirthdays();
        $deaths = $this->driversRepository->getDriverDeaths();

        return view('drivers.dates')
            ->with('birthdays', $birthdays)
            ->with('deaths', $deaths);
    }

    /**
     * Get the driver by its slug.
     *
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(string $slug)
    {
        $driver = $this->driversRepository->getDriverBySlug($slug);

        return view('drivers.view')
            ->with('driver', $driver);
    }
}
