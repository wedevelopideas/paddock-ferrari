<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Feed\Repositories\FeedFDARepository;

class FeedFDAController extends Controller
{
    /**
     * @var FeedFDARepository
     */
    private $feedFDARepository;

    /**
     * FeedFDAController constructor.
     * @param FeedFDARepository $feedFDARepository
     */
    public function __construct(FeedFDARepository $feedFDARepository)
    {
        $this->feedFDARepository = $feedFDARepository;
    }

    /**
     * Shows the whole feed associated with Ferrari's FDA.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $lang = auth()->user()->lang;

        $feeds = $this->feedFDARepository->getAll($lang);

        return view('feed.fda.index')
            ->with('feeds', $feeds);
    }

    /**
     * Get feed entry by its date.
     *
     * @param string $date
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(string $date)
    {
        $lang = auth()->user()->lang;

        $feed = $this->feedFDARepository->getFeedByDateAndLang($date, $lang);

        $feeds = $this->feedFDARepository->getEntryByDate($date);

        return view('feed.fda.view')
            ->with('feed', $feed)
            ->with('feeds', $feeds);
    }
}
