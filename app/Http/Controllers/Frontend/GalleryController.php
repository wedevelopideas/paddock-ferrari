<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Media\Gallery\Repositories\GalleryAlbumsRepository;
use App\paddock\Media\Gallery\Repositories\GalleryImagesRepository;
use App\paddock\Media\Gallery\Repositories\GalleryTwitterRepository;

class GalleryController extends Controller
{
    /**
     * @var GalleryAlbumsRepository
     */
    private $galleryAlbumsRepository;

    /**
     * @var GalleryImagesRepository
     */
    private $galleryImagesRepository;

    /**
     * @var GalleryTwitterRepository
     */
    private $galleryTwitterRepository;

    /**
     * GalleryController constructor.
     * @param GalleryAlbumsRepository $galleryAlbumsRepository
     * @param GalleryImagesRepository $galleryImagesRepository
     * @param GalleryTwitterRepository $galleryTwitterRepository
     */
    public function __construct(
        GalleryAlbumsRepository $galleryAlbumsRepository,
        GalleryImagesRepository $galleryImagesRepository,
        GalleryTwitterRepository $galleryTwitterRepository
    ) {
        $this->galleryAlbumsRepository = $galleryAlbumsRepository;
        $this->galleryImagesRepository = $galleryImagesRepository;
        $this->galleryTwitterRepository = $galleryTwitterRepository;
    }

    /**
     * Shows all galleries.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $albums = $this->galleryAlbumsRepository->getAll();

        return view('media.gallery.index')
            ->with('albums', $albums);
    }

    /**
     * Shows the gallery albums by its slug.
     *
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function album(string $slug)
    {
        $album = $this->galleryAlbumsRepository->getAlbumBySlug($slug);

        $images = $this->galleryImagesRepository->getAllImagesByAlbumID($album->id);

        return view('media.gallery.album')
            ->with('album', $album)
            ->with('images', $images);
    }

    /**
     * Shows all profile images and banners associated with the Twitter accounts.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function twitter()
    {
        $accounts = $this->galleryTwitterRepository->getAll();

        return view('media.gallery.twitter')
            ->with('accounts', $accounts);
    }
}
