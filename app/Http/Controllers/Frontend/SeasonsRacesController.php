<?php

namespace App\Http\Controllers\Frontend;

use App\paddock\Results\Gaps;
use App\Http\Controllers\Controller;
use App\paddock\Reports\Repositories\ReportsTiresRepository;
use App\paddock\Seasons\Repositories\SeasonsRacesRepository;
use App\paddock\GrandPrixs\Repositories\GrandPrixsRepository;
use App\paddock\Results\Repositories\ResultsTimingsRepository;
use App\paddock\Seasons\Repositories\SeasonsDriversRepository;
use App\paddock\Reports\Repositories\ReportsTires2019Repository;
use App\paddock\Reports\Repositories\ReportsTechnicalsRepository;
use App\paddock\Reports\Repositories\ReportsNominatedTiresRepository;

class SeasonsRacesController extends Controller
{
    /**
     * @var GrandPrixsRepository
     */
    private $grandPrixsRepository;

    /**
     * @var ReportsNominatedTiresRepository
     */
    private $reportsNominatedTiresRepository;

    /**
     * @var ReportsTechnicalsRepository
     */
    private $reportsTechnicalsRepository;

    /**
     * @var ReportsTires2019Repository
     */
    private $reportsTires2019Repository;

    /**
     * @var ReportsTiresRepository
     */
    private $reportsTiresRepository;

    /**
     * @var ResultsTimingsRepository
     */
    private $resultsTimingsRepository;

    /**
     * @var SeasonsDriversRepository
     */
    private $seasonsDriversRepository;

    /**
     * @var SeasonsRacesRepository
     */
    private $seasonsRacesRepository;

    /**
     * SeasonsRacesController constructor.
     * @param GrandPrixsRepository $grandPrixsRepository
     * @param ReportsNominatedTiresRepository $reportsNominatedTiresRepository
     * @param ReportsTechnicalsRepository $reportsTechnicalsRepository
     * @param ReportsTires2019Repository $reportsTires2019Repository
     * @param ReportsTiresRepository $reportsTiresRepository
     * @param ResultsTimingsRepository $resultsTimingsRepository
     * @param SeasonsDriversRepository $seasonsDriversRepository
     * @param SeasonsRacesRepository $seasonsRacesRepository
     */
    public function __construct(
        GrandPrixsRepository $grandPrixsRepository,
        ReportsNominatedTiresRepository $reportsNominatedTiresRepository,
        ReportsTechnicalsRepository $reportsTechnicalsRepository,
        ReportsTires2019Repository $reportsTires2019Repository,
        ReportsTiresRepository $reportsTiresRepository,
        ResultsTimingsRepository $resultsTimingsRepository,
        SeasonsDriversRepository $seasonsDriversRepository,
        SeasonsRacesRepository $seasonsRacesRepository
    ) {
        $this->grandPrixsRepository = $grandPrixsRepository;
        $this->reportsNominatedTiresRepository = $reportsNominatedTiresRepository;
        $this->reportsTechnicalsRepository = $reportsTechnicalsRepository;
        $this->reportsTires2019Repository = $reportsTires2019Repository;
        $this->reportsTiresRepository = $reportsTiresRepository;
        $this->resultsTimingsRepository = $resultsTimingsRepository;
        $this->seasonsDriversRepository = $seasonsDriversRepository;
        $this->seasonsRacesRepository = $seasonsRacesRepository;
    }

    /**
     * Get the race by the season.
     *
     * @param int $season
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(int $season, string $slug)
    {
        $nominated = null;
        $technicals = null;
        $tires = null;

        $race = $this->seasonsRacesRepository->getRaceBySeasonAndGP($season, $slug);

        if (! is_null($race)) {
            $nominated = $this->reportsNominatedTiresRepository->getNominatedTires($race->season, $race->gp_id);

            $technicals = $this->reportsTechnicalsRepository->getTechnicalReportsForDrivers($race->season, $race->gp_id);

            if ($season === 2019) {
                $tires = $this->reportsTires2019Repository->getTyreReportsForDrivers($race->season, $race->gp_id);
            } else {
                $tires = $this->reportsTiresRepository->getTyreReportsForDrivers($race->season, $race->gp_id);
            }
        }

        return view('season.race')
            ->with('nominated', $nominated)
            ->with('race', $race)
            ->with('technicals', $technicals)
            ->with('tires', $tires);
    }

    /**
     * Get the live timing of the race by the season.
     *
     * @param int $season
     * @param string $slug
     * @param int $session
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function live(int $season, string $slug, int $session)
    {
        $current_season = $season;
        $last_season = $season - 1;
        $data = null;

        $race = $this->seasonsRacesRepository->getRaceBySeasonAndGP($current_season, $slug);

        if (! is_null($race)) {
            $grandprix = $this->grandPrixsRepository->getGrandPrixByID($race->gp_id);

            $driver = $this->seasonsDriversRepository->getDriversBySeason($current_season);

            $driver1_c = $this->resultsTimingsRepository->getTimeBySeasonGPSessionAndDriver(
                $current_season,
                (int) $race->gp_id,
                $session,
                $driver[0]->id
            );
            $driver2_c = $this->resultsTimingsRepository->getTimeBySeasonGPSessionAndDriver(
                $current_season,
                (int) $race->gp_id,
                $session,
                $driver[1]->id
            );

            $driver1_l = $this->resultsTimingsRepository->getTimeBySeasonGPSessionAndDriver(
                $last_season,
                (int) $race->gp_id,
                $session,
                $driver[0]->id
            );
            $driver2_l = $this->resultsTimingsRepository->getTimeBySeasonGPSessionAndDriver(
                $last_season,
                (int) $race->gp_id,
                $session,
                $driver[1]->id
            );

            $data = [
                'driver1' => [
                    'part' => $driver[0]['name'],
                    'string' => 'Time Comparision '.$driver[0]['hashtag'].' #'.$grandprix['hashtag'].' '.$grandprix['emoji'].' '.$this->gp_session($session),
                    'this_year' => (is_null($driver1_c)) ? '' : $driver1_c->secintime(),
                    'last_year' => (is_null($driver1_l)) ? '' : $driver1_l->secintime(),
                    'gap' => (is_null($driver1_l)) ? '' : Gaps::gapintime(
                        $driver1_c->laptime,
                        $driver1_l->laptime
                    ),
                ],
                'driver2' => [
                    'part' => $driver[1]['name'],
                    'string' => 'Time Comparision '.$driver[1]['hashtag'].' #'.$grandprix['hashtag'].' '.$grandprix['emoji'].' '.$this->gp_session($session),
                    'this_year' => (is_null($driver2_c)) ? '' : $driver2_c->secintime(),
                    'last_year' => (is_null($driver2_l)) ? '' : $driver2_l->secintime(),
                    'gap' => (is_null($driver2_l)) ? '' : Gaps::gapintime($driver2_c->laptime, $driver2_l->laptime),
                ],
                'drivers' => [
                    'part' => 'Team',
                    'string' => 'Time Comparision '.$driver[0]['hashtag'].' / '.$driver[1]['hashtag'].' #'.$grandprix['hashtag'].' '.$grandprix['emoji'].' '.$this->gp_session($session),
                    'time1' => (is_null($driver1_c)) ? '' : $driver1_c->secintime(),
                    'time2' => (is_null($driver2_c)) ? '' : $driver2_c->secintime(),
                    'gap' => Gaps::team((int) $driver1_c['laptime'], (int) $driver2_c['laptime']),
                ],
                'season' => [
                    'current' => $current_season,
                    'last' => $last_season,
                ],
            ];
        }

        return view('season.live')
            ->with('data', $data)
            ->with('race', $race);
    }

    /**
     * Get the tyre report of the race by the season.
     *
     * @param int $season
     * @param string $gpslug
     * @param string $driverslug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reportTires(int $season, string $gpslug, string $driverslug)
    {
        $nominated = null;
        $tyre = null;

        $race = $this->seasonsRacesRepository->getRaceBySeasonAndGP($season, $gpslug);

        if (! is_null($race)) {
            $nominated = $this->reportsNominatedTiresRepository->getNominatedTires($race->season, $race->gp_id);

            $tyre = $this->reportsTires2019Repository->getTyreReportsForDriver($race->season, $race->gp_id, $driverslug);
        }

        return view('season.report_tires')
            ->with('nominated', $nominated)
            ->with('race', $race)
            ->with('tyre', $tyre);
    }

    /**
     * Shows the session of the grand prix.
     *
     * @param int $session
     * @return string
     */
    private function gp_session(int $session)
    {
        $output = '';

        switch ($session) {
            case 1:
                $output = 'FP1';
                break;
            case 2:
                $output = 'FP2';
                break;
            case 3:
                $output = 'FP3';
                break;
            case 4:
                $output = 'Quali';
                break;
            case 5:
                $output = 'Race';
                break;
        }

        return $output;
    }
}
