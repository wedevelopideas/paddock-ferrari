<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Settings\Repositories\SettingsRepository;
use App\paddock\Seasons\Repositories\SeasonsCalendarRepository;

class CalendarController extends Controller
{
    /**
     * @var SeasonsCalendarRepository
     */
    private $seasonsCalendarRepository;

    /**
     * @var SettingsRepository
     */
    private $settingsRepository;

    /**
     * CalendarController constructor.
     * @param SeasonsCalendarRepository $seasonsCalendarRepository
     * @param SettingsRepository $settingsRepository
     */
    public function __construct(
        SettingsRepository $settingsRepository,
        SeasonsCalendarRepository $seasonsCalendarRepository
    ) {
        $this->seasonsCalendarRepository = $seasonsCalendarRepository;
        $this->settingsRepository = $settingsRepository;
    }

    /**
     * Shows all sessions of the current season.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sessions()
    {
        $season = $this->settingsRepository->getSettings('season');

        $sessions = $this->seasonsCalendarRepository->getCurrentSeasonCalendar((int) $season);

        return view('calendar')
            ->with('season', $season)
            ->with('sessions', $sessions);
    }
}
