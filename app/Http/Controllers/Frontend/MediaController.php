<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Media\Gallery\Repositories\GalleryAlbumsRepository;
use App\paddock\Media\Youtube\Repositories\YouTubeVideosRepository;

class MediaController extends Controller
{
    /**
     * @var GalleryAlbumsRepository
     */
    private $galleryAlbumsRepository;

    /**
     * @var YouTubeVideosRepository
     */
    private $youTubeVideosRepository;

    /**
     * MediaController constructor.
     * @param GalleryAlbumsRepository $galleryAlbumsRepository
     * @param YouTubeVideosRepository $youTubeVideosRepository
     */
    public function __construct(
        YouTubeVideosRepository $youTubeVideosRepository,
        GalleryAlbumsRepository $galleryAlbumsRepository
    ) {
        $this->galleryAlbumsRepository = $galleryAlbumsRepository;
        $this->youTubeVideosRepository = $youTubeVideosRepository;
    }

    /**
     * Shows the landing page of the media area.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $albums = $this->galleryAlbumsRepository->getLatestGalleryAlbums();
        $videos = $this->youTubeVideosRepository->getLatestVideos();

        return view('media.index')
            ->with('albums', $albums)
            ->with('videos', $videos);
    }
}
