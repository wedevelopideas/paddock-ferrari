<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Cars\Repository\CarsRepository;
use App\paddock\Results\Requests\AddResultsRequest;
use App\paddock\Tracks\Repositories\TracksRepository;
use App\paddock\Drivers\Repositories\DriversRepository;
use App\paddock\Results\Repositories\ResultsRepository;
use App\paddock\Seasons\Repositories\SeasonsRepository;
use App\paddock\Settings\Repositories\SettingsRepository;
use App\paddock\GrandPrixs\Repositories\GrandPrixsRepository;
use App\paddock\Results\Repositories\OverallResultsRepository;
use App\paddock\Results\Repositories\GrandPrixResultsRepository;
use App\paddock\Results\Repositories\CurrentSeasonResultsRepository;

class ResultsController extends Controller
{
    /**
     * @var CarsRepository
     */
    private $carsRepository;

    /**
     * @var CurrentSeasonResultsRepository
     */
    private $currentSeasonResultsRepository;

    /**
     * @var DriversRepository
     */
    private $driversRepository;

    /**
     * @var GrandPrixResultsRepository
     */
    private $grandPrixResultsRepository;

    /**
     * @var GrandPrixsRepository
     */
    private $grandPrixsRepository;

    /**
     * @var OverallResultsRepository
     */
    private $overallResultsRepository;

    /**
     * @var ResultsRepository
     */
    private $resultsRepository;

    /**
     * @var SeasonsRepository
     */
    private $seasonsRepository;

    /**
     * @var SettingsRepository
     */
    private $settingsRepository;

    /**
     * @var TracksRepository
     */
    private $tracksRepository;

    /**
     * ResultsController constructor.
     * @param CarsRepository $carsRepository
     * @param CurrentSeasonResultsRepository $currentSeasonResultsRepository
     * @param DriversRepository $driversRepository
     * @param GrandPrixResultsRepository $grandPrixResultsRepository
     * @param GrandPrixsRepository $grandPrixsRepository
     * @param OverallResultsRepository $overallResultsRepository
     * @param ResultsRepository $resultsRepository
     * @param SeasonsRepository $seasonsRepository
     * @param SettingsRepository $settingsRepository
     * @param TracksRepository $tracksRepository
     */
    public function __construct(
        CarsRepository $carsRepository,
        CurrentSeasonResultsRepository $currentSeasonResultsRepository,
        DriversRepository $driversRepository,
        GrandPrixResultsRepository $grandPrixResultsRepository,
        GrandPrixsRepository $grandPrixsRepository,
        OverallResultsRepository $overallResultsRepository,
        ResultsRepository $resultsRepository,
        SeasonsRepository $seasonsRepository,
        SettingsRepository $settingsRepository,
        TracksRepository $tracksRepository
    ) {
        $this->carsRepository = $carsRepository;
        $this->currentSeasonResultsRepository = $currentSeasonResultsRepository;
        $this->driversRepository = $driversRepository;
        $this->grandPrixResultsRepository = $grandPrixResultsRepository;
        $this->grandPrixsRepository = $grandPrixsRepository;
        $this->overallResultsRepository = $overallResultsRepository;
        $this->resultsRepository = $resultsRepository;
        $this->seasonsRepository = $seasonsRepository;
        $this->settingsRepository = $settingsRepository;
        $this->tracksRepository = $tracksRepository;
    }

    /**
     * Shows a list of all Grand Prix where points have been taken.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $season = $this->settingsRepository->getSettings('season');

        $results = $this->resultsRepository->getAllResultsByGrandPrix();

        $currentSeason = $this->currentSeasonResultsRepository->getResults((int) $season);
        $overall = $this->overallResultsRepository->getResults();

        return view('results.index')
            ->with('currentSeason', $currentSeason)
            ->with('overall', $overall)
            ->with('results', $results);
    }

    /**
     * Shows the form for adding results.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        $cars = $this->carsRepository->getAll();
        $drivers = $this->driversRepository->getAll();
        $grandprixs = $this->grandPrixsRepository->getAll();
        $seasons = $this->seasonsRepository->getAll();
        $tracks = $this->tracksRepository->getAll();

        return view('results.add')
            ->with('cars', $cars)
            ->with('drivers', $drivers)
            ->with('grandprixs', $grandprixs)
            ->with('seasons', $seasons)
            ->with('tracks', $tracks);
    }

    /**
     * Stores new results.
     *
     * @param AddResultsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddResultsRequest $request)
    {
        $data = [
            'season' => $request->input('season'),
            'gp_id' => $request->input('gp_id'),
            'track_id' => $request->input('track_id'),
            'session_id' => $request->input('session_id'),
            'dateofresult' => $request->input('dateofresult'),
            'car_id' => $request->input('car_id'),
            'car_nr' => $request->input('car_nr'),
            'driver_id' => $request->input('driver_id'),
            'team' => $request->input('team'),
            'place' => $request->input('place'),
            'laps' => $request->input('laps'),
            'points' => $request->input('points'),
            'dnf' => $request->input('dnf'),
            'dns' => $request->input('dns'),
            'dsq' => $request->input('dsq'),
        ];

        $this->resultsRepository->store($data);

        return redirect()
            ->route('results');
    }

    /**
     * Shows the Grand Prix results.
     *
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function grandPrix(string $slug)
    {
        $grandprix = $this->grandPrixsRepository->getGrandPrixBySlug($slug);

        $results = $this->grandPrixResultsRepository->getResultsForRace($grandprix->id);
        $stats = $this->grandPrixResultsRepository->getResults($grandprix->id);

        return view('results.grandprix')
            ->with('grandprix', $grandprix)
            ->with('results', $results)
            ->with('stats', $stats);
    }
}
