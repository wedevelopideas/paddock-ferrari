<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Media\Youtube\Requests\AddYouTubeVideosRequest;
use App\paddock\Media\Youtube\Requests\EditYouTubeVideosRequest;
use App\paddock\Media\Youtube\Repositories\YouTubeVideosRepository;

class YouTubeController extends Controller
{
    /**
     * @var YouTubeVideosRepository
     */
    private $youtubeVideosRepository;

    /**
     * YouTubeController constructor.
     * @param YouTubeVideosRepository $youtubeVideosRepository
     */
    public function __construct(YouTubeVideosRepository $youtubeVideosRepository)
    {
        $this->youtubeVideosRepository = $youtubeVideosRepository;
    }

    /**
     * Shows all youtube videos.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $videos = $this->youtubeVideosRepository->getAll();

        return view('media.youtube.index')
            ->with('videos', $videos);
    }

    /**
     * Shows the form for adding YouTube videos.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        return view('media.youtube.add');
    }

    /**
     * Stores new youtube videos.
     *
     * @param AddYouTubeVideosRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddYouTubeVideosRequest $request)
    {
        $data = [
            'youtube_id' => $request->input('youtube_id'),
            'title' => $request->input('title'),
            'description' => $request->input('description'),
        ];

        $this->youtubeVideosRepository->store($data);

        return redirect()
            ->route('media.youtube');
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $video = $this->youtubeVideosRepository->getVideoByID($id);

        return view('media.youtube.edit')
            ->with('video', $video);
    }

    /**
     * Updates the youtube video.
     *
     * @param int $id
     * @param EditYouTubeVideosRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, EditYouTubeVideosRequest $request)
    {
        $data = [
            'youtube_id' => $request->input('youtube_id'),
            'title' => $request->input('title'),
            'description' => $request->input('description'),
        ];

        $this->youtubeVideosRepository->update($id, $data);

        return redirect()
            ->route('media.youtube');
    }

    /**
     * Get the video by its youtube id.
     *
     * @param string $youtube_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(string $youtube_id)
    {
        $video = $this->youtubeVideosRepository->getVideoByYouTubeID($youtube_id);

        return view('media.youtube.view')
            ->with('video', $video);
    }
}
