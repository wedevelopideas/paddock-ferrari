<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Users\Repositories\UsersRepository;
use App\paddock\Users\Requests\EditUserLanguageRequest;
use App\paddock\Users\Requests\EditUserTimezoneRequest;
use App\paddock\Languages\Repositories\LanguagesRepository;

class SettingsController extends Controller
{
    /**
     * @var LanguagesRepository
     */
    private $languagesRepository;

    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * SettingsController constructor.
     * @param LanguagesRepository $languagesRepository
     * @param UsersRepository $usersRepository
     */
    public function __construct(
        UsersRepository $usersRepository,
        LanguagesRepository $languagesRepository
    ) {
        $this->languagesRepository = $languagesRepository;
        $this->usersRepository = $usersRepository;
    }

    /**
     * Shows the user dashboard.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user_id = auth()->id();
        $user = $this->usersRepository->getUserByID($user_id);

        $langs = $this->languagesRepository->getActiveLanguages();

        return view('settings.index')
            ->with('langs', $langs)
            ->with('user', $user);
    }

    /**
     * Updates the user's language.
     *
     * @param EditUserLanguageRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function languages(EditUserLanguageRequest $request)
    {
        $user_id = auth()->id();

        $data = [
            'lang' => $request->input('lang'),
        ];

        $this->usersRepository->updateLanguage($user_id, $data);

        return redirect()
            ->route('settings');
    }

    /**
     * Updates the user's timezone.
     *
     * @param EditUserTimezoneRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function timezones(EditUserTimezoneRequest $request)
    {
        $user_id = auth()->id();

        $data = [
            'timezone' => $request->input('timezone'),
        ];

        $this->usersRepository->updateTimezone($user_id, $data);

        return redirect()
            ->route('settings');
    }
}
