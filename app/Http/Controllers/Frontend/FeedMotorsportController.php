<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Feed\Repositories\FeedMotorsportRepository;
use App\paddock\Languages\Repositories\LanguagesRepository;

class FeedMotorsportController extends Controller
{
    /**
     * @var FeedMotorsportRepository
     */
    private $feedMotorsportRepository;

    /**
     * @var LanguagesRepository
     */
    private $languagesRepository;

    /**
     * FeedMotorsportController constructor.
     * @param FeedMotorsportRepository $feedMotorsportRepository
     * @param LanguagesRepository $languagesRepository
     */
    public function __construct(
        LanguagesRepository $languagesRepository,
        FeedMotorsportRepository $feedMotorsportRepository
    ) {
        $this->feedMotorsportRepository = $feedMotorsportRepository;
        $this->languagesRepository = $languagesRepository;
    }

    /**
     * Shows the whole feed associated to Motorsport and Ferrari.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $lang = auth()->user()->lang;

        $feeds = $this->feedMotorsportRepository->getAll($lang);
        $langs = $this->languagesRepository->getNewsActiveLanguages();

        return view('feed.motorsport.index')
            ->with('feeds', $feeds)
            ->with('langs', $langs);
    }

    /**
     * Shows the form for adding motorsport feeds.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        $languages = $this->languagesRepository->getNewsActiveLanguages();

        return view('feed.motorsport.add')
            ->with('languages', $languages);
    }

    /**
     * Get feed entry by its id.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(int $id)
    {
        $feed = $this->feedMotorsportRepository->getFeedByID($id);

        $feeds = $this->feedMotorsportRepository->getFeedsByDate($feed->created_at->format('Y-m-d'));

        return view('feed.motorsport.view')
            ->with('feed', $feed)
            ->with('feeds', $feeds);
    }
}
