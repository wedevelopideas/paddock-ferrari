<?php

namespace App\Http\Controllers\Frontend;

use App\paddock\Timezone;
use App\Http\Controllers\Controller;
use App\paddock\Feed\Repositories\FeedRepository;
use App\paddock\Drivers\Repositories\DriversRepository;
use App\paddock\Feed\Repositories\FeedFerrariRepository;
use App\paddock\Settings\Repositories\SettingsRepository;
use App\paddock\Tests\Repositories\TestsResultsRepository;
use App\paddock\Tests\Repository\TestsCalendarsRepository;
use App\paddock\Seasons\Repositories\SeasonsRacesRepository;
use App\paddock\Results\Repositories\ResultsTimingsRepository;
use App\paddock\Seasons\Repositories\SeasonsDriversRepository;
use App\paddock\Seasons\Repositories\SeasonsCalendarRepository;
use App\paddock\Twitter\Repositories\TwitterAccountsRepository;
use App\paddock\Reports\Repositories\ReportsTires2019Repository;
use App\paddock\Reports\Repositories\ReportsTechnicalsRepository;
use App\paddock\SocialMedias\Repositories\SocialMediasRepository;
use App\paddock\Media\Youtube\Repositories\YouTubeVideosRepository;
use App\paddock\Results\Repositories\ChampionshipResultsRepository;
use App\paddock\Reports\Repositories\ReportsNominatedTiresRepository;

class DashboardController extends Controller
{
    /**
     * @var ChampionshipResultsRepository
     */
    private $championshipResultsRepository;

    /**
     * @var DriversRepository
     */
    private $driversRepository;

    /**
     * @var FeedFerrariRepository
     */
    private $feedFerrariRepository;

    /**
     * @var FeedRepository
     */
    private $feedRepository;

    /**
     * @var ReportsNominatedTiresRepository
     */
    private $reportsNominatedTiresRepository;

    /**
     * @var ReportsTechnicalsRepository
     */
    private $reportsTechnicalsRepository;

    /**
     * @var ReportsTires2019Repository
     */
    private $reportsTires2019Repository;

    /**
     * @var ResultsTimingsRepository
     */
    private $resultsTimingsRepository;

    /**
     * @var SeasonsCalendarRepository
     */
    private $seasonsCalendarRepository;

    /**
     * @var SeasonsDriversRepository
     */
    private $seasonsDriversRepository;

    /**
     * @var SeasonsRacesRepository
     */
    private $seasonsRacesRepository;

    /**
     * @var SettingsRepository
     */
    private $settingsRepository;

    /**
     * @var SocialMediasRepository
     */
    private $socialMediasRepository;

    /**
     * @var TestsCalendarsRepository
     */
    private $testsCalendarsRepository;

    /**
     * @var TestsResultsRepository
     */
    private $testsResultsRepository;

    /**
     * @var Timezone
     */
    private $timezone;

    /**
     * @var TwitterAccountsRepository
     */
    private $twitterAccountsRepository;

    /**
     * @var YouTubeVideosRepository
     */
    private $youTubeVideosRepository;

    /**
     * DashboardController constructor.
     * @param  ChampionshipResultsRepository  $championshipResultsRepository
     * @param  DriversRepository  $driversRepository
     * @param  FeedRepository  $feedRepository
     * @param  FeedFerrariRepository  $feedFerrariRepository
     * @param  ReportsNominatedTiresRepository  $reportsNominatedTiresRepository
     * @param  ReportsTechnicalsRepository  $reportsTechnicalsRepository
     * @param  ReportsTires2019Repository  $reportsTires2019Repository
     * @param  ResultsTimingsRepository  $resultsTimingsRepository
     * @param  SeasonsCalendarRepository  $seasonsCalendarRepository
     * @param  SeasonsDriversRepository  $seasonsDriversRepository
     * @param  SeasonsRacesRepository  $seasonsRacesRepository
     * @param  SettingsRepository  $settingsRepository
     * @param  SocialMediasRepository  $socialMediasRepository
     * @param  TestsCalendarsRepository  $testsCalendarsRepository
     * @param  TestsResultsRepository  $testsResultsRepository
     * @param  Timezone  $timezone
     * @param  TwitterAccountsRepository  $twitterAccountsRepository
     * @param  YouTubeVideosRepository  $youTubeVideosRepository
     */
    public function __construct(
        ChampionshipResultsRepository $championshipResultsRepository,
        DriversRepository $driversRepository,
        FeedRepository $feedRepository,
        FeedFerrariRepository $feedFerrariRepository,
        ReportsNominatedTiresRepository $reportsNominatedTiresRepository,
        ReportsTechnicalsRepository $reportsTechnicalsRepository,
        ReportsTires2019Repository $reportsTires2019Repository,
        ResultsTimingsRepository $resultsTimingsRepository,
        SeasonsCalendarRepository $seasonsCalendarRepository,
        SeasonsDriversRepository $seasonsDriversRepository,
        SeasonsRacesRepository $seasonsRacesRepository,
        SettingsRepository $settingsRepository,
        SocialMediasRepository $socialMediasRepository,
        TestsCalendarsRepository $testsCalendarsRepository,
        TestsResultsRepository $testsResultsRepository,
        Timezone $timezone,
        TwitterAccountsRepository $twitterAccountsRepository,
        YouTubeVideosRepository $youTubeVideosRepository
    ) {
        $this->championshipResultsRepository = $championshipResultsRepository;
        $this->driversRepository = $driversRepository;
        $this->feedFerrariRepository = $feedFerrariRepository;
        $this->feedRepository = $feedRepository;
        $this->reportsNominatedTiresRepository = $reportsNominatedTiresRepository;
        $this->reportsTechnicalsRepository = $reportsTechnicalsRepository;
        $this->reportsTires2019Repository = $reportsTires2019Repository;
        $this->resultsTimingsRepository = $resultsTimingsRepository;
        $this->seasonsCalendarRepository = $seasonsCalendarRepository;
        $this->seasonsDriversRepository = $seasonsDriversRepository;
        $this->seasonsRacesRepository = $seasonsRacesRepository;
        $this->settingsRepository = $settingsRepository;
        $this->socialMediasRepository = $socialMediasRepository;
        $this->testsCalendarsRepository = $testsCalendarsRepository;
        $this->testsResultsRepository = $testsResultsRepository;
        $this->timezone = $timezone;
        $this->twitterAccountsRepository = $twitterAccountsRepository;
        $this->youTubeVideosRepository = $youTubeVideosRepository;
    }

    /**
     * Shows the user dashboard.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dashboard()
    {
        $lang = auth()->user()->lang;
        $gp = $this->settingsRepository->getSettings('grandprix');
        $season = $this->settingsRepository->getSettings('season');

        $awayzone = $this->timezone->awayzone();
        $currentRace = $this->seasonsRacesRepository->getCurrentRace((int) $season, (int) $gp);
        $drivers = $this->seasonsDriversRepository->getDriversBySeason((int) $season);
        $events = $this->driversRepository->getDriverEvents();
        $feeds = $this->feedRepository->getLatestArticles($lang);
        $homezone = $this->timezone->homezone();
        $races = $this->seasonsRacesRepository->getRaceEvents();
        $sessions = $this->seasonsCalendarRepository->getSessionsForGP((int) $season, (int) $gp);
        $twitterAccounts = $this->twitterAccountsRepository->getAll();
        $video = $this->youTubeVideosRepository->getLatestVideo();

        return view('dashboard.index')
            ->with('awayzone', $awayzone)
            ->with('currentRace', $currentRace)
            ->with('drivers', $drivers)
            ->with('events', $events)
            ->with('feeds', $feeds)
            ->with('homezone', $homezone)
            ->with('races', $races)
            ->with('sessions', $sessions)
            ->with('twitterAccounts', $twitterAccounts)
            ->with('video', $video);
    }

    /**
     * Shows the race dashboard.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function race()
    {
        $gp = $this->settingsRepository->getSettings('grandprix');
        $lang = auth()->user()->lang;
        $season = $this->settingsRepository->getSettings('season');

        $grandprix = $this->seasonsRacesRepository->getCurrentRace((int) $season, (int) $gp);

        $nominated = $this->reportsNominatedTiresRepository->getNominatedTires((int) $season, (int) $gp);
        $tires = $this->reportsTires2019Repository->getTyreReportsForDrivers((int) $season, (int) $gp);

        $technicals = $this->reportsTechnicalsRepository->getTechnicalReportsForDrivers((int) $season, (int) $gp);

        $sessions = $this->seasonsCalendarRepository->getSessionsForGP((int) $season, (int) $gp);

        $drivers = $this->seasonsDriversRepository->getDriversBySeason((int) $season);
        $driver1 = $this->resultsTimingsRepository->getTimesForDriver((int) $season, (int) $gp, $drivers[0]->id);
        $driver2 = $this->resultsTimingsRepository->getTimesForDriver((int) $season, (int) $gp, $drivers[1]->id);

        $stD1 = $this->championshipResultsRepository->getStandingByDriver((int) $season, $drivers[0]->id);
        $stD2 = $this->championshipResultsRepository->getStandingByDriver((int) $season, $drivers[1]->id);
        $stT = $this->championshipResultsRepository->getStandingByTeam((int) $season);

        $feeds = $this->feedFerrariRepository->getFeedByHashtagAndLang($grandprix->hashtag, $lang);

        return view('dashboard.race')
            ->with('drivers', $drivers)
            ->with('driver1', $driver1)
            ->with('driver2', $driver2)
            ->with('feeds', $feeds)
            ->with('grandprix', $grandprix)
            ->with('nominated', $nominated)
            ->with('sessions', $sessions)
            ->with('stD1', $stD1)
            ->with('stD2', $stD2)
            ->with('stT', $stT)
            ->with('technicals', $technicals)
            ->with('tires', $tires);
    }

    /**
     * Shows the social media dashboard.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function socialMedia()
    {
        $socials = $this->socialMediasRepository->getAll();
        $twitterAccounts = $this->twitterAccountsRepository->getAll();

        return view('dashboard.socialmedia')
            ->with('socials', $socials)
            ->with('twitterAccounts', $twitterAccounts);
    }

    /**
     * Shows the test dashboard.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tests()
    {
        $season = $this->settingsRepository->getSettings('season');
        $week = $this->settingsRepository->getSettings('week');

        $days = $this->testsCalendarsRepository->getTestWeekBySeasonAndWeek((int) $season, (int) $week);
        $results = $this->testsResultsRepository->getTestResultsBySeasonAndWeek((int) $season, (int) $week);
        $track = $this->testsCalendarsRepository->getTestWeeksTrack((int) $season, (int) $week);

        return view('dashboard.tests')
            ->with('days', $days)
            ->with('results', $results)
            ->with('season', $season)
            ->with('track', $track)
            ->with('week', $week);
    }
}
