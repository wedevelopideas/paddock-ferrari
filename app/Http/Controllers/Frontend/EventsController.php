<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Events\Requests\AddEventsRequest;
use App\paddock\Events\Requests\EditEventsRequest;
use App\paddock\Events\Repositories\EventsRepository;
use App\paddock\Drivers\Repositories\DriversRepository;
use App\paddock\Languages\Repositories\LanguagesRepository;

class EventsController extends Controller
{
    /**
     * @var DriversRepository
     */
    private $driversRepository;

    /**
     * @var EventsRepository
     */
    private $eventsRepository;

    /**
     * @var LanguagesRepository
     */
    private $languagesRepository;

    /**
     * EventsController constructor.
     * @param DriversRepository $driversRepository
     * @param EventsRepository $eventsRepository
     * @param LanguagesRepository $languagesRepository
     */
    public function __construct(
        DriversRepository $driversRepository,
        EventsRepository $eventsRepository,
        LanguagesRepository $languagesRepository
    ) {
        $this->driversRepository = $driversRepository;
        $this->eventsRepository = $eventsRepository;
        $this->languagesRepository = $languagesRepository;
    }

    /**
     * Show a list of all events.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $lang = auth()->user()->lang;

        $events = $this->eventsRepository->getAllEventsByLang($lang);

        return view('events.index')
            ->with('events', $events);
    }

    /**
     * Shows the form for adding events.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        $drivers = $this->driversRepository->getAll();
        $langs = $this->languagesRepository->getActiveLanguages();

        return view('events.add')
            ->with('drivers', $drivers)
            ->with('langs', $langs);
    }

    /**
     * Stores new events.
     *
     * @param AddEventsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddEventsRequest $request)
    {
        $data = [
            'type' => $request->input('type'),
            'lang' => $request->input('lang'),
            'driver_id' => $request->input('driver_id'),
            'event_date' => $request->input('event_date'),
            'event_text' => $request->input('event_text'),
            'event_hashtags' => $request->input('event_hashtags'),
            'event_link' => $request->input('event_link'),
            'event_f1tv_link' => $request->input('event_f1tv_link'),
        ];

        $this->eventsRepository->store($data);

        return redirect()
            ->route('events');
    }

    /**
     * Get the events by its id.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $event = $this->eventsRepository->getEventByID($id);

        $drivers = $this->driversRepository->getAll();
        $langs = $this->languagesRepository->getActiveLanguages();

        return view('events.edit')
            ->with('drivers', $drivers)
            ->with('event', $event)
            ->with('langs', $langs);
    }

    /**
     * Updates the events.
     *
     * @param int $id
     * @param EditEventsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, EditEventsRequest $request)
    {
        $data = [
            'type' => $request->input('type'),
            'lang' => $request->input('lang'),
            'driver_id' => $request->input('driver_id'),
            'event_date' => $request->input('event_date'),
            'event_text' => $request->input('event_text'),
            'event_hashtags' => $request->input('event_hashtags'),
            'event_link' => $request->input('event_link'),
            'event_f1tv_link' => $request->input('event_f1tv_link'),
        ];

        $this->eventsRepository->update($id, $data);

        return redirect()
            ->route('events.view', ['driver_id' => $request->input('driver_id'), 'date' => $request->input('event_date')]);
    }

    /**
     * Get the single event.
     *
     * @param int $driver_id
     * @param string $event_date
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(int $driver_id, string $event_date)
    {
        $lang = auth()->user()->lang;

        $event = $this->eventsRepository->getEventByDriverIdDateAndLang($lang, $driver_id, $event_date);
        $event_list = $this->eventsRepository->getEventByDriverIdAndDate($driver_id, $event_date);

        return view('events.view')
            ->with('event', $event)
            ->with('event_list', $event_list);
    }
}
