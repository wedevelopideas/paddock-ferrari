<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Feed\Repositories\FeedRepository;
use App\paddock\Users\Repositories\UsersRepository;

class UsersController extends Controller
{
    /**
     * @var FeedRepository
     */
    private $feedRepository;

    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * UsersController constructor.
     * @param FeedRepository $feedRepository
     * @param UsersRepository $usersRepository
     */
    public function __construct(
        UsersRepository $usersRepository,
        FeedRepository $feedRepository
    ) {
        $this->feedRepository = $feedRepository;
        $this->usersRepository = $usersRepository;
    }

    /**
     * Shows the user profile.
     *
     * @param string $user_name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(string $user_name)
    {
        $user = $this->usersRepository->getUserByUserName($user_name);

        $articles = $this->feedRepository->getAllArticlesByUserID($user->id, $user->lang);

        return view('users.index')
            ->with('articles', $articles)
            ->with('user', $user);
    }
}
