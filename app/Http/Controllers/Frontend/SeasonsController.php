<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\paddock\Settings\Repositories\SettingsRepository;
use App\paddock\Seasons\Repositories\SeasonsRacesRepository;

class SeasonsController extends Controller
{
    /**
     * @var SettingsRepository
     */
    private $settingsRepository;

    /**
     * @var SeasonsRacesRepository
     */
    private $seasonsRacesRepository;

    /**
     * SeasonsController constructor.
     * @param SettingsRepository $settingsRepository
     * @param SeasonsRacesRepository $seasonsRacesRepository
     */
    public function __construct(
        SeasonsRacesRepository $seasonsRacesRepository,
        SettingsRepository $settingsRepository
    ) {
        $this->settingsRepository = $settingsRepository;
        $this->seasonsRacesRepository = $seasonsRacesRepository;
    }

    /**
     * Shows list of all races in the current season.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $season = $this->settingsRepository->getSettings('season');

        $races = $this->seasonsRacesRepository->getRacesBySeason((int) $season);

        return view('season.index')
            ->with('races', $races)
            ->with('season', $season);
    }
}
