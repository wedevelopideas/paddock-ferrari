<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Media\Gallery\Requests\AddGalleryAlbumRequest;
use App\paddock\Media\Gallery\Requests\EditGalleryAlbumRequest;
use App\paddock\Media\Gallery\Repositories\GalleryAlbumsRepository;

class GalleryAlbumsController extends Controller
{
    /**
     * @var GalleryAlbumsRepository
     */
    private $galleryAlbumsRepository;

    /**
     * GalleryAlbumsController constructor.
     * @param GalleryAlbumsRepository $galleryAlbumsRepository
     */
    public function __construct(GalleryAlbumsRepository $galleryAlbumsRepository)
    {
        $this->galleryAlbumsRepository = $galleryAlbumsRepository;
    }

    /**
     * Shows the form for adding gallery albums.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        return view('backend.gallery.albums.add');
    }

    /**
     * Stores gallery albums.
     *
     * @param AddGalleryAlbumRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddGalleryAlbumRequest $request)
    {
        $data = [
            'name' => $request->input('name'),
            'cover_id' => 0,
            'creator_id' => auth()->id(),
        ];

        $this->galleryAlbumsRepository->store($data);

        return redirect()
            ->route('backend.gallery');
    }

    /**
     * Get the gallery album by its id.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $album = $this->galleryAlbumsRepository->getAlbumByID($id);

        return view('backend.gallery.albums.edit')
            ->with('album', $album);
    }

    /**
     * Updates gallery albums.
     *
     * @param int $id
     * @param EditGalleryAlbumRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, EditGalleryAlbumRequest $request)
    {
        $data = [
            'name' => $request->input('name'),
        ];

        $this->galleryAlbumsRepository->update($id, $data);

        return redirect()
            ->route('backend.gallery');
    }
}
