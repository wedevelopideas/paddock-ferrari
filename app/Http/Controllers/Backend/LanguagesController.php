<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Languages\Requests\AddLanguagesRequest;
use App\paddock\Languages\Requests\EditLanguagesRequest;
use App\paddock\Languages\Repositories\LanguagesRepository;

class LanguagesController extends Controller
{
    /**
     * @var LanguagesRepository
     */
    private $languagesRepository;

    /**
     * LanguagesController constructor.
     * @param LanguagesRepository $languagesRepository
     */
    public function __construct(LanguagesRepository $languagesRepository)
    {
        $this->languagesRepository = $languagesRepository;
    }

    /**
     * Shows a list of all languages.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $langs = $this->languagesRepository->getAll();

        return view('backend.languages.index')
            ->with('langs', $langs);
    }

    /**
     * Shows the form for adding languages.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        return view('backend.languages.add');
    }

    /**
     * Stores new languages.
     *
     * @param AddLanguagesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddLanguagesRequest $request)
    {
        $data = [
            'code' => $request->input('code'),
            'name' => $request->input('name'),
            'motorsport_link' => $request->input('motorsport_link'),
            'news' => $request->has('news'),
            'active' => $request->has('active'),
        ];

        $this->languagesRepository->store($data);

        return redirect()
            ->route('backend.langs');
    }

    /**
     * Get the language by its code.
     *
     * @param string $code
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(string $code)
    {
        $lang = $this->languagesRepository->getLanguageByCode($code);

        return view('backend.languages.edit')
            ->with('lang', $lang);
    }

    /**
     * Updates the languages.
     *
     * @param string $code
     * @param EditLanguagesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(string $code, EditLanguagesRequest $request)
    {
        $data = [
            'code' => $request->input('code'),
            'name' => $request->input('name'),
            'motorsport_link' => $request->input('motorsport_link'),
            'news' => $request->has('news'),
            'active' => $request->has('active'),
        ];

        $this->languagesRepository->update($code, $data);

        return redirect()
            ->route('backend.langs');
    }
}
