<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Drivers\Repositories\DriversRepository;
use App\paddock\Seasons\Repositories\SeasonsRepository;
use App\paddock\Seasons\Requests\AddSeasonsDriversRequest;
use App\paddock\Seasons\Requests\EditSeasonsDriversRequest;
use App\paddock\Seasons\Repositories\SeasonsDriversRepository;

class SeasonsDriversController extends Controller
{
    /**
     * @var SeasonsDriversRepository
     */
    private $seasonsDriversRepository;

    /**
     * @var DriversRepository
     */
    private $driversRepository;

    /**
     * @var SeasonsRepository
     */
    private $seasonsRepository;

    /**
     * SeasonsDriversController constructor.
     * @param SeasonsDriversRepository $seasonsDriversRepository
     * @param SeasonsRepository $seasonsRepository
     * @param DriversRepository $driversRepository
     */
    public function __construct(
        SeasonsDriversRepository $seasonsDriversRepository,
        SeasonsRepository $seasonsRepository,
        DriversRepository $driversRepository
    ) {
        $this->driversRepository = $driversRepository;
        $this->seasonsDriversRepository = $seasonsDriversRepository;
        $this->seasonsRepository = $seasonsRepository;
    }

    /**
     * Get all drivers of the season.
     *
     * @param int $season
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(int $season)
    {
        $saison = $this->seasonsRepository->getSeasonBySeason($season);

        $drivers = $this->seasonsDriversRepository->getDriversBySeason($season);

        return view('backend.seasons.drivers.index')
            ->with('drivers', $drivers)
            ->with('saison', $saison);
    }

    /**
     * Shows the form for adding season drivers.
     *
     * @param int $season
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(int $season)
    {
        $season = $this->seasonsRepository->getSeasonBySeason($season);

        $drivers = $this->driversRepository->getAll();

        return view('backend.seasons.drivers.add')
            ->with('drivers', $drivers)
            ->with('season', $season);
    }

    /**
     * Stores new season drivers.
     *
     * @param int $season
     * @param AddSeasonsDriversRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(int $season, AddSeasonsDriversRequest $request)
    {
        $data = [
            'season' => $season,
            'driver_id' => $request->input('driver_id'),
        ];

        $this->seasonsDriversRepository->store($data);

        return redirect()
            ->route('backend.seasons.drivers', ['season' => $season]);
    }

    /**
     * Get driver by season and id.
     *
     * @param int $season
     * @param int $id
     * @return mixed
     */
    public function edit(int $season, int $id)
    {
        $season = $this->seasonsRepository->getSeasonBySeason($season);

        $drivers = $this->driversRepository->getAll();
        $racer = $this->seasonsDriversRepository->getDriverByID($id);

        return view('backend.seasons.drivers.edit')
            ->with('drivers', $drivers)
            ->with('racer', $racer)
            ->with('season', $season);
    }

    /**
     * Updates seasons drivers.
     *
     * @param int $season
     * @param int $id
     * @param EditSeasonsDriversRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $season, int $id, EditSeasonsDriversRequest $request)
    {
        $data = [
            'season' => $season,
            'driver_id' => $request->input('driver_id'),
        ];

        $this->seasonsDriversRepository->update($id, $data);

        return redirect()
            ->route('backend.seasons.drivers', ['season' => $season]);
    }
}
