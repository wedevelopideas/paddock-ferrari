<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Drivers\Repositories\DriversRepository;
use App\paddock\Seasons\Repositories\SeasonsRepository;
use App\paddock\Reports\Requests\AddReportsTiresRequest;
use App\paddock\Reports\Repositories\ReportsTiresRepository;
use App\paddock\Reports\Requests\AddReportsTires2019Request;
use App\paddock\GrandPrixs\Repositories\GrandPrixsRepository;
use App\paddock\Reports\Repositories\ReportsTires2019Repository;

class ReportsTiresController extends Controller
{
    /**
     * @var DriversRepository
     */
    private $driversRepository;

    /**
     * @var GrandPrixsRepository
     */
    private $grandPrixsRepository;

    /**
     * @var ReportsTires2019Repository
     */
    private $reportsTires2019Repository;

    /**
     * @var ReportsTiresRepository
     */
    private $reportsTiresRepository;

    /**
     * @var SeasonsRepository
     */
    private $seasonsRepository;

    /**
     * ReportsTiresController constructor.
     * @param DriversRepository $driversRepository
     * @param GrandPrixsRepository $grandPrixsRepository
     * @param ReportsTires2019Repository $reportsTires2019Repository
     * @param ReportsTiresRepository $reportsTiresRepository
     * @param SeasonsRepository $seasonsRepository
     */
    public function __construct(
        DriversRepository $driversRepository,
        GrandPrixsRepository $grandPrixsRepository,
        ReportsTires2019Repository $reportsTires2019Repository,
        ReportsTiresRepository $reportsTiresRepository,
        SeasonsRepository $seasonsRepository
    ) {
        $this->driversRepository = $driversRepository;
        $this->grandPrixsRepository = $grandPrixsRepository;
        $this->reportsTires2019Repository = $reportsTires2019Repository;
        $this->reportsTiresRepository = $reportsTiresRepository;
        $this->seasonsRepository = $seasonsRepository;
    }

    /**
     * Shows the form for adding tyre reports.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        $drivers = $this->driversRepository->getAll();
        $gps = $this->grandPrixsRepository->getAll();
        $seasons = $this->seasonsRepository->getAll();

        return view('backend.reports.tires.add')
            ->with('drivers', $drivers)
            ->with('gps', $gps)
            ->with('seasons', $seasons);
    }

    /**
     * Stores new tyre reports.
     *
     * @param AddReportsTiresRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddReportsTiresRequest $request)
    {
        $data = [
            'season' => $request->input('season'),
            'race' => $request->input('race'),
            'gp_id' => $request->input('gp_id'),
            'driver_id' => $request->input('driver_id'),
            'hypersoft' => $request->input('hypersoft'),
            'ultrasoft' => $request->input('ultrasoft'),
            'supersoft' => $request->input('supersoft'),
            'soft' => $request->input('soft'),
            'medium' => $request->input('medium'),
            'hard' => $request->input('hard'),
            'superhard' => $request->input('superhard'),
        ];

        $this->reportsTiresRepository->store($data);

        return redirect()
            ->route('backend.reports');
    }

    /**
     * Shows the form for adding 2019 tyre reports.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add2019()
    {
        $drivers = $this->driversRepository->getAll();
        $gps = $this->grandPrixsRepository->getAll();
        $seasons = $this->seasonsRepository->getAll();

        return view('backend.reports.tires.add_2019')
            ->with('drivers', $drivers)
            ->with('gps', $gps)
            ->with('seasons', $seasons);
    }

    /**
     * Stores new 2019 tyre reports.
     *
     * @param AddReportsTires2019Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store2019(AddReportsTires2019Request $request)
    {
        $data = [
            'season' => $request->input('season'),
            'race' => $request->input('race'),
            'gp_id' => $request->input('gp_id'),
            'driver_id' => $request->input('driver_id'),
            'c1' => $request->input('c1'),
            'c2' => $request->input('c2'),
            'c3' => $request->input('c3'),
            'c4' => $request->input('c4'),
            'c5' => $request->input('c5'),
        ];

        $this->reportsTires2019Repository->store($data);

        return redirect()
            ->route('backend.reports');
    }
}
