<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Media\Gallery\Repositories\GalleryAlbumsRepository;

class GalleryController extends Controller
{
    /**
     * @var GalleryAlbumsRepository
     */
    private $galleryAlbumsRepository;

    /**
     * GalleryController constructor.
     * @param GalleryAlbumsRepository $galleryAlbumsRepository
     */
    public function __construct(GalleryAlbumsRepository $galleryAlbumsRepository)
    {
        $this->galleryAlbumsRepository = $galleryAlbumsRepository;
    }

    /**
     * Shows galleries.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $albums = $this->galleryAlbumsRepository->getAll();

        return view('backend.gallery.index')
            ->with('albums', $albums);
    }
}
