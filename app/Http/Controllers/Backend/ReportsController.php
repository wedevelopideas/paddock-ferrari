<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Settings\Repositories\SettingsRepository;
use App\paddock\Reports\Repositories\ReportsTiresRepository;
use App\paddock\Seasons\Repositories\SeasonsRacesRepository;
use App\paddock\Reports\Repositories\ReportsTires2019Repository;
use App\paddock\Reports\Repositories\ReportsTechnicalsRepository;

class ReportsController extends Controller
{
    /**
     * @var ReportsTechnicalsRepository
     */
    private $reportsTechnicalsRepository;

    /**
     * @var ReportsTires2019Repository
     */
    private $reportsTires2019Repository;

    /**
     * @var ReportsTiresRepository
     */
    private $reportsTiresRepository;

    /**
     * @var SeasonsRacesRepository
     */
    private $seasonsRacesRepository;

    /**
     * @var SettingsRepository
     */
    private $settingsRepository;

    /**
     * ReportsController constructor.
     * @param ReportsTechnicalsRepository $reportsTechnicalsRepository
     * @param ReportsTires2019Repository $reportsTires2019Repository
     * @param ReportsTiresRepository $reportsTiresRepository
     * @param SeasonsRacesRepository $seasonsRacesRepository
     * @param SettingsRepository $settingsRepository
     */
    public function __construct(
        ReportsTechnicalsRepository $reportsTechnicalsRepository,
        ReportsTires2019Repository $reportsTires2019Repository,
        ReportsTiresRepository $reportsTiresRepository,
        SeasonsRacesRepository $seasonsRacesRepository,
        SettingsRepository $settingsRepository
    ) {
        $this->reportsTechnicalsRepository = $reportsTechnicalsRepository;
        $this->reportsTires2019Repository = $reportsTires2019Repository;
        $this->reportsTiresRepository = $reportsTiresRepository;
        $this->seasonsRacesRepository = $seasonsRacesRepository;
        $this->settingsRepository = $settingsRepository;
    }

    /**
     * Shows a list of all reports.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $season = $this->settingsRepository->getSettings('season');

        $races = $this->seasonsRacesRepository->getRacesBySeason((int) $season);

        return view('backend.reports.index')
            ->with('races', $races);
    }

    /**
     * Shows a list of all reports by season.
     *
     * @param int $season
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function season(int $season)
    {
        $races = $this->seasonsRacesRepository->getRacesBySeason($season);

        return view('backend.reports.index')
            ->with('races', $races);
    }

    /**
     * Shows reports.
     *
     * @param int $season
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(int $season, string $slug)
    {
        if ($season === 2019) {
            $report = $this->reportsTires2019Repository->getRaceBySeasonAndGP($season, $slug);

            if ($report) {
                $technicals = $this->reportsTechnicalsRepository->getTechnicalReportsForDrivers($report->season, $report->gp_id);
                $tires = $this->reportsTires2019Repository->getTyreReportsForDrivers($report->season, $report->gp_id);

                return view('backend.reports.view_2019')
                    ->with('report', $report)
                    ->with('technicals', $technicals)
                    ->with('tires', $tires);
            }
        }

        if ($season !== 2019) {
            $report = $this->reportsTiresRepository->getRaceBySeasonAndGP($season, $slug);

            if ($report) {
                $technicals = $this->reportsTechnicalsRepository->getTechnicalReportsForDrivers($report->season, $report->gp_id);
                $tires = $this->reportsTiresRepository->getTyreReportsForDrivers($report->season, $report->gp_id);

                return view('backend.reports.view')
                    ->with('report', $report)
                    ->with('technicals', $technicals)
                    ->with('tires', $tires);
            }
        }
    }
}
