<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Drivers\Requests\AddDriversRequest;
use App\paddock\Drivers\Requests\EditDriversRequest;
use App\paddock\Drivers\Repositories\DriversRepository;
use App\paddock\Countries\Repositories\CountriesRepository;

class DriversController extends Controller
{
    /**
     * @var CountriesRepository
     */
    private $countriesRepository;

    /**
     * @var DriversRepository
     */
    private $driversRepository;

    /**
     * DriversController constructor.
     * @param CountriesRepository $countriesRepository
     * @param DriversRepository $driversRepository
     */
    public function __construct(
        DriversRepository $driversRepository,
        CountriesRepository $countriesRepository
    ) {
        $this->countriesRepository = $countriesRepository;
        $this->driversRepository = $driversRepository;
    }

    /**
     * Shows a list of all drivers.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $drivers = $this->driversRepository->getAll();

        return view('backend.drivers.index')
            ->with('drivers', $drivers);
    }

    /**
     * Shows the form for adding drivers.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        $countries = $this->countriesRepository->getAll();

        return view('backend.drivers.add')
            ->with('countries', $countries);
    }

    /**
     * Stores new drivers.
     *
     * @param AddDriversRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddDriversRequest $request)
    {
        $data = [
            'country_id' => $request->input('country_id'),
            'status' => $request->input('status'),
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'hashtag' => $request->input('hashtag'),
            'dateofbirth' => $request->input('dateofbirth'),
            'dateofdeath' => $request->input('dateofdeath'),
            'image' => $request->input('image'),
        ];

        $this->driversRepository->store($data);

        return redirect()
            ->route('backend.drivers');
    }

    /**
     * Get the driver by its id.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $driver = $this->driversRepository->getDriverByID($id);
        $countries = $this->countriesRepository->getAll();

        return view('backend.drivers.edit')
            ->with('driver', $driver)
            ->with('countries', $countries);
    }

    /**
     * Updates the driver.
     *
     * @param int $id
     * @param EditDriversRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, EditDriversRequest $request)
    {
        $data = [
            'country_id' => $request->input('country_id'),
            'status' => $request->input('status'),
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'hashtag' => $request->input('hashtag'),
            'dateofbirth' => $request->input('dateofbirth'),
            'dateofdeath' => $request->input('dateofdeath'),
            'image' => $request->input('image'),
        ];

        $this->driversRepository->update($id, $data);

        return redirect()
            ->route('backend.drivers');
    }

    /**
     * Shows an overview of the driver.
     *
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(string $slug)
    {
        $driver = $this->driversRepository->getDriverBySlug($slug);

        return view('backend.drivers.view')
            ->with('driver', $driver);
    }
}
