<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Settings\Requests\SettingsRequest;
use App\paddock\Users\Repositories\UsersRepository;
use App\paddock\Tracks\Repositories\TracksRepository;
use App\paddock\Drivers\Repositories\DriversRepository;
use App\paddock\Seasons\Repositories\SeasonsRepository;
use App\paddock\Hashtags\Repositories\HashtagsRepository;
use App\paddock\Settings\Repositories\SettingsRepository;
use App\paddock\Countries\Repositories\CountriesRepository;
use App\paddock\Languages\Repositories\LanguagesRepository;
use App\paddock\Seasons\Repositories\SeasonsRacesRepository;
use App\paddock\GrandPrixs\Repositories\GrandPrixsRepository;
use App\paddock\Twitter\Repositories\TwitterAccountsRepository;
use App\paddock\Media\Youtube\Repositories\YouTubeVideosRepository;

class DashboardController extends Controller
{
    /**
     * @var CountriesRepository
     */
    private $countriesRepository;

    /**
     * @var DriversRepository
     */
    private $driversRepository;

    /**
     * @var GrandPrixsRepository
     */
    private $grandPrixsRepository;

    /**
     * @var HashtagsRepository
     */
    private $hashtagsRepository;

    /**
     * @var LanguagesRepository
     */
    private $languagesRepository;

    /**
     * @var SeasonsRacesRepository
     */
    private $seasonsRacesRepository;

    /**
     * @var SeasonsRepository
     */
    private $seasonsRepository;

    /**
     * @var SettingsRepository
     */
    private $settingsRepository;

    /**
     * @var TracksRepository
     */
    private $tracksRepository;

    /**
     * @var TwitterAccountsRepository
     */
    private $twitterAccountsRepository;

    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * @var YouTubeVideosRepository
     */
    private $youTubeVideosRepository;

    /**
     * DashboardController constructor.
     * @param CountriesRepository $countriesRepository
     * @param DriversRepository $driversRepository
     * @param GrandPrixsRepository $grandPrixsRepository
     * @param HashtagsRepository $hashtagsRepository
     * @param LanguagesRepository $languagesRepository
     * @param SeasonsRacesRepository $seasonsRacesRepository
     * @param SeasonsRepository $seasonsRepository
     * @param SettingsRepository $settingsRepository
     * @param TracksRepository $tracksRepository
     * @param TwitterAccountsRepository $twitterAccountsRepository
     * @param UsersRepository $usersRepository
     * @param YouTubeVideosRepository $youTubeVideosRepository
     */
    public function __construct(
        CountriesRepository $countriesRepository,
        DriversRepository $driversRepository,
        GrandPrixsRepository $grandPrixsRepository,
        HashtagsRepository $hashtagsRepository,
        LanguagesRepository $languagesRepository,
        SeasonsRacesRepository $seasonsRacesRepository,
        SeasonsRepository $seasonsRepository,
        SettingsRepository $settingsRepository,
        TracksRepository $tracksRepository,
        TwitterAccountsRepository $twitterAccountsRepository,
        UsersRepository $usersRepository,
        YouTubeVideosRepository $youTubeVideosRepository
    ) {
        $this->countriesRepository = $countriesRepository;
        $this->driversRepository = $driversRepository;
        $this->grandPrixsRepository = $grandPrixsRepository;
        $this->hashtagsRepository = $hashtagsRepository;
        $this->languagesRepository = $languagesRepository;
        $this->seasonsRacesRepository = $seasonsRacesRepository;
        $this->seasonsRepository = $seasonsRepository;
        $this->settingsRepository = $settingsRepository;
        $this->tracksRepository = $tracksRepository;
        $this->twitterAccountsRepository = $twitterAccountsRepository;
        $this->usersRepository = $usersRepository;
        $this->youTubeVideosRepository = $youTubeVideosRepository;
    }

    /**
     * Shows the backend dashboard.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $countries = $this->countriesRepository->count();
        $drivers = $this->driversRepository->count();
        $gps = $this->grandPrixsRepository->count();
        $hashtags = $this->hashtagsRepository->count();
        $langs = $this->languagesRepository->count();
        $seasons = $this->seasonsRepository->count();
        $seasonsRaces = $this->seasonsRacesRepository->count();
        $settings = $this->settingsRepository->getAll();
        $tracks = $this->tracksRepository->count();
        $twitterAccounts = $this->twitterAccountsRepository->count();
        $users = $this->usersRepository->count();
        $videos = $this->youTubeVideosRepository->count();

        return view('backend.index')
            ->with('countries', $countries)
            ->with('drivers', $drivers)
            ->with('gps', $gps)
            ->with('hashtags', $hashtags)
            ->with('langs', $langs)
            ->with('seasons', $seasons)
            ->with('seasonsRaces', $seasonsRaces)
            ->with('settings', $settings)
            ->with('tracks', $tracks)
            ->with('twitterAccounts', $twitterAccounts)
            ->with('users', $users)
            ->with('videos', $videos);
    }

    /**
     * Stores settings.
     *
     * @param SettingsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveSettings(SettingsRequest $request)
    {
        foreach ($request->except('_token') as $key => $value) {
            $this->settingsRepository->setSettings((string) $key, $value);
        }

        return redirect()
            ->route('backend');
    }
}
