<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\GrandPrixs\Requests\AddGrandPrixsRequest;
use App\paddock\GrandPrixs\Requests\EditGrandPrixsRequest;
use App\paddock\Countries\Repositories\CountriesRepository;
use App\paddock\GrandPrixs\Repositories\GrandPrixsRepository;

class GrandPrixsController extends Controller
{
    /**
     * @var CountriesRepository
     */
    private $countriesRepository;

    /**
     * @var GrandPrixsRepository
     */
    private $grandPrixsRepository;

    /**
     * GrandPrixsController constructor.
     * @param CountriesRepository $countriesRepository
     * @param GrandPrixsRepository $grandPrixsRepository
     */
    public function __construct(
        GrandPrixsRepository $grandPrixsRepository,
        CountriesRepository $countriesRepository
    ) {
        $this->countriesRepository = $countriesRepository;
        $this->grandPrixsRepository = $grandPrixsRepository;
    }

    /**
     * Shows a list of all grand prix.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $gps = $this->grandPrixsRepository->getAll();

        return view('backend.grandprixs.index')
            ->with('gps', $gps);
    }

    /**
     * Shows the form for adding grand prix.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        $countries = $this->countriesRepository->getAll();

        return view('backend.grandprixs.add')
            ->with('countries', $countries);
    }

    /**
     * Stores new grand prix.
     *
     * @param AddGrandPrixsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddGrandPrixsRequest $request)
    {
        $data = [
            'country_id' => $request->input('country_id'),
            'name' => $request->input('name'),
            'full_name' => $request->input('full_name'),
            'hashtag' => $request->input('hashtag'),
            'emoji' => $request->input('emoji'),
            'timezone' => $request->input('timezone'),
            'active' => $request->has('active'),
        ];

        $this->grandPrixsRepository->store($data);

        return redirect()
            ->route('backend.gps');
    }

    /**
     * Get the grand prix by its id.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $gp = $this->grandPrixsRepository->getGrandPrixByID($id);

        $countries = $this->countriesRepository->getAll();

        return view('backend.grandprixs.edit')
            ->with('countries', $countries)
            ->with('gp', $gp);
    }

    /**
     * Updates the grand prix.
     *
     * @param int $id
     * @param EditGrandPrixsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, EditGrandPrixsRequest $request)
    {
        $data = [
            'country_id' => $request->input('country_id'),
            'name' => $request->input('name'),
            'full_name' => $request->input('full_name'),
            'hashtag' => $request->input('hashtag'),
            'emoji' => $request->input('emoji'),
            'timezone' => $request->input('timezone'),
            'active' => $request->has('active'),
        ];

        $this->grandPrixsRepository->update($id, $data);

        return redirect()
            ->route('backend.gps');
    }
}
