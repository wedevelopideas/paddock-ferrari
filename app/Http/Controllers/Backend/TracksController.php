<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Tracks\Requests\AddTracksRequest;
use App\paddock\Tracks\Requests\EditTracksRequest;
use App\paddock\Tracks\Repositories\TracksRepository;
use App\paddock\Countries\Repositories\CountriesRepository;

class TracksController extends Controller
{
    /**
     * @var CountriesRepository
     */
    private $countriesRepository;

    /**
     * @var TracksRepository
     */
    private $tracksRepository;

    /**
     * TracksController constructor.
     * @param CountriesRepository $countriesRepository
     * @param TracksRepository $tracksRepository
     */
    public function __construct(
        TracksRepository $tracksRepository,
        CountriesRepository $countriesRepository
    ) {
        $this->countriesRepository = $countriesRepository;
        $this->tracksRepository = $tracksRepository;
    }

    /**
     * Shows a list of all tracks.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $tracks = $this->tracksRepository->getAll();

        return view('backend.tracks.index')
            ->with('tracks', $tracks);
    }

    /**
     * Shows the form for adding tracks.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        $countries = $this->countriesRepository->getAll();

        return view('backend.tracks.add')
            ->with('countries', $countries);
    }

    /**
     * Stores new tracks.
     *
     * @param AddTracksRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddTracksRequest $request)
    {
        $data = [
            'country_id' => $request->input('country_id'),
            'name' => $request->input('name'),
            'laps' => $request->input('laps'),
            'length' => $request->input('length'),
            'active' => $request->has('active'),
        ];

        $this->tracksRepository->store($data);

        return redirect()
            ->route('backend.tracks');
    }

    /**
     * Get the track by its id.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $track = $this->tracksRepository->getTrackByID($id);

        $countries = $this->countriesRepository->getAll();

        return view('backend.tracks.edit')
            ->with('countries', $countries)
            ->with('track', $track);
    }

    /**
     * Stores new tracks.
     *
     * @param int $id
     * @param EditTracksRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, EditTracksRequest $request)
    {
        $data = [
            'country_id' => $request->input('country_id'),
            'name' => $request->input('name'),
            'laps' => $request->input('laps'),
            'length' => $request->input('length'),
            'active' => $request->has('active'),
        ];

        $this->tracksRepository->update($id, $data);

        return redirect()
            ->route('backend.tracks');
    }
}
