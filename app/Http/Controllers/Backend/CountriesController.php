<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Countries\Requests\AddCountriesRequest;
use App\paddock\Countries\Requests\EditCountriesRequest;
use App\paddock\Countries\Repositories\CountriesRepository;

class CountriesController extends Controller
{
    /**
     * @var CountriesRepository
     */
    private $countriesRepository;

    /**
     * CountriesController constructor.
     * @param CountriesRepository $countriesRepository
     */
    public function __construct(CountriesRepository $countriesRepository)
    {
        $this->countriesRepository = $countriesRepository;
    }

    /**
     * Shows a list of all countries.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $countries = $this->countriesRepository->getAll();

        return view('backend.countries.index')
            ->with('countries', $countries);
    }

    /**
     * Shows the form for adding countries.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        return view('backend.countries.add');
    }

    /**
     * Stores new countries.
     *
     * @param AddCountriesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddCountriesRequest $request)
    {
        $data = [
            'code' => $request->input('code'),
            'name' => $request->input('name'),
        ];

        $this->countriesRepository->store($data);

        return redirect()
            ->route('backend.countries');
    }

    /**
     * Get the country by its code.
     *
     * @param string $code
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(string $code)
    {
        $country = $this->countriesRepository->getCountryByCode($code);

        return view('backend.countries.edit')
            ->with('country', $country);
    }

    /**
     * Updates the country.
     *
     * @param string $code
     * @param EditCountriesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(string $code, EditCountriesRequest $request)
    {
        $data = [
            'code' => $request->input('code'),
            'name' => $request->input('name'),
        ];

        $this->countriesRepository->update($code, $data);

        return redirect()
            ->route('backend.countries');
    }
}
