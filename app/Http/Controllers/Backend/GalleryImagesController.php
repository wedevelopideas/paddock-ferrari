<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Media\Gallery\Requests\AddGalleryImageRequest;
use App\paddock\Media\Gallery\Requests\EditGalleryImageRequest;
use App\paddock\Media\Gallery\Repositories\GalleryAlbumsRepository;
use App\paddock\Media\Gallery\Repositories\GalleryImagesRepository;

class GalleryImagesController extends Controller
{
    /**
     * @var GalleryAlbumsRepository
     */
    private $galleryAlbumsRepository;

    /**
     * @var GalleryImagesRepository
     */
    private $galleryImagesRepository;

    /**
     * GalleryImagesController constructor.
     * @param GalleryAlbumsRepository $galleryAlbumsRepository
     * @param GalleryImagesRepository $galleryImagesRepository
     */
    public function __construct(
        GalleryImagesRepository $galleryImagesRepository,
        GalleryAlbumsRepository $galleryAlbumsRepository
    ) {
        $this->galleryAlbumsRepository = $galleryAlbumsRepository;
        $this->galleryImagesRepository = $galleryImagesRepository;
    }

    /**
     * Get all images of the album.
     *
     * @param int $album_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(int $album_id)
    {
        $album = $this->galleryAlbumsRepository->getAlbumByID($album_id);
        $images = $this->galleryImagesRepository->getAllImagesByAlbumID($album_id);

        return view('backend.gallery.images.view')
            ->with('album', $album)
            ->with('images', $images);
    }

    /**
     * Shows the form for adding gallery images.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(int $id)
    {
        $album = $this->galleryAlbumsRepository->getAlbumByID($id);

        return view('backend.gallery.images.add')
            ->with('album', $album);
    }

    /**
     * Stores gallery images.
     *
     * @param int $album_id
     * @param AddGalleryImageRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(int $album_id, AddGalleryImageRequest $request)
    {
        $data = [
            'album_id' => $album_id,
            'creator_id' => auth()->id(),
            'image' => $request->input('image'),
            'source' => $request->input('source'),
        ];

        $this->galleryImagesRepository->store($data);

        return redirect()
            ->route('backend.gallery.images.view', ['album_id' => $album_id]);
    }

    /**
     * Get image by album id and id.
     *
     * @param int $album_id
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $album_id, int $id)
    {
        $album = $this->galleryAlbumsRepository->getAlbumByID($album_id);
        $image = $this->galleryImagesRepository->getImageByAlbumIDAndID($album_id, $id);

        return view('backend.gallery.images.edit')
            ->with('album', $album)
            ->with('image', $image);
    }

    /**
     * Updates gallery images.
     *
     * @param int $album_id
     * @param int $id
     * @param EditGalleryImageRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $album_id, int $id, EditGalleryImageRequest $request)
    {
        $data = [
            'album_id' => $album_id,
            'creator_id' => auth()->id(),
            'image' => $request->input('image'),
            'source' => $request->input('source'),
        ];

        $this->galleryImagesRepository->update($id, $data);

        return redirect()
            ->route('backend.gallery.images.view', ['album_id' => $album_id]);
    }
}
