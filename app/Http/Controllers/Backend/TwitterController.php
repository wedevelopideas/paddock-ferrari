<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Twitter\Requests\AddTwitterAccountRequest;
use App\paddock\Twitter\Requests\EditTwitterAccountRequest;
use App\paddock\Twitter\Repositories\TwitterAccountsRepository;

class TwitterController extends Controller
{
    /**
     * @var TwitterAccountsRepository
     */
    private $twitterAccountsRepository;

    /**
     * TwitterController constructor.
     * @param TwitterAccountsRepository $twitterAccountsRepository
     */
    public function __construct(TwitterAccountsRepository $twitterAccountsRepository)
    {
        $this->twitterAccountsRepository = $twitterAccountsRepository;
    }

    /**
     * Get a list of all Twitter accounts.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $accounts = $this->twitterAccountsRepository->getAll();

        return view('backend.twitter.index')
            ->with('accounts', $accounts);
    }

    /**
     * Shows the form for adding Twitter accounts.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        return view('backend.twitter.add');
    }

    /**
     * Stores the new Twitter account.
     *
     * @param AddTwitterAccountRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddTwitterAccountRequest $request)
    {
        $data = [
            'twitter_id' => $request->input('twitter_id'),
            'name' => $request->input('name'),
            'screen_name' => $request->input('screen_name'),
            'description' => $request->input('description'),
            'profile_image_url' => $request->input('profile_image_url'),
            'owner' => $request->has('owner'),
        ];

        $this->twitterAccountsRepository->store($data);

        return redirect()
            ->route('backend.twitter');
    }

    /**
     * Get the account by id.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $account = $this->twitterAccountsRepository->getAccountByID($id);

        return view('backend.twitter.edit')
            ->with('account', $account);
    }

    /**
     * Updates the Twitter account.
     *
     * @param int $id
     * @param EditTwitterAccountRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, EditTwitterAccountRequest $request)
    {
        $data = [
            'twitter_id' => $request->input('twitter_id'),
            'name' => $request->input('name'),
            'screen_name' => $request->input('screen_name'),
            'description' => $request->input('description'),
            'profile_image_url' => $request->input('profile_image_url'),
            'owner' => $request->has('owner'),
        ];

        $this->twitterAccountsRepository->update($id, $data);

        return redirect()
            ->route('backend.twitter');
    }
}
