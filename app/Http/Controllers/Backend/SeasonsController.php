<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Seasons\Requests\AddSeasonsRequest;
use App\paddock\Seasons\Requests\EditSeasonsRequest;
use App\paddock\Seasons\Repositories\SeasonsRepository;

class SeasonsController extends Controller
{
    /**
     * @var SeasonsRepository
     */
    private $seasonsRepository;

    /**
     * SeasonsController constructor.
     * @param SeasonsRepository $seasonsRepository
     */
    public function __construct(SeasonsRepository $seasonsRepository)
    {
        $this->seasonsRepository = $seasonsRepository;
    }

    /**
     * Shows a list of all seasons.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $seasons = $this->seasonsRepository->getAll();

        return view('backend.seasons.index')
            ->with('seasons', $seasons);
    }

    /**
     * Shows the form for adding seasons.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        return view('backend.seasons.add');
    }

    /**
     * Stores new seasons.
     *
     * @param AddSeasonsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddSeasonsRequest $request)
    {
        $data = [
            'season' => $request->input('season'),
        ];

        $this->seasonsRepository->store($data);

        return redirect()
            ->route('backend.seasons');
    }

    /**
     * Get the season by its year.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $season = $this->seasonsRepository->getSeasonByID($id);

        return view('backend.seasons.edit')
            ->with('season', $season);
    }

    /**
     * Updates the seasons.
     *
     * @param int $id
     * @param EditSeasonsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, EditSeasonsRequest $request)
    {
        $data = [
            'season' => $request->input('season'),
        ];

        $this->seasonsRepository->update($id, $data);

        return redirect()
            ->route('backend.seasons');
    }
}
