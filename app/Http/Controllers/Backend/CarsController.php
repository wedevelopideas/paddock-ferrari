<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Cars\Requests\AddCarsRequest;
use App\paddock\Cars\Requests\EditCarsRequest;
use App\paddock\Cars\Repository\CarsRepository;

class CarsController extends Controller
{
    /**
     * @var CarsRepository
     */
    private $carsRepository;

    /**
     * CarsController constructor.
     * @param CarsRepository $carsRepository
     */
    public function __construct(CarsRepository $carsRepository)
    {
        $this->carsRepository = $carsRepository;
    }

    /**
     * Shows a list of all cars.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $cars = $this->carsRepository->getAll();

        return view('backend.cars.index')
            ->with('cars', $cars);
    }

    /**
     * Shows the form for adding cars.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        return view('backend.cars.add');
    }

    /**
     * Stores new cars.
     *
     * @param AddCarsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddCarsRequest $request)
    {
        $data = [
            'name' => $request->input('name'),
        ];

        $this->carsRepository->store($data);

        return redirect()
            ->route('backend.cars');
    }

    /**
     * Get the car by its id.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $car = $this->carsRepository->getCarByID($id);

        return view('backend.cars.edit')
            ->with('car', $car);
    }

    /**
     * Updates the car.
     *
     * @param int $id
     * @param EditCarsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, EditCarsRequest $request)
    {
        $data = [
            'name' => $request->input('name'),
        ];

        $this->carsRepository->update($id, $data);

        return redirect()
            ->route('backend.cars');
    }
}
