<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Drivers\Repositories\DriversRepository;
use App\paddock\Seasons\Repositories\SeasonsRepository;
use App\paddock\GrandPrixs\Repositories\GrandPrixsRepository;
use App\paddock\Reports\Requests\AddReportsTechnicalsRequest;
use App\paddock\Reports\Repositories\ReportsTechnicalsRepository;

class ReportsTechnicalsController extends Controller
{
    /**
     * @var DriversRepository
     */
    private $driversRepository;

    /**
     * @var GrandPrixsRepository
     */
    private $grandPrixsRepository;

    /**
     * @var ReportsTechnicalsRepository
     */
    private $reportsTechnicalsRepository;

    /**
     * @var SeasonsRepository
     */
    private $seasonsRepository;

    /**
     * ReportsTechnicalsController constructor.
     * @param DriversRepository $driversRepository
     * @param GrandPrixsRepository $grandPrixsRepository
     * @param ReportsTechnicalsRepository $reportsTechnicalsRepository
     * @param SeasonsRepository $seasonsRepository
     */
    public function __construct(
        DriversRepository $driversRepository,
        GrandPrixsRepository $grandPrixsRepository,
        ReportsTechnicalsRepository $reportsTechnicalsRepository,
        SeasonsRepository $seasonsRepository
    ) {
        $this->driversRepository = $driversRepository;
        $this->grandPrixsRepository = $grandPrixsRepository;
        $this->reportsTechnicalsRepository = $reportsTechnicalsRepository;
        $this->seasonsRepository = $seasonsRepository;
    }

    /**
     * Shows the form for adding technical reports.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        $drivers = $this->driversRepository->getAll();
        $gps = $this->grandPrixsRepository->getAll();
        $seasons = $this->seasonsRepository->getAll();

        return view('backend.reports.technicals.add')
            ->with('drivers', $drivers)
            ->with('gps', $gps)
            ->with('seasons', $seasons);
    }

    /**
     * Stores new technical reports.
     *
     * @param AddReportsTechnicalsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddReportsTechnicalsRequest $request)
    {
        $data = [
            'season' => $request->input('season'),
            'race' => $request->input('race'),
            'gp_id' => $request->input('gp_id'),
            'driver_id' => $request->input('driver_id'),
            'ice' => $request->input('ice'),
            'tc' => $request->input('tc'),
            'mguh' => $request->input('mguh'),
            'mguk' => $request->input('mguk'),
            'es' => $request->input('es'),
            'ce' => $request->input('ce'),
        ];

        $this->reportsTechnicalsRepository->store($data);

        return redirect()
            ->route('backend.reports');
    }
}
