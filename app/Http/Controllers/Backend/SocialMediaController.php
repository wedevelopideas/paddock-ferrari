<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\SocialMedias\Requests\AddSocialMediasRequest;
use App\paddock\SocialMedias\Requests\EditSocialMediasRequest;
use App\paddock\SocialMedias\Repositories\SocialMediasRepository;

class SocialMediaController extends Controller
{
    /**
     * @var SocialMediasRepository
     */
    private $socialMediasRepository;

    /**
     * SocialMediaController constructor.
     * @param SocialMediasRepository $socialMediasRepository
     */
    public function __construct(SocialMediasRepository $socialMediasRepository)
    {
        $this->socialMediasRepository = $socialMediasRepository;
    }

    /**
     * Shows the social media backend.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $socials = $this->socialMediasRepository->getAll();

        return view('backend.socialmedia.index')
            ->with('socials', $socials);
    }

    /**
     * Shows the form for adding social media.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        return view('backend.socialmedia.add');
    }

    /**
     * Stores new social media.
     *
     * @param AddSocialMediasRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddSocialMediasRequest $request)
    {
        $data = [
            'name' => $request->input('name'),
            'image' => $request->input('image'),
            'website' => $request->input('website'),
            'facebook' => $request->input('facebook'),
            'twitter' => $request->input('twitter'),
            'instagram' => $request->input('instagram'),
            'youtube' => $request->input('youtube'),
        ];

        $this->socialMediasRepository->store($data);

        return redirect()
            ->route('backend.socialmedia');
    }

    /**
     * Get the social media entry by its id.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $social = $this->socialMediasRepository->getSocialMediaByID($id);

        return view('backend.socialmedia.edit')
            ->with('social', $social);
    }

    /**
     * Updates social medias.
     *
     * @param int $id
     * @param EditSocialMediasRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, EditSocialMediasRequest $request)
    {
        $data = [
            'name' => $request->input('name'),
            'image' => $request->input('image'),
            'website' => $request->input('website'),
            'facebook' => $request->input('facebook'),
            'twitter' => $request->input('twitter'),
            'instagram' => $request->input('instagram'),
            'youtube' => $request->input('youtube'),
        ];

        $this->socialMediasRepository->update($id, $data);

        return redirect()
            ->route('backend.socialmedia');
    }
}
