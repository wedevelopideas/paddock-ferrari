<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Tracks\Repositories\TracksRepository;
use App\paddock\Seasons\Repositories\SeasonsRepository;
use App\paddock\Seasons\Requests\AddSeasonsRacesRequest;
use App\paddock\Seasons\Requests\EditSeasonsRacesRequest;
use App\paddock\Seasons\Repositories\SeasonsRacesRepository;
use App\paddock\GrandPrixs\Repositories\GrandPrixsRepository;

class SeasonsRacesController extends Controller
{
    /**
     * @var GrandPrixsRepository
     */
    private $grandPrixsRepository;

    /**
     * @var SeasonsRacesRepository
     */
    private $seasonsRacesRepository;

    /**
     * @var SeasonsRepository
     */
    private $seasonsRepository;

    /**
     * @var TracksRepository
     */
    private $tracksRepository;

    /**
     * SeasonsRacesController constructor.
     * @param GrandPrixsRepository $grandPrixsRepository
     * @param SeasonsRepository $seasonsRepository
     * @param SeasonsRacesRepository $seasonsRacesRepository
     * @param TracksRepository $tracksRepository
     */
    public function __construct(
        GrandPrixsRepository $grandPrixsRepository,
        SeasonsRacesRepository $seasonsRacesRepository,
        SeasonsRepository $seasonsRepository,
        TracksRepository $tracksRepository
    ) {
        $this->grandPrixsRepository = $grandPrixsRepository;
        $this->seasonsRacesRepository = $seasonsRacesRepository;
        $this->seasonsRepository = $seasonsRepository;
        $this->tracksRepository = $tracksRepository;
    }

    /**
     * Shows all races of a season.
     *
     * @param int $year
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(int $year)
    {
        $season = $this->seasonsRepository->getSeasonBySeason($year);

        $races = $this->seasonsRacesRepository->getRacesBySeason($year);

        return view('backend.seasons.races.index')
            ->with('races', $races)
            ->with('season', $season);
    }

    /**
     * Shows the form for adding season races.
     *
     * @param int $season
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(int $season)
    {
        $gps = $this->grandPrixsRepository->getAll();
        $seasons = $this->seasonsRepository->getAll();
        $tracks = $this->tracksRepository->getAll();

        return view('backend.seasons.races.add')
            ->with('gps', $gps)
            ->with('seasons', $seasons)
            ->with('tracks', $tracks)
            ->with('year', $season);
    }

    /**
     * Stores new season races.
     *
     * @param int $season
     * @param AddSeasonsRacesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(int $season, AddSeasonsRacesRequest $request)
    {
        $data = [
            'season' => $request->input('season'),
            'gp_id' => $request->input('gp_id'),
            'track_id' => $request->input('track_id'),
            'raceday' => $request->input('raceday'),
            'status' => $request->input('status'),
        ];

        $this->seasonsRacesRepository->store($data);

        return redirect()
            ->route('backend.seasons.races', ['season' => $season]);
    }

    /**
     * Get season races by season and id.
     *
     * @param int $season
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $season, int $id)
    {
        $race = $this->seasonsRacesRepository->getRaceByID($id);

        $gps = $this->grandPrixsRepository->getAll();
        $seasons = $this->seasonsRepository->getAll();
        $tracks = $this->tracksRepository->getAll();

        return view('backend.seasons.races.edit')
            ->with('gps', $gps)
            ->with('race', $race)
            ->with('seasons', $seasons)
            ->with('tracks', $tracks)
            ->with('year', $season);
    }

    /**
     * Updates the season races.
     *
     * @param int $season
     * @param int $id
     * @param EditSeasonsRacesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $season, int $id, EditSeasonsRacesRequest $request)
    {
        $data = [
            'season' => $request->input('season'),
            'gp_id' => $request->input('gp_id'),
            'track_id' => $request->input('track_id'),
            'raceday' => $request->input('raceday'),
            'status' => $request->input('status'),
        ];

        $this->seasonsRacesRepository->update($id, $data);

        return redirect()
            ->route('backend.seasons.races', ['season' => $season]);
    }
}
