<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Hashtags\Requests\AddHashtagsRequest;
use App\paddock\Hashtags\Requests\EditHashtagsRequest;
use App\paddock\Hashtags\Repositories\HashtagsRepository;

class HashtagsController extends Controller
{
    /**
     * @var HashtagsRepository
     */
    private $hashtagsRepository;

    /**
     * HashtagsController constructor.
     * @param HashtagsRepository $hashtagsRepository
     */
    public function __construct(HashtagsRepository $hashtagsRepository)
    {
        $this->hashtagsRepository = $hashtagsRepository;
    }

    /**
     * Shows a list of all hashtags.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $hashtags = $this->hashtagsRepository->getAll();

        return view('backend.hashtags.index')
            ->with('hashtags', $hashtags);
    }

    /**
     * Shows the form for adding hashtags.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        return view('backend.hashtags.add');
    }

    /**
     * Stores new hashtags.
     *
     * @param AddHashtagsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddHashtagsRequest $request)
    {
        $data = [
            'name' => $request->input('name'),
        ];

        $this->hashtagsRepository->store($data);

        return redirect()
            ->route('backend.hashtags');
    }

    /**
     * Get the hashtag by its id.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $hashtag = $this->hashtagsRepository->getHashtagByID($id);

        return view('backend.hashtags.edit')
            ->with('hashtag', $hashtag);
    }

    /**
     * Updates the hashtags.
     *
     * @param int $id
     * @param EditHashtagsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, EditHashtagsRequest $request)
    {
        $data = [
            'name' => $request->input('name'),
        ];

        $this->hashtagsRepository->update($id, $data);

        return redirect()
            ->route('backend.hashtags');
    }
}
