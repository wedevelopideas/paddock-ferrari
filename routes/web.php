<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Backend', 'prefix' => 'backend', 'middleware' => 'auth'], function () {
    Route::get('', ['uses' => 'DashboardController@index', 'as' => 'backend']);
    Route::post('', ['uses' => 'DashboardController@saveSettings', 'as' => 'backend.settings.save']);

    Route::group(['prefix' => 'cars'], function () {
        Route::get('', ['uses' => 'CarsController@index', 'as' => 'backend.cars']);
        Route::get('add', ['uses' => 'CarsController@add', 'as' => 'backend.cars.add']);
        Route::post('add', ['uses' => 'CarsController@store']);
        Route::get('{id}/update', ['uses' => 'CarsController@edit', 'as' => 'backend.cars.edit']);
        Route::post('{id}/update', ['uses' => 'CarsController@update']);
    });

    Route::group(['prefix' => 'countries'], function () {
        Route::get('', ['uses' => 'CountriesController@index', 'as' => 'backend.countries']);
        Route::get('add', ['uses' => 'CountriesController@add', 'as' => 'backend.countries.add']);
        Route::post('add', ['uses' => 'CountriesController@store']);
        Route::get('{code}/update', ['uses' => 'CountriesController@edit', 'as' => 'backend.countries.edit']);
        Route::post('{code}/update', ['uses' => 'CountriesController@update']);
    });

    Route::group(['prefix' => 'drivers'], function () {
        Route::get('', ['uses' => 'DriversController@index', 'as' => 'backend.drivers']);
        Route::get('add', ['uses' => 'DriversController@add', 'as' => 'backend.drivers.add']);
        Route::post('add', ['uses' => 'DriversController@store']);
        Route::get('{id}/edit', ['uses' => 'DriversController@edit', 'as' => 'backend.drivers.edit']);
        Route::post('{id}/edit', ['uses' => 'DriversController@update']);
        Route::get('{slug}', ['uses' => 'DriversController@view', 'as' => 'backend.drivers.view']);
    });

    Route::group(['prefix' => 'gallery'], function () {
        Route::get('', ['uses' => 'GalleryController@index', 'as' => 'backend.gallery']);

        Route::group(['prefix' => 'album'], function () {
            Route::get('add', ['uses' => 'GalleryAlbumsController@add', 'as' => 'backend.gallery.album.add']);
            Route::post('add', ['uses' => 'GalleryAlbumsController@store']);
            Route::get('{id}/edit', ['uses' => 'GalleryAlbumsController@edit', 'as' => 'backend.gallery.album.edit']);
            Route::post('{id}/edit', ['uses' => 'GalleryAlbumsController@update']);
        });

        Route::group(['prefix' => 'images'], function () {
            Route::get('{album_id}', ['uses' => 'GalleryImagesController@view', 'as' => 'backend.gallery.images.view']);
            Route::get('{album_id}/add', ['uses' => 'GalleryImagesController@add', 'as' => 'backend.gallery.images.add']);
            Route::post('{album_id}/add', ['uses' => 'GalleryImagesController@store']);
            Route::get('{album_id}/{id}/edit', ['uses' => 'GalleryImagesController@edit', 'as' => 'backend.gallery.images.edit']);
            Route::post('{album_id}/{id}/edit', ['uses' => 'GalleryImagesController@update']);
        });
    });

    Route::group(['prefix' => 'grandprixs'], function () {
        Route::get('', ['uses' => 'GrandPrixsController@index', 'as' => 'backend.gps']);
        Route::get('add', ['uses' => 'GrandPrixsController@add', 'as' => 'backend.gps.add']);
        Route::post('add', ['uses' => 'GrandPrixsController@store']);
        Route::get('{id}/edit', ['uses' => 'GrandPrixsController@edit', 'as' => 'backend.gps.edit']);
        Route::post('{id}/edit', ['uses' => 'GrandPrixsController@update']);
    });

    Route::group(['prefix' => 'langs'], function () {
        Route::get('', ['uses' => 'LanguagesController@index', 'as' => 'backend.langs']);
        Route::get('add', ['uses' => 'LanguagesController@add', 'as' => 'backend.langs.add']);
        Route::post('add', ['uses' => 'LanguagesController@store']);
        Route::get('{code}/update', ['uses' => 'LanguagesController@edit', 'as' => 'backend.langs.edit']);
        Route::post('{code}/update', ['uses' => 'LanguagesController@update']);
    });

    Route::group(['prefix' => 'reports'], function () {
        Route::get('', ['uses' => 'ReportsController@index', 'as' => 'backend.reports']);

        Route::group(['prefix' => 'technicals'], function () {
            Route::get('add', ['uses' => 'ReportsTechnicalsController@add', 'as' => 'backend.reports.technicals.add']);
            Route::post('add', ['uses' => 'ReportsTechnicalsController@store']);
        });

        Route::group(['prefix' => 'tires'], function () {
            Route::get('add', ['uses' => 'ReportsTiresController@add', 'as' => 'backend.reports.tires.add']);
            Route::post('add', ['uses' => 'ReportsTiresController@store']);
            Route::get('add/2019', ['uses' => 'ReportsTiresController@add2019', 'as' => 'backend.reports.tires.add.2019']);
            Route::post('add/2019', ['uses' => 'ReportsTiresController@store2019']);
        });

        Route::get('{season}', ['uses' => 'ReportsController@season', 'as' => 'backend.reports.season']);
        Route::get('{season}/{slug}', ['uses' => 'ReportsController@view', 'as' => 'backend.reports.view']);
    });

    Route::group(['prefix' => 'seasons'], function () {
        Route::get('', ['uses' => 'SeasonsController@index', 'as' => 'backend.seasons']);
        Route::get('add', ['uses' => 'SeasonsController@add', 'as' => 'backend.seasons.add']);
        Route::post('add', ['uses' => 'SeasonsController@store']);
        Route::get('{id}/edit', ['uses' => 'SeasonsController@edit', 'as' => 'backend.seasons.edit']);
        Route::post('{id}/edit', ['uses' => 'SeasonsController@update']);

        Route::group(['prefix' => '{season}'], function () {
            Route::group(['prefix' => 'drivers'], function () {
                Route::get('', ['uses' => 'SeasonsDriversController@index', 'as' => 'backend.seasons.drivers']);
                Route::get('add', ['uses' => 'SeasonsDriversController@add', 'as' => 'backend.seasons.drivers.add']);
                Route::post('add', ['uses' => 'SeasonsDriversController@store']);
                Route::get('{id}/edit', ['uses' => 'SeasonsDriversController@edit', 'as' => 'backend.seasons.drivers.edit']);
                Route::post('{id}/edit', ['uses' => 'SeasonsDriversController@update']);
            });

            Route::get('races', ['uses' => 'SeasonsRacesController@index', 'as' => 'backend.seasons.races']);
            Route::get('races/add', ['uses' => 'SeasonsRacesController@add', 'as' => 'backend.seasons.races.add']);
            Route::post('races/add', ['uses' => 'SeasonsRacesController@store']);
            Route::get('races/{id}/edit', ['uses' => 'SeasonsRacesController@edit', 'as' => 'backend.seasons.races.edit']);
            Route::post('races/{id}/edit', ['uses' => 'SeasonsRacesController@update']);
        });
    });

    Route::group(['prefix' => 'socialmedia'], function () {
        Route::get('', ['uses' => 'SocialMediaController@index', 'as' => 'backend.socialmedia']);
        Route::get('add', ['uses' => 'SocialMediaController@add', 'as' => 'backend.socialmedia.add']);
        Route::post('add', ['uses' => 'SocialMediaController@store']);
        Route::get('{id}/edit', ['uses' => 'SocialMediaController@edit', 'as' => 'backend.socialmedia.edit']);
        Route::post('{id}/edit', ['uses' => 'SocialMediaController@update']);

        Route::group(['prefix' => 'hashtags'], function () {
            Route::get('', ['uses' => 'HashtagsController@index', 'as' => 'backend.hashtags']);
            Route::get('add', ['uses' => 'HashtagsController@add', 'as' => 'backend.hashtags.add']);
            Route::post('add', ['uses' => 'HashtagsController@store']);
            Route::get('{id}/edit', ['uses' => 'HashtagsController@edit', 'as' => 'backend.hashtags.edit']);
            Route::post('{id}/edit', ['uses' => 'HashtagsController@update']);
        });

        Route::group(['prefix' => 'twitter'], function () {
            Route::get('', ['uses' => 'TwitterController@index', 'as' => 'backend.twitter']);
            Route::get('add', ['uses' => 'TwitterController@add', 'as' => 'backend.twitter.add']);
            Route::post('add', ['uses' => 'TwitterController@store']);
            Route::get('{id}/edit', ['uses' => 'TwitterController@edit', 'as' => 'backend.twitter.edit']);
            Route::post('{id}/edit', ['uses' => 'TwitterController@update']);
        });
    });

    Route::group(['prefix' => 'tracks'], function () {
        Route::get('', ['uses' => 'TracksController@index', 'as' => 'backend.tracks']);
        Route::get('add', ['uses' => 'TracksController@add', 'as' => 'backend.tracks.add']);
        Route::post('add', ['uses' => 'TracksController@store']);
        Route::get('{id}/edit', ['uses' => 'TracksController@edit', 'as' => 'backend.tracks.edit']);
        Route::post('{id}/edit', ['uses' => 'TracksController@update']);
    });

    Route::group(['prefix' => 'users'], function () {
        Route::get('', ['uses' => 'UsersController@index', 'as' => 'backend.users']);
    });
});

Route::group(['namespace' => 'SQL', 'middleware' => 'auth'], function () {
    Route::group(['prefix' => 'sql'], function () {
        Route::get('', ['uses' => 'DashboardController@index', 'as' => 'sql']);
    });
});

Route::group(['namespace' => 'Frontend'], function () {
    Route::group(['namespace' => 'Auth'], function () {
        Route::get('', ['uses' => 'LoginController@showLoginForm', 'as' => 'index']);

        Route::get('login', ['uses' => 'LoginController@showLoginForm', 'as' => 'login']);
        Route::post('login', ['uses' => 'LoginController@login']);
        Route::get('logout', ['uses' => 'LoginController@logout', 'as' => 'logout']);
    });

    Route::group(['middleware' => 'auth'], function () {
        Route::get('dashboard', ['uses' => 'DashboardController@dashboard', 'as' => 'dashboard']);
        Route::get('calendar', ['uses' => 'CalendarController@sessions', 'as' => 'calendar']);
        Route::get('socialmedia', ['uses' => 'DashboardController@socialMedia', 'as' => 'socialmedia']);

        Route::group(['prefix' => 'dashboard'], function () {
            Route::get('race', ['uses' => 'DashboardController@race', 'as' => 'dashboard.race']);
        });

        Route::group(['prefix' => 'drivers'], function () {
            Route::get('', ['uses' => 'DriversController@index', 'as' => 'drivers']);
            Route::get('dates', ['uses' => 'DriversController@dates', 'as' => 'drivers.dates']);
            Route::group(['prefix' => '{slug}'], function () {
                Route::get('', ['uses' => 'DriversController@view', 'as' => 'drivers.view']);
                Route::group(['prefix' => 'stats'], function () {
                    Route::get('wins', ['uses' => 'DriversStatsController@wins', 'as' => 'drivers.stats.wins']);
                });
            });
        });

        Route::group(['prefix' => 'events'], function () {
            Route::get('', ['uses' => 'EventsController@index', 'as' => 'events']);
            Route::get('add', ['uses' => 'EventsController@add', 'as' => 'events.add']);
            Route::post('add', ['uses' => 'EventsController@store']);
            Route::get('{id}/edit', ['uses' => 'EventsController@edit', 'as' => 'events.edit']);
            Route::post('{id}/edit', ['uses' => 'EventsController@update']);
            Route::get('{driver_id}/{date}', ['uses' => 'EventsController@view', 'as' => 'events.view']);
        });

        Route::group(['prefix' => 'feed'], function () {
            Route::get('', ['uses' => 'FeedController@index', 'as' => 'feed']);
            Route::get('meta', ['uses' => 'FeedController@meta', 'as' => 'feed.meta']);
            Route::post('meta/results', ['uses' => 'FeedController@metaResults', 'as' => 'feed.meta.result']);

            Route::group(['prefix' => 'fda'], function () {
                Route::get('', ['uses' => 'FeedFDAController@index', 'as' => 'feed.fda']);
                Route::get('{date}', ['uses' => 'FeedFDAController@view', 'as' => 'feed.fda.view']);
            });

            Route::group(['prefix' => 'ferrari'], function () {
                Route::get('', ['uses' => 'FeedFerrariController@index', 'as' => 'feed.ferrari']);
                Route::get('{date}/{session}', ['uses' => 'FeedFerrariController@view', 'as' => 'feed.ferrari.view']);
            });

            Route::group(['prefix' => 'history'], function () {
                Route::get('', ['uses' => 'FeedHistoryController@index', 'as' => 'feed.history']);
                Route::get('{date}', ['uses' => 'FeedHistoryController@view', 'as' => 'feed.history.view']);
            });

            Route::group(['prefix' => 'motorsport'], function () {
                Route::get('', ['uses' => 'FeedMotorsportController@index', 'as' => 'feed.motorsport']);
                Route::get('add', ['uses' => 'FeedMotorsportController@add', 'as' => 'feed.motorsport.add']);
                Route::post('add', ['uses' => 'FeedMotorsportController@store']);
                Route::get('{id}', ['uses' => 'FeedMotorsportController@view', 'as' => 'feed.motorsport.view']);
            });
        });

        Route::group(['prefix' => 'media'], function () {
            Route::get('', ['uses' => 'MediaController@index', 'as' => 'media']);

            Route::group(['prefix' => 'gallery'], function () {
                Route::get('', ['uses' => 'GalleryController@index', 'as' => 'media.gallery']);
                Route::get('twitter', ['uses' => 'GalleryController@twitter', 'as' => 'media.gallery.twitter']);
                Route::get('{slug}', ['uses' => 'GalleryController@album', 'as' => 'media.gallery.album']);
            });

            Route::group(['prefix' => 'youtube'], function () {
                Route::get('', ['uses' => 'YouTubeController@index', 'as' => 'media.youtube']);
                Route::get('add', ['uses' => 'YouTubeController@add', 'as' => 'media.youtube.add']);
                Route::post('add', ['uses' => 'YouTubeController@store']);
                Route::get('{id}/edit', ['uses' => 'YouTubeController@edit', 'as' => 'media.youtube.edit']);
                Route::post('{id}/edit', ['uses' => 'YouTubeController@update']);
                Route::get('{youtube_id}', ['uses' => 'YouTubeController@view', 'as' => 'media.youtube.view']);
            });
        });

        Route::group(['prefix' => 'results'], function () {
            Route::get('', ['uses' => 'ResultsController@index', 'as' => 'results']);
            Route::get('add', ['uses' => 'ResultsController@add', 'as' => 'results.add']);
            Route::post('add', ['uses' => 'ResultsController@store']);
            Route::get('{slug}', ['uses' => 'ResultsController@grandPrix', 'as' => 'results.gp']);
        });

        Route::group(['prefix' => 'season'], function () {
            Route::get('', ['uses' => 'SeasonsController@index', 'as' => 'season']);

            Route::group(['prefix' => '{season}'], function () {
                Route::group(['prefix' => '{gpslug}'], function () {
                    Route::get('', ['uses' => 'SeasonsRacesController@index', 'as' => 'season.race']);
                    Route::get('live/{session}', ['uses' => 'SeasonsRacesController@live', 'as' => 'season.race.live']);

                    Route::group(['prefix' => 'report'], function () {
                        Route::get('{driverslug}/tires', ['uses' => 'SeasonsRacesController@reportTires', 'as' => 'season.race.report.tires']);
                    });
                });
            });
        });

        Route::group(['prefix' => 'settings'], function () {
            Route::get('', ['uses' => 'SettingsController@index', 'as' => 'settings']);
            Route::post('languages', ['uses' => 'SettingsController@languages', 'as' => 'settings.langs']);
            Route::post('timezones', ['uses' => 'SettingsController@timezones', 'as' => 'settings.timezones']);
        });

        Route::group(['prefix' => 'tests'], function () {
            Route::get('', ['uses' => 'TestsController@index', 'as' => 'tests']);
            Route::get('dashboard', ['uses' => 'DashboardController@tests', 'as' => 'tests.dashboard']);
            Route::get('results/{season}/{week}', ['uses' => 'TestsController@results', 'as' => 'tests.results']);
        });

        Route::get('{user_name}', ['uses' => 'UsersController@index', 'as' => 'user']);
    });
});
