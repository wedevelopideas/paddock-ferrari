<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Seasons\Models\Seasons;

$factory->define(Seasons::class, function (Faker $faker) {
    return [
        'season' => $faker->unique()->year,
    ];
});
