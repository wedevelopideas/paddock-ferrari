<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Users\Models\Users;
use App\paddock\Feed\Models\FeedMotorsport;
use App\paddock\Languages\Models\Languages;

$factory->define(FeedMotorsport::class, function (Faker $faker) {
    return [
        'author_id' => function () {
            return factory(Users::class)->create()->id;
        },
        'lang' => function () {
            return factory(Languages::class)->create()->code;
        },
        'title' => $faker->unique()->title,
        'description' => $faker->text,
        'image' => $faker->imageUrl(),
        'link' => $faker->url,
        'domain' => $faker->domainName,
    ];
});
