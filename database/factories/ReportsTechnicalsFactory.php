<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Drivers\Models\Drivers;
use App\paddock\Seasons\Models\Seasons;
use App\paddock\GrandPrixs\Models\GrandPrixs;
use App\paddock\Reports\Models\ReportsTechnicals;

$factory->define(ReportsTechnicals::class, function (Faker $faker) {
    return [
        'season' => function () {
            return factory(Seasons::class)->create()->id;
        },
        'race' => $faker->randomDigit,
        'gp_id' => function () {
            return factory(GrandPrixs::class)->create()->id;
        },
        'driver_id' => function () {
            return factory(Drivers::class)->create()->id;
        },
        'ice' => random_int(1, 6),
        'tc' => random_int(1, 6),
        'mguh' => random_int(1, 6),
        'mguk' => random_int(1, 6),
        'es' => random_int(1, 6),
        'ce' => random_int(1, 6),
    ];
});
