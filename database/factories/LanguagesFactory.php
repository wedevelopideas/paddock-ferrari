<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Languages\Models\Languages;

$factory->define(Languages::class, function (Faker $faker) {
    return [
        'code' => $faker->unique()->languageCode,
        'name' => $faker->unique()->country,
        'motorsport_link' => $faker->url,
        'news' => $faker->boolean,
        'active' => $faker->boolean,
    ];
});
