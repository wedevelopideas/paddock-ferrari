<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Countries\Models\Countries;
use App\paddock\GrandPrixs\Models\GrandPrixs;

$factory->define(GrandPrixs::class, function (Faker $faker) {
    return [
        'country_id' => function () {
            return factory(Countries::class)->create()->code;
        },
        'name' => $faker->unique()->name,
        'slug' => $faker->slug,
        'full_name' => $faker->name,
        'hashtag' => $faker->text,
        'emoji' => $faker->emoji,
        'timezone' => $faker->timezone,
        'active' => $faker->boolean,
    ];
});
