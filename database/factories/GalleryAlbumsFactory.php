<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Users\Models\Users;
use App\paddock\Media\Gallery\Models\GalleryAlbums;

$factory->define(GalleryAlbums::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'slug' => $faker->slug,
        'cover_id' => random_int(0, 10),
        'creator_id' => function () {
            return factory(Users::class)->create()->id;
        },
    ];
});
