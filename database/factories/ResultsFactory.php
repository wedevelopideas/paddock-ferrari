<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Cars\Models\Cars;
use App\paddock\Tracks\Models\Tracks;
use App\paddock\Drivers\Models\Drivers;
use App\paddock\Results\Models\Results;
use App\paddock\Seasons\Models\Seasons;
use App\paddock\GrandPrixs\Models\GrandPrixs;

$factory->define(Results::class, function (Faker $faker) {
    return [
        'season' => function () {
            return factory(Seasons::class)->create()->id;
        },
        'gp_id' => function () {
            return factory(GrandPrixs::class)->create()->id;
        },
        'track_id' => function () {
            return factory(Tracks::class)->create()->id;
        },
        'session_id' => random_int(1, 5),
        'dateofresult' => $faker->date('Y-m-d'),
        'car_id' => function () {
            return factory(Cars::class)->create()->id;
        },
        'car_nr' => random_int(1, 100),
        'driver_id' => function () {
            return factory(Drivers::class)->create()->id;
        },
        'team' => $faker->boolean,
        'place' => random_int(1, 20),
        'laps' => random_int(1, 100),
        'points' => random_int(0, 25),
        'dnf' => $faker->boolean,
        'dns' => $faker->boolean,
        'dsq' => $faker->boolean,
    ];
});
