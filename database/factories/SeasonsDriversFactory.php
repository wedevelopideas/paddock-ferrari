<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\paddock\Drivers\Models\Drivers;
use App\paddock\Seasons\Models\Seasons;
use App\paddock\Seasons\Models\SeasonsDrivers;

$factory->define(SeasonsDrivers::class, function () {
    return [
        'season' => function () {
            return factory(Seasons::class)->create()->season;
        },
        'driver_id' => function () {
            return factory(Drivers::class)->create()->id;
        },
    ];
});
