<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Tracks\Models\Tracks;
use App\paddock\Drivers\Models\Drivers;
use App\paddock\Seasons\Models\Seasons;
use App\paddock\Tests\Models\TestsResults;

$factory->define(TestsResults::class, function (Faker $faker) {
    return [
        'testday' => $faker->date('Y-m-d'),
        'season' => function () {
            return factory(Seasons::class)->create()->season;
        },
        'week' => random_int(1, 10),
        'track_id' => function () {
            return factory(Tracks::class)->create()->id;
        },
        'driver_id' => function () {
            return factory(Drivers::class)->create()->id;
        },
        'laptime' => random_int(0, 270000),
        'laps' => random_int(0, 200),
    ];
});
