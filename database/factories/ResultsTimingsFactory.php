<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Drivers\Models\Drivers;
use App\paddock\Seasons\Models\Seasons;
use App\paddock\GrandPrixs\Models\GrandPrixs;
use App\paddock\Results\Models\ResultsTimings;

$factory->define(ResultsTimings::class, function (Faker $faker) {
    return [
        'season' => function () {
            return factory(Seasons::class)->create()->season;
        },
        'gp_id' => function () {
            return factory(GrandPrixs::class)->create()->id;
        },
        'session' => $faker->randomElement([1, 2, 3, 4, 5]),
        'driver_id' => function () {
            return factory(Drivers::class)->create()->id;
        },
        'laptime' => $faker->randomDigit,
    ];
});
