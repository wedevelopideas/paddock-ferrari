<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Drivers\Models\Drivers;
use App\paddock\Seasons\Models\Seasons;
use App\paddock\GrandPrixs\Models\GrandPrixs;
use App\paddock\Reports\Models\ReportsTires2019;

$factory->define(ReportsTires2019::class, function (Faker $faker) {
    return [
        'season' => function () {
            return factory(Seasons::class)->create()->id;
        },
        'race' => $faker->randomDigit,
        'gp_id' => function () {
            return factory(GrandPrixs::class)->create()->id;
        },
        'driver_id' => function () {
            return factory(Drivers::class)->create()->id;
        },
        'c1' => random_int(0, 13),
        'c2' => random_int(0, 13),
        'c3' => random_int(0, 13),
        'c4' => random_int(0, 13),
        'c5' => random_int(0, 13),
    ];
});
