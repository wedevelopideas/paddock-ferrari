<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Seasons\Models\Seasons;
use App\paddock\GrandPrixs\Models\GrandPrixs;
use App\paddock\Seasons\Models\SeasonsCalendar;

$factory->define(SeasonsCalendar::class, function (Faker $faker) {
    return [
        'season' => function () {
            return factory(Seasons::class)->create()->season;
        },
        'gp_id' => function () {
            return factory(GrandPrixs::class)->create()->id;
        },
        'session_id' => random_int(1, 5),
        'start' => $faker->time('Y-m-d H:i:s'),
        'end' => $faker->time('Y-m-d H:i:s'),
    ];
});
