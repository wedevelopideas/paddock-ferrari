<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Users\Models\Users;
use App\paddock\Media\Gallery\Models\GalleryAlbums;
use App\paddock\Media\Gallery\Models\GalleryImages;

$factory->define(GalleryImages::class, function (Faker $faker) {
    return [
        'album_id' => function () {
            return factory(GalleryAlbums::class)->create()->id;
        },
        'creator_id' => function () {
            return factory(Users::class)->create()->id;
        },
        'image' => $faker->imageUrl(),
        'source' => $faker->name,
    ];
});
