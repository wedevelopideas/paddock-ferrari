<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Drivers\Models\Drivers;
use App\paddock\Seasons\Models\Seasons;
use App\paddock\Tests\Models\TestsLineup;

$factory->define(TestsLineup::class, function (Faker $faker) {
    return [
        'testday' => $faker->date('Y-m-d'),
        'season' => function () {
            return factory(Seasons::class)->create()->season;
        },
        'week' => random_int(1, 10),
        'driver_id' => function () {
            return factory(Drivers::class)->create()->id;
        },
        'status' => $faker->randomElement([1, 2, 3]),
    ];
});
