<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Media\Youtube\Models\YouTubeVideos;

$factory->define(YouTubeVideos::class, function (Faker $faker) {
    return [
        'youtube_id' => substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_-'), 0, 11),
        'title' => $faker->title,
        'description' => $faker->text,
        'thumbnail' => $faker->imageUrl(),
    ];
});
