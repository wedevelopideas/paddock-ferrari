<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Seasons\Models\Seasons;
use App\paddock\GrandPrixs\Models\GrandPrixs;
use App\paddock\Reports\Models\ReportsNominatedTires;

$factory->define(ReportsNominatedTires::class, function (Faker $faker) {
    return [
        'season' => function () {
            return factory(Seasons::class)->create()->id;
        },
        'race' => $faker->randomDigit,
        'gp_id' => function () {
            return factory(GrandPrixs::class)->create()->id;
        },
        'compound1' => $faker->text,
        'compound2' => $faker->text,
        'compound3' => $faker->text,
    ];
});
