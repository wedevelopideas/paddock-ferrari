<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Drivers\Models\Drivers;
use App\paddock\Countries\Models\Countries;

$factory->define(Drivers::class, function (Faker $faker) {
    return [
        'country_id' => function () {
            return factory(Countries::class)->create()->code;
        },
        'status' => $faker->randomElement([1, 2]),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'name' => $faker->unique()->name,
        'slug' => $faker->slug,
        'hashtag' => $faker->text,
        'dateofbirth' => $faker->date('Y-m-d'),
        'dateofdeath' => $faker->date('Y-m-d'),
        'image' => $faker->imageUrl(),
    ];
});
