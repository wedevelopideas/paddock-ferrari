<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Twitter\Models\TwitterAccounts;

$factory->define(TwitterAccounts::class, function (Faker $faker) {
    return [
        'twitter_id' => random_int(1, 140000000),
        'name' => $faker->name,
        'screen_name' => $faker->userName,
        'description' => $faker->text,
        'profile_image_url' => $faker->imageUrl('400', '400'),
        'owner' => $faker->boolean,
    ];
});
