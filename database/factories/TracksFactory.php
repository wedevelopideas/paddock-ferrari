<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Tracks\Models\Tracks;
use App\paddock\Countries\Models\Countries;

$factory->define(Tracks::class, function (Faker $faker) {
    return [
        'country_id' => function () {
            return factory(Countries::class)->create()->code;
        },
        'name' => $faker->unique()->name,
        'slug' => $faker->slug,
        'laps' => random_int(1, 70),
        'length' => $faker->randomFloat(),
        'active' => $faker->boolean,
    ];
});
