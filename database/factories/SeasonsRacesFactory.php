<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Tracks\Models\Tracks;
use App\paddock\Seasons\Models\SeasonsRaces;
use App\paddock\GrandPrixs\Models\GrandPrixs;

$factory->define(SeasonsRaces::class, function (Faker $faker) {
    return [
        'season' => $faker->unique()->year,
        'gp_id' => function () {
            return factory(GrandPrixs::class)->create()->id;
        },
        'track_id' => function () {
            return factory(Tracks::class)->create()->id;
        },
        'raceday' => $faker->unique()->date('Y-m-d'),
        'status' => $faker->randomElement([0, 1]),
    ];
});
