<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Hashtags\Models\Hashtags;

$factory->define(Hashtags::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name,
        'slug' => $faker->slug,
    ];
});
