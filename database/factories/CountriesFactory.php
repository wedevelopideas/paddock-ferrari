<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Countries\Models\Countries;

$factory->define(Countries::class, function (Faker $faker) {
    return [
        'code' => $faker->unique()->countryCode,
        'name' => $faker->unique()->country,
    ];
});
