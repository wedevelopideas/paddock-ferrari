<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Illuminate\Support\Str;
use Faker\Generator as Faker;
use App\paddock\Users\Models\Users;
use App\paddock\Languages\Models\Languages;

$factory->define(Users::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('password'),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'display_name' => $faker->name,
        'user_name' => $faker->userName,
        'avatar' => $faker->imageUrl(),
        'lang' => function () {
            return factory(Languages::class)->create()->code;
        },
        'timezone' => $faker->timezone,
        'remember_token' => Str::random(10),
    ];
});
