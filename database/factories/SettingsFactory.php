<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Settings\Models\Settings;

$factory->define(Settings::class, function (Faker $faker) {
    return [
        'key' => $faker->text,
        'value' => $faker->text,
    ];
});
