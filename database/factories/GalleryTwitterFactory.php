<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\paddock\Twitter\Models\TwitterAccounts;
use App\paddock\Media\Gallery\Models\GalleryTwitter;

$factory->define(GalleryTwitter::class, function () {
    return [
        'twitter_id' => function () {
            return factory(TwitterAccounts::class)->create()->twitter_id;
        },
        'profile_banner_id' => random_int(1, 100000),
        'profile_image_id' => random_int(1, 100000),
    ];
});
