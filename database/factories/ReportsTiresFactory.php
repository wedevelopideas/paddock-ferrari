<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Drivers\Models\Drivers;
use App\paddock\Seasons\Models\Seasons;
use App\paddock\Reports\Models\ReportsTires;
use App\paddock\GrandPrixs\Models\GrandPrixs;

$factory->define(ReportsTires::class, function (Faker $faker) {
    return [
        'season' => function () {
            return factory(Seasons::class)->create()->id;
        },
        'race' => $faker->randomDigit,
        'gp_id' => function () {
            return factory(GrandPrixs::class)->create()->id;
        },
        'driver_id' => function () {
            return factory(Drivers::class)->create()->id;
        },
        'hypersoft' => random_int(0, 13),
        'ultrasoft' => random_int(0, 13),
        'supersoft' => random_int(0, 13),
        'soft' => random_int(0, 13),
        'medium' => random_int(0, 13),
        'hard' => random_int(0, 13),
        'superhard' => random_int(0, 13),
    ];
});
