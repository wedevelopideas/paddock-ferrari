<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Seasons\Models\Seasons;
use App\paddock\Tests\Models\TestsCalendars;

$factory->define(TestsCalendars::class, function (Faker $faker) {
    return [
        'testday' => $faker->date('Y-m-d'),
        'season' => function () {
            return factory(Seasons::class)->create()->season;
        },
        'week' => random_int(1, 10),
        'track_id' => function () {
            return factory(App\paddock\Tracks\Models\Tracks::class)->create()->id;
        },
    ];
});
