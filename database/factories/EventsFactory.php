<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\paddock\Events\Models\Events;
use App\paddock\Drivers\Models\Drivers;
use App\paddock\Languages\Models\Languages;

$factory->define(Events::class, function (Faker $faker) {
    return [
        'type' => random_int(1, 5),
        'lang' => function () {
            return factory(Languages::class)->create()->code;
        },
        'driver_id' => function () {
            return factory(Drivers::class)->create()->id;
        },
        'event_date' => $faker->date('Y-m-d'),
        'event_text' => $faker->text,
        'event_twitter' => urlencode($faker->text),
        'event_hashtags' => $faker->text,
        'event_link' => $faker->url,
        'event_f1tv_link' => $faker->url,
    ];
});
