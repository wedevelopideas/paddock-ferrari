<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsNominatedTiresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports_nominated_tires', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('season');
            $table->unsignedTinyInteger('race');
            $table->unsignedSmallInteger('gp_id');
            $table->string('compound1');
            $table->string('compound2');
            $table->string('compound3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports_nominated_tires');
    }
}
