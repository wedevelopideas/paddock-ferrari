<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryTwitterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery_twitter', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('twitter_id');
            $table->unsignedInteger('profile_banner_id');
            $table->unsignedInteger('profile_image_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gallery_twitter');
    }
}
