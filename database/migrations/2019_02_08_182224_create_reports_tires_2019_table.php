<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTires2019Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports_tires_2019', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('season');
            $table->unsignedTinyInteger('race');
            $table->unsignedSmallInteger('gp_id');
            $table->unsignedSmallInteger('driver_id');
            $table->unsignedTinyInteger('c1')->nullable();
            $table->unsignedTinyInteger('c2')->nullable();
            $table->unsignedTinyInteger('c3')->nullable();
            $table->unsignedTinyInteger('c4')->nullable();
            $table->unsignedTinyInteger('c5')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports_tires_2019');
    }
}
