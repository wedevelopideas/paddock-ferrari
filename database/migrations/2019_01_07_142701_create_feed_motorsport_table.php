<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedMotorsportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feed_motorsport', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->unsignedSmallInteger('author_id');
            $table->string('lang', 5);
            $table->string('title');
            $table->text('description');
            $table->string('image');
            $table->string('link');
            $table->string('domain');
            $table->nullableTimestamps();

            $table->unique(['lang', 'title']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feed_motorsport');
    }
}
