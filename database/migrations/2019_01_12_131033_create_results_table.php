<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('season');
            $table->unsignedSmallInteger('gp_id');
            $table->unsignedSmallInteger('track_id');
            $table->unsignedSmallInteger('session_id');
            $table->date('dateofresult');
            $table->unsignedSmallInteger('car_id');
            $table->unsignedSmallInteger('car_nr');
            $table->unsignedSmallInteger('driver_id');
            $table->boolean('team');
            $table->unsignedSmallInteger('place');
            $table->unsignedSmallInteger('laps');
            $table->decimal('points', 5, 2);
            $table->boolean('dnf');
            $table->boolean('dns');
            $table->boolean('dsq');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
