<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrandPrixsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grand_prixs', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('country_id', 5);
            $table->string('name');
            $table->string('slug');
            $table->string('full_name');
            $table->string('hashtag');
            $table->string('emoji', 5);
            $table->string('timezone');
            $table->boolean('active');

            $table->unique('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grand_prixs');
    }
}
