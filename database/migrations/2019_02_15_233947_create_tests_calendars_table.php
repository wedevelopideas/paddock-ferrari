<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestsCalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests_calendars', function (Blueprint $table) {
            $table->increments('id');
            $table->date('testday');
            $table->unsignedSmallInteger('season');
            $table->unsignedSmallInteger('week');
            $table->unsignedSmallInteger('track_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tests_calendars');
    }
}
