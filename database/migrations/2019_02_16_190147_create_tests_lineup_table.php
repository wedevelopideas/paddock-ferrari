<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestsLineupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests_lineup', function (Blueprint $table) {
            $table->increments('id');
            $table->date('testday');
            $table->unsignedSmallInteger('season');
            $table->unsignedSmallInteger('week');
            $table->unsignedSmallInteger('driver_id');
            $table->unsignedSmallInteger('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tests_lineup');
    }
}
