<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('type');
            $table->string('lang', 5);
            $table->unsignedSmallInteger('driver_id');
            $table->date('event_date');
            $table->string('event_text');
            $table->string('event_twitter');
            $table->string('event_hashtags');
            $table->string('event_link')->nullable();
            $table->string('event_f1tv_link')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
