<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feed_history', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('author_id');
            $table->string('lang', 5);
            $table->date('date');
            $table->string('title');
            $table->text('description');
            $table->string('image');
            $table->string('link');
            $table->string('domain');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feed_history');
    }
}
