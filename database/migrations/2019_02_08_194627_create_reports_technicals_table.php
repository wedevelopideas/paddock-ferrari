<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTechnicalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports_technicals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('season');
            $table->unsignedTinyInteger('race');
            $table->unsignedSmallInteger('gp_id');
            $table->unsignedSmallInteger('driver_id');
            $table->unsignedTinyInteger('ice');
            $table->unsignedTinyInteger('tc');
            $table->unsignedTinyInteger('mguh');
            $table->unsignedTinyInteger('mguk');
            $table->unsignedTinyInteger('es');
            $table->unsignedTinyInteger('ce');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports_technicals');
    }
}
