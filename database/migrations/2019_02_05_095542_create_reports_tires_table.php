<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTiresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports_tires', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('season');
            $table->unsignedTinyInteger('race');
            $table->unsignedSmallInteger('gp_id');
            $table->unsignedSmallInteger('driver_id');
            $table->unsignedTinyInteger('hypersoft')->nullable();
            $table->unsignedTinyInteger('ultrasoft')->nullable();
            $table->unsignedTinyInteger('supersoft')->nullable();
            $table->unsignedTinyInteger('soft')->nullable();
            $table->unsignedTinyInteger('medium')->nullable();
            $table->unsignedTinyInteger('hard')->nullable();
            $table->unsignedTinyInteger('superhard')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports_tires');
    }
}
