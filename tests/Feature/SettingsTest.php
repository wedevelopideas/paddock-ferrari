<?php

namespace Tests\Feature;

use Tests\TestCase;

class SettingsTest extends TestCase
{
    /** @test */
    public function test_show_the_settings_page()
    {
        $response = $this->actingAs($this->user)->get(route('settings'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_updates_the_language_of_the_user()
    {
        $data = [
            'lang' => 'en',
        ];

        $response = $this->actingAs($this->user)->post(route('settings.langs'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('settings'));
    }

    /** @test */
    public function test_updates_the_timezone_of_the_user()
    {
        $data = [
            'timezone' => 'Europe/Rome',
        ];

        $response = $this->actingAs($this->user)->post(route('settings.timezones'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('settings'));
    }
}
