<?php

namespace Tests\Feature;

use Tests\TestCase;

class SeasonsRacesTest extends TestCase
{
    /** @test */
    public function test_shows_the_race_of_the_season()
    {
        $response = $this->actingAs($this->user)->get(route('season.race', ['season' => $this->season->season, 'gpslug' => $this->grandPrix->slug]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_live_page_for_the_race_of_the_season()
    {
        $response = $this->actingAs($this->user)->get(route('season.race.live', ['season' => $this->season->season, 'gpslug' => $this->grandPrix->slug, 'session' => 1]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_tyre_report_of_the_driver_for_the_race_of_the_season()
    {
        $response = $this->actingAs($this->user)->get(route('season.race.report.tires', ['season' => $this->season->season, 'gpslug' => $this->grandPrix->slug, 'driverslug' => $this->driver->slug]));
        $response->assertSuccessful();
    }
}
