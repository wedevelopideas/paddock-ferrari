<?php

namespace Tests\Feature;

use Tests\TestCase;

class UsersTest extends TestCase
{
    /** @test */
    public function test_shows_users_profile()
    {
        $response = $this->actingAs($this->user)->get(route('user', ['user_name' => $this->user->user_name]));
        $response->assertSuccessful();
    }
}
