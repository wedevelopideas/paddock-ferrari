<?php

namespace Tests\Feature;

use Tests\TestCase;

class MediaTest extends TestCase
{
    /** @test */
    public function test_shows_media_landing_page()
    {
        $response = $this->actingAs($this->user)->get(route('media'));
        $response->assertSuccessful();
    }
}
