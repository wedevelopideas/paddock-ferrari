<?php

namespace Tests\Feature;

use Tests\TestCase;

class TestsTest extends TestCase
{
    /** @test */
    public function test_shows_test_list()
    {
        $response = $this->actingAs($this->user)->get(route('tests'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_test_results()
    {
        $response = $this->actingAs($this->user)->get(route('tests.results', ['season' => $this->testResult->season, 'week' => $this->testResult->week]));
        $response->assertSuccessful();
    }
}
