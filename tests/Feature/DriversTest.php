<?php

namespace Tests\Feature;

use Tests\TestCase;

class DriversTest extends TestCase
{
    /** @test */
    public function test_shows_drivers_list()
    {
        $response = $this->actingAs($this->user)->get(route('drivers'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_drivers_dates_list()
    {
        $response = $this->actingAs($this->user)->get(route('drivers.dates'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_drivers_view()
    {
        $response = $this->actingAs($this->user)->get(route('drivers.view', ['slug' => $this->driver->slug]));
        $response->assertSuccessful();
    }
}
