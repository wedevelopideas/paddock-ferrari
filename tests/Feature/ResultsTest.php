<?php

namespace Tests\Feature;

use Tests\TestCase;

class ResultsTest extends TestCase
{
    /** @test */
    public function test_shows_results_list()
    {
        $response = $this->actingAs($this->user)->get(route('results'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_results_form_to_add()
    {
        $response = $this->actingAs($this->user)->get(route('results.add'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_stores_results()
    {
        $data = [
            'season' => $this->result->season,
            'gp_id' => $this->result->gp_id,
            'track_id' => $this->result->track_id,
            'session_id' => $this->result->session_id,
            'dateofresult' => $this->result->dateofresult,
            'car_id' => $this->result->car_id,
            'car_nr' => $this->result->car_nr,
            'driver_id' => $this->result->driver_id,
            'team' => $this->result->team,
            'place' => $this->result->place,
            'laps' => $this->result->laps,
            'points' => $this->result->points,
            'dnf' => $this->result->dnf,
            'dns' => $this->result->dns,
            'dsq' => $this->result->dsq,
        ];

        $response = $this->actingAs($this->user)->post(route('results.add'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('results'));
    }

    /** @test */
    public function test_shows_results_by_grand_prix()
    {
        $response = $this->actingAs($this->user)->get(route('results.gp', ['slug' => $this->grandPrix->slug]));
        $response->assertSuccessful();
    }
}
