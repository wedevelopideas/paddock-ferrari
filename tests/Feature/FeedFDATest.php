<?php

namespace Tests\Feature;

use Tests\TestCase;

class FeedFDATest extends TestCase
{
    /** @test */
    public function test_shows_fda_feed_list()
    {
        $response = $this->actingAs($this->user)->get(route('feed.fda'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_fda_view()
    {
        $data = [
            'lang' => $this->feedFDA->lang,
            'date' => $this->feedFDA->created_at,
        ];

        $response = $this->actingAs($this->user)->get(route('feed.fda.view', ['date' => $data['date']]));
        $response->assertSuccessful();
    }
}
