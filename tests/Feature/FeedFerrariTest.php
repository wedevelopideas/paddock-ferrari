<?php

namespace Tests\Feature;

use Tests\TestCase;

class FeedFerrariTest extends TestCase
{
    /** @test */
    public function test_shows_ferrari_feed_list()
    {
        $response = $this->actingAs($this->user)->get(route('feed.ferrari'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_ferrari_view()
    {
        $data = [
            'session' => 1,
            'lang' => 'en',
            'date' => '2019-01-07 17:25:00',
        ];

        $response = $this->actingAs($this->user)->get(route('feed.ferrari.view', ['date' => $data['date'], 'session' => $data['session']]));
        $response->assertSuccessful();
    }
}
