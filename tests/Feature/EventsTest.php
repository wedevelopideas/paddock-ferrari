<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\paddock\Events\Models\Events;
use App\paddock\Drivers\Models\Drivers;

class EventsTest extends TestCase
{
    /** @test_shows_events_list */
    public function test_shows_events_list(): void
    {
        $response = $this->actingAs($this->user)->get(route('events'));
        $response->assertSuccessful();
    }

    /** @test_shows_events_form_to_add */
    public function test_shows_events_form_to_add(): void
    {
        $response = $this->actingAs($this->user)->get(route('events.add'));
        $response->assertSuccessful();
    }

    /** @test_stores_events */
    public function test_stores_events(): void
    {
        $data = [
            'type' => $this->event->type,
            'lang' => $this->event->lang,
            'driver_id' => $this->event->driver_id,
            'event_date' => $this->event->event_date,
            'event_text' => $this->event->event_text,
            'event_twitter' => $this->event->event_twitter,
            'event_hashtags' => $this->event->event_hashtags,
            'event_link' => $this->event->event_link,
            'event_f1tv_link' => $this->event->event_f1tv_link,
        ];

        $response = $this->actingAs($this->user)->post(route('events.add'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('events'));
    }

    /** @test_shows_events_form_to_edit */
    public function test_shows_events_form_to_edit(): void
    {
        $response = $this->actingAs($this->user)->get(route('events.edit', ['code' => $this->event->id]));
        $response->assertSuccessful();
    }

    /** @test_updates_events */
    public function test_updates_events(): void
    {
        $data = [
            'type' => $this->event->type,
            'lang' => $this->event->lang,
            'driver_id' => $this->event->driver_id,
            'event_date' => $this->event->event_date,
            'event_text' => $this->event->event_text,
            'event_twitter' => $this->event->event_twitter,
            'event_hashtags' => $this->event->event_hashtags,
            'event_f1tv_link' => $this->event->event_f1tv_link,
        ];

        $response = $this->actingAs($this->user)->post(route('events.edit', ['code' => $this->event->id]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('events.view', ['driver_id' => $data['driver_id'], 'date' => $data['event_date']]));
    }

    /** @test_shows_event */
    public function test_shows_event(): void
    {
        $driver = factory(Drivers::class)->create();

        $data = factory(Events::class)->create([
            'driver_id' => $driver->id,
            'event_date' => '1969-01-03',
        ]);

        $response = $this->actingAs($this->user)->get(route('events.view', ['driver_id' => $data['driver_id'], 'date' => $data['event_date']]));
        $response->assertSuccessful();
    }
}
