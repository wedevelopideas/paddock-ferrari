<?php

namespace Tests\Feature;

use Tests\TestCase;

class FeedHistoryTest extends TestCase
{
    /** @test */
    public function test_shows_history_feed_list()
    {
        $response = $this->actingAs($this->user)->get(route('feed.history'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_histoy_view()
    {
        $response = $this->actingAs($this->user)->get(route('feed.history.view', ['date' => $this->feedHistory->date]));
        $response->assertSuccessful();
    }
}
