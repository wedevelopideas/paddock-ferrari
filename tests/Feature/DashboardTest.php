<?php

namespace Tests\Feature;

use Tests\TestCase;

class DashboardTest extends TestCase
{
    /** @test */
    public function test_show_the_user_dashboard()
    {
        $response = $this->actingAs($this->user)->get(route('dashboard'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_the_race_dashboard()
    {
        $response = $this->actingAs($this->user)->get(route('dashboard.race'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_the_social_media_dashboard()
    {
        $response = $this->actingAs($this->user)->get(route('socialmedia'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_the_test_dashboard()
    {
        $response = $this->actingAs($this->user)->get(route('tests.dashboard'));
        $response->assertSuccessful();
    }
}
