<?php

namespace Tests\Feature\SQL;

use Tests\TestCase;

class DashboardTest extends TestCase
{
    /** @test */
    public function test_shows_the_dashboard()
    {
        $response = $this->actingAs($this->user)->get(route('sql'));
        $response->assertSuccessful();
    }
}
