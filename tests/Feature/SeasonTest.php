<?php

namespace Tests\Feature;

use Tests\TestCase;

class SeasonTest extends TestCase
{
    /** @test */
    public function test_shows_current_season()
    {
        $response = $this->actingAs($this->user)->get(route('season'));
        $response->assertSuccessful();
    }
}
