<?php

namespace Tests\Feature;

use Tests\TestCase;

class YouTubeVideoTest extends TestCase
{
    /** @test */
    public function test_shows_youtube_video_list()
    {
        $response = $this->actingAs($this->user)->get(route('media.youtube'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_youtube_video_form_to_add()
    {
        $response = $this->actingAs($this->user)->get(route('media.youtube.add'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_stores_youtube_video()
    {
        $data = [
            'youtube_id' => 'jMQH43dKnZU',
            'title' => 'Michael Schumacher: In One Word',
            'description' => 'Team mates, colleagues and rivals alike come together to describe the great Michael Schumacher in just one word. Which word would you use?\n\nFor more F1® videos, visit http://www.Formula1.com\n\nLike F1® on Facebook: https://www.facebook.com/Formula1/\n\nFollow F1® on Twitter: http://www.twitter.com/F1\n\nFollow F1® on Instagram: http://www.instagram.com/F1\n\n#F1 #Michael50 #KeepFightingMichael',
            'thumbnail' => 'https://i.ytimg.com/vi/jMQH43dKnZU/mqdefault.jpg',
        ];

        $response = $this->actingAs($this->user)->post(route('media.youtube.add'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('media.youtube'));
    }

    /** @test */
    public function test_shows_youtube_video_form_to_edit()
    {
        $response = $this->actingAs($this->user)->get(route('media.youtube.edit', ['id' => $this->youtubeVideo->id]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_updates_youtube_video()
    {
        $data = [
            'youtube_id' => 'jMQH43dKnZU',
            'title' => 'Michael Schumacher: In One Word',
            'description' => 'Team mates, colleagues and rivals alike come together to describe the great Michael Schumacher in just one word. Which word would you use?\n\nFor more F1® videos, visit http://www.Formula1.com\n\nLike F1® on Facebook: https://www.facebook.com/Formula1/\n\nFollow F1® on Twitter: http://www.twitter.com/F1\n\nFollow F1® on Instagram: http://www.instagram.com/F1\n\n#F1 #Michael50 #KeepFightingMichael',
            'thumbnail' => 'https://i.ytimg.com/vi/jMQH43dKnZU/mqdefault.jpg',
        ];

        $response = $this->actingAs($this->user)->post(route('media.youtube.edit', ['id' => $this->youtubeVideo->id]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('media.youtube'));
    }

    /** @test */
    public function test_shows_youtube_video()
    {
        $response = $this->actingAs($this->user)->get(route('media.youtube.view', ['youtube_id' => $this->youtubeVideo->youtube_id]));
        $response->assertSuccessful();
    }
}
