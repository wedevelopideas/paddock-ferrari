<?php

namespace Tests\Feature;

use Tests\TestCase;

class CalendarTest extends TestCase
{
    /** @test */
    public function test_shows_current_season_calendar()
    {
        $response = $this->actingAs($this->user)->get(route('calendar'));
        $response->assertSuccessful();
    }
}
