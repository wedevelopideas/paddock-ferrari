<?php

namespace Tests\Feature;

use Tests\TestCase;

class GalleryTest extends TestCase
{
    /** @test */
    public function test_shows_all_gallery_albums()
    {
        $response = $this->actingAs($this->user)->get(route('media.gallery'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_gallery_album_with_images()
    {
        $response = $this->actingAs($this->user)->get(route('media.gallery.album', ['slug' => $this->galleryAlbum->slug]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_gallery_album_for_twitter()
    {
        $response = $this->actingAs($this->user)->get(route('media.gallery.twitter'));
        $response->assertSuccessful();
    }
}
