<?php

namespace Tests\Feature;

use Tests\TestCase;

class FeedsTest extends TestCase
{
    /** @test */
    public function test_shows_feeds_list()
    {
        $response = $this->actingAs($this->user)->get(route('feed'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_meta_form()
    {
        $response = $this->actingAs($this->user)->get(route('feed.meta'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shares_meta_results()
    {
        $link = 'https://formula1.ferrari.com/en/ferrari-announcement-2019/';

        stream_context_set_default([
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
            ],
        ]);

        $data = get_meta_tags($link);

        $response = $this->actingAs($this->user)->post(route('feed.meta.result'), ['link' => $link]);
        $response->assertStatus(302);
        $response->assertRedirect(route('feed.meta'));

        $this->assertEquals('Ferrari Announcement', html_entity_decode($data['twitter:title']));
        $this->assertEquals('January 7 2019', html_entity_decode($data['twitter:description']));
        $this->assertEquals('https://static.formula1.ferrari.com/imgresize-cache/431bb196b0b14e48203ede750cd7c0e4.jpg', $data['twitter:image']);
        $this->assertEquals('https://formula1.ferrari.com/en/ferrari-announcement-2019/', $link);
    }
}
