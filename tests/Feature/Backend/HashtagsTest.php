<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class HashtagsTest extends TestCase
{
    /** @test */
    public function test_shows_hashtags_list()
    {
        $response = $this->actingAs($this->user)->get(route('backend.hashtags'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_hashtags_form_to_add()
    {
        $response = $this->actingAs($this->user)->get(route('backend.hashtags.add'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_stores_hashtags()
    {
        $data = [
            'name' => 'Seb5',
        ];

        $response = $this->actingAs($this->user)->post(route('backend.hashtags.add'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.hashtags'));
    }

    /** @test */
    public function test_shows_hashtags_form_to_edit()
    {
        $response = $this->actingAs($this->user)->get(route('backend.hashtags.edit', ['code' => $this->hashtag->id]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_updates_hashtags()
    {
        $data = [
            'name' => 'Seb5',
        ];

        $response = $this->actingAs($this->user)->post(route('backend.hashtags.edit', ['code' => $this->hashtag->id]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.hashtags'));
    }
}
