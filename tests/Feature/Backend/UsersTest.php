<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class UsersTest extends TestCase
{
    /** @test */
    public function test_shows_users_list()
    {
        $response = $this->actingAs($this->user)->get(route('backend.users'));
        $response->assertSuccessful();
    }
}
