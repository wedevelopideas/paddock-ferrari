<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class GrandPrixsTest extends TestCase
{
    /** @test */
    public function test_shows_grandprixs_list()
    {
        $response = $this->actingAs($this->user)->get(route('backend.gps'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_grandprixs_form_to_add()
    {
        $response = $this->actingAs($this->user)->get(route('backend.gps.add'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_stores_grandprixs()
    {
        $data = [
            'country_id' => 'it',
            'name' => 'Italy',
            'slug' => 'italy',
            'full_name' => 'Italian Grand Prix',
            'hashtag' => 'ItalianGP',
            'emoji' => '🇮🇹',
            'timezone' => 'Europe/Rome',
            'active' => 0,
        ];

        $response = $this->actingAs($this->user)->post(route('backend.gps.add'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.gps'));
    }

    /** @test */
    public function test_shows_grandprixs_form_to_edit()
    {
        $response = $this->actingAs($this->user)->get(route('backend.gps.edit', ['code' => $this->grandPrix->id]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_updates_grandprixs()
    {
        $data = [
            'country_id' => 'it',
            'name' => 'Italy',
            'slug' => 'italy',
            'full_name' => 'Italian Grand Prix',
            'hashtag' => 'ItalianGP',
            'emoji' => '🇮🇹',
            'timezone' => 'Europe/Rome',
            'active' => 1,
        ];

        $response = $this->actingAs($this->user)->post(route('backend.gps.edit', ['code' => $this->grandPrix->id]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.gps'));
    }
}
