<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class ReportsTechnicalsTest extends TestCase
{
    /** @test */
    public function test_shows_reports_technicals_form_to_add()
    {
        $response = $this->actingAs($this->user)->get(route('backend.reports.technicals.add'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_stores_reports_technicals()
    {
        $data = [
            'season' => $this->reportTechnical->season,
            'race' => $this->reportTechnical->race,
            'gp_id' => $this->reportTechnical->gp_id,
            'driver_id' => $this->reportTechnical->driver_id,
            'ice' => $this->reportTechnical->ice,
            'tc' => $this->reportTechnical->tc,
            'mguh' => $this->reportTechnical->mguh,
            'mguk' => $this->reportTechnical->mguk,
            'es' => $this->reportTechnical->es,
            'ce' => $this->reportTechnical->ce,
        ];

        $response = $this->actingAs($this->user)->post(route('backend.reports.technicals.add'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.reports'));
    }
}
