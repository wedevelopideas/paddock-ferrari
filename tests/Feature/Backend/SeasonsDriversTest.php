<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class SeasonsDriversTest extends TestCase
{
    /** @test */
    public function test_shows_seasons_drivers_list()
    {
        $response = $this->actingAs($this->user)->get(route('backend.seasons.drivers', ['season' => $this->season->season]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_seasons_drivers_form_to_add()
    {
        $response = $this->actingAs($this->user)->get(route('backend.seasons.drivers.add', ['season' => $this->season->season]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_stores_seasons_drivers()
    {
        $data = [
            'season' => 2019,
            'driver_id' => 1,
        ];

        $response = $this->actingAs($this->user)->post(route('backend.seasons.drivers.add', ['season' => $data['season']]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.seasons.drivers', ['season' => $data['season']]));
    }

    /** @test */
    public function test_shows_seasons_drivers_form_to_edit()
    {
        $response = $this->actingAs($this->user)->get(route('backend.seasons.drivers.edit', ['season' => $this->seasonDriver->season, 'id' => $this->seasonDriver->id]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_updates_seasons_drivers()
    {
        $data = [
            'season' => 2019,
            'driver_id' => 1,
        ];

        $response = $this->actingAs($this->user)->post(route('backend.seasons.drivers.edit', ['season' => $data['season'], 'id' => $this->seasonDriver->id]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.seasons.drivers', ['season' => $data['season']]));
    }
}
