<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class ReportsTest extends TestCase
{
    /** @test */
    public function test_shows_reports()
    {
        $response = $this->actingAs($this->user)->get(route('backend.reports'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_reports_season()
    {
        $response = $this->actingAs($this->user)->get(route('backend.reports.season', ['season' => $this->season->season]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_reports_view()
    {
        $response = $this->actingAs($this->user)->get(route('backend.reports.view', ['season' => $this->season->season, 'slug' => $this->grandPrix->slug]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_reports_view_2019()
    {
        $response = $this->actingAs($this->user)->get(route('backend.reports.view', ['season' => 2019, 'slug' => $this->grandPrix->slug]));
        $response->assertSuccessful();
    }
}
