<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class CarsTest extends TestCase
{
    /** @test */
    public function test_shows_cars_list()
    {
        $response = $this->actingAs($this->user)->get(route('backend.cars'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_cars_form_to_add()
    {
        $response = $this->actingAs($this->user)->get(route('backend.cars.add'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_stores_cars()
    {
        $data = [
            'name' => 'SF71H',
        ];

        $response = $this->actingAs($this->user)->post(route('backend.cars.add'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.cars'));
    }

    /** @test */
    public function test_shows_cars_form_to_edit()
    {
        $response = $this->actingAs($this->user)->get(route('backend.cars.edit', ['code' => $this->car->id]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_updates_cars()
    {
        $data = [
            'name' => 'SF71H',
        ];

        $response = $this->actingAs($this->user)->post(route('backend.cars.edit', ['code' => $this->car->id]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.cars'));
    }
}
