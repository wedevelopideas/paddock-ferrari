<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;
use App\paddock\Drivers\Models\Drivers;

class DriversTest extends TestCase
{
    /** @test */
    public function test_shows_drivers_list()
    {
        $response = $this->actingAs($this->user)->get(route('backend.drivers'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_drivers_form_to_add()
    {
        $response = $this->actingAs($this->user)->get(route('backend.drivers.add'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_stores_drivers()
    {
        $data = [
            'country_id' => 'de',
            'status' => Drivers::WORKS,
            'first_name' => 'Sebastian',
            'last_name' => 'Vettel',
            'hashtag' => '#Seb5',
            'dateofbirth' => '1987-07-03',
            'dateofdeath' => null,
            'image' => '',
        ];

        $response = $this->actingAs($this->user)->post(route('backend.drivers.add'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.drivers'));
    }

    /** @test */
    public function test_shows_drivers_form_to_edit()
    {
        $response = $this->actingAs($this->user)->get(route('backend.drivers.edit', ['code' => $this->driver->id]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_updates_drivers()
    {
        $data = [
            'country_id' => 'de',
            'status' => Drivers::WORKS,
            'first_name' => 'Sebastian',
            'last_name' => 'Vettel',
            'hashtag' => 'Seb5',
            'dateofbirth' => '1987-07-03',
            'dateofdeath' => null,
            'image' => '',
        ];

        $response = $this->actingAs($this->user)->post(route('backend.drivers.edit', ['code' => $this->driver->id]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.drivers'));
    }

    /** @test */
    public function test_shows_drivers_overview()
    {
        $response = $this->actingAs($this->user)->get(route('backend.drivers.view', ['slug' => $this->driver->slug]));
        $response->assertSuccessful();
    }
}
