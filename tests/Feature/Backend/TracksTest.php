<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;
use Illuminate\Support\Str;

class TracksTest extends TestCase
{
    /** @test */
    public function test_shows_tracks_list()
    {
        $response = $this->actingAs($this->user)->get(route('backend.tracks'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_tracks_form_to_add()
    {
        $response = $this->actingAs($this->user)->get(route('backend.tracks.add'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_stores_tracks()
    {
        $data = [
            'country_id' => 'it',
            'name' => 'Autodromo Nazionale Monza',
            'slug' => Str::slug('Autodromo Nazionale Monza'),
            'laps' => 53,
            'length' => 5.793,
            'active' => 0,
        ];

        $response = $this->actingAs($this->user)->post(route('backend.tracks.add'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.tracks'));
    }

    /** @test */
    public function test_shows_tracks_form_to_edit()
    {
        $response = $this->actingAs($this->user)->get(route('backend.tracks.edit', ['code' => $this->driver->id]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_updates_tracks()
    {
        $data = [
            'country_id' => 'it',
            'name' => 'Autodromo Nazionale Monza',
            'slug' => Str::slug('Autodromo Nazionale Monza'),
            'laps' => 53,
            'length' => 5.793,
            'active' => 1,
        ];

        $response = $this->actingAs($this->user)->post(route('backend.tracks.edit', ['code' => $this->driver->id]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.tracks'));
    }
}
