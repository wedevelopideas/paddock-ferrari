<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class GalleryImagesTest extends TestCase
{
    /** @test */
    public function test_shows_gallery_images_in_album()
    {
        $response = $this->actingAs($this->user)->get(route('backend.gallery.images.view', ['album_id' => $this->galleryImage->album_id]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_gallery_images_form_to_add()
    {
        $response = $this->actingAs($this->user)->get(route('backend.gallery.images.add', ['album_id' => $this->galleryAlbum->id]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_stores_gallery_images()
    {
        $data = [
            'album_id' => 1,
            'creator_id' => 1,
            'image' => 'https://imgr2.auto-motor-und-sport.de/F1-Michael-Schumacher-fotoshowBig-9b27e57f-304712.jpg',
            'source' => 'AMuS',
        ];

        $response = $this->actingAs($this->user)->post(route('backend.gallery.images.add', ['album_id' => $this->galleryAlbum->id]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.gallery.images.view', ['album_id' => $this->galleryAlbum->id]));
    }

    /** @test */
    public function test_shows_gallery_images_form_to_edit()
    {
        $response = $this->actingAs($this->user)->get(route('backend.gallery.images.edit', ['album_id' => $this->galleryImage->album_id, 'id' => $this->galleryImage->id]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_updates_gallery_images()
    {
        $data = [
            'album_id' => 1,
            'creator_id' => 1,
            'image' => 'https://imgr2.auto-motor-und-sport.de/F1-Michael-Schumacher-fotoshowBig-9b27e57f-304712.jpg',
            'source' => 'AMuS',
        ];

        $response = $this->actingAs($this->user)->post(route('backend.gallery.images.edit', ['album_id' => $data['album_id'], 'id' => $this->galleryImage->id]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.gallery.images.view', ['album_id' => $data['album_id']]));
    }
}
