<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class GalleryTest extends TestCase
{
    /** @test */
    public function test_shows_gallery_albums_list()
    {
        $response = $this->actingAs($this->user)->get(route('backend.gallery'));
        $response->assertSuccessful();
    }
}
