<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class SeasonsTest extends TestCase
{
    /** @test */
    public function test_shows_seasons_list()
    {
        $response = $this->actingAs($this->user)->get(route('backend.seasons'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_seasons_form_to_add()
    {
        $response = $this->actingAs($this->user)->get(route('backend.seasons.add'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_stores_seasons()
    {
        $data = [
            'season' => date('Y'),
        ];

        $response = $this->actingAs($this->user)->post(route('backend.seasons.add'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.seasons'));
    }

    /** @test */
    public function test_shows_seasons_form_to_edit()
    {
        $response = $this->actingAs($this->user)->get(route('backend.seasons.edit', ['id' => $this->season->id]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_updates_seasons()
    {
        $data = [
            'season' => date('Y'),
        ];

        $response = $this->actingAs($this->user)->post(route('backend.seasons.edit', ['id' => $this->season->id]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.seasons'));
    }
}
