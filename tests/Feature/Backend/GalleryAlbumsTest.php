<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class GalleryAlbumsTest extends TestCase
{
    /** @test */
    public function test_shows_gallery_albums_form_to_add()
    {
        $response = $this->actingAs($this->user)->get(route('backend.gallery.album.add'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_stores_gallery_albums()
    {
        $data = [
            'name' => 'Abu Dhabi GP - Sunday',
            'slug' => 'abu-dhabi-gp-sunday',
            'cover_id' => 0,
            'creator_id' => 1,
        ];

        $response = $this->actingAs($this->user)->post(route('backend.gallery.album.add'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.gallery'));
    }

    /** @test */
    public function test_shows_gallery_albums_form_to_edit()
    {
        $response = $this->actingAs($this->user)->get(route('backend.gallery.album.edit', ['id' => $this->galleryAlbum->id]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_update_gallery_albums()
    {
        $data = [
            'name' => 'Abu Dhabi GP - Sunday',
            'slug' => 'abu-dhabi-gp-sunday',
            'cover_id' => 0,
            'creator_id' => 1,
        ];

        $response = $this->actingAs($this->user)->post(route('backend.gallery.album.edit', ['id' => $this->galleryAlbum->id]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.gallery'));
    }
}
