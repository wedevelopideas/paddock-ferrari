<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class LanguagesTest extends TestCase
{
    /** @test */
    public function test_shows_languages_list_active()
    {
        $data = [
            'code' => 'en',
            'name' => 'English',
            'news' => true,
            'active' => true,
        ];

        $response = $this->actingAs($this->user)->get(route('backend.langs'), $data);
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_languages_list_non_active()
    {
        $data = [
            'code' => 'it',
            'name' => 'Italian',
            'news' => false,
            'active' => false,
        ];

        $response = $this->actingAs($this->user)->get(route('backend.langs'), $data);
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_languages_form_to_add()
    {
        $response = $this->actingAs($this->user)->get(route('backend.langs.add'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_stores_languages()
    {
        $data = [
            'code' => 'en',
            'name' => 'English',
            'motorsport_link' => 'https://www.motorsport.com/f1/news/',
            'news' => false,
            'active' => false,
        ];

        $response = $this->actingAs($this->user)->post(route('backend.langs.add'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.langs'));
    }

    /** @test */
    public function test_shows_languages_form_to_edit()
    {
        $response = $this->actingAs($this->user)->get(route('backend.langs.edit', ['code' => $this->lang->code]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_updates_languages()
    {
        $data = [
            'code' => 'en',
            'name' => 'English',
            'motorsport_link' => 'https://www.motorsport.com/f1/news/',
            'news' => true,
            'active' => true,
        ];

        $response = $this->actingAs($this->user)->post(route('backend.langs.edit', ['code' => $data['code']]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.langs'));
    }
}
