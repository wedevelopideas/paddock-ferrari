<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class ReportsTiresTest extends TestCase
{
    /** @test */
    public function test_shows_reports_tires_form_to_add()
    {
        $response = $this->actingAs($this->user)->get(route('backend.reports.tires.add'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_stores_reports_tires()
    {
        $data = [
            'season' => $this->reportTyre->season,
            'race' => $this->reportTyre->race,
            'gp_id' => $this->reportTyre->gp_id,
            'driver_id' => $this->reportTyre->driver_id,
            'hypersoft' => $this->reportTyre->hypersoft,
            'ultrasoft' => $this->reportTyre->ultrasoft,
            'supersoft' => $this->reportTyre->supersoft,
            'soft' => $this->reportTyre->soft,
            'medium' => $this->reportTyre->medium,
            'hard' => $this->reportTyre->hard,
            'superhard' => $this->reportTyre->superhard,
        ];

        $response = $this->actingAs($this->user)->post(route('backend.reports.tires.add'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.reports'));
    }

    /** @test */
    public function test_shows_reports_tires_2019_form_to_add()
    {
        $response = $this->actingAs($this->user)->get(route('backend.reports.tires.add.2019'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_stores_reports_tires_2019()
    {
        $data = [
            'season' => $this->reportTyre2019->season,
            'race' => $this->reportTyre2019->race,
            'gp_id' => $this->reportTyre2019->gp_id,
            'driver_id' => $this->reportTyre2019->driver_id,
            'c1' => $this->reportTyre2019->c1,
            'c2' => $this->reportTyre2019->c2,
            'c3' => $this->reportTyre2019->c3,
            'c4' => $this->reportTyre2019->c4,
            'c5' => $this->reportTyre2019->c5,
        ];

        $response = $this->actingAs($this->user)->post(route('backend.reports.tires.add.2019'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.reports'));
    }
}
