<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class CountriesTest extends TestCase
{
    /** @test */
    public function test_shows_countries_list()
    {
        $response = $this->actingAs($this->user)->get(route('backend.countries'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_countries_form_to_add()
    {
        $response = $this->actingAs($this->user)->get(route('backend.countries.add'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_stores_countries()
    {
        $data = [
            'code' => 'it',
            'name' => 'Italy',
        ];

        $response = $this->actingAs($this->user)->post(route('backend.countries.add'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.countries'));
    }

    /** @test */
    public function test_shows_countries_form_to_edit()
    {
        $response = $this->actingAs($this->user)->get(route('backend.countries.edit', ['code' => $this->country->code]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_updates_countries()
    {
        $data = [
            'code' => 'it',
            'name' => 'Italy',
        ];

        $response = $this->actingAs($this->user)->post(route('backend.countries.edit', ['code' => $data['code']]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.countries'));
    }
}
