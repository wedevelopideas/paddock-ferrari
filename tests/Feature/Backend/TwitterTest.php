<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class TwitterTest extends TestCase
{
    /** @test */
    public function test_shows_twitter_accounts_list()
    {
        $data = [
            'twitter_id' => 108247668,
            'name' => 'Scuderia Ferrari',
            'screen_name' => 'ScuderiaFerrari',
            'profile_image_url' => 'https://pbs.twimg.com/profile_images/947659786555940865/P5eYYYIx_400x400.jpg',
            'owner' => true,
        ];

        $response = $this->actingAs($this->user)->get(route('backend.twitter'), $data);
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_twitter_accounts_list_without_owner()
    {
        $data = [
            'twitter_id' => 108247668,
            'name' => 'Scuderia Ferrari',
            'screen_name' => 'ScuderiaFerrari',
            'profile_image_url' => 'https://pbs.twimg.com/profile_images/947659786555940865/P5eYYYIx_400x400.jpg',
            'owner' => false,
        ];

        $response = $this->actingAs($this->user)->get(route('backend.twitter'), $data);
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_twitter_accounts_form_to_add()
    {
        $response = $this->actingAs($this->user)->get(route('backend.twitter.add'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_stores_new_twitter_accounts()
    {
        $data = [
            'twitter_id' => 108247668,
            'name' => 'Scuderia Ferrari',
            'screen_name' => 'ScuderiaFerrari',
            'description' => 'Scuderia Ferrari - The Official Page',
            'profile_image_url' => 'https://pbs.twimg.com/profile_images/947659786555940865/P5eYYYIx_400x400.jpg',
            'owner' => false,
        ];

        $response = $this->actingAs($this->user)->post(route('backend.twitter.add'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.twitter'));
    }

    /** @test */
    public function test_shows_twitter_accounts_form_to_edit()
    {
        $response = $this->actingAs($this->user)->get(route('backend.twitter.edit', ['id' => $this->twitterAccount->id]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_updates_new_twitter_accounts()
    {
        $data = [
            'id' => 1,
            'twitter_id' => 108247668,
            'name' => 'Scuderia Ferrari',
            'screen_name' => 'ScuderiaFerrari',
            'description' => 'Scuderia Ferrari - The Official Page',
            'profile_image_url' => 'https://pbs.twimg.com/profile_images/947659786555940865/P5eYYYIx_400x400.jpg',
            'owner' => false,
        ];

        $response = $this->actingAs($this->user)->post(route('backend.twitter.edit', ['id' => $data['id']]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.twitter'));
    }
}
