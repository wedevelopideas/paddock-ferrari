<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class SocialMediaTest extends TestCase
{
    /** @test */
    public function test_shows_social_media()
    {
        $response = $this->actingAs($this->user)->get(route('backend.socialmedia'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_social_media_form_to_add()
    {
        $response = $this->actingAs($this->user)->get(route('backend.socialmedia.add'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_stores_social_media()
    {
        $data = [
            'name' => 'Scuderia Ferrari',
            'image' => 'https://formula1.ferrari.com/wp-content/themes/ferrari/static/assets/images/logo-2018.png',
            'website' => 'https://formula1.ferrari.com/',
            'facebook' => 'https://www.facebook.com/ScuderiaFerrari',
            'twitter' => 'https://twitter.com/ScuderiaFerrari',
            'instagram' => 'https://www.instagram.com/scuderiaferrari/',
            'youtube' => 'https://www.youtube.com/user/ferrariworld',
        ];

        $response = $this->actingAs($this->user)->post(route('backend.socialmedia.add'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.socialmedia'));
    }

    /** @test */
    public function test_shows_social_media_form_to_edit()
    {
        $response = $this->actingAs($this->user)->get(route('backend.socialmedia.edit', ['id' => $this->socialMedia->id]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_updates_social_media()
    {
        $data = [
            'name' => 'Scuderia Ferrari',
            'image' => 'https://formula1.ferrari.com/wp-content/themes/ferrari/static/assets/images/logo-2018.png',
            'website' => 'https://formula1.ferrari.com/',
            'facebook' => 'https://www.facebook.com/ScuderiaFerrari',
            'twitter' => 'https://twitter.com/ScuderiaFerrari',
            'instagram' => 'https://www.instagram.com/scuderiaferrari/',
            'youtube' => 'https://www.youtube.com/user/ferrariworld',
        ];

        $response = $this->actingAs($this->user)->post(route('backend.socialmedia.edit', ['id' => $this->socialMedia->id]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.socialmedia'));
    }
}
