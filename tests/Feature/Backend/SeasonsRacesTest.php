<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class SeasonsRacesTest extends TestCase
{
    /** @test */
    public function test_shows_seasons_races_list()
    {
        $response = $this->actingAs($this->user)->get(route('backend.seasons.races', ['season' => $this->season->season]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_seasons_races_form_to_add()
    {
        $response = $this->actingAs($this->user)->get(route('backend.seasons.races.add', ['season' => $this->season->season]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_stores_seasons_races()
    {
        $data = [
            'season' => 2019,
            'gp_id' => 37,
            'track_id' => 68,
            'raceday' => '2019-12-01',
            'status' => 1,
        ];

        $response = $this->actingAs($this->user)->post(route('backend.seasons.races.add', ['season' => $data['season']]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.seasons.races', ['season' => $data['season']]));
    }

    /** @test */
    public function test_shows_seasons_races_form_to_edit()
    {
        $response = $this->actingAs($this->user)->get(route('backend.seasons.races.edit', ['season' => $this->seasonRace->season, 'id' => $this->seasonRace->id]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_updates_seasons_races()
    {
        $data = [
            'season' => 2019,
            'gp_id' => 37,
            'track_id' => 68,
            'raceday' => '2019-12-01',
            'status' => 1,
        ];

        $response = $this->actingAs($this->user)->post(route('backend.seasons.races.edit', ['season' => $data['season'], 'id' => $this->seasonRace->id]), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.seasons.races', ['season' => $data['season']]));
    }
}
