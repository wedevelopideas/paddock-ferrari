<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class DashboardTest extends TestCase
{
    /** @test */
    public function test_shows_the_backend_dashboard()
    {
        $response = $this->actingAs($this->user)->get(route('backend'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_updates_settings()
    {
        $data = [
            'key' => 'season',
            'value' => '2019',
        ];

        $response = $this->actingAs($this->user)->post(route('backend.settings.save'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend'));
    }
}
