<?php

namespace Tests\Feature;

use Tests\TestCase;

class DriversStatsTest extends TestCase
{
    /** @test */
    public function test_shows_wins_of_the_driver()
    {
        $response = $this->actingAs($this->user)->get(route('drivers.stats.wins', ['slug' => $this->driver->slug]));
        $response->assertSuccessful();
    }
}
