<?php

namespace Tests\Feature;

use Tests\TestCase;

class FeedMotorsportTest extends TestCase
{
    /** @test */
    public function test_shows_motorsport_feed_list()
    {
        $response = $this->actingAs($this->user)->get(route('feed.motorsport'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_motorsport_view()
    {
        $response = $this->actingAs($this->user)->get(route('feed.motorsport.view', ['id' => $this->feedMotorsport->id]));
        $response->assertSuccessful();
    }
}
