<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\paddock\Languages\Models\Languages;

class LanguagesTest extends TestCase
{
    /** @test */
    public function test_get_flag_english()
    {
        $data = [
            'code' => 'en',
            'name' => 'English',
            'news' => true,
            'active' => true,
        ];

        $language = factory(Languages::class)->make($data);
        $expected = 'england';
        $actual = $language->getFlag();

        $this->assertEquals($expected, $actual, 'getFlag did not return expected value');
    }
}
