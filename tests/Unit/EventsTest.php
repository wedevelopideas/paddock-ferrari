<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\paddock\Events\Models\Events;

class EventsTest extends TestCase
{
    /** @test */
    public function test_get_flag_english()
    {
        $data = [
            'lang' => 'en',
        ];

        $event = factory(Events::class)->make($data);
        $expected = 'england';
        $actual = $event->getFlag();

        $this->assertEquals($expected, $actual, 'getFlag did not return expected value');
    }
}
