<?php

namespace Tests;

use App\paddock\Cars\Models\Cars;
use App\paddock\Users\Models\Users;
use App\paddock\Feed\Models\FeedFDA;
use App\paddock\Events\Models\Events;
use App\paddock\Tracks\Models\Tracks;
use App\paddock\Drivers\Models\Drivers;
use App\paddock\Results\Models\Results;
use App\paddock\Seasons\Models\Seasons;
use App\paddock\Feed\Models\FeedFerrari;
use App\paddock\Feed\Models\FeedHistory;
use App\paddock\Hashtags\Models\Hashtags;
use App\paddock\Tests\Models\TestsLineup;
use App\paddock\Tests\Models\TestsResults;
use App\paddock\Countries\Models\Countries;
use App\paddock\Feed\Models\FeedMotorsport;
use App\paddock\Languages\Models\Languages;
use App\paddock\Reports\Models\ReportsTires;
use App\paddock\Seasons\Models\SeasonsRaces;
use App\paddock\Tests\Models\TestsCalendars;
use App\paddock\GrandPrixs\Models\GrandPrixs;
use App\paddock\Seasons\Models\SeasonsDrivers;
use App\paddock\Seasons\Models\SeasonsCalendar;
use App\paddock\Twitter\Models\TwitterAccounts;
use App\paddock\Reports\Models\ReportsTires2019;
use App\paddock\Reports\Models\ReportsTechnicals;
use App\paddock\SocialMedias\Models\SocialMedias;
use App\paddock\Media\Gallery\Models\GalleryAlbums;
use App\paddock\Media\Gallery\Models\GalleryImages;
use App\paddock\Media\Youtube\Models\YouTubeVideos;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    /**
     * @var Cars
     */
    protected $car;

    /**
     * @var Countries
     */
    protected $country;

    /**
     * @var Drivers
     */
    protected $driver;

    /**
     * @var Events
     */
    protected $event;

    /**
     * @var FeedFDA
     */
    protected $feedFDA;

    /**
     * @var FeedFerrari
     */
    protected $feedFerrari;

    /**
     * @var FeedHistory
     */
    protected $feedHistory;

    /**
     * @var FeedMotorsport
     */
    protected $feedMotorsport;

    /**
     * @var GalleryAlbums
     */
    protected $galleryAlbum;

    /**
     * @var GalleryImages
     */
    protected $galleryImage;

    /**
     * @var GrandPrixs
     */
    protected $grandPrix;

    /**
     * @var Hashtags
     */
    protected $hashtag;

    /**
     * @var Languages
     */
    protected $lang;

    /**
     * @var ReportsTechnicals
     */
    protected $reportTechnical;

    /**
     * @var ReportsTires2019
     */
    protected $reportTyre2019;

    /**
     * @var ReportsTires
     */
    protected $reportTyre;

    /**
     * @var Results
     */
    protected $result;

    /**
     * @var Seasons
     */
    protected $season;

    /**
     * @var SeasonsCalendar
     */
    protected $seasonCalendar;

    /**
     * @var SeasonsDrivers
     */
    protected $seasonDriver;

    /**
     * @var SeasonsRaces
     */
    protected $seasonRace;

    /**
     * @var SocialMedias
     */
    protected $socialMedia;

    /**
     * @var TestsCalendars
     */
    protected $testCalendar;

    /**
     * @var TestsLineup
     */
    protected $testLineUp;

    /**
     * @var TestsResults
     */
    protected $testResult;

    /**
     * @var Tracks
     */
    protected $track;

    /**
     * @var TwitterAccounts
     */
    protected $twitterAccount;

    /**
     * @var Users
     */
    protected $user;

    /**
     * @var YouTubeVideos
     */
    protected $youtubeVideo;

    /**
     * Set up the test.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->car = factory(Cars::class)->create();
        $this->country = factory(Countries::class)->create();
        $this->driver = factory(Drivers::class)->create();
        $this->event = factory(Events::class)->create();
        $this->feedFDA = factory(FeedFDA::class)->create();
        $this->feedFerrari = factory(FeedFerrari::class)->create();
        $this->feedHistory = factory(FeedHistory::class)->create();
        $this->feedMotorsport = factory(FeedMotorsport::class)->create();
        $this->galleryAlbum = factory(GalleryAlbums::class)->create();
        $this->galleryImage = factory(GalleryImages::class)->create();
        $this->grandPrix = factory(GrandPrixs::class)->create();
        $this->hashtag = factory(Hashtags::class)->create();
        $this->lang = factory(Languages::class)->create();
        $this->reportTechnical = factory(ReportsTechnicals::class)->create();
        $this->reportTyre2019 = factory(ReportsTires2019::class)->create();
        $this->reportTyre = factory(ReportsTires::class)->create();
        $this->result = factory(Results::class)->create();
        $this->season = factory(Seasons::class)->create();
        $this->seasonCalendar = factory(SeasonsCalendar::class)->create();
        $this->seasonDriver = factory(SeasonsDrivers::class)->create();
        $this->seasonRace = factory(SeasonsRaces::class)->create();
        $this->socialMedia = factory(SocialMedias::class)->create();
        $this->testCalendar = factory(TestsCalendars::class)->create();
        $this->testLineUp = factory(TestsLineup::class)->create();
        $this->testResult = factory(TestsResults::class)->create();
        $this->track = factory(Tracks::class)->create();
        $this->twitterAccount = factory(TwitterAccounts::class)->create();
        $this->user = factory(Users::class)->create();
        $this->youtubeVideo = factory(YouTubeVideos::class)->create();
    }

    /**
     * Reset the migrations.
     */
    public function tearDown(): void
    {
        $this->artisan('migrate:reset');
        parent::tearDown();
    }
}
