@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="trophy icon"></i>
                        <span class="content">
                            {{ $grandprix->full_name }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="fourteen wide column">
                    @include('_partials.results.grandprix')
                </div>
                <div class="two wide column">
                    <a href="{{ route('results') }}" class="ui red right floated labeled icon button">
                        <i class="left arrow icon"></i>
                        {{ trans('common.results') }}
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <h3 class="ui header">
                        <i class="trophy icon"></i>
                        <span class="content">
                            {{ trans('common.results') }}
                        </span>
                    </h3>
                    <table class="ui table">
                        <thead>
                            <tr>
                                <th>{{ trans('common.season') }}</th>
                                <th>{{ trans('common.driver') }}</th>
                                <th>{{ trans('common.place') }}</th>
                                <th>{{ trans('common.points') }}</th>
                                <th>{{ trans('common.laps') }}</th>
                                <th>{{ trans('common.dnf') }}</th>
                                <th>{{ trans('common.dns') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($results as $result)
                            <tr>
                                <td>
                                    {{ $result->season }}
                                </td>
                                <td>
                                    {{ $result->driver->name }}
                                </td>
                                <td>
                                    {{ $result->place }}
                                </td>
                                <td>
                                    {{ $result->points }}
                                </td>
                                <td>
                                    {{ $result->laps }}
                                </td>
                                <td>
                                    {!! $result->dnfStatus() !!}
                                </td>
                                <td>
                                    {!! $result->dnsStatus() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection