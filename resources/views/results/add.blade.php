@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="checkered flag icon"></i>
                        <span class="content">
                            {{ trans('results.add') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    @include('_partials.messages')
                    <form action="" method="post" class="ui form">
                        @csrf

                        <div class="four fields">
                            <div class="field{{ $errors->has('season') ? ' error' : '' }}">
                                <label for="season">{{ trans('common.season') }}</label>
                                <select name="season" id="season">
                                    @foreach($seasons as $season)
                                        <option value="{{ $season->season }}">{{ $season->season }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="field{{ $errors->has('gp_id') ? ' error' : '' }}">
                                <label for="gp_id">{{ trans('common.grandprix') }}</label>
                                <select name="gp_id" id="gp_id">
                                    @foreach($grandprixs as $grandprix)
                                        <option value="{{ $grandprix->id }}">{{ $grandprix->full_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="field{{ $errors->has('track_id') ? ' error' : '' }}">
                                <label for="track_id">{{ trans('common.track') }}</label>
                                <select name="track_id" id="track_id">
                                    @foreach($tracks as $track)
                                        <option value="{{ $track->id }}">{{ $track->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="field{{ $errors->has('session_id') ? ' error' : '' }}">
                                <label for="session_id">{{ trans('common.session') }}</label>
                                <select name="session_id" id="session_id">
                                    <option value="1">{{ trans('results.race') }}</option>
                                    <option value="2">{{ trans('results.quali') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="four fields">
                            <div class="field{{ $errors->has('dateofresult') ? ' error' : '' }}">
                                <label for="dateofresult">{{ trans('common.date') }}</label>
                                <input type="date" name="dateofresult" id="dateofresult" placeholder="{{ trans('common.date') }}">
                            </div>
                            <div class="field{{ $errors->has('car_id') ? ' error' : '' }}">
                                <label for="car_id">{{ trans('common.car') }}</label>
                                <select name="car_id" id="car_id">
                                    @foreach($cars as $car)
                                        <option value="{{ $car->id }}">{{ $car->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="field{{ $errors->has('car_nr') ? ' error' : '' }}">
                                <label for="car_nr">{{ trans('common.number') }}</label>
                                <input type="number" name="car_nr" id="car_nr" placeholder="{{ trans('common.number') }}">
                            </div>
                            <div class="field{{ $errors->has('driver_id') ? ' error' : '' }}">
                                <label for="driver_id">{{ trans('common.driver') }}</label>
                                <select name="driver_id" id="driver_id">
                                    @foreach($drivers as $driver)
                                        <option value="{{ $driver->id }}">{{ $driver->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="four fields">
                            <div class="field{{ $errors->has('team') ? ' error' : '' }}">
                                <label for="team">{{ trans('common.team') }}</label>
                                <select name="team" id="team">
                                    <option value="1">{{ trans('common.yes') }}</option>
                                    <option value="0">{{ trans('common.no') }}</option>
                                </select>
                            </div>
                            <div class="field{{ $errors->has('place') ? ' error' : '' }}">
                                <label for="place">{{ trans('common.place') }}</label>
                                <input type="number" name="place" id="place" placeholder="{{ trans('common.place') }}">
                            </div>
                            <div class="field{{ $errors->has('laps') ? ' error' : '' }}">
                                <label for="laps">{{ trans('common.laps') }}</label>
                                <input type="number" name="laps" id="laps" placeholder="{{ trans('common.laps') }}">
                            </div>
                            <div class="field{{ $errors->has('points') ? ' error' : '' }}">
                                <label for="points">{{ trans('common.points') }}</label>
                                <input type="number" name="points" id="points" placeholder="{{ trans('common.points') }}">
                            </div>
                        </div>
                        <div class="three fields">
                            <div class="field{{ $errors->has('dnf') ? ' error' : '' }}">
                                <label for="dnf">{{ trans('common.dnf') }}</label>
                                <select name="dnf" id="dnf">
                                    <option value="1">{{ trans('common.yes') }}</option>
                                    <option value="0">{{ trans('common.no') }}</option>
                                </select>
                            </div>
                            <div class="field{{ $errors->has('dns') ? ' error' : '' }}">
                                <label for="dns">{{ trans('common.dns') }}</label>
                                <select name="dns" id="dns">
                                    <option value="1">{{ trans('common.yes') }}</option>
                                    <option value="0">{{ trans('common.no') }}</option>
                                </select>
                            </div>
                            <div class="field{{ $errors->has('dsq') ? ' error' : '' }}">
                                <label for="dsq">{{ trans('common.dsq') }}</label>
                                <select name="dsq" id="dsq">
                                    <option value="1">{{ trans('common.yes') }}</option>
                                    <option value="0">{{ trans('common.no') }}</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="ui red right floated labeled icon button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection