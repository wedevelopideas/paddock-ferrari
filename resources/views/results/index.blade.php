@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="trophy icon"></i>
                        <span class="content">
                            {{ trans('common.results') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('results.add') }}" class="ui red right floated labeled icon button">
                        <i class="add icon"></i>
                        {{ trans('results.add') }}
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="fourteen wide column">
                    <table class="ui table">
                        <thead>
                            <tr>
                                <th>{{ trans('common.grandprix') }}</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($results as $result)
                                <tr>
                                    <td>
                                        <i class="{{ $result->country_id }} flag"></i>
                                        {{ $result->full_name }}
                                    </td>
                                    <td>
                                        <a href="{{ route('results.gp', ['slug' => $result->slug]) }}" class="ui red icon button">
                                            <i class="unhide icon"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="two wide column">
                    @include('_partials.results.current_season')
                    <div class="ui divider"></div>
                    @include('_partials.results.overall')
                </div>
            </div>
        </div>
    </div>
@endsection