@extends('layouts.blank')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <h1 class="ui red image header">
                <img src="{{ asset('assets/images/logo.png') }}" alt="logo" class="image">
                <span class="content">
                    paddock | Scuderia Ferrari
                </span>
            </h1>
            @include('_partials.messages')
            <form action="{{ route('login') }}" method="post" class="ui large form">
                @csrf
                
                <div class="ui stacked segment">
                    <div class="field{{ $errors->has('email') ? ' error' : '' }}">
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="email" name="email" autocomplete="off" placeholder="{{ trans('common.email') }}">
                        </div>
                    </div>
                    <div class="field{{ $errors->has('password') ? ' error' : '' }}">
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input type="password" name="password" autocomplete="off" placeholder="{{ trans('common.password') }}">
                        </div>
                    </div>
                    <button type="submit" class="ui fluid large red submit button">{{ trans('common.login') }}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('styles')
    <style type="text/css">
        body {
            background-color: #DADADA;
        }
        body > .grid {
            height: 100%;
        }
        .image {
            margin-top: -100px;
        }
        .column {
            max-width: 450px;
        }
    </style>
@endsection