<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <meta name="description" content="paddock | Scuderia Ferrari">
    <link rel="stylesheet" href="{{ asset('assets/semantic.min.css') }}">
    <style type="text/css">
        body {
            background-color: #FFFFFF;
        }
        .ui.menu .item img.logo {
            margin-right: 1.5em;
        }
        .main.container {
            margin-top: 7em;
            padding: 0 1em;
        }
    @yield('styles')
    </style>
</head>
<body>
    <div class="ui fixed inverted menu">
        <div class="ui fluid container">
            <a href="{{ route('dashboard') }}" class="header item">
                <img class="logo" alt="logo" src="{{ asset('assets/images/logo.png') }}">
                paddock | Scuderia Ferrari
            </a>
            <a href="{{ route('events') }}" class="item">
                {{ trans('common.events') }}
            </a>
            <a href="{{ route('media') }}" class="item">
                {{ trans('common.media') }}
            </a>
            <a href="{{ route('drivers') }}" class="item">
                {{ trans('common.drivers') }}
            </a>
            <a href="{{ route('feed') }}" class="item">
                {{ trans('common.feed') }}
            </a>
            <a href="{{ route('season') }}" class="item">
                {{ trans('common.season') }}
            </a>
            <a href="{{ route('results') }}" class="item">
                {{ trans('common.results') }}
            </a>
            <a href="{{ route('tests') }}" class="item">
                {{ trans('common.tests') }}
            </a>
            <div class="right menu">
                <a href="{{ route('backend') }}" title="{{ trans('common.backend') }}" class="icon item">
                    <i class="dashboard icon"></i>
                </a>
                <a href="{{ route('logout') }}" title="{{ trans('common.logout') }}" class="icon item">
                    <i class="sign out icon"></i>
                </a>
            </div>
        </div>
    </div>
@yield('content')
<script src="{{ asset('assets/jquery.min.js') }}"></script>
<script src="{{ asset('assets/semantic.min.js') }}"></script>
</body>
</html>