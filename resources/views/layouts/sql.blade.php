<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <meta name="description" content="paddock | Scuderia Ferrari">
    <link rel="stylesheet" href="{{ asset('assets/semantic.min.css') }}">
    <style type="text/css">
        body {
            background-color: #FFFFFF;
        }
        .ui.menu .item img.logo {
            margin-right: 1.5em;
        }
        .main.container {
            margin-top: 7em;
            padding: 0 1em;
        }
    @yield('styles')
    </style>
</head>
<body>
    <div class="ui fixed inverted menu">
        <div class="ui fluid container">
            <a href="{{ route('sql') }}" class="header item">
                <img class="logo" alt="logo" src="{{ asset('assets/images/logo.png') }}">
                paddock (sql) | Scuderia Ferrari
            </a>
            <div class="right menu">
                <a href="{{ route('backend') }}" title="{{ trans('common.backend') }}" class="item">
                    <i class="dashboard icon"></i> {{ trans('common.backend') }}
                </a>
                <a href="{{ route('dashboard') }}" title="{{ trans('common.frontend') }}" class="item">
                    <i class="dashboard icon"></i> {{ trans('common.frontend') }}
                </a>
                <a href="{{ route('logout') }}" title="{{ trans('common.logout') }}" class="icon item">
                    <i class="sign out icon"></i>
                </a>
            </div>
        </div>
    </div>
@yield('content')
<script src="{{ asset('assets/jquery.min.js') }}"></script>
<script src="{{ asset('assets/semantic.min.js') }}"></script>
@yield('scripts')
</body>
</html>