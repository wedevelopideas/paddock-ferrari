<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <meta name="description" content="paddock | Scuderia Ferrari">
    <link rel="stylesheet" href="{{ asset('assets/semantic.min.css') }}">
    <style type="text/css">
        body {
            background-color: #FFFFFF;
        }
        .main.container {
            margin-top: 7em;
            padding: 0 1em;
        }
    @yield('styles')
    </style>
</head>
<body>
    <div class="ui fixed inverted menu">
        <div class="ui fluid container">
            <a href="{{ route('backend') }}" class="header item">
                <img class="logo" alt="logo" src="{{ asset('assets/images/logo.png') }}">
            </a>
            <a href="{{ route('backend.countries') }}" class="item">
                {{ trans('common.countries') }}
            </a>
            <a href="{{ route('backend.langs') }}" class="item">
                {{ trans('common.langs') }}
            </a>
            <a href="{{ route('backend.seasons') }}" class="item">
                {{ trans('common.seasons') }}
            </a>
            <a href="{{ route('backend.gps') }}" class="item">
                {{ trans('common.grandprixs') }}
            </a>
            <a href="{{ route('backend.tracks') }}" class="item">
                {{ trans('common.tracks') }}
            </a>
            <a href="{{ route('backend.drivers') }}" class="item">
                {{ trans('common.drivers') }}
            </a>
            <a href="{{ route('backend.cars') }}" class="item">
                {{ trans('common.cars') }}
            </a>
            <a href="{{ route('backend.reports') }}" class="item">
                {{ trans('common.reports') }}
            </a>
            <a href="{{ route('backend.gallery') }}" class="item">
                {{ trans('common.gallery') }}
            </a>
            <a href="{{ route('backend.socialmedia') }}" class="item">
                {{ trans('common.socialmedia') }}
            </a>
            <a href="{{ route('backend.users') }}" class="item">
                {{ trans('common.users') }}
            </a>
            <div class="right menu">
                <a href="{{ route('sql') }}" title="{{ trans('common.database') }}" class="icon item">
                    <i class="database icon"></i>
                </a>
                <a href="{{ route('dashboard') }}" title="{{ trans('common.frontend') }}" class="icon item">
                    <i class="dashboard icon"></i>
                </a>
                <a href="{{ route('logout') }}" title="{{ trans('common.logout') }}" class="icon item">
                    <i class="sign out icon"></i>
                </a>
            </div>
        </div>
    </div>
@yield('content')
<script src="{{ asset('assets/jquery.min.js') }}"></script>
<script src="{{ asset('assets/semantic.min.js') }}"></script>
@yield('scripts')
</body>
</html>