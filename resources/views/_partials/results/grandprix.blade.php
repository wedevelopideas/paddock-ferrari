<div class="ui horizontal list">
    <div class="item">
        <div class="content">
            <div class="ui red label">
                {{ trans('common.statistics') }}
            </div>
        </div>
    </div>
    <div class="item">
        <div class="content">
            <div class="ui labels">
                <div class="ui red label">
                    {{ trans('results.wins') }}
                </div>
                <div class="ui label">
                    {{ $stats['wins'] }}
                </div>
            </div>
        </div>
    </div>
    <div class="item">
        <div class="content">
            <div class="ui labels">
                <div class="ui red label">
                    {{ trans('results.pole_positions') }}
                </div>
                <div class="ui label">
                    {{ $stats['poles'] }}
                </div>
            </div>
        </div>
    </div>
    <div class="item">
        <div class="content">
            <div class="ui labels">
                <div class="ui red label">
                    {{ trans('results.fastest_laps') }}
                </div>
                <div class="ui label">
                    0
                </div>
            </div>
        </div>
    </div>
    <div class="item">
        <div class="content">
            <div class="ui labels">
                <div class="ui red label">
                    {{ trans('results.podiums') }}
                </div>
                <div class="ui label">
                    {{ $stats['podiums'] }}
                </div>
            </div>
        </div>
    </div>
    <div class="item">
        <div class="content">
            <div class="ui labels">
                <div class="ui red label">
                    {{ trans('results.points') }}
                </div>
                <div class="ui label">
                    {{ $stats['points'] }}
                </div>
            </div>
        </div>
    </div>
    <div class="item">
        <div class="content">
            <div class="ui labels">
                <div class="ui red label">
                    {{ trans('results.laps') }}
                </div>
                <div class="ui label">
                    {{ $stats['laps'] }}
                </div>
            </div>
        </div>
    </div>
</div>