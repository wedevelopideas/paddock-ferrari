<div class="ui list">
    <div class="item">
        <div class="content">
            <div class="ui red fluid label">
                {{ trans('common.overall') }}
            </div>
        </div>
    </div>
    <div class="item">
        <div class="content">
            <div class="ui labels">
                <div class="ui red fluid label">
                    {{ trans('results.wins') }}
                </div>
                <div class="ui fluid label">
                    {{ $overall['wins'] }}
                </div>
            </div>
        </div>
    </div>
    <div class="item">
        <div class="content">
            <div class="ui labels">
                <div class="ui red fluid label">
                    {{ trans('results.pole_positions') }}
                </div>
                <div class="ui fluid label">
                    {{ $overall['poles'] }}
                </div>
            </div>
        </div>
    </div>
    <div class="item">
        <div class="content">
            <div class="ui labels">
                <div class="ui red fluid label">
                    {{ trans('results.fastest_laps') }}
                </div>
                <div class="ui fluid label">
                    0
                </div>
            </div>
        </div>
    </div>
    <div class="item">
        <div class="content">
            <div class="ui labels">
                <div class="ui red fluid label">
                    {{ trans('results.podiums') }}
                </div>
                <div class="ui fluid label">
                    {{ $overall['podiums'] }}
                </div>
            </div>
        </div>
    </div>
    <div class="item">
        <div class="content">
            <div class="ui labels">
                <div class="ui red fluid label">
                    {{ trans('results.points') }}
                </div>
                <div class="ui fluid label">
                    {{ $overall['points'] }}
                </div>
            </div>
        </div>
    </div>
    <div class="item">
        <div class="content">
            <div class="ui labels">
                <div class="ui red fluid label">
                    {{ trans('results.laps') }}
                </div>
                <div class="ui fluid label">
                    {{ $overall['laps'] }}
                </div>
            </div>
        </div>
    </div>
</div>