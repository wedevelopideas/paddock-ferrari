<table class="ui table">
    <thead>
        <tr>
            <th>
                {{ trans('reports.selected_sets') }}
            </th>
            <th>
                {{ trans('reports.tires.new.'.$nominated->compound1) }}
            </th>
            <th>
                {{ trans('reports.tires.new.'.$nominated->compound2) }}
            </th>
            <th>
                {{ trans('reports.tires.new.'.$nominated->compound3) }}
            </th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    @foreach($tires as $tyre)
        <tr>
            <td>
                {{ $tyre->name }}
            </td>
            @if ($tyre->c1)
                <td>
                    {{ $tyre->c1 }}
                </td>
            @endif
            @if ($tyre->c2)
                <td>
                    {{ $tyre->c2 }}
                </td>
            @endif
            @if ($tyre->c3)
                <td>
                    {{ $tyre->c3 }}
                </td>
            @endif
            @if ($tyre->c4)
                <td>
                    {{ $tyre->c4 }}
                </td>
            @endif
            @if ($tyre->c5)
                <td>
                    {{ $tyre->c5 }}
                </td>
            @endif
            <td>
                <a href="{{ route('season.race.report.tires', ['season' => $race->season, 'slug' => $race->slug, 'driverslug' => $tyre->slug]) }}" class="ui twitter mini labeled icon button">
                    <i class="twitter icon"></i>
                    {{ trans('common.twitter') }}
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>