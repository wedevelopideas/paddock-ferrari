<table class="ui table">
    <tbody>
    @foreach($tires as $tyre)
        <tr>
            <td>
                {{ $tyre->name }}
            </td>
            @if ($tyre->superhard)
                <td>
                    {{ $tyre->superhard }}
                </td>
            @endif
            @if ($tyre->hard)
                <td>
                    {{ $tyre->hard }}
                </td>
            @endif
            @if ($tyre->medium)
                <td>
                    {{ $tyre->medium }}
                </td>
            @endif
            @if ($tyre->soft)
                <td>
                    {{ $tyre->soft }}
                </td>
            @endif
            @if ($tyre->supersoft)
                <td>
                    {{ $tyre->supersoft }}
                </td>
            @endif
            @if ($tyre->ultrasoft)
                <td>
                    {{ $tyre->ultrasoft }}
                </td>
            @endif
            @if ($tyre->hypersoft)
                <td>
                    {{ $tyre->hypersoft }}
                </td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>