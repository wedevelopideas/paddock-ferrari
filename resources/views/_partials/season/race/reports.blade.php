<div class="ui red segment">
    <h3 class="ui header">
        {{ trans('common.reports') }}
    </h3>
    <h4 class="ui header">
        {{ trans('reports.technical') }}
    </h4>
    <table class="ui table">
        <thead>
            <tr>
                <th>{{ trans('common.name') }}</th>
                <th>{{ trans('reports.technicals_short.ice') }}</th>
                <th>{{ trans('reports.technicals_short.tc') }}</th>
                <th>{{ trans('reports.technicals_short.mguh') }}</th>
                <th>{{ trans('reports.technicals_short.mguk') }}</th>
                <th>{{ trans('reports.technicals_short.es') }}</th>
                <th>{{ trans('reports.technicals_short.ce') }}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($technicals as $technical)
            <tr>
                <td>
                    {{ $technical->name }}
                </td>
                <td>
                    {{ $technical->ice }}
                </td>
                <td>
                    {{ $technical->tc }}
                </td>
                <td>
                    {{ $technical->mguh }}
                </td>
                <td>
                    {{ $technical->mguk }}
                </td>
                <td>
                    {{ $technical->es }}
                </td>
                <td>
                    {{ $technical->ce }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <h4 class="ui header">
        {{ trans('reports.tyre') }}
    </h4>
    @if ($race->season === 2019)
        @include('_partials.season.race.reports_tires_2019')
    @else
        @include('_partials.season.race.reports_tires')
    @endif
</div>