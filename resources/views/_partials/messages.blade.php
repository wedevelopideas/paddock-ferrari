@if ($errors->any())
    <div class="ui error message">
        <ul class="list">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session()->has('error'))
    <div class="ui error message">
        <p>{{ session()->get('error') }}</p>
    </div>
@endif