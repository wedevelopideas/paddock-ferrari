<div class="ui red segment" style="min-height:265px">
    <div class="ui stackable two column grid">
        <div class="column">
            <div class="ui medium fluid image">
                <img src="{{ auth()->user()->avatar }}" alt="{{ auth()->user()->display_name }}">
            </div>
        </div>
        <div class="column">
            <a href="{{ route('user', ['user_name' => auth()->user()->user_name]) }}" class="ui red fluid label" style="margin-bottom: 1em">
                <i class="user icon"></i>
                {{ auth()->user()->display_name }}
            </a>
            <div class="ui vertical fluid buttons">
                <a href="{{ route('settings') }}" class="ui red icon labeled button">
                    <i class="settings icon"></i>
                    {{ trans('common.settings') }}
                </a>
                <a href="{{ route('calendar') }}" class="ui red icon labeled button">
                    <i class="calendar icon"></i>
                    {{ trans('common.calendar') }}
                </a>
                <a href="{{ route('socialmedia') }}" class="ui red icon labeled button">
                    <i class="share square outline icon"></i>
                    {{ trans('common.socialmedia') }}
                </a>
                <a href="{{ route('tests.dashboard') }}" class="ui red icon labeled button">
                    <i class="road icon"></i>
                    {{ trans('common.tests') }}
                </a>
                <a href="{{ route('dashboard.race') }}" class="ui red icon labeled button">
                    <i class="ticket icon"></i>
                    {{ trans('common.race') }}
                </a>
            </div>
        </div>
    </div>
</div>