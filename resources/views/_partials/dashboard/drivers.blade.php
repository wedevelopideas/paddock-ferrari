<div class="ui red clearing segment" style="min-height:265px">
    <h4 class="ui header">
        <i class="users icon"></i>
        <span class="content">
            {{ trans('common.current_drivers') }}
        </span>
    </h4>
    @foreach($drivers as $d => $driver)
        <a href="{{ route('drivers.view', ['slug' => $driver->slug]) }}" class="ui small {{ ($d !== 1) ? 'left' : 'right' }} floated image">
            <img src="{{ $driver->image }}" alt="{{ $driver->name }}">
            <div class="ui red bottom attached label">
                {{ $driver->name }}
            </div>
        </a>
    @endforeach
</div>