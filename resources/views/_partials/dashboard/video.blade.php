<div class="ui red segment" style="min-height:265px">
    <div class="ui red fluid card">
        <a href="{{ route('media.youtube') }}" class="image">
            <img src="{{ $video->teaser }}" alt="{{ $video->title }}">
            <div class="ui red bottom attached label">
                <div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                    <i class="time icon"></i>
                    {{ $video->created_at->diffForHumans() }} |
                    {{ $video->title }}
                </div>
            </div>
        </a>
    </div>
</div>