<div class="ui red clearing segment" style="min-height:265px">
    <h4 class="ui header">
        <i class="calendar alternate icon"></i>
        <span class="content">
            {{ trans('common.events') }}
        </span>
    </h4>
    <div class="ui list">
        @foreach($events as $event)
        <div class="item">
            @if ($event->type === 'birthday')
                <i class="birthday cake middle aligned icon"></i>
            @elseif ($event->type === 'death')
                <i class="archive middle aligned icon"></i>
            @endif
            <div class="content">
                <div class="header">
                    {{ $event->name }}
                </div>
                <div class="description">
                    {{ date('d/m/Y', strtotime($event->date)) }}
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>