<div class="ui red clearing segment" style="min-height:265px">
    <h4 class="ui header">
        <i class="feed icon"></i>
        <span class="content">
            {{ trans('common.feed') }}
        </span>
    </h4>
    <div class="ui list">
        @foreach($feeds as $feed)
            <div class="item">
                <div class="content">
                    <a href="{{ $feed['link'] }}" style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;" class="header">
                        {{ $feed['title'] }}
                    </a>
                    <div class="description">
                        {{ $feed['type'] }} |
                        {{ $feed['date'] }}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>