<div class="ui red segment">
    <div class="ui stackable equal width grid">
        @foreach($twitterAccounts as $account)
            <div class="center aligned column">
                <h4 class="ui image header">
                    <img src="{{ $account->profile_image_url }}" alt="{{ '@'.$account->screen_name }}" class="ui mini rounded image">
                    <span class="content">
                        {{ $account->name }}<br>
                        <span class="sub header">{{ '@'.$account->screen_name }}</span>
                    </span>
                </h4>
            </div>
        @endforeach
    </div>
</div>