<div class="ui red segment">
    <table class="ui table">
        <thead>
            <tr>
                <th>{{ trans('common.date') }}</th>
                <th>{{ trans('common.name') }}</th>
                <th>{{ trans('common.laptime') }}</th>
                <th>{{ trans('common.laps') }}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($results as $result)
            <tr>
                <td>{{ $result->testday->format('d/m/Y') }}</td>
                <td>{{ $result->drivername }}</td>
                <td>{{ $result->secintime() }}</td>
                <td>{{ $result->laps }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <a href="{{ route('tests.results', ['season' => $season, 'week' => $week]) }}">{{ trans('common.results') }}</a>
</div>