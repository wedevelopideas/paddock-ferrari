<div class="ui red segment">
    <div class="ui stackable equal width grid">
        <div class="center aligned middle aligned column">
            {{ $track->name }}
        </div>
        @foreach($days as $day)
        <div class="center aligned column">
            {{ $day->testday->format('d/m/Y') }}
            <p>{{ $day->driver() }}</p>
        </div>
        @endforeach
    </div>
</div>