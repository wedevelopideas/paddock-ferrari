<div class="ui red clearing segment" style="min-height:265px">
    <h4 class="ui header">
        <i class="calendar alternate icon"></i>
        <span class="content">
            {{ trans('common.races') }}
        </span>
    </h4>
    <div class="ui list">
        @foreach($races as $race)
            <div class="item">
                <i class="trophy middle aligned icon"></i>
                <div class="content">
                    {{ $race->season }}
                    {{ $race->grandprix->full_name }}
                    <div class="description">
                        {{ $race->track->name }}
                        <small>{{ $race->raceday->format('d/m/Y') }}</small>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>