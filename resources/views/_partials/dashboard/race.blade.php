<div class="ui red clearing segment">
    <div class="ui stackable grid">
        <div class="fourteen wide column">
            <h1 class="ui header">
                {{ $currentRace->full_name }}
                <span class="sub header">
                    <i class="{{ $currentRace->country }} flag"></i>
                    {{ $currentRace->name }} |
                    <i class="calendar icon"></i> {{ $currentRace->raceday->format('d/m/Y') }}
                </span>
            </h1>
        </div>
        <div class="two wide right aligned column">
            <div class="ui red mini labels">
                <div class="ui label">
                    <i class="time icon"></i>
                    {{ $awayzone }}
                </div>
                <div class="ui label">
                    <i class="home icon"></i>
                    {{ $homezone }}
                </div>
            </div>
        </div>
        <div class="four wide column">
            <h4 class="ui header">
                {{ trans('common.timetable') }}
            </h4>
            <div class="ui list">
                @foreach($sessions as $session)
                    <div class="item">
                        <div class="right floated content">
                            {{ $session->start->timezone(auth()->user()->timezone)->formatLocalized('%H:%M') }} -
                            {{ $session->end->timezone(auth()->user()->timezone)->formatLocalized('%H:%M') }}
                        </div>
                        <div class="content">
                            <strong>
                                {{ $session->start->timezone(auth()->user()->timezone)->formatLocalized('%a %d.%m') }}
                            </strong>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>