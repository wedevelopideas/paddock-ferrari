<div class="ui red segment">
    <h3 class="ui header">
        <i class="clock icon"></i>
        <span class="content">
            {{ trans('common.timetable') }}
        </span>
    </h3>
    <div class="ui list">
        @foreach($sessions as $session)
            <div class="item">
                <div class="right floated content">
                    <a href="{{ route('season.race.live', ['season' => $session->season, 'gpslug' => $grandprix->slug, 'session' => $session->session_id]) }}">
                        <i class="info circle icon"></i>
                    </a>
                    {{ $session->start->timezone(auth()->user()->timezone)->formatLocalized('%H:%M') }} -
                    {{ $session->end->timezone(auth()->user()->timezone)->formatLocalized('%H:%M') }}
                </div>
                <div class="content">
                    <strong>
                        {{ $session->start->timezone(auth()->user()->timezone)->formatLocalized('%a') }}
                    </strong>
                </div>
            </div>
        @endforeach
    </div>
</div>