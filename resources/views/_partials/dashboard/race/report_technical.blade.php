<div class="ui red segment">
    <table class="ui table">
        <thead>
            <tr>
                <th>{{ trans('reports.technical') }}</th>
                <th>{{ trans('reports.technicals_short.ice') }}</th>
                <th>{{ trans('reports.technicals_short.tc') }}</th>
                <th>{{ trans('reports.technicals_short.mguh') }}</th>
                <th>{{ trans('reports.technicals_short.mguk') }}</th>
                <th>{{ trans('reports.technicals_short.es') }}</th>
                <th>{{ trans('reports.technicals_short.ce') }}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($technicals as $technical)
            <tr>
                <td>
                    {{ $technical->name }}
                </td>
                <td>
                    {{ $technical->ice }}
                </td>
                <td>
                    {{ $technical->tc }}
                </td>
                <td>
                    {{ $technical->mguh }}
                </td>
                <td>
                    {{ $technical->mguk }}
                </td>
                <td>
                    {{ $technical->es }}
                </td>
                <td>
                    {{ $technical->ce }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>