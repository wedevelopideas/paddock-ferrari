<div class="ui red segment">
    <h3 class="ui header">
        <i class="time icon"></i>
        <span class="content">
            {{ trans('common.results') }}
        </span>
    </h3>
    <div class="ui two column grid">
        <div class="column">
            <div class="ui list">
                <div class="item">
                    <div class="content">
                        {{ $drivers[0]->name }}
                    </div>
                </div>
                @if ($driver1->count() > 0)
                    @foreach($driver1 as $time)
                        <div class="item">
                            <div class="right floated content">
                                <div class="ui red label">
                                    {{ $time->session }}
                                </div>
                            </div>
                            <div class="content">
                                {{ $time->secintime() }}
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="item">
                        <div class="content">
                            {{ trans('errors.no_laptime') }}
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <div class="column">
            <div class="ui list">
                <div class="item">
                    <div class="content">
                        {{ $drivers[1]->name }}
                    </div>
                </div>
                @if ($driver2->count() > 0)
                    @foreach($driver2 as $time)
                        <div class="item">
                            <div class="right floated content">
                                <div class="ui red label">
                                    {{ $time->session }}
                                </div>
                            </div>
                            <div class="content">
                                {{ $time->secintime() }}
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="item">
                        <div class="content">
                            {{ trans('errors.no_laptime') }}
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>