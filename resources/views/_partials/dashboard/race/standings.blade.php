<div class="ui red segment">
    <h3 class="ui header">
        <i class="clipboard icon"></i>
        <span class="content">
            {{ trans('common.standings') }}
        </span>
    </h3>
    <div class="ui list">
        <div class="item">
            <div class="right floated content">
                <div class="ui red label">{{ number_format($stD1, 0) }}</div>
            </div>
            <div class="middle aligned content">
                Sebastian Vettel
            </div>
        </div>
        <div class="item">
            <div class="right floated content">
                <div class="ui red label">{{ number_format($stD2, 0) }}</div>
            </div>
            <div class="middle aligned content">
                Charles Leclerc
            </div>
        </div>
        <div class="item">
            <div class="right floated content">
                <div class="ui red label">{{ number_format($stT , 0)}}</div>
            </div>
            <div class="middle aligned content">
                Team
            </div>
        </div>
    </div>
</div>