<div class="ui red segment">
    <table class="ui table">
        <thead>
            <tr>
                <th>
                    {{ trans('reports.selected_sets') }}
                </th>
                <th>
                    {{ trans('reports.tires.new.'.$nominated->compound1) }}
                </th>
                <th>
                    {{ trans('reports.tires.new.'.$nominated->compound2) }}
                </th>
                <th>
                    {{ trans('reports.tires.new.'.$nominated->compound3) }}
                </th>
            </tr>
        </thead>
        <tbody>
        @foreach($tires as $tyre)
            <tr>
                <td>
                    {{ $tyre->name }}
                </td>
                @if ($tyre->c1)
                    <td>
                        {{ $tyre->c1 }}
                    </td>
                @endif
                @if ($tyre->c2)
                    <td>
                        {{ $tyre->c2 }}
                    </td>
                @endif
                @if ($tyre->c3)
                    <td>
                        {{ $tyre->c3 }}
                    </td>
                @endif
                @if ($tyre->c4)
                    <td>
                        {{ $tyre->c4 }}
                    </td>
                @endif
                @if ($tyre->c5)
                    <td>
                        {{ $tyre->c5 }}
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
</div>