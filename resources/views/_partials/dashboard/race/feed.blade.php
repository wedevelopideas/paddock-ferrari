<div class="ui red segment">
    <div class="ui small feed">
        <h3 class="ui header">
            <i class="feed icon"></i>
            <span class="content">
                {{ trans('feed.ferrari') }}
            </span>
        </h3>
        @if ($feeds->count() > 0)
            @foreach($feeds as $feed)
                <div class="event">
                    <div class="content">
                        <div class="date">
                            {{ $feed->created_at->diffForHumans() }}
                        </div>
                        <div class="summary">
                            <a href="{{ route('feed.ferrari.view', ['date' => $feed->created_at->format('Y-m-d'), 'session' => $feed->session]) }}">
                                {{ $feed->title }}
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="event">
                <div class="content">
                    <div class="summary">
                        {{ trans('errors.no_feeds') }}
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>