@foreach($socials as $social)
    <div class="four wide column">
        <div class="ui red segment" style="min-height:265px">
            <img src="{{ $social->image }}" alt="{{ $social->name }}" class="ui small left floated image">
            <div class="ui vertical right floated buttons">
                @if ($social->website)
                    <a href="{{ $social->website }}" target="_blank" class="ui red icon labeled button">
                        <i class="world icon"></i>
                        {{ trans('common.website') }}
                    </a>
                @endif
                @if ($social->facebook)
                    <a href="{{ $social->facebook }}" target="_blank" class="ui red icon labeled button">
                        <i class="facebook icon"></i>
                        {{ trans('common.facebook') }}
                    </a>
                @endif
                @if ($social->twitter)
                    <a href="{{ $social->twitter }}" target="_blank" class="ui red icon labeled button">
                        <i class="twitter icon"></i>
                        {{ trans('common.twitter') }}
                    </a>
                @endif
                @if ($social->instagram)
                    <a href="{{ $social->instagram }}" target="_blank" class="ui red icon labeled button">
                        <i class="instagram icon"></i>
                        {{ trans('common.instagram') }}
                    </a>
                @endif
                @if ($social->youtube)
                    <a href="{{ $social->youtube }}" target="_blank" class="ui red icon labeled button">
                        <i class="youtube icon"></i>
                        {{ trans('common.youtube') }}
                    </a>
                @endif
            </div>
        </div>
    </div>
@endforeach