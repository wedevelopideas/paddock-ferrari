@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="world icon"></i>
                        <span class="content">
                            {{ trans('common.current_season', ['year' => $season]) }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui table">
                        <tbody>
                        @foreach($races as $race)
                            <tr>
                                <td>{{ $race->raceday->format('d/m/Y') }}</td>
                                <td>
                                    <i class="{{ $race->grandprix->country_id }} flag"></i>
                                    {{ $race->grandprix->full_name }}
                                </td>
                                <td>
                                    <a href="{{ route('season.race', ['season' => $race->season, 'gpslug' => $race->grandprix->slug]) }}" class="ui red icon labeled button">
                                        <i class="right arrow icon"></i>
                                        {{ trans('common.race') }}
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection