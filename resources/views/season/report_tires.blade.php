@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    @if (isset($race))
        <div class="ui main fluid container">
            <div class="ui stackable grid">
                <div class="row">
                    <div class="column">
                        <h1 class="ui header">
                            <i class="ticket icon"></i>
                            <span class="content">
                                {{ $race->full_name }}
                            </span>
                        </h1>
                    </div>
                </div>
                <div class="row">
                    <div class="column">
                        <a href="{{ route('season.race', ['season' => $race->season, 'gpslug' => $race->slug]) }}" class="ui red right floated labeled icon button">
                            <i class="left arrow icon"></i>
                            {{ $race->name }}
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="four wide column">
                        <img src="{{ $tyre->image }}" alt="{{ $tyre->name }}" class="ui image">
                    </div>
                    <div class="twelve wide column">
                        #{{ $race->hashtag }} {{ $race->emoji }}<br>
                        Selected tyre sets for {{ $tyre->hashtag }}<br>
                        @if ($tyre->c1)
                            {{ $tyre->selectedCompound($nominated, 'c1') }}{{ $tyre->c1 }} (C1)<br>
                        @endif
                        @if ($tyre->c2)
                            {{ $tyre->selectedCompound($nominated, 'c2') }}{{ $tyre->c2 }} (C2)<br>
                        @endif
                        @if ($tyre->c3)
                            {{ $tyre->selectedCompound($nominated, 'c3') }}{{ $tyre->c3 }} (C3)<br>
                        @endif
                        @if ($tyre->c4)
                            {{ $tyre->selectedCompound($nominated, 'c4') }}{{ $tyre->c4 }} (C4)<br>
                        @endif
                        @if ($tyre->c5)
                            {{ $tyre->selectedCompound($nominated, 'c5') }}{{ $tyre->c5 }} (C5)<br>
                        @endif
                        #ForzaFerrari #essereFerrari 🔴
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection