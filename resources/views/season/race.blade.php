@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    @if (isset($race))
        <div class="ui main fluid container">
            <div class="ui stackable grid">
                <div class="row">
                    <div class="column">
                        <h1 class="ui header">
                            <i class="ticket icon"></i>
                            <span class="content">
                            {{ $race->full_name }}
                        </span>
                        </h1>
                    </div>
                </div>
                <div class="row">
                    <div class="column">
                        @include('_partials.season.race.reports')
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection