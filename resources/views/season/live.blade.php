@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    @if (isset($race))
        <div class="ui main fluid container">
            <div class="ui stackable grid">
                <div class="row">
                    <div class="column">
                        <h1 class="ui header">
                            <i class="ticket icon"></i>
                            <span class="content">
                                {{ $race->full_name }}
                            </span>
                        </h1>
                    </div>
                </div>
                <div class="row">
                    <div class="four wide column">
                        <div class="ui red segment">
                            <form action="#" method="post" class="ui form">
                                @csrf
                                <div class="field">
                                    <label for="driver1">{{ $data['driver1']['part'] }}</label>
                                    <div class="ui left icon input">
                                        <i class="stopwatch icon"></i>
                                        <input type="text" id="driver1" name="time[1]" value="{{ $data['driver1']['this_year'] }}">
                                    </div>
                                </div>
                                <div class="field">
                                    <label for="driver2">{{ $data['driver2']['part'] }}</label>
                                    <div class="ui left icon input">
                                        <i class="stopwatch icon"></i>
                                        <input type="text" id="driver2" name="time[2]" value="{{ $data['driver2']['this_year'] }}">
                                    </div>
                                </div>
                                <div class="two ui buttons">
                                    <button class="ui red labeled icon disabled button">
                                        <i class="save icon"></i>
                                        {{ trans('common.save') }}
                                    </button>
                                    <a href="https://www.formula1.com/en/f1-live.html" target="_blank" class="ui red labeled icon button">
                                        <i class="time icon"></i>
                                        Formula1.com
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="twelve wide column">
                        <div class="ui red segment">
                            <table class="ui table">
                                <tbody>
                                    <tr>
                                        <td class="top aligned">
                                            <h4 class="ui dividing header">{{ $data['driver1']['part'] }}</h4>
                                            <p>
                                                {{ $data['driver1']['string'] }}<br>
                                                {{ $data['season']['last'] }}<br>
                                                {{ $data['driver1']['last_year'] }}<br><br>
                                                {{ $data['season']['current'] }}<br>
                                                {{ $data['driver1']['this_year'] }} {{ $data['driver1']['gap'] }}<br>
                                                #ForzaFerrari #essereFerrari 🔴
                                            </p>
                                        </td>
                                        <td class="top aligned">
                                            <h4 class="ui dividing header">{{ $data['driver2']['part'] }}</h4>
                                            <p>
                                                {{ $data['driver2']['string'] }}<br>
                                                {{ $data['season']['last'] }}<br>
                                                {{ $data['driver2']['last_year'] }}<br><br>
                                                {{ $data['season']['current'] }}<br>
                                                {{ $data['driver2']['this_year'] }} {{ $data['driver2']['gap'] }}<br>
                                                #ForzaFerrari #essereFerrari 🔴
                                            </p>
                                        </td>
                                        <td class="top aligned">
                                            <h4 class="ui dividing header">{{ $data['drivers']['part'] }}</h4>
                                            <p>
                                                {{ $data['drivers']['string'] }}<br>
                                                Seb {{ $data['drivers']['time1'] }}<br>
                                                Charles {{ $data['drivers']['time2'] }} {{ $data['drivers']['gap'] }}<br>
                                                #ForzaFerrari #essereFerrari 🔴
                                            </p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection