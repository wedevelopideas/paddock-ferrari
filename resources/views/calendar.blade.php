@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="calendar icon"></i>
                        <span class="content">
                            {{ trans('common.current_season', ['year' => $season]) }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui table">
                        <thead>
                            <tr>
                                <th>{{ trans('common.grandprix') }}</th>
                                <th colspan="2">{{ trans('common.friday') }}</th>
                                <th colspan="2">{{ trans('common.saturday') }}</th>
                                <th>{{ trans('common.sunday') }}</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th>{{ trans('common.fp1') }}</th>
                                <th>{{ trans('common.fp2') }}</th>
                                <th>{{ trans('common.fp3') }}</th>
                                <th>{{ trans('common.qualifying') }}</th>
                                <th>{{ trans('common.race') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($sessions as $session)
                            <tr>
                                <td>{{ $session->grandprix->name }}</td>
                                <td>
                                    {{ $session->sessionTime(1)->start->timezone(auth()->user()->timezone)->format('H:i') }} -
                                    {{ $session->sessionTime(1)->end->timezone(auth()->user()->timezone)->format('H:i') }}
                                    <small style="display: inherit;">{{ $session->sessionTime(1)->start->timezone(auth()->user()->timezone)->format('d/m/Y') }}</small>
                                </td>
                                <td>
                                    {{ $session->sessionTime(2)->start->timezone(auth()->user()->timezone)->format('H:i') }} -
                                    {{ $session->sessionTime(2)->end->timezone(auth()->user()->timezone)->format('H:i') }}
                                </td>
                                <td>
                                    {{ $session->sessionTime(3)->start->timezone(auth()->user()->timezone)->format('H:i') }} -
                                    {{ $session->sessionTime(3)->end->timezone(auth()->user()->timezone)->format('H:i') }}
                                    <small style="display: inherit;">{{ $session->sessionTime(3)->start->timezone(auth()->user()->timezone)->format('d/m/Y') }}</small>
                                </td>
                                <td>
                                    {{ $session->sessionTime(4)->start->timezone(auth()->user()->timezone)->format('H:i') }} -
                                    {{ $session->sessionTime(4)->end->timezone(auth()->user()->timezone)->format('H:i') }}
                                </td>
                                <td>
                                    {{ $session->sessionTime(5)->start->timezone(auth()->user()->timezone)->format('H:i') }} -
                                    {{ $session->sessionTime(5)->end->timezone(auth()->user()->timezone)->format('H:i') }}
                                    <small style="display: inherit;">{{ $session->sessionTime(5)->start->timezone(auth()->user()->timezone)->format('d/m/Y') }}</small>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection