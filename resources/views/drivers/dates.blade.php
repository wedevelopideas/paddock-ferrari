@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="users icon"></i>
                        <span class="content">
                            {{ trans('common.drivers') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="eight wide column">
                    <table class="ui table">
                        <thead>
                            <tr>
                                <th colspan="2">{{ trans('common.dateofbirth') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($birthdays as $birthday)
                            <tr>
                                <td>{{ $birthday->name }}</td>
                                <td>{{ $birthday->dateofbirth->format('d/m/Y') }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="eight wide column">
                    <table class="ui table">
                        <thead>
                            <tr>
                                <th colspan="2">{{ trans('common.dateofdeath') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($deaths as $death)
                            <tr>
                                <td>{{ $death->name }}</td>
                                <td>{{ $death->dateofdeath->format('d/m/Y') }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection