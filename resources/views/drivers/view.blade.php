@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="users icon"></i>
                        <span class="content">
                            {{ $driver->name }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="three wide column">
                    @include('drivers._partials.sidebar')
                </div>
            </div>
        </div>
    </div>
@endsection