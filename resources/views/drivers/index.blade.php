@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="users icon"></i>
                        <span class="content">
                            {{ trans('common.drivers') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('drivers.dates') }}" class="ui red right floated icon labeled button">
                        <i class="calendar outline icon"></i>
                        {{ trans('common.dates') }}
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui table">
                        <thead>
                            <tr>
                                <th>{{ trans('common.name') }}</th>
                                <th>{{ trans('common.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if ($drivers->count())
                            @foreach($drivers as $driver)
                                <tr>
                                    <td><i class="{{ $driver->country_id }} flag"></i> {{ $driver->name }}</td>
                                    <td>
                                        <a href="{{ route('drivers.view', ['slug' => $driver->slug]) }}" class="ui red icon button">
                                            <i class="user icon"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="2" class="center aligned">{{ trans('errors.no_data') }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection