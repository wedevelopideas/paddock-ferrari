@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="users icon"></i>
                        <span class="content">
                            {{ $driver->name }}
                            <span class="sub header">{{ trans('drivers.stats.wins') }}</span>
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="three wide column">
                    @include('drivers._partials.sidebar')
                </div>
                <div class="thirteen wide column">
                    <div class="ui red segment">
                        <div class="ui list">
                        @foreach($wins->reverse() as $w => $win)
                            <div class="item">
                                <div class="content">
                                    <div class="header">
                                        ({{ ++$w }}) {{ $win->grandprix }} <small>{{ $win->track }}</small>
                                    </div>
                                    <div class="description">
                                        {{ $win->dateofresult }}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection