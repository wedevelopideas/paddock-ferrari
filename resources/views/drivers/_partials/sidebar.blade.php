<div class="ui red segment">
    <div class="ui list">
        <div class="item">
            <div class="content">
                <div class="header">
                    {{ trans('common.dateofbirth') }}
                </div>
                <div class="description">
                    {{ $driver->dateofbirth->format('d/m/Y') }}
                </div>
            </div>
        </div>
        @if ($driver->dateofdeath)
            <div class="item">
                <div class="content">
                    <div class="header">
                        {{ trans('common.dateofdeath') }}
                    </div>
                    <div class="description">
                        {{ $driver->dateofdeath->format('d/m/Y') }}
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
@if (Request::is('drivers/'.$driver->slug.'/*'))
<a href="{{ route('drivers.view', ['slug' => $driver->slug]) }}" class="fluid ui red icon labeled button">
    <i class="left arrow icon"></i>
    {{ $driver->name }}
</a>
@endif
@if ($driver->hashtag)
    <div class="ui red segment">
        <div class="ui list">
            <div class="item">
                <div class="content">
                    <div class="header">
                        {{ trans('common.hashtag') }}
                    </div>
                    <div class="description">
                        <a href="https://twitter.com/search?q={{ urlencode($driver->hashtag) }}" target="_blank">{{ $driver->hashtag }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
<div class="ui red segment">
    <h4 class="ui header">
        {{ trans('common.statistics') }}
    </h4>
    <div class="ui list">
        <div class="item">
            <div class="content">
                <a href="{{ route('drivers.stats.wins', ['slug' => $driver->slug]) }}">{{ trans('drivers.stats.wins') }}</a>
            </div>
        </div>
    </div>
</div>