@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="feed icon"></i>
                        <span class="content">
                            {{ $feed->title }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="twelve wide column">
                    <div class="ui red segment">
                        <div class="ui centered image">
                            <img src="{{ $feed->image }}" alt="{{ $feed->title }}">
                        </div>
                    </div>
                </div>
                <div class="four wide column">
                    <div class="ui red segment">
                        <div class="ui red label">
                            <i class="time icon"></i>
                            {{ $feed->created_at->timezone(auth()->user()->timezone)->format('d/m/Y H:i') }}
                        </div>
                        <a href="{{ route('user', ['user_name' => $feed->author->user_name]) }}" class="ui red label">
                            <i class="user icon"></i>
                            {{ $feed->author->display_name }}
                        </a>
                    </div>
                    <div class="ui red segment">
                        <div class="ui list">
                            @foreach($feeds as $entry)
                                <div class="item">
                                    <div class="right floated content">
                                        <a href="{{ $entry->link }}" target="_blank" class="ui red mini icon button">
                                            <i class="info icon"></i>
                                        </a>
                                        <a href="{{ $entry->getTweetLink() }}" target="_blank" class="ui twitter mini icon button">
                                            <i class="retweet icon"></i>
                                        </a>
                                    </div>
                                    <div class="content">
                                        <div class="header">
                                            <i class="{{ $entry->getFlag() }} flag"></i>
                                            {{ $entry->title }}
                                        </div>
                                        <div class="description">
                                            {{ $entry->getDescription() }}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection