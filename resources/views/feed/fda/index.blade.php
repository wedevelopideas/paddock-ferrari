@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="feed icon"></i>
                        <span class="content">
                            {{ trans('feed.fda') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui red segment">
                        <div class="ui stackable grid">
                            <div class="fourteen wide center aligned middle aligned column">
                                <a href="https://fda.ferrari.com/en/news-fda/" target="_blank">
                                    <i class="england flag"></i>
                                </a>
                                <a href="https://fda.ferrari.com/it/news-fda-it/" target="_blank">
                                    <i class="it flag"></i>
                                </a>
                            </div>
                            <div class="two wide column">
                                <a href="{{ route('feed') }}" class="ui red right floated icon labeled button">
                                    <i class="feed icon"></i>
                                    {{ trans('common.feed') }}
                                </a>
                                <div class="ui red right floated icon button">
                                    <i class="add icon"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui red four cards">
                        @foreach($feeds as $feed)
                            <a href="{{ route('feed.fda.view', ['date' => $feed->created_at->format('Y-m-d')]) }}" class="card">
                                <div class="image">
                                    <img src="{{ $feed->image }}" alt="{{ $feed->title }}">
                                    <div class="ui red bottom attached label">
                                        <div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                            {{ $feed->title }}
                                        </div>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection