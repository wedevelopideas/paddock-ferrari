@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="feed icon"></i>
                        <span class="content">
                            {{ trans('common.feed') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui unstackable five column grid">
                        <div class="center aligned middle aligned column">
                            <a href="{{ route('feed.ferrari') }}">
                                {{ trans('feed.ferrari') }}
                            </a>
                        </div>
                        <div class="center aligned middle aligned column">
                            <a href="{{ route('feed.fda') }}">
                                {{ trans('feed.fda') }}
                            </a>
                        </div>
                        <div class="center aligned middle aligned column">
                            <a href="{{ route('feed.history') }}">
                                {{ trans('feed.history') }}
                            </a>
                        </div>
                        <div class="center aligned middle aligned column">
                            <a href="{{ route('feed.motorsport') }}">
                                {{ trans('feed.motorsport') }}
                            </a>
                        </div>
                        <div class="center aligned middle aligned column">
                            <a href="{{ route('feed.meta') }}" class="ui red icon button">
                                <i class="share icon"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui red segment">
                        <div class="ui red four stackable cards">
                            <div class="ui red card">
                                <a href="{{ route('feed.ferrari') }}" class="image">
                                    <img src="{{ $ferrari->image }}" alt="{{ $ferrari->title }}">
                                    <div class="ui red top right attached label">{{ trans('feed.ferrari') }}</div>
                                    <div class="ui red bottom attached label">
                                        <div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                            {{ $ferrari->title }}
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="ui red card">
                                <a href="{{ route('feed.fda') }}" class="image">
                                    <img src="{{ $fda->image }}" alt="{{ $fda->title }}">
                                    <div class="ui red top right attached label">{{ trans('feed.fda') }}</div>
                                    <div class="ui red bottom attached label">
                                        <div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                            {{ $fda->title }}
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @if (!empty($history))
                                <div class="ui red card">
                                    <a href="{{ route('feed.history') }}" class="image">
                                        <img src="{{ $history->image }}" alt="{{ $history->title }}">
                                        <div class="ui red top right attached label">{{ trans('feed.history') }}</div>
                                        <div class="ui red bottom attached label">
                                            <div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                                {{ $history->title }}
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endif
                            <div class="ui red card">
                                <a href="{{ route('feed.motorsport') }}" class="image">
                                    <img src="{{ $motorsport->image }}" alt="{{ $motorsport->title }}">
                                    <div class="ui red top right attached label">{{ trans('feed.motorsport') }}</div>
                                    <div class="ui red bottom attached label">
                                        <div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                            {{ $motorsport->title }}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection