@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="feed icon"></i>
                        <span class="content">
                            {{ trans('feed.motorsport') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui red segment">
                        <div class="ui stackable grid">
                            <div class="fourteen wide center aligned middle aligned column">
                                @foreach($langs as $lang)
                                    <a href="{{ $lang->motorsport_link }}" target="_blank">
                                        <i class="{{ $lang->getFlag() }} flag"></i>
                                    </a>
                                @endforeach
                            </div>
                            <div class="two wide column">
                                <a href="{{ route('feed') }}" class="ui red right floated icon labeled button">
                                    <i class="feed icon"></i>
                                    {{ trans('common.feed') }}
                                </a>
                                <a href="{{ route('feed.motorsport.add') }}" class="ui red right floated icon button">
                                    <i class="add icon"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui red segment">
                        <div class="ui red four cards">
                            @foreach($feeds as $feed)
                                <div class="card">
                                    <a href="{{ route('feed.motorsport.view', ['id' => $feed->id]) }}" class="image">
                                        <img src="{{ $feed->image }}" alt="{{ $feed->title }}">
                                        <div class="ui red bottom attached label">
                                            <div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                                {{ $feed->title }}
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection