@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="feed icon"></i>
                        <span class="content">
                            {{ trans('feed.motorsport') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <form action="" method="post" class="ui form">
                        @csrf

                        @foreach($languages as $language)
                        <div class="field">
                            <i class="{{ $language->getFlag() }} flag"></i> {{ $language->name }}
                        </div>
                        @endforeach
                        <button class="ui red right floated icon labeled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection