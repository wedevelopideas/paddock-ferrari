@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="feed icon"></i>
                        <span class="content">
                            {{ trans('feed.history') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui red segment">
                        <div class="ui stackable grid">
                            <div class="fourteen wide center aligned middle aligned column">
                                <a href="https://formula1.ferrari.com/en/home-days-en/" target="_blank">
                                    <i class="england flag"></i>
                                </a>
                                <a href="https://formula1.ferrari.com/it/home-days-it/" target="_blank">
                                    <i class="it flag"></i>
                                </a>
                            </div>
                            <div class="two wide column">
                                <a href="{{ route('feed') }}" class="ui red right floated icon labeled button">
                                    <i class="feed icon"></i>
                                    {{ trans('common.feed') }}
                                </a>
                                <div class="ui red right floated icon button">
                                    <i class="add icon"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if ($current)
                <div class="row">
                    <div class="column">
                        <div class="ui red segment">
                            <div class="ui items">
                                <div class="item">
                                    <a href="{{ route('feed.history.view', ['date' => $current->date->format('Y-m-d')]) }}" class="ui medium image">
                                        <img src="{{ $current->image }}" alt="{{ $current->title }}">
                                        <div class="ui red bottom attached label">
                                            {{ trans('feed.happened_today') }}
                                        </div>
                                    </a>
                                    <div class="content">
                                        <a href="{{ route('feed.history.view', ['date' => $current->date->format('Y-m-d')]) }}" class="header">{{ $current->title }}</a>
                                        <div class="meta">
                                            <span>{{ $current->author->display_name }}</span>
                                        </div>
                                        <div class="description">
                                            <p>{{ $current->description }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="column">
                    <div class="ui red four cards">
                        @foreach($feeds as $feed)
                            <div class="card">
                                <a href="{{ route('feed.history.view', ['date' => $feed->date->format('Y-m-d')]) }}" class="image">
                                    <img src="{{ $feed->image }}" alt="{{ $feed->title }}">
                                    <div class="ui red top right attached label">
                                        {{ $feed->date->format('d/m/Y') }}
                                    </div>
                                    <div class="ui red bottom attached label">
                                        <div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                            {{ $feed->title }}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection