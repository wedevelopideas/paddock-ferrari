@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="feed icon"></i>
                        <span class="content">
                            {{ trans('common.feed') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    @include('_partials.messages')
                    <form action="{{ route('feed.meta.result') }}" method="post" class="ui form">
                        @csrf

                        <div class="field{{ $errors->has('link') ? ' error' : '' }}">
                            <div class="ui labeled input">
                                <div class="ui red label">
                                    <i class="share icon"></i>
                                </div>
                                <input type="url" name="link" placeholder="{{ trans('common.link') }}" value="{{ (empty(old('link'))) ? Session::get('results')['link'] : old('link') }}">
                            </div>
                        </div>
                        <button type="submit" class="ui red labeled icon button">
                            <i class="share icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
            @if (Session::has('results'))
                <div class="row">
                    <div class="four wide column">
                        <img src="{{ Session::get('results')['image'] }}" alt="{{ Session::get('results')['title'] }}" class="ui bordered image">
                    </div>
                    <div class="twelve wide column">
                        <div class="ui list">
                            <div class="item">
                                <div class="content">
                                    <div class="header">
                                        {{ trans('common.title') }}
                                    </div>
                                    <div class="description">
                                        {{ Session::get('results')['title'] }}
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="content">
                                    <div class="header">
                                        {{ trans('media.description') }}
                                    </div>
                                    <div class="description">
                                        {{ Session::get('results')['description'] }}
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="content">
                                    <div class="header">
                                        {{ trans('common.image') }}
                                    </div>
                                    <div class="description">
                                        {{ Session::get('results')['image'] }}
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="content">
                                    <div class="header">
                                        {{ trans('common.link') }}
                                    </div>
                                    <div class="description">
                                        {{ Session::get('results')['link'] }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection