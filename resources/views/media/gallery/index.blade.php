@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="images icon"></i>
                        <span class="content">
                            {{ trans('common.galleries') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('media.gallery.twitter') }}" class="ui right floated twitter icon labeled button">
                        <i class="twitter icon"></i>
                        {{ trans('common.twitter') }}
                    </a>
                    <a href="https://formula1.ferrari.com/en/gallery/" target="_blank" class="ui right floated red icon labeled button">
                        <i class="images icon"></i>
                        Ferrari
                    </a>
                    <a href="https://www.motorsportimages.com/" target="_blank" class="ui right floated red icon labeled button">
                        <i class="images icon"></i>
                        Motorsport Images
                    </a>
                    <a href="https://www.auto-motor-und-sport.de/formel-1/news/" target="_blank" class="ui right floated red icon labeled button">
                        <i class="images icon"></i>
                        Auto Motor und Sport
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui red cards">
                        @foreach($albums as $album)
                            <div class="card">
                                <a href="{{ route('media.gallery.album', ['slug' => $album->slug]) }}" class="image">
                                    <img src="{{ $album->image }}" alt="{{ $album->name }}">
                                    <div class="ui red bottom attached label">
                                        <div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                            <i class="time icon"></i>
                                            {{ $album->created_at->diffForHumans() }} |
                                            {{ $album->name }}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection