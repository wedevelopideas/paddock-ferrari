@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="images icon"></i>
                        <span class="content">
                            {{ trans('common.galleries') }}
                            <small>{{ trans('common.twitter') }}</small>
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui table">
                        <thead>
                            <tr>
                                <th>{{ trans('common.name') }}</th>
                                <th>{{ trans('common.image') }}</th>
                                <th>{{ trans('common.image') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($accounts as $account)
                            <tr>
                                <td>{{ $account->name }}</td>
                                <td>
                                    @if ($account->profile_banner)
                                        <img src="{{ $account->profile_banner }}" alt="{{ $account->name }}" class="ui small image">
                                    @endif
                                </td>
                                <td>
                                    @if ($account->profile_image)
                                        <img src="{{ $account->profile_image }}" alt="{{ $account->name }}" class="ui small image">
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection