@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="images icon"></i>
                        <span class="content">
                            {{ $album->name }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui red cards">
                        @foreach($images as $image)
                            <div class="card">
                                <div class="image">
                                    <img src="{{ $image->image }}" alt="{{ $album->name }}">
                                    @if ($image->source)
                                    <div class="ui red top right attached label">
                                        <i class="copyright outline icon"></i>
                                        {{ $image->source }}
                                    </div>
                                    @endif
                                    <div class="ui red bottom attached label">
                                        <div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                            <i class="time icon"></i>
                                            {{ $image->created_at->diffForHumans() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection