@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="play icon"></i>
                        <span class="content">
                            {{ trans('common.media') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui red clearing segment">
                        <div class="ui red stackable cards">
                            @foreach($albums as $album)
                                <div class="card">
                                    <a href="{{ route('media.gallery') }}" class="image">
                                        <img src="{{ $album->image }}" alt="{{ $album->name }}">
                                        <div class="ui red bottom attached label">
                                            <div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                                <i class="time icon"></i>
                                                {{ $album->created_at->diffForHumans() }} |
                                                {{ $album->name }}
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui red clearing segment">
                        <div class="ui red stackable cards">
                            @foreach($videos as $video)
                                <div class="card">
                                    <a href="{{ route('media.youtube') }}" class="image">
                                        <img src="{{ $video->thumbnail }}" alt="{{ $video->title }}">
                                        <div class="ui red bottom attached label">
                                            <div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                                <i class="time icon"></i>
                                                {{ $video->created_at->diffForHumans() }} |
                                                {{ $video->title }}
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection