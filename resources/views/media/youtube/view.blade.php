@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="youtube icon"></i>
                        <span class="content">
                            {{ $video->title }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="ten wide column">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/{{ $video->youtube_id }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="six wide column">
                    <div>
                        <a href="{{ route('media.youtube') }}" class="ui red mini right floated icon labeled button">
                            <i class="youtube icon"></i>
                            {{ trans('common.youtube') }}
                        </a>
                        <div class="ui red label">
                            <i class="time icon"></i>
                            {{ $video->created_at->diffForHumans() }}
                        </div>
                    </div>
                    <div style="padding-top: 1em;">
                        <div class="ui form">
                            <div class="field">
                                <div class="ui labeled left icon input">
                                    <i class="youtube red icon"></i>
                                    <input type="url" name="link" placeholder="{{ trans('common.link') }}" value="https://www.youtube.com/watch?v={{ $video->youtube_id }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="padding-top: 1em;">
                        <p>{!! nl2br(e($video->description)) !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection