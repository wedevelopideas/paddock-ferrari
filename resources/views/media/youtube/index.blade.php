@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="youtube icon"></i>
                        <span class="content">
                            {{ trans('common.youtube') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('media.youtube.add') }}" class="ui red right floated icon labeled button">
                        <i class="add icon"></i>
                        {{ trans('media.add_video') }}
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui red four cards">
                        @foreach($videos as $video)
                            <div class="card">
                                <a href="{{ route('media.youtube.view', ['youtube_id' => $video->youtube_id]) }}" class="image">
                                    <img src="{{ $video->thumbnail }}" alt="{{ $video->title }}">
                                </a>
                                <div class="content">
                                    <div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">{{ $video->title }}</div>
                                    <div class="meta">
                                        <div class="ui red tiny label">
                                            <i class="time icon"></i>
                                            {{ $video->created_at->diffForHumans() }}
                                        </div>
                                        <a href="{{ route('media.youtube.edit', ['id' => $video->id]) }}" class="ui red tiny icon label">
                                            <i class="edit icon"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection