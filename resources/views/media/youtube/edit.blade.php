@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="youtube icon"></i>
                        <span class="content">
                            {{ trans('media.edit_video') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    @include('_partials.messages')
                    <form action="{{ route('media.youtube.edit', ['id' => $video->id]) }}" method="post" class="ui form">
                        @csrf

                        <div class="two fields">
                            <div class="field{{ $errors->has('youtube_id') ? ' error' : '' }}">
                                <label for="youtube_id">{{ trans('media.youtube_id') }}</label>
                                <input type="text" name="youtube_id" id="youtube_id" placeholder="{{ trans('media.youtube_id') }}" value="{{ $video->youtube_id }}">
                            </div>
                            <div class="field{{ $errors->has('title') ? ' error' : '' }}">
                                <label for="title">{{ trans('common.title') }}</label>
                                <input type="text" name="title" id="title" placeholder="{{ trans('common.title') }}" value="{{ $video->title }}">
                            </div>
                        </div>
                        <div class="field{{ $errors->has('description') ? ' error' : '' }}">
                            <label for="description">{{ trans('media.description') }}</label>
                            <textarea name="description" id="description" cols="30" rows="10" placeholder="{{ trans('media.description') }}">{{ $video->description }}</textarea>
                        </div>
                        <button type="submit" class="ui red right floated icon labeled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection