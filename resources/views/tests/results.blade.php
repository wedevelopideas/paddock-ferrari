@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <table class="ui table">
                        <thead>
                            <tr>
                                <th>{{ trans('common.date') }}</th>
                                <th>{{ trans('common.name') }}</th>
                                <th>{{ trans('common.laptime') }}</th>
                                <th>{{ trans('common.gap') }}</th>
                                <th>{{ trans('common.laps') }}</th>
                                <th>{{ trans('common.race_distance_in_percent') }}</th>
                                <th>{{ trans('common.race_distance_in_km') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($results as $result)
                            <tr>
                                <td>{{ $result->testday->format('d/m/Y') }}</td>
                                <td>{{ $result->drivername }}</td>
                                <td>{{ $result->secintime() }}</td>
                                <td>{{ $result->gap($result->laptime, $fastest) }}</td>
                                <td>{{ $result->laps }}</td>
                                <td>{{ number_format($result->laps * 100 / $result->racelaps, 2)  }} %</td>
                                <td>{{ $result->laps * $result->tracklength }} km</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui table">
                        <thead>
                        <tr>
                            <th>{{ trans('common.date') }}</th>
                            <th>{{ trans('common.name') }}</th>
                            <th>{{ trans('common.laptime') }}</th>
                            <th>{{ trans('common.gap') }}</th>
                            <th>{{ trans('common.laps') }}</th>
                            <th>{{ trans('common.race_distance_in_percent') }}</th>
                            <th>{{ trans('common.race_distance_in_km') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($fastests as $result)
                            <tr>
                                <td>{{ $result->testday->format('d/m/Y') }}</td>
                                <td>{{ $result->drivername }}</td>
                                <td>{{ $result->secintime() }}</td>
                                <td>{{ $result->gap($result->laptime, $fastest) }}</td>
                                <td>{{ $result->laps }}</td>
                                <td>{{ number_format($result->laps * 100 / $result->racelaps, 2)  }} %</td>
                                <td>{{ $result->laps * $result->tracklength }} km</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection