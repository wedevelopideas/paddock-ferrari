@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="car icon"></i>
                        <span class="content">
                            {{ trans('common.tests') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui table">
                        <tbody>
                        @foreach($tests as $test)
                            <tr>
                                <td>
                                    {{ $test->start->format('d/m/Y') }}
                                    <small>{{ $test->end->format('d/m/Y') }}</small>
                                </td>
                                <td>
                                    <a href="{{ route('tests.results', ['season' => $test->season, 'week' => $test->week]) }}">
                                        {{ trans('common.results') }}
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection