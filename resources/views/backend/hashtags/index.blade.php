@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="hashtag icon"></i>
                        <span class="content">
                            {{ trans('common.hashtags') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('backend.hashtags.add') }}" class="ui red right floated icon labeled button">
                        <i class="add icon"></i>
                        {{ trans('hashtags.add') }}
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui table">
                        <thead>
                        <tr>
                            <th>{{ trans('common.hashtag') }}</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if ($hashtags->count())
                            @foreach($hashtags as $hashtag)
                                <tr>
                                    <td>{{ $hashtag->name }}</td>
                                    <td>
                                        <a href="{{ route('backend.hashtags.edit', ['id' => $hashtag->id]) }}" class="ui red icon button">
                                            <i class="edit icon"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="2" class="center aligned">{{ trans('errors.no_data') }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection