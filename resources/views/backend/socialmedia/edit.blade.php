@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="share square outline icon"></i>
                        <span class="content">
                            {{ trans('socialmedia.twitter.edit_socialmedia') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    @include('_partials.messages')
                    <form action="{{ route('backend.socialmedia.edit', ['id' => $social->id]) }}" method="post" class="ui form">
                        @csrf

                        <div class="two fields">
                            <div class="field{{ $errors->has('name') ? ' error' : '' }}">
                                <label for="name">{{ trans('common.name') }}</label>
                                <input type="text" name="name" id="name" placeholder="{{ trans('common.name') }}" value="{{ $social->name }}">
                            </div>
                            <div class="field{{ $errors->has('image') ? ' error' : '' }}">
                                <label for="image">{{ trans('common.image') }}</label>
                                <input type="text" name="image" id="image" placeholder="{{ trans('common.image') }}" value="{{ $social->image }}">
                            </div>
                        </div>
                        <div class="field{{ $errors->has('website') ? ' error' : '' }}">
                            <label for="website">{{ trans('common.website') }}</label>
                            <input type="text" name="website" id="website" placeholder="{{ trans('common.website') }}" value="{{ $social->website }}">
                        </div>
                        <div class="two fields">
                            <div class="field{{ $errors->has('facebook') ? ' error' : '' }}">
                                <label for="facebook">{{ trans('common.facebook') }}</label>
                                <input type="text" name="facebook" id="facebook" placeholder="{{ trans('common.facebook') }}" value="{{ $social->facebook }}">
                            </div>
                            <div class="field{{ $errors->has('twitter') ? ' error' : '' }}">
                                <label for="twitter">{{ trans('common.twitter') }}</label>
                                <input type="text" name="twitter" id="twitter" placeholder="{{ trans('common.twitter') }}" value="{{ $social->twitter }}">
                            </div>
                        </div>
                        <div class="two fields">
                            <div class="field{{ $errors->has('instagram') ? ' error' : '' }}">
                                <label for="instagram">{{ trans('common.instagram') }}</label>
                                <input type="text" name="instagram" id="instagram" placeholder="{{ trans('common.instagram') }}" value="{{ $social->instagram }}">
                            </div>
                            <div class="field{{ $errors->has('youtube') ? ' error' : '' }}">
                                <label for="youtube">{{ trans('common.youtube') }}</label>
                                <input type="text" name="youtube" id="youtube" placeholder="{{ trans('common.youtube') }}" value="{{ $social->youtube }}">
                            </div>
                        </div>
                        <button type="submit" class="ui red right floated icon labeled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection