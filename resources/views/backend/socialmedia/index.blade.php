@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="share square outline icon"></i>
                        <span class="content">
                            {{ trans('common.socialmedia') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="twelve wide column">
                    <a href="{{ route('backend.hashtags') }}" class="ui red icon labeled button">
                        <i class="hashtag icon"></i>
                        {{ trans('common.hashtags') }}
                    </a>
                    <a href="{{ route('backend.twitter') }}" class="ui twitter icon labeled button">
                        <i class="twitter icon"></i>
                        {{ trans('common.twitter') }}
                    </a>
                </div>
                <div class="four wide column">
                    <a href="{{ route('backend.socialmedia.add') }}" class="ui red right floated labeled icon button">
                        <i class="add icon"></i>
                        {{ trans('socialmedia.twitter.add_socialmedia') }}
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui table">
                        <thead>
                            <tr>
                                <th>{{ trans('common.name') }}</th>
                                <th>{{ trans('common.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($socials as $social)
                            <tr>
                                <td>{{ $social->name }}</td>
                                <td>
                                    <a href="{{ route('backend.socialmedia.edit', ['id' => $social->id]) }}" class="ui red icon button">
                                        <i class="edit icon"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection