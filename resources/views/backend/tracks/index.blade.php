@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="road icon"></i>
                        <span class="content">
                            {{ trans('common.tracks') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('backend.tracks.add') }}" class="ui red right floated icon labeled button">
                        <i class="add icon"></i>
                        {{ trans('tracks.add') }}
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui table">
                        <thead>
                        <tr>
                            <th>{{ trans('common.name') }}</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if ($tracks->count())
                            @foreach($tracks as $track)
                                <tr>
                                    <td{{ ($track->active === 0) ? ' class=disabled' : '' }}><i class="{{ $track->country_id }} flag"></i> {{ $track->name }}</td>
                                    <td>
                                        <a href="{{ route('backend.tracks.edit', ['id' => $track->id]) }}" class="ui red icon button">
                                            <i class="edit icon"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="2" class="center aligned">{{ trans('errors.no_data') }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection