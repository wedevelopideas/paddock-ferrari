@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="road icon"></i>
                        <span class="content">
                            {{ trans('tracks.edit') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    @include('_partials.messages')
                    <form action="{{ route('backend.tracks.edit', ['id' => $track->id]) }}" method="post" class="ui form">
                        @csrf

                        <div class="two fields">
                            <div class="field{{ $errors->has('country_id') ? ' error' : '' }}">
                                <label for="country_id">{{ trans('common.country') }}</label>
                                <select name="country_id" id="country_id">
                                    @foreach($countries as $country)
                                        <option value="{{ $country->code }}"{{ ($country->code === $track->country_id) ? ' selected' : '' }}>{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="field{{ $errors->has('name') ? ' error' : '' }}">
                                <label for="name">{{ trans('common.name') }}</label>
                                <input type="text" name="name" id="name" placeholder="{{ trans('common.name') }}" value="{{ $track->name }}">
                            </div>
                        </div>
                        <div class="two fields">
                            <div class="field{{ $errors->has('laps') ? ' error' : '' }}">
                                <label for="laps">{{ trans('common.laps') }}</label>
                                <input type="number" name="laps" placeholder="{{ trans('common.laps') }}" value="{{ $track->laps }}">
                            </div>
                            <div class="field{{ $errors->has('length') ? ' error' : '' }}">
                                <label for="length">{{ trans('tracks.length') }}</label>
                                <input type="number" name="length" placeholder="{{ trans('tracks.length') }}" value="{{ $track->length }}">
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" name="active" tabindex="0" {{ ($track->active === true) ? 'checked' : '' }} value="{{ $track->active }}" class="hidden">
                                <label>{{ trans('tracks.active') }}</label>
                            </div>
                        </div>
                        <button type="submit" class="ui red right floated icon labeled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('.ui.checkbox')
            .checkbox()
        ;
    </script>
@endsection