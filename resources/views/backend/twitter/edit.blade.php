@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="twitter square icon"></i>
                        <span class="content">
                            {{ trans('socialmedia.twitter.edit_msg') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    @include('_partials.messages')
                    <form action="{{ route('backend.twitter.edit', ['id' => $account->id]) }}" method="post" class="ui form">
                        @csrf

                        <div class="field{{ $errors->has('twitter_id') ? ' error' : '' }}">
                            <label for="twitter_id">{{ trans('socialmedia.twitter.id') }}</label>
                            <input type="number" name="twitter_id" id="twitter_id" placeholder="{{ trans('socialmedia.twitter.id') }}" value="{{ $account->twitter_id }}">
                        </div>
                        <div class="two fields">
                            <div class="field{{ $errors->has('name') ? ' error' : '' }}">
                                <label for="name">{{ trans('socialmedia.twitter.name') }}</label>
                                <input type="text" name="name" id="name" placeholder="{{ trans('socialmedia.twitter.name') }}" value="{{ $account->name }}">
                            </div>
                            <div class="field{{ $errors->has('screen_name') ? ' error' : '' }}">
                                <label for="screen_name">{{ trans('socialmedia.twitter.screen_name') }}</label>
                                <input type="text" name="screen_name" id="screen_name" placeholder="{{ trans('socialmedia.twitter.screen_name') }}" value="{{ $account->screen_name }}">
                            </div>
                        </div>
                        <div class="field{{ $errors->has('description') ? ' error' : '' }}">
                            <label for="description">{{ trans('socialmedia.twitter.description') }}</label>
                            <textarea type="text" name="description" id="description" placeholder="{{ trans('socialmedia.twitter.description') }}">{{ $account->description }}</textarea>
                        </div>
                        <div class="field{{ $errors->has('profile_image_url') ? ' error' : '' }}">
                            <label for="profile_image_url">{{ trans('socialmedia.twitter.profile_image') }}</label>
                            <input type="text" name="profile_image_url" id="profile_image_url" placeholder="{{ trans('socialmedia.twitter.profile_image') }}" value="{{ $account->profile_image_url }}">
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" name="owner" tabindex="0" {{ ($account->owner === true) ? 'checked' : '' }} value="{{ $account->owner }}" class="hidden">
                                <label>{{ trans('socialmedia.twitter.owner_msg') }}</label>
                            </div>
                        </div>
                        <button type="submit" class="ui red right floated icon labeled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('.ui.checkbox')
            .checkbox()
        ;
    </script>
@endsection