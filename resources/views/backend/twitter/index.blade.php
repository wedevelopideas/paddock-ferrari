@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="twitter square icon"></i>
                        <span class="content">
                            {{ trans('common.twitter') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('backend.twitter.add') }}" class="ui twitter right floated icon labeled button">
                        <i class="add icon"></i>
                        {{ trans('socialmedia.twitter.add') }}
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui table">
                        <thead>
                            <tr>
                                <th>{{ trans('common.id') }}</th>
                                <th>{{ trans('common.name') }}</th>
                                <th>{{ trans('common.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if ($accounts->count())
                            @foreach($accounts as $account)
                                <tr>
                                    <td>{{ $account->twitter_id }}</td>
                                    <td>
                                        <h4 class="ui image header">
                                            <img src="{{ $account->profile_image_url }}" alt="{{ '@'.$account->screen_name }}" class="ui mini rounded image">
                                            <span class="content">
                                                {{ $account->name }} {!! $account->owner() !!}<br>
                                                <span class="sub header">{{ '@'.$account->screen_name }}</span>
                                            </span>
                                        </h4>
                                    </td>
                                    <td>
                                        <div class="ui buttons">
                                            <a href="{{ route('backend.twitter.edit', ['id' => $account->id]) }}" title="{{ trans('socialmedia.twitter.edit_msg') }}" class="ui red icon button">
                                                <i class="edit icon"></i>
                                            </a>
                                            <a href="{{ $account->twitterLink() }}" target="_blank" title="{{ '@'.$account->screen_name }}" class="ui red icon button">
                                                <i class="twitter icon"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="3" class="center aligned">{{ trans('errors.no_data') }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection