@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="world icon"></i>
                        <span class="content">
                            {{ trans('common.grandprixs') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('backend.gps.add') }}" class="ui red right floated icon labeled button">
                        <i class="add icon"></i>
                        {{ trans('grandprixs.add') }}
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui table">
                        <thead>
                        <tr>
                            <th>{{ trans('common.grandprix') }}</th>
                            <th>{{ trans('common.full_name') }}</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if ($gps->count())
                            @foreach($gps as $gp)
                                <tr>
                                    <td{{ ($gp->active === 0) ? ' class=disabled' : '' }}><i class="{{ $gp->country_id }} flag"></i> {{ $gp->name }}</td>
                                    <td{{ ($gp->active === 0) ? ' class=disabled' : '' }}>{{ $gp->full_name }}</td>
                                    <td>
                                        <a href="{{ route('backend.gps.edit', ['id' => $gp->id]) }}" class="ui red icon button">
                                            <i class="edit icon"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="2" class="center aligned">{{ trans('errors.no_data') }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection