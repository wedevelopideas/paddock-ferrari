@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="world icon"></i>
                        <span class="content">
                            {{ trans('grandprixs.edit') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    @include('_partials.messages')
                    <form action="{{ route('backend.gps.edit', ['id' => $gp->id]) }}" method="post" class="ui form">
                        @csrf

                        <div class="two fields">
                            <div class="field{{ $errors->has('country_id') ? ' error' : '' }}">
                                <label for="country_id">{{ trans('common.country') }}</label>
                                <select name="country_id" id="country_id">
                                    @foreach($countries as $country)
                                        <option value="{{ $country->code }}"{{ ($country->code === $gp->country_id) ? ' selected' : '' }}>{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="field{{ $errors->has('name') ? ' error' : '' }}">
                                <label for="name">{{ trans('common.name') }}</label>
                                <input type="text" name="name" id="name" placeholder="{{ trans('common.name') }}" value="{{ $gp->name }}">
                            </div>
                        </div>
                        <div class="field{{ $errors->has('full_name') ? ' error' : '' }}">
                            <label for="full_name">{{ trans('common.full_name') }}</label>
                            <input type="text" name="full_name" id="full_name" placeholder="{{ trans('common.full_name') }}" value="{{ $gp->full_name }}">
                        </div>
                        <div class="two fields">
                            <div class="field{{ $errors->has('hashtag') ? ' error' : '' }}">
                                <label for="hashtag">{{ trans('common.hashtag') }}</label>
                                <input type="text" name="hashtag" id="hashtag" placeholder="{{ trans('common.hashtag') }}" value="{{ $gp->hashtag }}">
                            </div>
                            <div class="field{{ $errors->has('emoji') ? ' error' : '' }}">
                                <label for="emoji">{{ trans('common.emoji') }}</label>
                                <input type="text" name="emoji" id="emoji" placeholder="{{ trans('common.emoji') }}" value="{{ $gp->emoji }}">
                            </div>
                        </div>
                        <div class="field{{ $errors->has('timezone') ? ' error' : '' }}">
                            <label for="timezone">{{ trans('common.timezone') }}</label>
                            <select name="timezone" id="timezone">
                                @foreach(timezone_identifiers_list() as $timezone)
                                    <option value="{{ $timezone }}" {{ ($timezone === $gp->timezone) ? ' selected' : '' }}>{{ $timezone }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" name="active" tabindex="0" {{ ($gp->active === true) ? 'checked' : '' }} value="{{ $gp->active }}" class="hidden">
                                <label>{{ trans('grandprixs.active') }}</label>
                            </div>
                        </div>
                        <button type="submit" class="ui red right floated icon labeled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('.ui.checkbox')
            .checkbox()
        ;
    </script>
@endsection