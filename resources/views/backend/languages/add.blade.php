@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="language icon"></i>
                        <span class="content">
                            {{ trans('langs.add') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    @include('_partials.messages')
                    <form action="{{ route('backend.langs.add') }}" method="post" class="ui form">
                        @csrf

                        <div class="two fields">
                            <div class="field{{ $errors->has('code') ? ' error' : '' }}">
                                <label for="code">{{ trans('common.code') }}</label>
                                <input type="text" name="code" placeholder="{{ trans('common.code') }}" value="{{ old('code') }}">
                            </div>
                            <div class="field{{ $errors->has('name') ? ' error' : '' }}">
                                <label for="name">{{ trans('common.name') }}</label>
                                <input type="text" name="name" placeholder="{{ trans('common.name') }}" value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="field{{ $errors->has('motorsport_link') ? ' error' : '' }}">
                            <label for="motorsport_link">{{ trans('common.link') }}</label>
                            <input type="url" name="motorsport_link" placeholder="{{ trans('common.link') }}" value="{{ old('motorsport_link') }}">
                        </div>
                        <div class="inline fields">
                            <div class="ui checkbox">
                                <input type="checkbox" name="news" tabindex="0" value="1" class="hidden">
                                <label>{{ trans('langs.news') }}</label>
                            </div>
                        </div>
                        <div class="inline fields">
                            <div class="ui checkbox">
                                <input type="checkbox" name="active" tabindex="0" value="1" class="hidden">
                                <label>{{ trans('langs.active') }}</label>
                            </div>
                        </div>
                        <button type="submit" class="ui red right floated icon labeled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('.ui.checkbox')
            .checkbox()
        ;
    </script>
@endsection