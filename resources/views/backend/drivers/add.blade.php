@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="users icon"></i>
                        <span class="content">
                            {{ trans('drivers.add') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    @include('_partials.messages')
                    <form action="{{ route('backend.drivers.add') }}" method="post" class="ui form">
                        @csrf

                        <div class="two fields">
                            <div class="field{{ $errors->has('country_id') ? ' error' : '' }}">
                                <label for="country_id">{{ trans('common.country') }}</label>
                                <select name="country_id" id="country_id">
                                    @foreach($countries as $country)
                                        <option value="{{ $country->code }}">{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="field{{ $errors->has('status') ? ' error' : '' }}">
                                <label for="status">{{ trans('common.status') }}</label>
                                <select name="status" id="status">
                                    <option value="1">{{ trans('drivers.works') }}</option>
                                    <option value="2">{{ trans('drivers.test') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="two fields">
                            <div class="field{{ $errors->has('first_name') ? ' error' : '' }}">
                                <label for="first_name">{{ trans('common.first_name') }}</label>
                                <input type="text" name="first_name" id="first_name" placeholder="{{ trans('common.first_name') }}" value="{{ old('first_name') }}">
                            </div>
                            <div class="field{{ $errors->has('last_name') ? ' error' : '' }}">
                                <label for="last_name">{{ trans('common.last_name') }}</label>
                                <input type="text" name="last_name" id="last_name" placeholder="{{ trans('common.last_name') }}" value="{{ old('last_name') }}">
                            </div>
                        </div>
                        <div class="field{{ $errors->has('hashtag') ? ' error' : '' }}">
                            <label for="hashtag">{{ trans('common.hashtag') }}</label>
                            <input type="text" name="hashtag" id="hashtag" placeholder="{{ trans('common.hashtag') }}" value="{{ old('hashtag') }}">
                        </div>
                        <div class="two fields">
                            <div class="field{{ $errors->has('dateofbirth') ? ' error' : '' }}">
                                <label for="dateofbirth">{{ trans('common.dateofbirth') }}</label>
                                <input type="date" name="dateofbirth" id="dateofbirth" placeholder="{{ trans('common.dateofbirth') }}" value="{{ old('dateofbirth') }}">
                            </div>
                            <div class="field{{ $errors->has('dateofdeath') ? ' error' : '' }}">
                                <label for="dateofdeath">{{ trans('common.dateofdeath') }}</label>
                                <input type="date" name="dateofdeath" id="dateofdeath" placeholder="{{ trans('common.dateofdeath') }}" value="{{ old('dateofdeath') }}">
                            </div>
                        </div>
                        <div class="field{{ $errors->has('image') ? ' error' : '' }}">
                            <label for="image">{{ trans('common.image') }}</label>
                            <input type="text" name="image" id="image" placeholder="{{ trans('common.image') }}" value="{{ old('image') }}">
                        </div>
                        <button type="submit" class="ui red right floated icon labeled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection