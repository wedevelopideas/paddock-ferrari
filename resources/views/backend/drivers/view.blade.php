@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="user icon"></i>
                        <span class="content">
                            {{ $driver->name }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                </div>
            </div>
        </div>
    </div>
@endsection