@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="users icon"></i>
                        <span class="content">
                            {{ trans('common.drivers') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('backend.drivers.add') }}" class="ui red right floated icon labeled button">
                        <i class="add icon"></i>
                        {{ trans('drivers.add') }}
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui table">
                        <thead>
                        <tr>
                            <th>{{ trans('common.name') }}</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if ($drivers->count())
                            @foreach($drivers as $driver)
                                <tr>
                                    <td><i class="{{ $driver->country_id }} flag"></i> {{ $driver->name }}</td>
                                    <td>
                                        <div class="ui red buttons">
                                            <a href="{{ route('backend.drivers.edit', ['id' => $driver->id]) }}" class="ui red icon button">
                                                <i class="edit icon"></i>
                                            </a>
                                            <a href="{{ route('backend.drivers.view', ['slug' => $driver->slug]) }}" class="ui red icon button">
                                                <i class="unhide icon"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="2" class="center aligned">{{ trans('errors.no_data') }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection