@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="images icon"></i>
                        <span class="content">
                            {{ trans('common.galleries') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('backend.gallery.album.add') }}" class="ui red right floated icon labeled button">
                        <i class="add icon"></i>
                        {{ trans('media.add_gallery_album') }}
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui table">
                        <thead>
                            <tr>
                                <th>{{ trans('common.name') }}</th>
                                <th>{{ trans('common.created') }}</th>
                                <th>{{ trans('common.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($albums as $album)
                            <tr>
                                <td>{{ $album->name }}</td>
                                <td>{{ $album->created_at->diffForHumans() }}</td>
                                <td>
                                    <div class="ui buttons">
                                        <a href="{{ route('backend.gallery.album.edit', ['id' => $album->album_id]) }}" class="ui red icon button">
                                            <i class="edit icon"></i>
                                        </a>
                                        <a href="{{ route('backend.gallery.images.view', ['album_id' => $album->album_id]) }}" class="ui red icon button">
                                            <i class="images icon"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection