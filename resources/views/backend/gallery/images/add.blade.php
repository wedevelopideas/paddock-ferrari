@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="images icon"></i>
                        <span class="content">
                            {{ trans('media.add_gallery_image') }}
                            <div class="sub header">
                                {{ $album->name }}
                            </div>
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    @include('_partials.messages')
                    <form action="{{ route('backend.gallery.images.add', ['album_id' => $album->id]) }}" method="post" class="ui form">
                        @csrf

                        <div class="field{{ $errors->has('image') ? ' error' : '' }}">
                            <label for="image">{{ trans('common.image') }}</label>
                            <input type="url" name="image" id="image" placeholder="{{ trans('common.image') }}" value="{{ old('image') }}">
                        </div>
                        <div class="field{{ $errors->has('source') ? ' error' : '' }}">
                            <label for="source">{{ trans('common.source') }}</label>
                            <input type="text" name="source" id="source" placeholder="{{ trans('common.source') }}" value="{{ old('source') }}">
                        </div>
                        <button type="submit" class="ui red right floated icon labeled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection