@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="images icon"></i>
                        <span class="content">
                            {{ $album->name }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('backend.gallery.images.add', ['album_id' => $album->id]) }}" class="ui red right floated labeled icon button">
                        <i class="add icon"></i>
                        {{ trans('media.add_gallery_image') }}
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui four cards">
                        @foreach($images as $image)
                            <div class="card">
                                <a href="{{ route('backend.gallery.images.edit', ['album_id' => $image->album_id, 'id' => $image->id]) }}" class="image">
                                    <img src="{{ $image->image }}" alt="{{ $album->name }}">
                                    <div class="ui red bottom right attached label">
                                        <i class="time icon"></i>
                                        {{ $image->created_at->diffForHumans() }}
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection