@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="sixteen wide column">
                <div class="ui red segment">
                    <h4 class="ui header">
                        <i class="line chart icon"></i>
                        <span class="content">
                            {{ trans('common.statistics') }}
                        </span>
                    </h4>
                    <div class="ui small statistics">
                        <div class="statistic">
                            <div class="value">
                                <i class="world icon"></i> {{ $countries }}
                            </div>
                            <div class="label">
                                {{ trans('common.countries') }}
                            </div>
                        </div>
                        <div class="statistic">
                            <div class="value">
                                <i class="language icon"></i> {{ $langs }}
                            </div>
                            <div class="label">
                                {{ trans('common.langs') }}
                            </div>
                        </div>
                        <div class="statistic">
                            <div class="value">
                                <i class="world icon"></i> {{ $seasons }}
                            </div>
                            <div class="label">
                                {{ trans('common.seasons') }}
                            </div>
                        </div>
                        <div class="statistic">
                            <div class="value">
                                <i class="world icon"></i> {{ $seasonsRaces }}
                            </div>
                            <div class="label">
                                {{ trans('common.races') }}
                            </div>
                        </div>
                        <div class="statistic">
                            <div class="value">
                                <i class="world icon"></i> {{ $gps }}
                            </div>
                            <div class="label">
                                {{ trans('common.grandprixs') }}
                            </div>
                        </div>
                        <div class="statistic">
                            <div class="value">
                                <i class="road icon"></i> {{ $tracks }}
                            </div>
                            <div class="label">
                                {{ trans('common.tracks') }}
                            </div>
                        </div>
                        <div class="statistic">
                            <div class="value">
                                <i class="users icon"></i> {{ $drivers }}
                            </div>
                            <div class="label">
                                {{ trans('common.drivers') }}
                            </div>
                        </div>
                        <div class="statistic">
                            <div class="value">
                                <i class="hashtag icon"></i> {{ $hashtags }}
                            </div>
                            <div class="label">
                                {{ trans('common.hashtags') }}
                            </div>
                        </div>
                        <div class="statistic">
                            <div class="value">
                                <i class="twitter square icon"></i> {{ $twitterAccounts }}
                            </div>
                            <div class="label">
                                {{ trans('socialmedia.twitter.accounts') }}
                            </div>
                        </div>
                        <div class="statistic">
                            <div class="value">
                                <i class="users icon"></i> {{ $users }}
                            </div>
                            <div class="label">
                                {{ trans('common.users') }}
                            </div>
                        </div>
                        <div class="statistic">
                            <div class="value">
                                <i class="youtube icon"></i> {{ $videos }}
                            </div>
                            <div class="label">
                                {{ trans('common.videos') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="four wide column">
                <div class="ui red segment">
                    <h4 class="ui header">
                        <i class="settings icon"></i>
                        <span class="content">
                            {{ trans('common.settings') }}
                        </span>
                    </h4>
                    @include('_partials.messages')
                    <form action="{{ route('backend.settings.save') }}" method="post" class="ui form">
                        @csrf

                        @foreach($settings as $setting)
                            <div class="field">
                                <label for="{{ $setting->key }}">{{ trans('common.'.$setting->key.'') }}</label>
                                <input type="text" name="{{ $setting->key }}" placeholder="{{ trans('common.'.$setting->key.'') }}" value="{{ $setting->value }}">
                            </div>
                        @endforeach
                        <button type="submit" class="ui red labeled icon button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection