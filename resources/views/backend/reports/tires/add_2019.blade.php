@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="road icon"></i>
                        <span class="content">
                            {{ trans('reports.add_tires') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    @include('_partials.messages')
                    <form action="{{ route('backend.reports.tires.add.2019') }}" method="post" class="ui form">
                        @csrf

                        <div class="fields">
                            <div class="two wide field{{ $errors->has('season') ? ' error' : '' }}">
                                <label for="season">{{ trans('common.season') }}</label>
                                <select name="season" id="season">
                                    @foreach($seasons as $season)
                                        <option value="{{ $season->season }}">{{ $season->season }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="two wide field{{ $errors->has('race') ? ' error' : '' }}">
                                <label for="race">{{ trans('common.race') }}</label>
                                <input type="number" name="race" id="race" placeholder="{{ trans('common.race') }}" value="{{ old('race') }}">
                            </div>
                            <div class="four wide field{{ $errors->has('gp_id') ? ' error' : '' }}">
                                <label for="gp">{{ trans('common.grandprix') }}</label>
                                <select name="gp_id" id="gp">
                                    @foreach($gps as $gp)
                                        <option value="{{ $gp->id }}">{{ $gp->full_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="four wide field{{ $errors->has('gp_id') ? ' error' : '' }}">
                                <label for="driver">{{ trans('common.driver') }}</label>
                                <select name="driver_id" id="driver">
                                    @foreach($drivers as $driver)
                                        <option value="{{ $driver->id }}">{{ $driver->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="fields">
                            <div class="field{{ $errors->has('c1') ? ' error' : '' }}">
                                <label for="c1">{{ trans('reports.tires.new.c1') }}</label>
                                <input type="number" name="c1" id="c1" placeholder="{{ trans('reports.tires.new.c1') }}" value="{{ old('c1') }}">
                            </div>
                            <div class="field{{ $errors->has('c2') ? ' error' : '' }}">
                                <label for="c2">{{ trans('reports.tires.new.c2') }}</label>
                                <input type="number" name="c2" id="c2" placeholder="{{ trans('reports.tires.new.c2') }}" value="{{ old('c2') }}">
                            </div>
                            <div class="field{{ $errors->has('c3') ? ' error' : '' }}">
                                <label for="c3">{{ trans('reports.tires.new.c3') }}</label>
                                <input type="number" name="c3" id="c3" placeholder="{{ trans('reports.tires.new.c3') }}" value="{{ old('c3') }}">
                            </div>
                            <div class="field{{ $errors->has('c4') ? ' error' : '' }}">
                                <label for="c4">{{ trans('reports.tires.new.c4') }}</label>
                                <input type="number" name="c4" id="c4" placeholder="{{ trans('reports.tires.new.c4') }}" value="{{ old('c4') }}">
                            </div>
                            <div class="field{{ $errors->has('c5') ? ' error' : '' }}">
                                <label for="c5">{{ trans('reports.tires.new.c5') }}</label>
                                <input type="number" name="c5" id="c5" placeholder="{{ trans('reports.tires.new.c5') }}" value="{{ old('c5') }}">
                            </div>
                        </div>
                        <button type="submit" class="ui red right floated icon labeled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection