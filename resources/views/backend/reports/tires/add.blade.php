@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="road icon"></i>
                        <span class="content">
                            {{ trans('reports.add_tires') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    @include('_partials.messages')
                    <form action="{{ route('backend.reports.tires.add') }}" method="post" class="ui form">
                        @csrf

                        <div class="fields">
                            <div class="two wide field{{ $errors->has('season') ? ' error' : '' }}">
                                <label for="season">{{ trans('common.season') }}</label>
                                <select name="season" id="season">
                                    @foreach($seasons as $season)
                                        <option value="{{ $season->season }}">{{ $season->season }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="two wide field{{ $errors->has('race') ? ' error' : '' }}">
                                <label for="race">{{ trans('common.race') }}</label>
                                <input type="number" name="race" id="race" placeholder="{{ trans('common.race') }}" value="{{ old('race') }}">
                            </div>
                            <div class="four wide field{{ $errors->has('gp_id') ? ' error' : '' }}">
                                <label for="gp">{{ trans('common.grandprix') }}</label>
                                <select name="gp_id" id="gp">
                                    @foreach($gps as $gp)
                                        <option value="{{ $gp->id }}">{{ $gp->full_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="four wide field{{ $errors->has('gp_id') ? ' error' : '' }}">
                                <label for="driver">{{ trans('common.driver') }}</label>
                                <select name="driver_id" id="driver">
                                    @foreach($drivers as $driver)
                                        <option value="{{ $driver->id }}">{{ $driver->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="fields">
                            <div class="field{{ $errors->has('hypersoft') ? ' error' : '' }}">
                                <label for="hypersoft">{{ trans('reports.tires.old.hypersoft') }}</label>
                                <input type="number" name="hypersoft" id="hypersoft" placeholder="{{ trans('reports.tires.old.hypersoft') }}" value="{{ old('hypersoft') }}">
                            </div>
                            <div class="field{{ $errors->has('ultrasoft') ? ' error' : '' }}">
                                <label for="ultrasoft">{{ trans('reports.tires.old.ultrasoft') }}</label>
                                <input type="number" name="ultrasoft" id="ultrasoft" placeholder="{{ trans('reports.tires.old.ultrasoft') }}" value="{{ old('ultrasoft') }}">
                            </div>
                            <div class="field{{ $errors->has('supersoft') ? ' error' : '' }}">
                                <label for="supersoft">{{ trans('reports.tires.old.supersoft') }}</label>
                                <input type="number" name="supersoft" id="supersoft" placeholder="{{ trans('reports.tires.old.supersoft') }}" value="{{ old('supersoft') }}">
                            </div>
                            <div class="field{{ $errors->has('soft') ? ' error' : '' }}">
                                <label for="soft">{{ trans('reports.tires.old.soft') }}</label>
                                <input type="number" name="soft" id="soft" placeholder="{{ trans('reports.tires.old.soft') }}" value="{{ old('soft') }}">
                            </div>
                            <div class="field{{ $errors->has('medium') ? ' error' : '' }}">
                                <label for="medium">{{ trans('reports.tires.old.medium') }}</label>
                                <input type="number" name="medium" id="medium" placeholder="{{ trans('reports.tires.old.medium') }}" value="{{ old('medium') }}">
                            </div>
                            <div class="field{{ $errors->has('hard') ? ' error' : '' }}">
                                <label for="hard">{{ trans('reports.tires.old.hard') }}</label>
                                <input type="number" name="hard" id="hard" placeholder="{{ trans('reports.tires.old.hard') }}" value="{{ old('hard') }}">
                            </div>
                            <div class="field{{ $errors->has('superhard') ? ' error' : '' }}">
                                <label for="superhard">{{ trans('reports.tires.old.superhard') }}</label>
                                <input type="number" name="superhard" id="superhard" placeholder="{{ trans('reports.tires.old.superhard') }}" value="{{ old('superhard') }}">
                            </div>
                        </div>
                        <button type="submit" class="ui red right floated icon labeled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection