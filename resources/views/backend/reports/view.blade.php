@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="bug icon"></i>
                        <span class="content">
                            {{ $report->full_name }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <h3 class="ui header">
                        {{ trans('reports.technical') }}
                    </h3>
                    <table class="ui table">
                        <tbody>
                        @foreach($technicals as $technical)
                            <tr>
                                <td>
                                    {{ $technical->name }}
                                </td>
                                <td>
                                    {{ $technical->ice }}
                                </td>
                                <td>
                                    {{ $technical->tc }}
                                </td>
                                <td>
                                    {{ $technical->mguh }}
                                </td>
                                <td>
                                    {{ $technical->mguk }}
                                </td>
                                <td>
                                    {{ $technical->es }}
                                </td>
                                <td>
                                    {{ $technical->ce }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <h3 class="ui header">
                        {{ trans('reports.tyre') }}
                    </h3>
                    <table class="ui table">
                        <tbody>
                        @foreach($tires as $tyre)
                            <tr>
                                <td>
                                    {{ $tyre->name }}
                                </td>
                                @if ($tyre->superhard)
                                    <td>
                                        {{ $tyre->superhard }}
                                    </td>
                                @endif
                                @if ($tyre->hard)
                                    <td>
                                        {{ $tyre->hard }}
                                    </td>
                                @endif
                                @if ($tyre->medium)
                                    <td>
                                        {{ $tyre->medium }}
                                    </td>
                                @endif
                                @if ($tyre->soft)
                                    <td>
                                        {{ $tyre->soft }}
                                    </td>
                                @endif
                                @if ($tyre->supersoft)
                                    <td>
                                        {{ $tyre->supersoft }}
                                    </td>
                                @endif
                                @if ($tyre->ultrasoft)
                                    <td>
                                        {{ $tyre->ultrasoft }}
                                    </td>
                                @endif
                                @if ($tyre->hypersoft)
                                    <td>
                                        {{ $tyre->hypersoft }}
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection