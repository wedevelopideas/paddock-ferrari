@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="bug icon"></i>
                        <span class="content">
                            {{ trans('common.reports') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('backend.reports.tires.add') }}" class="ui right floated red icon labeled button">
                        <i class="road icon"></i>
                        {{ trans('reports.add_tires') }}
                    </a>
                    <a href="{{ route('backend.reports.technicals.add') }}" class="ui right floated red icon labeled button">
                        <i class="warning circle icon"></i>
                        {{ trans('reports.add_technicals') }}
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui table">
                        <tbody>
                        @foreach($races as $race)
                            <tr>
                                <td>
                                    {{ $race->full_name }}
                                </td>
                                <td>
                                    <a href="{{ route('backend.reports.view', ['season' => $race->season, 'slug' => $race->slug]) }}" class="ui red icon button">
                                        <i class="unhide icon"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection