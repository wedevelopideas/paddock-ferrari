@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="road icon"></i>
                        <span class="content">
                            {{ trans('reports.add_technicals') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    @include('_partials.messages')
                    <form action="{{ route('backend.reports.technicals.add') }}" method="post" class="ui form">
                        @csrf

                        <div class="fields">
                            <div class="two wide field{{ $errors->has('season') ? ' error' : '' }}">
                                <label for="season">{{ trans('common.season') }}</label>
                                <select name="season" id="season">
                                    @foreach($seasons as $season)
                                        <option value="{{ $season->season }}">{{ $season->season }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="two wide field{{ $errors->has('race') ? ' error' : '' }}">
                                <label for="race">{{ trans('common.race') }}</label>
                                <input type="number" name="race" id="race" placeholder="{{ trans('common.race') }}" value="{{ old('race') }}">
                            </div>
                            <div class="four wide field{{ $errors->has('gp_id') ? ' error' : '' }}">
                                <label for="gp">{{ trans('common.grandprix') }}</label>
                                <select name="gp_id" id="gp">
                                    @foreach($gps as $gp)
                                        <option value="{{ $gp->id }}">{{ $gp->full_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="four wide field{{ $errors->has('gp_id') ? ' error' : '' }}">
                                <label for="driver">{{ trans('common.driver') }}</label>
                                <select name="driver_id" id="driver">
                                    @foreach($drivers as $driver)
                                        <option value="{{ $driver->id }}">{{ $driver->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="fields">
                            <div class="field{{ $errors->has('ice') ? ' error' : '' }}">
                                <label for="ice">{{ trans('reports.technicals_short.ice') }}</label>
                                <input type="number" name="ice" id="ice" placeholder="{{ trans('reports.technicals.ice') }}" value="{{ old('ice') }}">
                            </div>
                            <div class="field{{ $errors->has('tc') ? ' error' : '' }}">
                                <label for="tc">{{ trans('reports.technicals_short.tc') }}</label>
                                <input type="number" name="tc" id="tc" placeholder="{{ trans('reports.technicals.tc') }}" value="{{ old('tc') }}">
                            </div>
                            <div class="field{{ $errors->has('mguh') ? ' error' : '' }}">
                                <label for="mguh">{{ trans('reports.technicals_short.mguh') }}</label>
                                <input type="number" name="mguh" id="mguh" placeholder="{{ trans('reports.technicals.mguh') }}" value="{{ old('mguh') }}">
                            </div>
                            <div class="field{{ $errors->has('mguk') ? ' error' : '' }}">
                                <label for="mguk">{{ trans('reports.technicals_short.mguk') }}</label>
                                <input type="number" name="mguk" id="mguk" placeholder="{{ trans('reports.technicals.mguk') }}" value="{{ old('mguk') }}">
                            </div>
                            <div class="field{{ $errors->has('es') ? ' error' : '' }}">
                                <label for="es">{{ trans('reports.technicals_short.es') }}</label>
                                <input type="number" name="es" id="es" placeholder="{{ trans('reports.technicals.es') }}" value="{{ old('es') }}">
                            </div>
                            <div class="field{{ $errors->has('ce') ? ' error' : '' }}">
                                <label for="ce">{{ trans('reports.technicals_short.ce') }}</label>
                                <input type="number" name="ce" id="ce" placeholder="{{ trans('reports.technicals.ce') }}" value="{{ old('ce') }}">
                            </div>
                        </div>
                        <button type="submit" class="ui red right floated icon labeled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection