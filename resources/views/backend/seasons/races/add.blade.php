@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="world icon"></i>
                        <span class="content">
                            {{ trans('seasons.add_races', ['season' => $year]) }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    @include('_partials.messages')
                    <form action="{{ route('backend.seasons.races.add', ['season' => $year]) }}" method="post" class="ui form">
                        @csrf

                        <div class="four fields">
                            <div class="field{{ $errors->has('raceday') ? ' error' : '' }}">
                                <label for="raceday">{{ trans('common.date') }}</label>
                                <input type="date" name="raceday" id="raceday" placeholder="{{ trans('common.date') }}" value="{{ old('raceday') }}">
                            </div>
                            <div class="field{{ $errors->has('season') ? ' error' : '' }}">
                                <label for="season">{{ trans('common.season') }}</label>
                                <select name="season" id="season">
                                    @foreach($seasons as $season)
                                        <option value="{{ $season->season }}"{{ ($season->season === $year) ? ' selected' : '' }}>{{ $season->season }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="field{{ $errors->has('gp_id') ? ' error' : '' }}">
                                <label for="gp">{{ trans('common.grandprix') }}</label>
                                <select name="gp_id" id="gp">
                                    @foreach($gps as $gp)
                                        <option value="{{ $gp->id }}">{{ $gp->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="field{{ $errors->has('track_id') ? ' error' : '' }}">
                                <label for="track">{{ trans('common.track') }}</label>
                                <select name="track_id" id="track">
                                    @foreach($tracks as $track)
                                        <option value="{{ $track->id }}">{{ $track->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" name="active" tabindex="0" value="1" class="hidden">
                                <label>{{ trans('seasons.confirmed') }}</label>
                            </div>
                        </div>
                        <button type="submit" class="ui red right floated icon labeled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('.ui.checkbox')
            .checkbox()
        ;
    </script>
@endsection