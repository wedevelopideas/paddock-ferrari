@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="world icon"></i>
                        <span class="content">
                            {{ trans('seasons.seasons_races', ['season' => $season->season]) }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('backend.seasons.races.add', ['season' => $season->season]) }}" class="ui red right floated icon labeled button">
                        <i class="add icon"></i>
                        {{ trans('seasons.add_races', ['season' => $season->season]) }}
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui table">
                        <thead>
                        <tr>
                            <th>{{ trans('common.date') }}</th>
                            <th>{{ trans('common.grandprix') }}</th>
                            <th>{{ trans('common.track') }}</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if ($races->count())
                            @foreach($races as $race)
                                <tr>
                                    <td>{{ $race->raceday->format('d/m/Y') }}</td>
                                    <td>{{ $race->grandprix->full_name }}</td>
                                    <td>{{ $race->track->name }}</td>
                                    <td>
                                        <a href="{{ route('backend.seasons.races.edit', ['season' => $race->season, 'id' => $race->id]) }}" class="ui red icon button">
                                            <i class="edit icon"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4" class="center aligned">{{ trans('errors.no_data') }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection