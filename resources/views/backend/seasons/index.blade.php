@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="world icon"></i>
                        <span class="content">
                            {{ trans('common.seasons') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('backend.seasons.add') }}" class="ui red right floated icon labeled button">
                        <i class="add icon"></i>
                        {{ trans('seasons.add') }}
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui table">
                        <thead>
                        <tr>
                            <th>{{ trans('common.season') }}</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if ($seasons->count())
                            @foreach($seasons as $season)
                                <tr>
                                    <td>{{ $season->season }}</td>
                                    <td>
                                        <div class="ui buttons">
                                            <a href="{{ route('backend.seasons.edit', ['id' => $season->id]) }}" class="ui red icon button">
                                                <i class="edit icon"></i>
                                            </a>
                                            <a href="{{ route('backend.seasons.races', ['season' => $season->season]) }}" class="ui red icon button">
                                                <i class="checkered flag icon"></i>
                                            </a>
                                            <a href="{{ route('backend.seasons.drivers', ['season' => $season->season]) }}" class="ui red icon button">
                                                <i class="users icon"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="2" class="center aligned">{{ trans('errors.no_data') }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection