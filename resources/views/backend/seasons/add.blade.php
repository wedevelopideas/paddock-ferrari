@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="world icon"></i>
                        <span class="content">
                            {{ trans('seasons.add') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    @include('_partials.messages')
                    <form action="{{ route('backend.seasons.add') }}" method="post" class="ui form">
                        @csrf

                        <div class="field{{ $errors->has('season') ? ' error' : '' }}">
                            <label for="season">{{ trans('common.season') }}</label>
                            <input type="text" name="season" placeholder="{{ trans('common.season') }}" value="{{ old('season') }}">
                        </div>
                        <button type="submit" class="ui red right floated icon labeled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection