@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="world icon"></i>
                        <span class="content">
                            {{ trans('seasons.add_drivers', ['season' => $season->season]) }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    @include('_partials.messages')
                    <form action="{{ route('backend.seasons.drivers.add', ['season' => $season->season]) }}" method="post" class="ui form">
                        @csrf

                        <div class="field{{ $errors->has('driver_id') ? ' error' : '' }}">
                            <label for="driver">{{ trans('common.driver') }}</label>
                            <select name="driver_id" id="driver">
                                @foreach($drivers as $driver)
                                    <option value="{{ $driver->id }}">{{ $driver->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="ui red right floated icon labeled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection