@extends('layouts.backend')
@section('title', 'paddock Backend | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="car icon"></i>
                        <span class="content">
                            {{ trans('cars.edit') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    @include('_partials.messages')
                    <form action="{{ route('backend.cars.edit', ['id' => $car->id]) }}" method="post" class="ui form">
                        @csrf

                        <div class="field{{ $errors->has('name') ? ' error' : '' }}">
                            <label for="name">{{ trans('common.name') }}</label>
                            <input type="text" name="name" id="name" placeholder="{{ trans('common.name') }}" value="{{ $car->name }}">
                        </div>
                        <button type="submit" class="ui red right floated icon labeled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection