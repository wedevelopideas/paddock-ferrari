@extends('layouts.sql')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="database icon"></i>
                        <span class="content">
                            {{ trans('common.database') }}
                        </span>
                    </h1>
                </div>
            </div>
        </div>
    </div>
@endsection