@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="sixteen wide column">
                <div class="ui red segment">
                    <h1 class="ui header">
                        {{ $grandprix->full_name }}
                        <span class="sub header">
                            <i class="{{ $grandprix->country }} flag"></i> {{ $grandprix->name }} |
                            <i class="calendar icon"></i> {{ $grandprix->raceday->format('d/m/Y') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="six wide column">
                @include('_partials.dashboard.race.report_tires')
            </div>
            <div class="six wide column">
                @include('_partials.dashboard.race.report_technical')
            </div>
            <div class="two wide column">
                @include('_partials.dashboard.race.standings')
            </div>
            <div class="two wide column">
                @include('_partials.dashboard.race.timetable')
            </div>
            <div class="four wide column">
                @include('_partials.dashboard.race.results')
            </div>
            <div class="eight wide column">
                <div class="ui red segment"></div>
            </div>
            <div class="four wide column">
                @include('_partials.dashboard.race.feed')
            </div>
        </div>
    </div>
@endsection