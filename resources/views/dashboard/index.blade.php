@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="sixteen wide column">
                @include('_partials.dashboard.twitter')
            </div>
            <div class="four wide column">
                @include('_partials.dashboard.user')
            </div>
            <div class="eight wide column">
                @include('_partials.dashboard.race')
            </div>
            <div class="four wide column">
                @include('_partials.dashboard.drivers')
            </div>
            <div class="four wide column">
                @include('_partials.dashboard.video')
            </div>
            <div class="four wide column">
                @include('_partials.dashboard.feeds')
            </div>
            <div class="four wide column">
                @include('_partials.dashboard.races')
            </div>
            <div class="four wide column">
                @include('_partials.dashboard.events')
            </div>
        </div>
    </div>
@endsection