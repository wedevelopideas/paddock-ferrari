@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="sixteen wide column">
                @include('_partials.dashboard.twitter')
            </div>
            <div class="four wide column">
                @include('_partials.dashboard.user')
            </div>
            @include('_partials.dashboard.socialmedia')
        </div>
    </div>
@endsection