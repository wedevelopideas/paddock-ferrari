@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main fluid container">
        <div class="ui stackable grid">
            <div class="sixteen wide column">
                @include('_partials.dashboard.tests.plan')
            </div>
            <div class="four wide column">
                @include('_partials.dashboard.user')
            </div>
            <div class="four wide column">
                @include('_partials.dashboard.tests.results')
            </div>
        </div>
    </div>
@endsection