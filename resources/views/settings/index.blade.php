@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="settings icon"></i>
                        <span class="content">
                            {{ trans('common.settings') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui red clearing segment">
                        <form action="{{ route('settings.langs') }}" method="post" class="ui form">
                            @csrf

                            <div class="field{{ $errors->has('lang') ? ' error' : '' }}">
                                <label for="lang">{{ trans('common.lang') }}</label>
                                <select name="lang" id="lang">
                                    @foreach($langs as $lang)
                                        <option value="{{ $lang->code }}"{{ ($lang->code === auth()->user()->lang) ? ' selected' : '' }}>{{ trans('langs.'.strtolower($lang->name)) }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="ui red right floated labeled icon button">
                                <i class="save icon"></i>
                                {{ trans('common.save') }}
                            </button>
                        </form>
                    </div>
                    <div class="ui red clearing segment">
                        <form action="{{ route('settings.timezones') }}" method="post" class="ui form">
                            @csrf

                            <div class="field{{ $errors->has('timezone') ? ' error' : '' }}">
                                <label for="timezone">{{ trans('common.timezone') }}</label>
                                <select name="timezone" id="timezone">
                                    @foreach(timezone_identifiers_list() as $timezone)
                                        <option value="{{ $timezone }}"{{ ($timezone === auth()->user()->timezone) ? ' selected' : '' }}>{{ $timezone }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="ui red right floated labeled icon button">
                                <i class="save icon"></i>
                                {{ trans('common.save') }}
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection