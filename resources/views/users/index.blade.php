@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <div class="ui red segment">
                        <h1 class="ui header">
                            <img src="{{ $user->avatar }}" alt="{{ $user->display_name }}" class="ui circular image">
                            <span class="content">
                                {{ $user->display_name }}
                            </span>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui items">
                        @foreach($articles as $article)
                            <div class="item">
                                <div class="image">
                                    <img src="{{ $article->image }}" alt="{{ $article->title }}">
                                </div>
                                <div class="content">
                                    <div class="header">
                                        {{ $article->title }}
                                    </div>
                                    <div class="meta">
                                        <span>{{ $article->date->diffForHumans() }}</span>
                                    </div>
                                    <div class="description">
                                        @if ($article->description)
                                        <p>{{ $article->description }}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection