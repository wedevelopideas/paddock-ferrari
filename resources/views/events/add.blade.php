@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="calendar alternate icon"></i>
                        <span class="content">
                            {{ trans('events.add') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    @include('_partials.messages')
                    <form action="{{ route('events.add') }}" method="post" class="ui form">
                        @csrf

                        <div class="two fields">
                            <div class="field{{ $errors->has('type') ? ' errors' : '' }}">
                                <label for="type">{{ trans('common.type') }}</label>
                                <select name="type" id="type">
                                    <option value="1">Born</option>
                                    <option value="2">Death</option>
                                </select>
                            </div>
                            <div class="field{{ $errors->has('lang') ? ' errors' : '' }}">
                                <label for="lang">{{ trans('common.lang') }}</label>
                                <select name="lang" id="lang">
                                    @foreach($langs as $lang)
                                        <option value="{{ $lang->code }}">{{ trans('langs.'.strtolower($lang->name)) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="two fields">
                            <div class="field{{ $errors->has('driver_id') ? ' errors' : '' }}">
                                <label for="driver_id">{{ trans('common.driver') }}</label>
                                <select name="driver_id" id="driver_id">
                                    @foreach($drivers as $driver)
                                        <option value="{{ $driver->id }}">{{ $driver->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="field{{ $errors->has('event_date') ? ' errors' : '' }}">
                                <label for="event_date">{{ trans('common.date') }}</label>
                                <input type="date" name="event_date" id="event_date" placeholder="{{ trans('common.date') }}" value="{{ old('event_date') }}">
                            </div>
                        </div>
                        <div class="field{{ $errors->has('event_text') ? ' errors' : '' }}">
                            <label for="event_text">{{ trans('common.text') }}</label>
                            <input type="text" name="event_text" id="event_text" placeholder="{{ trans('common.text') }}" value="{{ old('event_text') }}">
                        </div>
                        <div class="three fields">
                            <div class="field{{ $errors->has('event_hashtags') ? ' errors' : '' }}">
                                <label for="event_hashtags">{{ trans('common.hashtags') }}</label>
                                <input type="text" name="event_hashtags" id="event_hashtags" placeholder="{{ trans('common.hashtags') }}" value="{{ old('event_hashtags') }}">
                            </div>
                            <div class="field{{ $errors->has('event_link') ? ' errors' : '' }}">
                                <label for="event_link">{{ trans('common.link') }}</label>
                                <input type="text" name="event_link" id="event_link" placeholder="{{ trans('common.link') }}" value="{{ old('event_link') }}">
                            </div>
                            <div class="field{{ $errors->has('event_f1tv_link') ? ' errors' : '' }}">
                                <label for="event_f1tv_link">{{ trans('common.f1tv_link') }}</label>
                                <input type="text" name="event_f1tv_link" id="event_f1tv_link" placeholder="{{ trans('common.f1tv_link') }}" value="{{ old('event_f1tv_link') }}">
                            </div>
                        </div>
                        <button type="submit" class="ui red right floated icon labeled button">
                            <i class="save icon"></i>
                            {{ trans('common.save') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection