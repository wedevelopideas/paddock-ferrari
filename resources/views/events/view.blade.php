@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="calendar alternate icon"></i>
                        <span class="content">
                            {{ $event->driver->name }}
                            <span class="sub header">
                                {{ $event->event_date->format('d/m/Y') }}
                            </span>
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="ui list">
                        @foreach($event_list as $item)
                            <div class="item">
                                <div class="right floated content">
                                    <a href="{{ $item->getTwitterLink() }}" target="_blank" class="ui twitter mini labeled icon button">
                                        <i class="share icon"></i>
                                        {{ trans('common.share') }}
                                    </a>
                                    <a href="{{ route('events.edit', ['id' => $item->id]) }}" class="ui red mini icon button">
                                        <i class="edit icon"></i>
                                    </a>
                                </div>
                                <div class="content">
                                    <i class="{{ $item->getFlag() }} flag"></i>
                                    {{ $item->event_text }}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection