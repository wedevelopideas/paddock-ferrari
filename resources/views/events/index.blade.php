@extends('layouts.frontend')
@section('title', 'paddock | Scuderia Ferrari')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        <i class="calendar alternate icon"></i>
                        <span class="content">
                            {{ trans('common.events') }}
                        </span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <a href="{{ route('events.add') }}" class="ui red right floated icon labeled button">
                        <i class="add icon"></i>
                        {{ trans('events.add') }}
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <table class="ui table">
                        <thead>
                        <tr>
                            <th>{{ trans('common.date') }}</th>
                            <th>{{ trans('common.type') }}</th>
                            <th>{{ trans('common.driver') }}</th>
                            <th>{{ trans('common.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if ($events->count())
                            @foreach($events as $event)
                                <tr>
                                    <td>{{ $event->event_date->format('d/m/Y') }}</td>
                                    <td>{{ $event->type }}</td>
                                    <td>{{ $event->driver->name }}</td>
                                    <td>
                                        <a href="{{ route('events.view', ['driver_id' => $event->driver_id, 'date' => $event->event_date->format('Y-m-d')]) }}" class="ui red icon button">
                                            <i class="calendar alternate icon"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4" class="center aligned">{{ trans('errors.no_data') }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection