<?php

return [
    'login_failed' => 'The email and password you entered did not match our records. Please try again.',
    'no_data' => 'No data available.',
    'no_feeds' => 'There are currently no feeds.',
    'no_laptime' => 'No time set',
];
