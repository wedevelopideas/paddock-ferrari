<?php

return [
    'fda' => 'FDA',
    'ferrari' => 'Ferrari',
    'happened_today' => 'It happened today',
    'history' => 'History',
    'motorsport' => 'Motorsport',
    'no_description' => 'No description',
];
