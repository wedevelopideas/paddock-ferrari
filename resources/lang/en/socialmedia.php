<?php

return [
    'twitter' => [
        'accounts' => 'Twitter accounts',
        'add' => 'Add account',
        'add_msg' => 'Add new Twitter account',
        'add_socialmedia' => 'Add social media',
        'description' => 'Description',
        'edit_msg' => 'Edit Twitter account',
        'edit_socialmedia' => 'Edit social media',
        'id' => 'Twitter ID',
        'name' => 'Name',
        'owner_msg' => 'Are you the owner of this account?',
        'profile_image' => 'Profile image',
        'screen_name' => 'Screen name',
    ],
];
