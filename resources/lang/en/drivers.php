<?php

return [
    'add' => 'Add driver',
    'edit' => 'Edit driver',
    'stats' => [
        'wins' => 'Wins',
    ],
    'test' => 'Test driver',
    'works' => 'Works driver',
];
