<?php

return [
    'active' => 'Track in the current race calendar',
    'add' => 'Add track',
    'edit' => 'Edit track',
    'length' => 'Track length',
];
