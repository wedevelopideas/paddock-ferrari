<?php

return [
    'active' => 'Displays the language for the selection',
    'add' => 'Add language',
    'edit' => 'Edit language',
    'news' => 'Displays the language for the feeds',

    'english' => 'English',
    'french' => 'French',
    'german' => 'German',
    'italian' => 'Italian',
    'spanish' => 'Spanish',
    'swedish' => 'Swedish',
];
