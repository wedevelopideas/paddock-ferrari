<?php

return [
    'add_technicals' => 'Add technical report',
    'add_tires' => 'Add tyre report',
    'selected_sets' => 'Selected sets',
    'technical' => 'Technical report',
    'technicals' => [
        'ce' => 'Control Electronics (CE)',
        'es' => 'Energy Store (ES)',
        'ice' => 'Internal Combustion Engine (ICE)',
        'mguh' => 'Motor Generator Unit – Heat (MGU-H)',
        'mguk' => 'Motor Generator Unit – Kinetic (MGU-K)',
        'tc' => 'Turbo Charger (TC)',
    ],
    'technicals_short' => [
        'ce' => 'CE',
        'es' => 'ES',
        'ice' => 'ICE',
        'mguh' => 'MGU-H',
        'mguk' => 'MGU-K',
        'tc' => 'TC',
    ],
    'tires' => [
        'new' => [
            'c1' => 'C1',
            'c2' => 'C2',
            'c3' => 'C3',
            'c4' => 'C4',
            'c5' => 'C5',
        ],
        'old' => [
            'hypersoft' => 'Hypersoft',
            'ultrasoft' => 'Ultrasoft',
            'supersoft' => 'Supersoft',
            'soft' => 'Soft',
            'medium' => 'Medium',
            'hard' => 'Hard',
            'superhard' => 'Superhard',
        ],
    ],
    'tyre' => 'Tyre report',
];
