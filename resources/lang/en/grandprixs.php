<?php

return [
    'active' => 'Grand Prix in the current race calendar',
    'add' => 'Add Grand Prix',
    'edit' => 'Edit Grand Prix',
];
