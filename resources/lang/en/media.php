<?php

return [
    'add_gallery_album' => 'Add gallery album',
    'add_gallery_image' => 'Add gallery image',
    'add_video' => 'Add video',
    'description' => 'Description',
    'edit_gallery_album' => 'Edit gallery album',
    'edit_video' => 'Edit video',
    'youtube_id' => 'YouTube ID',
];
