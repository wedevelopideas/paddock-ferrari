<?php

return [
    'add' => 'Add season',
    'add_drivers' => 'Add driver for :season season',
    'add_races' => 'Add race for :season season',
    'confirmed' => 'Confirmed',
    'edit' => 'Edit season',
    'edit_drivers' => 'Edit driver for :season season',
    'edit_races' => 'Edit race for :season season',
    'seasons_races' => ':season season',
];
