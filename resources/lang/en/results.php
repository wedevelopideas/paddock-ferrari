<?php

return [
    'add' => 'Add result',
    'fastest_laps' => 'Fastest laps',
    'laps' => 'Laps',
    'podiums' => 'Podiums',
    'points' => 'Points',
    'pole_positions' => 'Pole positions',
    'quali' => 'Qualifying',
    'race' => 'Race',
    'wins' => 'Wins',
];
